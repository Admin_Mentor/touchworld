ALTER TABLE `customers` ADD `credit_period_blocker` INT(11) NOT NULL DEFAULT '0' AFTER `grace_period`;

ALTER TABLE `profiles` ADD `sale_prefix_code` VARCHAR(250) NULL DEFAULT '19-20/' AFTER `gst_tin_code`;