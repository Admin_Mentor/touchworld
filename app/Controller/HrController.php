<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
/**
* Hr Controller
*/
class HrController extends AppController {
	public $uses=[
	'Staff',
	'Executive',
	'Document',
	'SaleTarget',
	'Sale',
	'BasicPay',
	'AllowDeduct',
	'ExecutiveBonus',
	'PayStructure',
	'Journal',
	'BonusConfiguration',
	'Customer',
	'ExecutiveBonusDetails',
	'Role',

	];
	public $components = array('RequestHandler');
	public function AddStaff()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$sub_group_id=$this->SubGroup->field('SubGroup.id',array('SubGroup.name'=>'SALARY PAID'));

		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Hr/AddStaff'));

		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		$roles=$this->Role->find('list',['order'=>['name']]);
		$this->set(compact('roles'));
		$Staff=$this->Staff->find('all');
		$this->set(compact('Staff'));

		if($this->request->data)
		{
			$data=$this->request->data['Staff'];
			$data['name']=strtoupper(trim($data['name']));
			$data['date_of_joining']=date('Y-m-d',strtotime($data['date_of_joining']));
			$StaffRow = $this->Staff->find('first', array(

				'order' => array('Staff.id' => 'DESC') ));

			if(!empty($StaffRow))
			{
				$PrevCode = $StaffRow['Staff']['code'];
				$code = $PrevCode + 1;


			}
			else{

				$code = '1000';

			}
			$data['code'] = $code; 
			$datasource_Staff = $this->Staff->getDataSource();
			try {

				$name = trim($data['name']);
				$opening_balance = 0;
				$date = date('Y-m-d',strtotime($data['date_of_joining']));
				$description = "";
				$function_return = $this->expense_head($name,$opening_balance,$date,$description);
				if ($function_return['result'] != 'Success')
					throw new Exception($function_return['message']);

				$AccountHead_paid_id = $function_return['AccountHead_paid_id'];
				$AccountHead_prepaid_id = $function_return['AccountHead_prepaid_id'];
				$AccountHead_outstanding_id = $function_return['AccountHead_outstanding_id'];
				$data['paid_account_head_id'] = $AccountHead_paid_id;
				$data['prepaid_account_head_id'] = $AccountHead_prepaid_id;
				$data['outstanding_account_head_id'] = $AccountHead_outstanding_id;
				$datasource_Staff->begin();
				$this->Staff->create();
				if(!$this->Staff->save($data))
				{
					$errors = $this->Staff->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_Staff->commit();
				$return['result']='Success';
				//pr($return);exit;
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_Staff->rollback();
			}
			//$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}

	public function get_staff_details_ajax($id)
	{
		$return=[
		'Staff'=>[],
		];
		if(isset($id))
		{
			$Staff=$this->Staff->findById($id);
			$Staff['Staff']['date_of_joining']=date('d-m-Y',strtotime($Staff['Staff']['date_of_joining']));
			$return['Staff']=$Staff['Staff'];	
		}
		echo json_encode($return);
		exit;
	}
	public function EditStaff()
	{
		if($this->request->data)
		{
			$data=$this->request->data['Staff'];
			$date = date('Y-m-d',strtotime($data['date_of_joining']));
			$staff_data=[
			'name'=>$data['name'],
			'contact_no'=>$data['contact_no'],
			'role_id'=>$data['role_id'],
			'date_of_joining'=>$date,
			'address'=>$data['address'],
			];
			$datasource_Staff = $this->Staff->getDataSource();
			try {
				$datasource_Staff->begin();
				$this->Staff->id=$data['id'];
				if(!$this->Staff->save($staff_data))
				{
					$errors = $this->Staff->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_Staff->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_Staff->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function BlockUnblockExecutive($id,$flag)
	{
		try {

			$this->Executive->id=$id;
			if(!$this->Executive->saveField('block',$flag))
				throw new Exception("Cant Block This Executive", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	public function DeleteStaff($id)
	{
		try {
			$Staff=$this->Staff->findById($id);
			$AccountHeads=$this->AccountHead->find('all',array(
				'conditions'=>array(
					'AccountHead.name LIKE'=>'%'. $Staff['Staff']['name'] . '%'),
			));
			foreach ($AccountHeads as $key => $value) {
				$JournalCredit=$this->Journal->find('first',array(
				'conditions'=>array('Journal.credit'=>$value['AccountHead']['id'],'flag=1'),
				));
				if(!empty($JournalCredit))
					throw new Exception("This Account have Journal Entry Cant Delete", 1);
				$JournalDebit=$this->Journal->find('first',array(
					'conditions'=>array('Journal.debit'=>$value['AccountHead']['id'],'flag=1'),
					));
				if(!empty($JournalDebit))
					throw new Exception("This Account have Journal Entry Cant Delete", 1);
			}
			$Executive=$this->Executive->findByStaffId($id);
			if($Executive)
				throw new Exception("This Staff is an Executive", 1);
			if(!$this->Staff->delete($id))
				throw new Exception("Cant Delete This Staff", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	public function api_get_invoice_details()
	{
		$return=[
		'status'=>'Empty',
		'Sale'=>[],
		];
		$data=$this->request->data;
		if(isset($data['invoice_no']))
		{
			$invoice_no=$data['invoice_no'];
			$return['status']='success';
			$Sale=$this->Sale->find('all',array(
				"joins"=>array(
					array(
						"table"=>'sale_items',
						"alias"=>'SaleItem',
						"type"=>'inner',
						"conditions"=>array('Sale.id=SaleItem.sale_id'),
						),
					array(
						"table"=>'products',
						"alias"=>'Product',
						"type"=>'inner',
						"conditions"=>array('Product.id=SaleItem.product_id'),
						),
					),
				'conditions'=>array(
					'invoice_no'=>$invoice_no,
					),
				'fields'=>array(
					'SaleItem.unit_price',
					'SaleItem.quantity',
					'SaleItem.total',
					'Product.piecepercart',
					'Product.code',
					'Product.id',
					'Product.name',
					),
				));
		}
		if(isset($Sale))
		{
			$All_Sale=[];
			foreach ($Sale as $key => $value) {
				$Single_Sale['unit_price']=$value['SaleItem']['unit_price'];
				$Single_Sale['quantity']=$value['SaleItem']['quantity'];
				$Single_Sale['total']=$value['SaleItem']['total'];
				$Single_Sale['name']=$value['Product']['name'];
				$Single_Sale['code']=$value['Product']['code'];
				$Single_Sale['piecepercart']=$value['Product']['piecepercart'];
				$Single_Sale['id']=$value['Product']['id'];
				array_push($All_Sale, $Single_Sale);
			}
			$return['Sale']=$All_Sale;	
		}
		echo json_encode($return);
		exit;
	}
	public function api_receipt_payment()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		];
		$data=$this->request->data;
		if($data)
		{
			$datesourse_Sale=$this->Sale->getDataSource();
			$datesourse_Journal=$this->Journal->getDataSource();
			try {
				$datesourse_Sale->begin();
				$datesourse_Journal->begin();
				$PaidList = $data['PaidList'];
				foreach ($PaidList as $key => $value) {
					$invoice_no=$value['invoice_no'];
					$amount=$value['amount'];
					$Sale=$this->Sale->findById($invoice_no);
					$this->Sale->id=$Sale['Sale']['id'];
					if($this->Sale->field('balance')<$amount)
						throw new Exception("balance Amount Must Be Greater than or equal to Payment Amount on invoice no:".$invoice_no."");
					if(!$this->Sale->saveField('balance',$this->Sale->field('balance')-$amount))
						throw new Exception("Error Processing Request in Sale Updation", 1);
					$work_flow='From Mobile';
					$remarks='invoice_no : '.$invoice_no;
					$date=date('Y-m-d');
					$debit=1;
					$credit=$Sale['Sale']['account_head_id'];
					$AccountingsController = new AccountingsController;
					$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
				$datesourse_Sale->commit();
				$datesourse_Journal->commit();
				$return['status']='success';
			} catch (Exception $e) {
				$datesourse_Sale->rollback();
				$datesourse_Journal->rollback();
				$return['status']=$e->getMessage();
			}
		}
		echo json_encode($return);
		exit;
	}
	public function document_upload()
	{
		$uploadData = '';
		if(!empty($this->request->data['Upolad_Form']['file']['name'])){
			$fileName = $this->request->data['Upolad_Form']['file']['name'];
			$uploadPath = WWW_ROOT.'docs'.DS;
			$uploadFile = $uploadPath.$fileName;
			if(move_uploaded_file($this->request->data['Upolad_Form']['file']['tmp_name'],$uploadFile))
			{
				$upload_Data=[
				'owner_id'=>$this->request->data['Upolad_Form']['id'],
				'file'=>'docs/'.$fileName,
				];
				if(!$this->Document->save($upload_Data))
				{
					$errors = $this->Document->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0]);
					}
				}
				$return['result'] = 'success';

			}
			else
			{
				$return['result'] = 'false';

			}
		}
		echo json_encode($return);
		exit;

	}
	public function SaleTarget()
	{
		$PermissionList = $this->Session->read('PermissionList');

		$menu_id = $this->Menu->field(

			'Menu.id',

			array('action ' => 'Hr/SaleTarget'));

		if(!in_array($menu_id, $PermissionList))

		{

			$this->Session->setFlash("Permission denied");

			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));

		}
		if($this->request->data)
		{
			$data=$this->request->data['SaleTarget'];
			$date = date('Y-m-d',strtotime($data['date']));
			$SaleTarget_data=[
			'executive_id'=>$data['executive_id'],
			'wef_date'=>$date,
			'target'=>$data['sale_target'],
			'collection_target'=>$data['collection_target'],
			'remarks'=>$data['remarks'],
			];
			$datasource_SaleTarget = $this->SaleTarget->getDataSource();
			try {
				$datasource_SaleTarget->begin();
				if(!$this->SaleTarget->save($SaleTarget_data))
				{
					$errors = $this->SaleTarget->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_SaleTarget->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_SaleTarget->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );

		}
		else{
			$Executive=$this->Executive->find('list');
			$this->set(compact('Executive'));
			$Saletarget=$this->SaleTarget->find('all', array(

				'order' => array('SaleTarget.id' => 'DESC') ));
			$this->set(compact('Saletarget'));

		}
	}
	public function DeleteSaleTarget($id)
	{
		try {
			if(!$this->SaleTarget->delete($id))
				throw new Exception("Cant Delete This Target", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	public function get_target_details_ajax($id)
	{
		$return=[
		'status'=>'Empty',
		'SaleTarget'=>[],
		];
		if(isset($id))
		{
			$SaleTarget=$this->SaleTarget->findById($id);
			$return['SaleTarget']=$SaleTarget['SaleTarget'];	
		}
		echo json_encode($return);
		exit;
	}
	public function EditSaleTarget()
	{
		if($this->request->data)
		{
			$data=$this->request->data['SaleTarget'];
			$date = date('Y-m-d',strtotime($data['date']));
			$SaleTarget_data=[
			'executive_id'=>$data['executive_id'],
			'wef_date'=>$date,
			'target'=>$data['sale_target'],
			'collection_target'=>$data['collection_target'],
			'remarks'=>$data['remarks'],
			];
			$datasource_SaleTarget = $this->SaleTarget->getDataSource();
			try {
				$datasource_SaleTarget->begin();
				$this->SaleTarget->id=$data['id'];
				if(!$this->SaleTarget->save($SaleTarget_data))
				{
					$errors = $this->SaleTarget->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_SaleTarget->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_SaleTarget->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function Evaluation()
	{
		$PermissionList = $this->Session->read('PermissionList');

		$menu_id = $this->Menu->field(

			'Menu.id',

			array('action ' => 'Hr/Evaluation'));

		if(!in_array($menu_id, $PermissionList))

		{

			$this->Session->setFlash("Permission denied");

			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));

		}

		$totalSale =0;
		$evaluationList = array();
		$first_date = date('Y-m-d',strtotime('first day of this month'));
		$last_date = date('Y-m-d',strtotime('last day of this month'));
		$month = date('m.Y');
		$executive_id_List = $this->Executive->find('all',array('fields'=>'Executive.id,Executive.name',));
		foreach ($executive_id_List as $key => $value) {
			$data['executive_id'] = $value['Executive']['id'];
			$data['executive_name'] = $value['Executive']['name'];
			$data['target'] = $this->sale_target($value['Executive']['id'],$first_date);
			$data['collection_target'] = $this->collection_target($value['Executive']['id'],$first_date);
			$saleamount = $this->sale_amount($value['Executive']['id'],$first_date,$last_date);
			$data['collection_total_amount'] = $this->collection_amount($value['Executive']['id'],$first_date,$last_date);



			$data['sale_amount'] = 0;
			if(!empty($saleamount)){
				$data['sale_amount'] = $saleamount[0]['amount'];
				$totalSale = $totalSale + $saleamount[0]['amount'];
			}
			$bonus = $this->bonus_amount($value['Executive']['id'],$month);
			$data['bonus_amount'] = 0;
			$data['bonus_id'] = 0;
			if(!empty($bonus)){
				$data['bonus_amount'] = $bonus['ExecutiveBonus']['amount'];
				$data['bonus_id'] = $bonus['ExecutiveBonus']['id'];
			}
			$BonusConfiguration=$this->BonusConfiguration->find('first',array(
				'conditions'=>array('(BonusConfiguration.wef_date) <='=>$first_date,'executive_id'=>$value['Executive']['id']),
				'fields'=>'max(BonusConfiguration.wef_date) as date',

				));

			$bonus_sales = $this->BonusConfiguration->field('BonusConfiguration.bonus_sales',
				array('BonusConfiguration.wef_date' => $BonusConfiguration[0]['date'],'executive_id'=>$value['Executive']['id']));
			$bonus_collection = $this->BonusConfiguration->field('BonusConfiguration.bonus_collection',
				array('BonusConfiguration.wef_date' => $BonusConfiguration[0]['date'],'executive_id'=>$value['Executive']['id']));
			$eligibility_sale_target = $this->BonusConfiguration->field('BonusConfiguration.eligibility_sale_target',
				array('BonusConfiguration.wef_date' => $BonusConfiguration[0]['date'],'executive_id'=>$value['Executive']['id']));
			$eligibility_collection_target = $this->BonusConfiguration->field('BonusConfiguration.eligibility_collection_target',
				array('BonusConfiguration.wef_date' => $BonusConfiguration[0]['date'],'executive_id'=>$value['Executive']['id']));
			$bonus_eligible_sale=($data['target']*$eligibility_sale_target)/100;
			$bonus_eligible_collection=($data['collection_target']*$eligibility_collection_target)/100;
			if($bonus_eligible_sale<=$data['sale_amount'] && $bonus_eligible_collection<$data['collection_total_amount'])
			{
				$data['bonus_eligible']=1;
			}
			else
			{
				$data['bonus_eligible']=0;
			}
			$data['calculated_bonus_amount']=(($data['collection_total_amount']*$bonus_collection)/100)+(($data['sale_amount']*$bonus_sales)/100);
			array_push($evaluationList, $data);

		}
// pr($evaluationList);exit;
		$this->set(compact('evaluationList'));
		$Executive=$this->Executive->find('list');
		$this->set(compact('Executive'));
		$this->set('totalSale',$totalSale);
	}
	function collection_amount($id,$first_date,$last_date){
		$conditions=array();
		$conditions['Executive.id']=$id;

		$this->Executive->unbindModel(array('hasMany' => array('Sale')));
		$Customer=$this->Executive->find('all'
			,
			array(
				'joins'=>array(
					array(
						'table'=>'executive_route_mappings',
						'alias'=>'ExecutiveRouteMapping',
						'type'=>'INNER',
						'conditions'=>array('ExecutiveRouteMapping.executive_id=Executive.id')
						),

					array(
						'table'=>'customers',
						'alias'=>'Customer',
						'type'=>'RIGHT',
						'conditions'=>array('Customer.route_id=ExecutiveRouteMapping.route_id')
						),
					array(
						'table'=>'account_heads',
						'alias'=>'AccountHead',
						'type'=>'INNER',
						'conditions'=>array('AccountHead.id=Customer.account_head_id')
						),

					),
//'conditions'=>array('Executive.id'=>$ex_id),
				'conditions'=>$conditions,
				'fields'=>array(
					'Customer.route_id',
					'Customer.account_head_id',
					'AccountHead.id',
					'AccountHead.name',
//'Executive.*',
					'ExecutiveRouteMapping.*',
					),
				));


		$collection_total_amount=0;
		foreach ($Customer as $key => $value) {

			$conditions=array();
			if(!empty($first_date)){
				$conditions['Journal.date between ? and ?']=[date('Y-m-d',strtotime($first_date)),date('Y-m-d',strtotime($last_date))];
			}
			$conditions['Journal.credit']=$value['AccountHead']['id'];
			$conditions['AccountHeadDebit.sub_group_id']=['1','2'];
			$conditions['Journal.flag']=1;
			$this->Journal->virtualFields = array( 'total_amount' => "SUM(Journal.amount)" );
			$Journal=$this->Journal->find('first',array('conditions'=>$conditions,
				'fields'=>array('Journal.total_amount','Journal.date')
				));

			if($Journal['Journal']['total_amount'])
			{
				$collection_total_amount=$collection_total_amount+floatval($Journal['Journal']['total_amount']);
			}

		}
		return $collection_total_amount;
	}

	function sale_target($id,$first_date){
		$SaleTargetList=$this->SaleTarget->find('first',array(
			'conditions'=>array('(SaleTarget.wef_date) <='=>$first_date,'executive_id'=>$id),
			'fields'=>'max(SaleTarget.wef_date) as date',

			));
		$target = $this->SaleTarget->field(

			'SaleTarget.target',

			array('SaleTarget.wef_date' => $SaleTargetList[0]['date'],'executive_id'=>$id));
		return $target;
	}
	function collection_target($id,$first_date){
		$CollectionTargetList=$this->SaleTarget->find('first',array(
			'conditions'=>array('(SaleTarget.wef_date) <='=>$first_date,'executive_id'=>$id),
			'fields'=>'max(SaleTarget.wef_date) as date',

			));
		$collection_target = $this->SaleTarget->field(

			'SaleTarget.collection_target',

			array('SaleTarget.wef_date' => $CollectionTargetList[0]['date'],'executive_id'=>$id));
		return $collection_target;
	}
	function sale_amount($id,$first_date,$last_date){
		$SaleAmountList=$this->Sale->find('first',array(
			'conditions'=>array('(Sale.date_of_order) between ? and ? '=>array($first_date,$last_date),'executive_id'=>$id),
			'fields'=>'sum(grand_total) as amount',
			'group'=> 'executive_id',

			));
// 	$target = $this->Sale->field(

// 'sum(grand_total) as amount',

// array('(Sale.date_of_order) between ? and ? '=>array(date('Y-m-d',strtotime('first day of this month')),date('Y-m-d',strtotime('last day of this month'))),'executive_id'=>$id));
		return $SaleAmountList;
	}
	function bonus_amount($staff_id,$month){
		$BonusAmountList=$this->ExecutiveBonus->find('first',array(
			'conditions'=>array('month_year'=>$month,'staff_id'=>$staff_id),
			));
	return $BonusAmountList;
	}
	function bonus_amount_calculation($staff_id,$month){
		$BonusAmountList=$this->ExecutiveBonus->find('first',array(
			'conditions'=>array('month_year'=>$month,'staff_id'=>$staff_id),
			));
		$date=explode('.',$month);
		$date_month=$date[0];
		$date_year=$date[1];
		$BonusAmount=$this->ExecutiveBonusDetails->find('all',array(
			'conditions'=>array('year(ExecutiveBonusDetails.receipt_date)'=>$date_year,'month(ExecutiveBonusDetails.receipt_date)'=>$date_month,'ExecutiveBonusDetails.staff_id'=>$staff_id),
			));
		$bonus=0;$bonus_return_amount=0;$bonus_amount=0;
		foreach ($BonusAmount as $key => $value) {
			$bonus_amount+=$value['ExecutiveBonusDetails']['bonus_amount'];
		}
          $bonus=$bonus_amount;
		return $bonus;
		//return $BonusAmountList;
	}
	function bonus_amount_executive($id,$month){
		$BonusAmountList=$this->ExecutiveBonus->find('first',array(
			'conditions'=>array('month_year'=>$month,'executive_id'=>$id),
			));
		$date=explode('.',$month);
		$date_month=$date[0];
		$date_year=$date[1];
		$BonusAmount=$this->ExecutiveBonusDetails->find('all',array(
			'conditions'=>array('year(ExecutiveBonusDetails.receipt_date)'=>$date_year,'month(ExecutiveBonusDetails.receipt_date)'=>$date_month,'ExecutiveBonusDetails.executive_id'=>$id),
			));
		$bonus=0;$bonus_return_amount=0;$bonus_amount=0;
		foreach ($BonusAmount as $key => $value) {
			$bonus_amount+=$value['ExecutiveBonusDetails']['bonus_amount'];
		}
		foreach ($BonusAmount as $key => $value1) {
			$bonus_return_amount+=$value1['ExecutiveBonusDetails']['bonus_return_amount'];
		}
          $bonus=$bonus_amount-$bonus_return_amount;
          return $bonus;
		//return $BonusAmountList;


	}

	public function evaluation_search_ajax()
	{
		$conditions=array();
// $conditions['Stock.flag']=1;

		if(!empty($this->request->data['executive_id']))
		{
			$conditions['Executive.id']=$this->request->data['executive_id'];
		}
		$executive_id_List = $this->Executive->find('all',array('fields'=>'Executive.id,Executive.name',
			'conditions'=>$conditions));

		$totalSale =0;
		$data['row']='';
		$month = date('m.Y');
		$first_date = date('Y-m-d',strtotime('first day of this month'));
		$last_date = date('Y-m-d',strtotime('last day of this month'));
		if(!empty($this->request->data['month']))
		{
			$month = $this->request->data['month'];
			$date = strtotime('01.'.$this->request->data['month']);
			$first_date = date('Y-m-d',strtotime('first day of this month', $date));
			$last_date = date('Y-m-d',strtotime('last day of this month', $date));

		}
		foreach ($executive_id_List as $key => $value) {


			$saleamount = $this->sale_amount($value['Executive']['id'],$first_date,$last_date);
			$sale_amount = 0;
			if(!empty($saleamount)){
				$sale_amount = $saleamount[0]['amount'];
				if($sale_amount==''){
					$sale_amount = 0;
				}

			}
			$target = $this->sale_target($value['Executive']['id'],$first_date);
			if(empty($target) || $target==''){
				$target =0;
			}
			$status =1;
			if(!empty($this->request->data['performance_id']))
			{
				if($this->request->data['performance_id']!=1){
					if($sale_amount < $target || $sale_amount==0)
						$status =0;
				}
				else{

					if($target < $sale_amount){
						$status =0;
					}

				}
			}
			$bonus = $this->bonus_amount($value['Executive']['id'],$month);
			$bonus_amount = 0;
			$bonus_id = 0;
			if(!empty($bonus)){
				$bonus_amount = $bonus['ExecutiveBonus']['amount'];
				$bonus_id = $bonus['ExecutiveBonus']['id'];
			}

			$BonusConfiguration=$this->BonusConfiguration->find('first',array(
				'conditions'=>array('(BonusConfiguration.wef_date) <='=>$first_date,'executive_id'=>$value['Executive']['id']),
				'fields'=>'max(BonusConfiguration.wef_date) as date',

				));

			$bonus_sales = $this->BonusConfiguration->field('BonusConfiguration.bonus_sales',
				array('BonusConfiguration.wef_date' => $BonusConfiguration[0]['date'],'executive_id'=>$value['Executive']['id']));
			$bonus_collection = $this->BonusConfiguration->field('BonusConfiguration.bonus_collection',
				array('BonusConfiguration.wef_date' => $BonusConfiguration[0]['date'],'executive_id'=>$value['Executive']['id']));
			$eligibility_sale_target = $this->BonusConfiguration->field('BonusConfiguration.eligibility_sale_target',
				array('BonusConfiguration.wef_date' => $BonusConfiguration[0]['date'],'executive_id'=>$value['Executive']['id']));
			$eligibility_collection_target = $this->BonusConfiguration->field('BonusConfiguration.eligibility_collection_target',
				array('BonusConfiguration.wef_date' => $BonusConfiguration[0]['date'],'executive_id'=>$value['Executive']['id']));
			$sale_target=$this->sale_target($value['Executive']['id'],$first_date);
			$collection_target=$this->collection_target($value['Executive']['id'],$first_date);
			$bonus_eligible_sale=($sale_target*$eligibility_sale_target)/100;
			$bonus_eligible_collection=($collection_target*$eligibility_collection_target)/100;
			$collection_total_amount=$this->collection_amount($value['Executive']['id'],$first_date,$last_date);
			if($bonus_eligible_sale<=$sale_amount && $bonus_eligible_collection<$collection_total_amount)
			{
				$bonus_eligible='<span class="glyphicon glyphicon-ok"></span>';
			}
			else
			{
				$bonus_eligible='<span class="glyphicon glyphicon-remove"></span>';
			}
			$calculated_bonus_amount=(($collection_total_amount*$bonus_collection)/100)+(($sale_amount*$bonus_sales)/100);
			if($status !=0)
			{
				$data['row']= $data['row'].'<tr class="blue-pd flash">';
				$data['row']= $data['row'].'<td class="executive_name">'.$value["Executive"]["name"].'</td>';
				$data['row']= $data['row'].'<td>'.$this->sale_target($value['Executive']['id'],$first_date).'</td>';
				$data['row']= $data['row'].'<td>'.$this->collection_target($value['Executive']['id'],$first_date).'</td>';
				$data['row']= $data['row'].'<td>'.$sale_amount.'</td>';
				$data['row']= $data['row'].'<td>'.$this->collection_amount($value['Executive']['id'],$first_date,$last_date).'</td>';
				$data['row']= $data['row'].'<td>'.$bonus_eligible.'</td>';
				$data['row']= $data['row'].'<td>'.$calculated_bonus_amount.'</td>';
				$data['row']= $data['row'].'<td data-id="'.$bonus_id.'" class="bonus">'.$bonus_amount.'</td>';
				$data['row']= $data['row'].'<td class="Edit" data-id="'.$value["Executive"]["id"].'"><a><i class="fa fa-edit"></i></a></td>';
				$data['row']= $data['row'].'</tr>';
				$data['executive_id'] = $value['Executive']['id'];
				$totalSale = $totalSale + $sale_amount;
			}


		}
		$data['totalSale']=$totalSale;
		echo json_encode($data);
		exit;
	}

	public function BasicPay()
	{
		$PermissionList = $this->Session->read('PermissionList');

		$menu_id = $this->Menu->field(

			'Menu.id',

			array('action ' => 'Hr/BasicPay'));

		if(!in_array($menu_id, $PermissionList))

		{

			$this->Session->setFlash("Permission denied");

			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));

		}
		if($this->request->data)
		{
			$data=$this->request->data['BasicPay'];
			$date = date('Y-m-d',strtotime($data['date']));
			$BasicPay_data=[
			'staff_id'=>$data['staff_id'],
			'wef_date'=>$date,
			'amount'=>$data['amount'],
			'remarks'=>$data['remarks'],
			];
			$datasource_BasicPay = $this->BasicPay->getDataSource();
			try {
				$datasource_BasicPay->begin();
				if(!$this->BasicPay->save($BasicPay_data))
				{
					$errors = $this->BasicPay->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_BasicPay->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_BasicPay->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );

		}
		else{
			$this->Staff->virtualFields = array(
				'staff_name' => "CONCAT(Staff.name, ' ', Staff.code)"
				);
			$Staff=$this->Staff->find('list', array('fields'=>['id','staff_name']));
			$this->set(compact('Staff'));
			$BasicPays=$this->BasicPay->find('all');
			$this->set(compact('BasicPays'));

		}
	}
	public function get_basicpay_details_ajax($id)
	{
		$return=[
		'status'=>'Empty',
		'BasicPay'=>[],
		];
		if(isset($id))
		{
			$BasicPay=$this->BasicPay->findById($id);
			$return['BasicPay']=$BasicPay['BasicPay'];	
		}
		echo json_encode($return);
		exit;
	}
	public function EditBasicPay()
	{
		if($this->request->data)
		{
			$data=$this->request->data['BasicPay'];
// pr($data);exit;
			$date = date('Y-m-d',strtotime($data['date']));
			$BasicPay_data=[
			'staff_id'=>$data['staff_id'],
			'wef_date'=>$date,
			'amount'=>$data['amount'],
			'remarks'=>$data['remarks'],
			];
			$datasource_BasicPay = $this->BasicPay->getDataSource();
			try {
				$datasource_BasicPay->begin();
				$this->BasicPay->id=$data['id'];
				if(!$this->BasicPay->save($BasicPay_data))
				{
					$errors = $this->BasicPay->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_BasicPay->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_BasicPay->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function DeleteBasicPay($id)
	{
		try {
			if(!$this->BasicPay->delete($id))
				throw new Exception("Cant Delete This Pay Structure", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	public function AllowDeduct()
	{
		$PermissionList = $this->Session->read('PermissionList');

		$menu_id = $this->Menu->field(

			'Menu.id',

			array('action ' => 'Hr/AllowDeduct'));

		if(!in_array($menu_id, $PermissionList))

		{

			$this->Session->setFlash("Permission denied");

			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));

		}
		if($this->request->data)
		{
			$data=$this->request->data['AllowDeduct'];
			$AllowDeduct_data=[
			'name'=>$data['name'],
			'is_percentage'=>$data['is_percentage'],
			'type'=>$data['type'],
			];
			$datasource_AllowDeduct = $this->AllowDeduct->getDataSource();
			try {
				$datasource_AllowDeduct->begin();
				if(!$this->AllowDeduct->save($AllowDeduct_data))
				{
					$errors = $this->AllowDeduct->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_AllowDeduct->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_AllowDeduct->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );

		}
		else{

			$AllowDeducts=$this->AllowDeduct->find('all');
			$this->set(compact('AllowDeducts'));

		}
	}
	public function get_allowdeduct_details_ajax($id)
	{
		$return=[
		'status'=>'Empty',
		'AllowDeduct'=>[],
		];
		if(isset($id))
		{
			$AllowDeduct=$this->AllowDeduct->findById($id);
			$return['AllowDeduct']=$AllowDeduct['AllowDeduct'];	
		}
		echo json_encode($return);
		exit;
	}
	public function EditAllowDeduct()
	{
		if($this->request->data)
		{
			$data=$this->request->data['AllowDeduct'];
			$AllowDeduct_data=[
			'name'=>$data['name'],
			'is_percentage'=>$data['is_percentage'],
			'type'=>$data['type'],
			];
			$datasource_AllowDeduct = $this->AllowDeduct->getDataSource();
			try {
				$datasource_AllowDeduct->begin();
				$this->AllowDeduct->id=$data['id'];
				if(!$this->AllowDeduct->save($AllowDeduct_data))
				{
					$errors = $this->AllowDeduct->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_AllowDeduct->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_AllowDeduct->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function DeleteAllowDeduct($id)
	{
		try {
			if(!$this->AllowDeduct->delete($id))
				throw new Exception("Cant Delete This Allowance/Deduction", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	public function AssignBonus()
	{
		$datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
		try {
			$datasource_ExecutiveBonus->begin();
			$userid=1;
			$data=$this->request->data;
			$id=$data['id'];
			$amount=trim($data['bonus_amount_new']);
			$executive_id = $data['executive_id'];
			$month=trim($data['month']);
// $date=date('Y-m-d H:i:s');
			if($id!=0)
			{
				$Bonus_function_return=$this->Bonus_Edit_Function($id,$amount);
			}
			else{
				$Bonus_function_return =$this->Bonus_Add_Function($executive_id,$month,$amount);
			}

			if($Bonus_function_return['result']!='Success')
				throw new Exception($Bonus_function_return['result'], 1);
			$datasource_ExecutiveBonus->commit();
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
			$datasource_ExecutiveBonus->rollback();
		}
		echo json_encode($return);
		exit;
	}
	public function Bonus_Edit_Function($id,$amount)
	{
		try {

			$this->ExecutiveBonus->id=$id;
			if(!$this->ExecutiveBonus->saveField('amount',$amount ))
				throw new Exception("Error Processing Bonus amount Updation", 1);
			$this->ExecutiveBonus->id=$id;
			$Bonus=$this->ExecutiveBonus->read();
			$return['bonus_id']=$Bonus['ExecutiveBonus']['id'];
			unset($Bonus['Bonus']['id']);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function Bonus_Add_Function($executive_id,$month,$amount)
	{
		try {
			if(!$executive_id)
				throw new Exception("Empty Executive Id", 1);
			if(!$month)
				throw new Exception("Empty Month", 1);

			if(!$amount && $amount!=0)
				throw new Exception("Empty Amount", 1);

			$bonus_data=[
			'executive_id'=>$executive_id,
			'month_year'=>$month,
			'amount'=>$amount,

			];
			$this->ExecutiveBonus->create();
			if(!$this->ExecutiveBonus->save($bonus_data))
			{
				$errors = $this->ExecutiveBonus->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$bonusid = $this->ExecutiveBonus->getLastInsertId();
			$this->ExecutiveBonus->id=$bonusid;
			$Bonus=$this->ExecutiveBonus->read();
			$return['bonus_id']=$Bonus['ExecutiveBonus']['id'];
			unset($Bonus['ExecutiveBonus']['id']);

			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function PayStructure()
	{
		$PermissionList = $this->Session->read('PermissionList');


		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Hr/PayStructure'));

		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		if(!$this->request->data){
			$this->Staff->virtualFields = array(
				'staff_name' => "CONCAT(Staff.name, ' ', Staff.code)"
				);
			$Staff_list=$this->Staff->find('list', array('fields'=>['id','staff_name']));
// $Staff_list=$this->Staff->find('list',array('fields'=>['Staff.id','Staff.name']));
			$this->set('Staff_list',$Staff_list);

			$totalSalary =0;
			$evaluationList = array();

			$month = date('m.Y',strtotime("-1 month"));
			$staff_id_List = $this->Staff->find('all',array('fields'=>'Staff.id,Staff.name',));
			foreach ($staff_id_List as $key => $value) {
				$data['staff_id'] = $value['Staff']['id'];
				$data['staff_name'] = $value['Staff']['name'];

				$pay_structure_row = $this->pay_structure_row($value['Staff']['id'],$month);
				if(!empty($pay_structure_row)){
					$data['id'] = $pay_structure_row['PayStructure']['id'];
					$data['basic_salary'] = $pay_structure_row['PayStructure']['basic_salary'];
					$data['bonus'] = $pay_structure_row['PayStructure']['bonus'];
					$data['allowance'] = $pay_structure_row['PayStructure']['allowance'];

					$data['deduction'] = $pay_structure_row['PayStructure']['deduction'];
					$data['net_salary'] = $pay_structure_row['PayStructure']['net_salary'];
					array_push($evaluationList, $data);
					$totalSalary = $totalSalary + $data['net_salary'];
				}



			}
			$this->set(compact('evaluationList'));

			$Staff=$this->Staff->find('list');
			$this->set(compact('Staff'));
			$this->set('totalSalary',$totalSalary);
		}
		else{
			$data = $this->request->data;
// $datasource_Journal = $this->Journal->getDataSource();
			try {
				if(!$this->request->data['staff_id'])
					throw new Exception("Empty Staff Id", 1);
				if(!$this->request->data['month'])
					throw new Exception("Empty Month", 1);

				$StaffRow=$this->Staff->findById($this->request->data['staff_id']);
				$outstanding_credit=$StaffRow['Staff']['outstanding_account_head_id'];
				$outstanding_debit=$StaffRow['Staff']['paid_account_head_id'];
				$outstanding_amount=$this->request->data['net_salary'];
// $outstanding=$data['outstanding'];
				$date= date('Y-m-d');
// $voucher_no =$StaffRow['Staff']['code'].'-'. $this->request->data['month'];
				$remarks = $StaffRow['Staff']['code'].':' .$this->request->data['month'].'Pay structure Created';
				$work_flow = "Expense";
				$user_id=1;
                  $voucher_no=$this->GetVoucherNo();
                  $Journal_data=[
				'remarks'=>$remarks,
				'credit'=>$outstanding_credit,
				'debit'=>$outstanding_debit,
				'amount'=>$outstanding_amount,
				'date'=>$date,
				'work_flow'=>$work_flow,
				'voucher_no'=>$voucher_no,
				'external_voucher'=>"",
				'flag'=>'1',
				'created_by'=>$user_id,
				'modified_by'=>$user_id,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
			];
			$this->Journal->create();
			if(!$this->Journal->save($Journal_data))
			{
				$errors = $this->Journal->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}

				$pay_data=[
				'staff_id'=>$this->request->data['staff_id'],
				'month_year'=>$this->request->data['month'],
				'basic_salary'=>$this->request->data['basic_salary'],
				'bonus'=>$this->request->data['bonus'],
				'allowance'=>$this->request->data['allowance'],
				'deduction'=>$this->request->data['deduction'],
				'net_salary'=>$this->request->data['net_salary'],
				'journal_id'=>$this->Journal->getLastInsertId(),

				];
				$this->PayStructure->create();
				if(!$this->PayStructure->save($pay_data))
				{
					$errors = $this->PayStructure->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}


				$return['result']='Success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}


	}
	public function GetVoucherNo()
{
	$Journal=$this->Journal->find('first',array('fields' => array('MAX(Journal.voucher_no) as voucher_no')));
	if($Journal) { $voucher_no=$Journal[0]['voucher_no']+1; } else { $voucher_no=1; }
// $Journal_gap=$this->Journal->query('SELECT a.voucher_no+1 AS start, MIN(b.voucher_no) - 1 AS end
// 	FROM journals AS a, journals AS b
// 	WHERE a.voucher_no < b.voucher_no
// 	GROUP BY a.voucher_no
// 	HAVING start < MIN(b.voucher_no) AND a.voucher_no+1 = MIN(b.voucher_no) - 1
// 	');
// if($Journal_gap)
// 	$voucher_no=$Journal_gap['0']['0']['start'];
	$voucher_no_one=$this->Journal->findByVoucherNo(1);
	if(!$voucher_no_one)
		$voucher_no=1;
	return $voucher_no;
}
	public function get_staff_pay_ajax()
	{
		$data = $this->request->data;
		$id = $this->request->data['Staff_id'];
		$month = $this->request->data['month'];
		try {
		if(isset($id))
		{
			$RowCount=$this->PayStructure->findByStaffIdAndMonthYearAndFlag($id,$month,1);
			if($RowCount)
			{
				throw new Exception("Already Created", 1);
			}
				$date = strtotime('01-'.$month);
				$first_date = date('Y-m-d',strtotime('first day of this month', $date));
				$basic_pay_amount = $this->basic_pay_amount($id,$first_date);
				if($basic_pay_amount){
					$return['basic_pay_amount'] = $basic_pay_amount;
					$return['bonus_amount'] = 0;
					$staff_id=$id;
					$executive_id = $this->Executive->field('Executive.id',array('staff_id ' => $id));

					if(!empty($executive_id)){
						$bonus = $this->bonus_amount_executive($executive_id,$month);
						if(!empty($bonus)){
							$return['bonus_amount'] = $bonus;
						}
					}
					else
					{
						$bonus = $this->bonus_amount_calculation($staff_id,$month);
						if(!empty($bonus)){
							//$return['bonus_amount'] = $bonus['ExecutiveBonus']['amount'];
							$return['bonus_amount']=$bonus;
						}
					}

					$return['status'] = 'Success';

				}
				else{
					$return['status'] = 'Set Basic Pay ';
				}

		}
	}
   catch (Exception $e) {
			$return['status']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	function basic_pay_amount($id,$first_date){
		$BasicPayList=$this->BasicPay->find('first',array(
			'conditions'=>array('(BasicPay.wef_date) <='=>$first_date,'staff_id'=>$id),
			'fields'=>'max(BasicPay.wef_date) as date',

			));
		$basic_pay = $this->BasicPay->field(
			'BasicPay.amount',
			array('BasicPay.wef_date' => $BasicPayList[0]['date'],'staff_id'=>$id));
		return $basic_pay;
	}
	public function PayList()
	{
		$PermissionList = $this->Session->read('PermissionList');

		$menu_id = $this->Menu->field(

			'Menu.id',

			array('action ' => 'Hr/PayList'));

		if(!in_array($menu_id, $PermissionList))

		{

			$this->Session->setFlash("Permission denied");

			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));

		}

		$totalSalary =0;
		$evaluationList = array();

		$month = date('m.Y',strtotime("-1 month"));
		$staff_id_List = $this->Staff->find('all',array('fields'=>'Staff.id,Staff.name',));
		foreach ($staff_id_List as $key => $value) {
			$data['staff_id'] = $value['Staff']['id'];
			$data['staff_name'] = $value['Staff']['name'];

			$pay_structure_row = $this->pay_structure_row($value['Staff']['id'],$month);
			if(!empty($pay_structure_row)){
				$data['id'] = $pay_structure_row['PayStructure']['id'];
				$data['basic_salary'] = $pay_structure_row['PayStructure']['basic_salary'];
				$data['bonus'] = $pay_structure_row['PayStructure']['bonus'];
				$data['allowance'] = $pay_structure_row['PayStructure']['allowance'];

				$data['deduction'] = $pay_structure_row['PayStructure']['deduction'];
				$data['net_salary'] = $pay_structure_row['PayStructure']['net_salary'];
				array_push($evaluationList, $data);
				$totalSalary = $totalSalary + $data['net_salary'];
			}



		}
		$this->set(compact('evaluationList'));
// $Staff=$this->Staff->find('list');
		$this->Staff->virtualFields = array(
			'staff_name' => "CONCAT(Staff.name, ' ', Staff.code)"
			);
		$Staff=$this->Staff->find('list', array('fields'=>['id','staff_name']));
		$this->set(compact('Staff'));
		$this->set('totalSalary',$totalSalary);
	}
	function pay_structure_row($id,$month){
		$Payrow=$this->PayStructure->find('first',array(
			'conditions'=>array('(PayStructure.month_year) '=>$month,'staff_id'=>$id,'flag'=>1),
			'fields'=>'PayStructure.*',

			));
		return $Payrow;
	}
	public function pay_search_ajax()
	{
		$conditions=array();

		if(!empty($this->request->data['staff_id']))
		{
			$conditions['Staff.id']=$this->request->data['staff_id'];
		}
		$staff_id_List = $this->Staff->find('all',array('fields'=>'Staff.id,Staff.name',
			'conditions'=>$conditions));

		$totalSalary =0;
		$data['row']='';
		$month = date('m.Y');
		$first_date = date('Y-m-d',strtotime('first day of this month'));
		$last_date = date('Y-m-d',strtotime('last day of this month'));
		if(!empty($this->request->data['month']))
		{
			$month = $this->request->data['month'];


		}
		foreach ($staff_id_List as $key => $value) {

			$staff_id = $value['Staff']['id'];
			$staff_name = $value['Staff']['name'];

			$pay_structure_row = $this->pay_structure_row($value['Staff']['id'],$month);
			if(!empty($pay_structure_row)){
				$id = $pay_structure_row['PayStructure']['id'];
				$basic_salary = $pay_structure_row['PayStructure']['basic_salary'];
				$bonus = $pay_structure_row['PayStructure']['bonus'];
				$allowance = $pay_structure_row['PayStructure']['allowance'];

				$deduction = $pay_structure_row['PayStructure']['deduction'];
				$net_salary = $pay_structure_row['PayStructure']['net_salary'];
				$status =1;
				if(!empty($this->request->data['performance_id']))
				{
					if($this->request->data['performance_id']!=1){
						if($net_salary < $basic_salary)
							$status =0;
					}
					else{

						if($basic_salary < $net_salary){
							$status =0;
						}

					}
				}

				if($status !=0)
				{
					$data['row']= $data['row'].'<tr class="blue-pd flash">';
					$data['row']= $data['row'].'<td class="staff_name">'.$staff_name.'</td>';
					$data['row']= $data['row'].'<td>'.$basic_salary.'</td>';

					$data['row']= $data['row'].'<td class="bonus">'.$bonus.'</td>';
					$data['row']= $data['row'].'<td >'.$allowance.'</td>';
					$data['row']= $data['row'].'<td >'.$deduction.'</td>';
					$data['row']= $data['row'].'<td >'.$net_salary.'</td>';
                    $data['row']= $data['row'].'<td><a OnClick="return confirm("Are you sure?");" href="'.$this->webroot.'Hr/DeletePaystructure/'.$id.'"><i class="fa fa-2x fa-trash blue-col"></i></a></td>';
					$data['row']= $data['row'].'</tr>';
					$data['staff_id'] = $value['Staff']['id'];
					$totalSalary = $totalSalary + $net_salary;
				}
			}





		}
		$data['totalSalary']=$totalSalary;
		echo json_encode($data);
		exit;
	}
	public function expense_head($name,$opening_balance,$date,$description){
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		$AccountingsController = new AccountingsController;
		try {
			$datasource_AccountHead->begin();


			$name=trim($name);
			$date=date('Y-m-d');

			$sub_group_id_paid = 36;
			$prepaid_sub_group_id = 37;
			$outstanding_subgroup_id =38;
			$function_return=$AccountingsController->AccountHeadCreate($sub_group_id_paid,$name.' PAID',$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['AccountHead_paid_id'] = $this->AccountHead->getLastInsertId();
			$prepaid_name=$name.' PREPAID';
			$function_return=$AccountingsController->AccountHeadCreate($prepaid_sub_group_id,$prepaid_name,'0',$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);

			$return['AccountHead_prepaid_id'] = $this->AccountHead->getLastInsertId();
			$outstanding_name=$name.' OUTSTANDING';
			$function_return=$AccountingsController->AccountHeadCreate($outstanding_subgroup_id,$outstanding_name,'0',$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['AccountHead_outstanding_id'] = $this->AccountHead->getLastInsertId();
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		return $return;
	}
	public function SalaryPayment()
	{
		$PermissionList = $this->Session->read('PermissionList');

		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Hr/SalaryPayment'));

		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		$AccountingsController = new AccountingsController;
		$sub_group_id=38;
		$user_id=1;
		if(!$this->request->data)
		{
			$data['Journal']['date']=date('d-m-Y');
			$data['from_date']=date('d-m-Y',strtotime('-1 month'));
			$data['to_date']=date('d-m-Y');
			$this->request->data=$data;

// $AccountHead_list=$AccountingsController->AccountHead_Option_ListBySubGroupId($sub_group_id);
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
			$this->set('AccountHead',$AccountHead_list);
// $AccountHeads=$AccountingsController->AccountHead_Table_ListBySubGroupId($sub_group_id);
			$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
			$account_all=[];
			foreach ($AccountHeads as $keyA => $valueA) {
				$account_single['id']=$valueA['AccountHead']['id'];
				$account_single['name']=$valueA['AccountHead']['name'];
				$account_single['display_name']=$valueA['Staff']['name'];
				$account_single['date']=$valueA['AccountHead']['created_at'];
				$account_single['total']=$valueA['AccountHead']['opening_balance'];
				$account_single['Paid']='0';
				$Debit_N_Credit_function=$AccountingsController->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
				$account_single['total']=$Debit_N_Credit_function['credit'];
				$account_single['Paid']+=$Debit_N_Credit_function['debit'];
				$account_all[$account_single['id']]=$account_single;
			}
			$this->set('All_Account',$account_all);
		}
		else
		{
			$datasource_Journal = $AccountingsController->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data['Journal'];
				$credit=$data['mode'];
				$debit=$data['account_head'];
				$amount=$data['amount'];
				$date=$data['date'];
				$remarks=$data['remarks'];
				$remarks=$data['remarks'];
				$work_flow='Staff/Salary Payment';
				$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
				$return['result']='Success';
				$datasource_Journal->commit();
				$this->Session->setFlash(__($return['result']));
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']='Error';
				$return['message']=$e->getMessage();
				$this->Session->setFlash(__($return['message']));
			}
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function AccountHead_Option_ListBySubGroupId($sub_group_id)
	{

		$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'staffs',
					'alias'=>'Staff',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Staff.outstanding_account_head_id')
					),
				),
			'conditions'=>array(
				'SubGroup.id'=>$sub_group_id,
				),
			'fields'=>array(
				'AccountHead.id',
				'Staff.name',
				'Staff.code',
// 'AccountHead.name',
				'SubGroup.id',
				)
			));
		$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']] = $value['Staff']['name'].' '.$value['Staff']['code'];
		}
		return $AccountHead_list;
	}
	public function AccountHead_Table_ListBySubGroupId($sub_group_id)
	{

		$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'staffs',
					'alias'=>'Staff',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Staff.outstanding_account_head_id')
					),
				),
			'conditions'=>array(
				'SubGroup.id'=>$sub_group_id,
				),
			'fields'=>array(
				'AccountHead.*',
				'Staff.*',

				'SubGroup.*',
				)
			));
		return $AccountHeads;
	}
	public function BonusConfiguration($id=null)
	{

		if(!$this->request->data)
		{
			if($id)
			{
				$BonusConfiguration=$this->BonusConfiguration->findById($id);
				$this->request->data['BonusConfiguration']['wef_date']=date('d-m-Y',strtotime($BonusConfiguration['BonusConfiguration']['wef_date']));
				$this->request->data['BonusConfiguration']['executive_id']=$BonusConfiguration['BonusConfiguration']['executive_id'];
				$this->request->data['BonusConfiguration']['bonus_sales']=$BonusConfiguration['BonusConfiguration']['bonus_sales'];
				$this->request->data['BonusConfiguration']['bonus_collection']=$BonusConfiguration['BonusConfiguration']['bonus_collection'];
				$this->request->data['BonusConfiguration']['eligibility_collection_target']=$BonusConfiguration['BonusConfiguration']['eligibility_collection_target'];
				$this->request->data['BonusConfiguration']['eligibility_sale_target']=$BonusConfiguration['BonusConfiguration']['eligibility_sale_target'];
			}
			else
			{
				$this->request->data['BonusConfiguration']['wef_date']=date('d-m-Y');
			}
		}
		else
		{
			$data=$this->request->data['BonusConfiguration'];
//pr($data);exit;

			$data['wef_date']=date('Y-m-d',strtotime($data['wef_date']));			
			$datasource_BonusConfiguration = $this->BonusConfiguration->getDataSource();
			try {
				$datasource_BonusConfiguration->begin();
				if($id) 
					$this->BonusConfiguration->id=$id;
				else 
				{
					if($this->BonusConfiguration->findByExecutiveIdAndWefDate($data['executive_id'],$data['wef_date']))
						throw new Exception("Duplicate Entry", 1);
					$this->BonusConfiguration->create();
				}
				if(!$this->BonusConfiguration->save($data))
				{
					$errors = $this->BonusConfiguration->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_BonusConfiguration->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_BonusConfiguration->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			return $this->redirect(array('action' => 'BonusConfiguration'));
		}
		$executives=$this->Executive->find('list');
		$BonusConfigurations=$this->BonusConfiguration->find('all');
		$this->set(compact('BonusConfigurations','executives'));
	}
	public function get_bonus_configuration_ajax()
	{

		$requestData=$this->request->data;
		$columns = [];
		$columns[]='BonusConfiguration.wef_date';
		$columns[]='Executive.name';
		$columns[]='BonusConfiguration.bonus_sales';
		$columns[]='BonusConfiguration.bonus_collection';
		$columns[]='BonusConfiguration.bonus_sale_target';
		$columns[]='BonusConfiguration.bonus_collection_target';
		$columns[]='BonusConfiguration.id';
		$conditions=[];
		$totalData=$this->BonusConfiguration->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'BonusConfiguration.wef_date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'Executive.name LIKE' =>'%'. $q . '%',
				);
			$totalFiltered=$this->BonusConfiguration->find('count',[
				'conditions'=>$conditions,
				]);
		}
		$Data=$this->BonusConfiguration->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'BonusConfiguration.*',
				'Executive.name',
				)
			));
		foreach ($Data as $key => $value) {

			$Data[$key]['BonusConfiguration']['wef_date']=date('d-m-Y',strtotime($value['BonusConfiguration']['wef_date']));

			$Data[$key]['BonusConfiguration']['action']='<span data-id="'.$value['BonusConfiguration']['id'].'"><a href="'.$this->webroot.'Hr/BonusConfiguration/'.$value['BonusConfiguration']['id'].'"><i class="fa fa-2x fa-edit"></i></a></span>&nbsp;&nbsp;<a>';
			$Data[$key]['BonusConfiguration']['action'].="<a onclick='return confirm('Are you sure?')' href='".$this->webroot."Hr/DeleteBonusConfiguration/".$value['BonusConfiguration']['id']."'><i class='fa fa-2x fa-trash blue-col'></i></a>";

		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data);
		exit;
	}
	public function DeleteBonusConfiguration($id)
	{
		try {
			if(!$this->BonusConfiguration->delete($id))
				throw new Exception("Cant Delete This Target", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		return $this->redirect(array('action' => 'BonusConfiguration'));
	}
	public function ExecutiveBonusDetails_delete($id)
	{
		try {
			if(!$this->ExecutiveBonusDetails->delete($id))
				throw new Exception("Cant Delete ExecutiveBonusDetails", 1);
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function Role()
	{

	}
	public function Role_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='id';
		$columns[]='name';
		$columns[]='id';
		$conditions=[];
		$totalData=$this->Role->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Role.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->Role->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->Role->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Role.*',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['Role']['action']='';
			if(!$value['Role']['freeze'])
			{
				$Data[$key]['Role']['action'] ='<span><i table_id="'.$value['Role']['id'].'" class="fa fa-2x fa-edit   edit_staff   blue-col"></i></span>&nbsp;&nbsp;';
				$Data[$key]['Role']['action'] .="<span><i table_id='".$value['Role']['id']."' class='fa fa-2x fa-trash delete_staff blue-col'></i></span>";
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function Role_Save_ajax()
	{
		try {
			$data=$this->request->data['Role'];
			$data['name']=strtoupper(trim($data['name']));
			if(!$data['id'])
			{
				$this->Role->create();
				if(!$this->Role->save($data))
				{
					$errors = $this->Role->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
			}
			else
			{
				$this->Role->id=$data['id'];
				$this->Role->saveField('name',$data['name']);
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function Get_Role_ajax($id)
	{
		$Role=$this->Role->findById($id);
		echo json_encode($Role); exit;
	}
	public function Delete_Role_ajax($id)
	{
		try {
			if(!$this->Role->delete($id)) throw new Exception("Cant Delete", 1);
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function DeletePaystructure($id)
	{
		$datasource_Paystructure = $this->PayStructure->getDataSource();
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Paystructure->begin();
			$datasource_Journal->begin();
			$this->PayStructure->id=$id;
			$PayStructure=$this->PayStructure->findAllById($id,[
				'PayStructure.id','PayStructure.journal_id',
			]);
			if(!$this->PayStructure->saveField('flag','0'))
				throw new Exception("Error Processing Request", 1);
			if(!empty($PayStructure[0]['PayStructure']['journal_id']))
			{
				$this->Journal->id=$PayStructure[0]['PayStructure']['journal_id'];
				if(!$this->Journal->saveField('flag','0'))
					throw new Exception("Error Processing Request", 1);
			}
			$datasource_Paystructure->commit();
			$datasource_Journal->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		return $this->redirect(array('action' => 'PayStructure'));

		} catch (Exception $e) {
			$datasource_Paystructure->rollback();
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
		}
	}
}