<?php
App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');


/**
* Api Controller
*/
class ApiController extends AppController {
public $uses=[
	'Type',
	'MasterGroup',
	'Group',
	'SubGroup',
	'AccountHead',
	'Executive',
	'Location',
	'Route',
	'Customer',
	'Warehouse',
	'Sale',
	'Staff',
	'ExecutiveRouteMapping',
	'SaleItem',
	'Stock',
	'Journal',
	'Product',
	'CustomerType',
	'ClosingDay',
	'NoSale',
	'CustomerGroup',
	'DayRegister',
	'SalesReturn',
	'SalesReturnItem',
	'Division',
	'CustomerType',
	'StockLog',
	'ProductTypeBrandMapping',
	'ProductType',
	'SaleTarget',
	'Profile',
	'StockTransfer',
	'StockTransferItem'

];
public $components = array('RequestHandler','Flash','Session');





//executive login --ubm
public function executive_login()
{
	$result=[];
	$data=$this->request->data;
	if(isset($data['user_name']))
	{
		$username=$data['user_name'];	
	}
	if(isset($data['password']))
	{
		$password=$data['password'];	
	}
	if(isset($username) && isset($password))
	{
		$Executive=$this->Executive->find('first',array('conditions'=>array('username'=>$username,'password'=>$password)));
	}
	if(!isset($Executive))
	{
		$result['status']="Invalid Username or Password";	
	}
	else
	{
		if(!$Executive)
		{ 
			$result['status']="Invalid Username or Password";
		}
		else
		{

			$this->Session->write('ExUser.id', $Executive['Executive']['id']);
			$this->Session->write('ExUser.name', $Executive['Executive']['username']);
			$this->Session->write('ExUser.status', "Active"); 
			$result['status']="Login Successful";
			$result['id']=$Executive['Executive']['id'];
			$result['name']=$Executive['Executive']['username'];
		}
	}
	echo json_encode($result);
	exit;
}
//executive login

//executive reset password --ubm
public function executive_reset_password()
{
	$result=[];
	$data=$this->request->data;
	if(isset($this->request->data['executive_id']))
	{
		$Executive_id=$data['executive_id'];	
	}
	if(isset($data['old_password']))
	{
		$oldpassword=$data['old_password'];	
	}
	if(isset($data['new_password']))
	{
		$password=$data['new_password'];	
	}
	if(isset($Executive_id) && isset($oldpassword))
	{

		$Executive=$this->Executive->find('first',array('conditions'=>array('Executive.id'=>$Executive_id,'password'=>$oldpassword)));

		if(empty($Executive))
		{
			$result['status']="Incorrect exist Password";	
		}
		else
		{


			$this->Executive->id=$Executive_id;
			if(!$this->Executive->saveField('password', $password))
			{
				$errors = $this->Executive->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}

			$Executive_new=$this->Executive->find('first',array('conditions'=>array('Executive.id'=>$Executive_id)));
			$result['status']="Reset Password Successful";
			$result['id']=$Executive_new['Executive']['id'];
			$result['name']=$Executive_new['Executive']['username'];
		}
	}
	else{
		$result['status']="Reset password unsuccesfull";
	}


	echo json_encode($result);
	exit;
}
//executive eset password

// route list for executives --ubm
public function api_get_route_list()
{
	$return=[
	'status'=>'Empty',
	'Routes'=>[],
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];
	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$ExecutiveRoute=$this->ExecutiveRouteMapping->find('list',array(
			"joins"=>array(
				array(
					"table"=>'routes',
					"alias"=>'Route',
					"type"=>'inner',
					"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
					),
				),
			'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),

			'fields'=>
			'Route.name',
			'Route.id',
			'ExecutiveRouteMapping.route_id',
			));
		if($ExecutiveRoute)
		{
			$return[ 'status' ]='success';
			$All_Location=[];
			foreach ($ExecutiveRoute as $key => $value) {
				$Single_Location['id']=$key;
				$Single_Location['name']=$value;
				array_push($All_Location, $Single_Location);
			}
			$return[ 'Routes' ]=$All_Location;
		}
	}

	echo json_encode($return);
	exit;
}
// route list for executives --ubm

// group lists --ubm
public function api_get_group_list()
{
	$return=[
	'status'=>'Empty',
	'Groups'=>[],
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];
	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$CustomerGroups=$this->CustomerGroup->find('list',array(

			'fields'=>
			'CustomerGroup.name',
			'CustomerGroup.id',

			));
		if($CustomerGroups)
		{
			$return[ 'status' ]='success';
			$All_Group=[];
			foreach ($CustomerGroups as $key => $value) {
				$Single_Group['id']=$key;
				$Single_Group['name']=$value;
				array_push($All_Group, $Single_Group);
			}
			$return[ 'Groups' ]=$All_Group;
		}
	}

	echo json_encode($return);
	exit;
}
// group lists --ubm
//route and group
public function api_get_route_group_list()
{
	$return=[
	'status'=>'Empty',
	'Routes'=>[],
	'Groups'=>[],
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];
	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$ExecutiveRoute=$this->Route->find('list',array(
			"joins"=>array(
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
					),
				),
			'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),

			'fields'=>
			'Route.name',
			'Route.id',


			));
		if($ExecutiveRoute)
		{
			$return[ 'status' ]='success';
			$All_Location=[];
			foreach ($ExecutiveRoute as $key => $value) {
				$Single_Location['id']=$key;
				$Single_Location['name']=$value;
				array_push($All_Location, $Single_Location);
			}
			$return[ 'Routes' ]=$All_Location;
		}
		$CustomerGroups=$this->CustomerGroup->find('list',array(

			'fields'=>
			'CustomerGroup.name',
			'CustomerGroup.id',

			));
		if($CustomerGroups)
		{
			$return[ 'status' ]='success';
			$All_Group=[];
			foreach ($CustomerGroups as $key => $value) {
				$Single_Group['id']=$key;
				$Single_Group['name']=$value;
				array_push($All_Group, $Single_Group);
			}
			$return[ 'Groups' ]=$All_Group;
		}
	}

	echo json_encode($return);
	exit;
}

//route and group

//Day register for Executive

public function api_executive_day_register()
{

	$user_id=1;
	$return=[
	'status'=>'Empty',
	'SaleTargets' => 0.00,
	'ExProfile' =>[],
	'CompProfile' =>[],
	'RoutesList' =>[]

	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{

		$first_date = date('Y-m-d',strtotime('first day of this month'));
		$date = date('Y-m-d');
		$executive=$data['executive_id'];
		$Executive=$this->Executive->findById($executive);
		$target = $this->sale_target($executive,$first_date);
		if($target)
		{


			$return[ 'SaleTargets' ]=$target;
		}

	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$rowcount = $this->DayRegister->find('count', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));


		if($rowcount > 0 ){

			$return[ 'status' ]= 'Already Registered';
		}
		else
		{
			$datasource_DayRegister = $this->DayRegister->getDataSource();
			try {
				$datasource_DayRegister->begin();
				$warehouse_id = $Executive['Executive']['warehouse_id'];
				$intial_total_stock = $this->current_van_stock($executive);
				$loading_stock =0;

				$TransferList=$this->StockTransfer->find('first',array(
					'conditions'=>array('(StockTransfer.date) '=>$date,'warehouse_to'=>$warehouse_id),
					'fields'=>'max(StockTransfer.date) as date,StockTransfer.id as id',

					));
				if(!empty($TransferList)){
					if(!empty($TransferList['StockTransfer']['id'])){
						$id = $TransferList['StockTransfer']['id'];
						$loading_stock = $this->stock_transfer_cost($TransferList['StockTransfer']['id']);
					}
				}
				$initial_stock = $intial_total_stock - $loading_stock;
				if($initial_stock <0){
					$initial_stock = 0.00;
				}
				$tableData = [
				'executive_id' => $executive,
				'date' => date('Y-m-d'),
				'route_id' => $data['route_id'],
				'customer_group_id' => $data['customer_group_id'],
				'initial_stock' =>$initial_stock,
				'loading_stock'=>$loading_stock

				];
				$this->DayRegister->create();
				if (!$this->DayRegister->save($tableData))
					throw new Exception("Error in day Registering", 1);
				$datasource_DayRegister->commit();

				$return['status']='success';
			} catch (Exception $e) {
				$datasource_DayRegister->rollback();
				$return['status']=$e->getMessage();
			}
		}

		$return['ExProfile']['id']=$Executive['Executive']['id'];
		$return['ExProfile']['username']=$Executive['Executive']['username'];
		$return['ExProfile']['name']=$Executive['Executive']['name'];
		$return['ExProfile']['mobile']=$Executive['Executive']['mobile'];
		$Profile=$this->Profile->findById(1);
		$return['CompProfile']['address_line_1']=$Profile['Profile']['address_line_1'];
		$return['CompProfile']['address_line_2' ]=$Profile['Profile']['address_line_2'];
		$return['CompProfile']['mail_address' ]=$Profile['Profile']['mail_address'];
		$return['CompProfile']['mobile' ]=$Profile['Profile']['mobile'];
		$return['CompProfile']['company_name' ]=$Profile['Profile']['company_name'];
		$return['CompProfile']['vat_code' ]=$Profile['Profile']['vat_code'];
		$return['CompProfile']['cr_no' ]='4030594850';
		$Regs = $this->DayRegister->find('first', array(

			'conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => date('Y-m-d'))));

		$return[ 'route_id' ]=$Regs['DayRegister']['route_id'];
		$route_name = $this->Route->field(
			'Route.name',
			array('id ' => $Regs['DayRegister']['route_id']));
		$return[ 'route_name' ]=$route_name;
		$ExecutiveRoute=$this->Route->find('list',array(
			"joins"=>array(
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
					),
				),
			'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),

			'fields'=>
			'Route.name',
			'Route.id',


			));
		if($ExecutiveRoute)
		{

			$All_Location=[];
			foreach ($ExecutiveRoute as $key => $value) {

				$route_code = $this->Route->field(
					'Route.code',
					array('id ' => $key));
				$Single_Location['route_code'] = $route_code;

				$Sale=$this->Sale->find('first',array('conditions'=>array('is_erp'=>0,'Sale.invoice_no LIKE'=>"%".$route_code."%"),'order' => 'Sale.id DESC',));

				if(!empty($Sale))
				{
					$SaleNostr = $Sale['Sale']['invoice_no'];
					$SaleNo = str_replace($route_code, '', $SaleNostr);


				}
				else{
					$SaleNo = 000;
				}

				$Single_Location['invoice_no']=$SaleNo;

				array_push($All_Location, $Single_Location);
			}
			$return[ 'RoutesList' ]=$All_Location;
		}

		$Reg_Group=[];
		$group_name = $this->CustomerGroup->field(
			'CustomerGroup.name',
			array('id ' => $Regs['DayRegister']['customer_group_id']));
		$return[ 'group_nid' ]=$Regs['DayRegister']['customer_group_id'];
		$return[ 'group_name' ]=$group_name;


	}
	echo json_encode($return);
	exit;
}
//Day register for Executive


// customer list for executives --ubm
public function api_get_customer_list()
{
	$return=[
	'status'=>'Empty',
	'Customers'=>[],
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$date = date('Y-m-d');
		$executive=$data['executive_id'];

		$register_row = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));
		if(!empty($register_row)){
			$customer_group_id = $register_row['DayRegister']['customer_group_id'];
			$route_id = $register_row['DayRegister']['route_id'];
		}

	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($customer_group_id))
	{
		$CustomerList=$this->AccountHead->find('first',array(
			"joins"=>array(
				array(
					"table"=>'customers',
					"alias"=>'Customer',
					"type"=>'inner',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),

				),

			'conditions'=>array('Customer.customer_group_id'=>$customer_group_id,
				'Customer.route_id'=>$route_id
				),

			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',

				),

			));

		if($CustomerList)
		{
			$return[ 'status' ]='success';
			$All_Customer=[];
			foreach ($CustomerList as $key => $value) {
				$Single_Customer['id']=$key;
				$Single_Customer['name']=$value;
				array_push($All_Customer, $Single_Customer);
			}
			$return[ 'Customers' ]=$All_Customer;
		}
	}
	else{
		$return[ 'status' ]= 'Group is not registerd';
	}
	echo json_encode($return);
	exit;
}
// customer list for executives --ubm

//sale action   --ubm
public function api_order_place()
{
	$user_id=1;
	$sale_account_head_id=6;
	$discount_received_account_head_id=12;
	$discount_paid_account_head_id=13;
	$return=[
	'status'=>'empty',
	];

	$data=$this->request->data;
// $data=$this->request->data['Sale'];

	if($data)
	{

		$executive=$data['executive_id'];
		$date = date('Y-m-d');
		$rowcount = $this->ClosingDay->find('count', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' => $date)));
		if($rowcount > 0 ){
			$return[ 'status' ]= 'Day Closed';
		}

		else{


			$datasource_Sale = $this->Sale->getDataSource();
			$datasource_SaleItem = $this->SaleItem->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
			$datasource_Journal = $this->Journal->getDataSource();
			try {
				$Sale=$this->Sale->find('count');
				if(!empty($Sale))
				{
					$invoice_no=$Sale+1;
				}
				else
				{
					$invoice_no=1;
				}
				$grand_total=$data['total_amount']-$data['discount_amount'];
				$Sale_data=[
				'account_head_id'=>$data['customer_id'],
				'bill_id'=>'1',
				'executive_id'=>$data['executive_id'],
				'customer_name'=>'',
				'customer_mobile'=>'',
				'invoice_no'=>$invoice_no,
				'date_of_order'=>$date,
				'date_of_delivered'=>$date,
				'total'=>$data['total_amount'],
				'other_name'=>'',
				'other_value'=>'0',
				'discount'=>'0',
				'status'=>'2',
				'sale_type'=>$data['sale_type'],
				'discount_amount'=>$data['discount_amount'],
				'grand_total'=>$grand_total,
				'balance'=>$grand_total-$data['paid_amount'],
				'created_by'=>$user_id,
				'modified_by'=>$user_id,
				'created_at'=>$data['date'],
				// 'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$this->Sale->create();
				if(!$this->Sale->save($Sale_data))
				{
					$errors = $this->Sale->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$Executive=$this->Executive->findById($data['executive_id']);
				$warehouse_id=$Executive['Executive']['warehouse_id'];
				$ordered_products=$data['ordered_products'];
				$sale_id=$this->Sale->getLastInsertId();

				for ($i=0; $i <count($ordered_products) ; $i++) {
					$SaleItem_data=[
					'warehouse_id'=>$warehouse_id,
					'product_id'=>$ordered_products[$i]['product_id'],
					'sale_id'=>$sale_id,
					'unit_price'=>$ordered_products[$i]['unit_price'],
					'quantity'=>$ordered_products[$i]['product_quantity'],
					'net_value'=>$ordered_products[$i]['product_total'],
					'total'=>$ordered_products[$i]['product_total'],
					];
					$this->SaleItem->create();
					if(!$this->SaleItem->save($SaleItem_data))
					{
						$errors = $this->SaleItem->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
				}
				$date=date('Y-m-d');
				$datasource_Stock->begin();
				$Stock_function_return=$this->GeneralStock_Edit_Function_in_Purchase($sale_id,$date);
				if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result']);
				$datasource_Stock->commit();
				$datasource_Journal->begin();
				$credit=$sale_account_head_id;
				$debit=$data['customer_id'];
				$sale_amount=$data['total_amount'];
				$work_flow='Sales';
				$invoice_no=$invoice_no;

				$date=date('Y-m-d');
				$remarks='Sale Invoice No :'.$invoice_no;
				$Accountings_function_return=$this->JournalCreate($credit,$debit,$sale_amount,$date,$remarks,$work_flow,$user_id);


				if($Accountings_function_return['result']!='Success')
					throw new Exception($Stock_function_return['message']);

				$datasource_Journal->commit();
				$discount = $data['discount_amount'];
				if($discount>0)
				{
					$debit=$discount_paid_account_head_id;
					$credit=$data['customer_id'];
				}
				else
				{
					$discount=$discount*-1;
					$credit=$discount_received_account_head_id;
					$debit=$data['customer_id'];
				}
				$Accountings_function_return=$this->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id);
				if($Accountings_function_return['result']!='Success')
					throw new Exception($Stock_function_return['message']);
				$paid=$data['paid_amount'];
				if($paid>0)
				{

					$cashhead = $this->Executive->field(
						'Executive.account_head_id',
						array('id ' => $data['executive_id']));

					$datasource_Journal->begin();
					$account_head_id=$data['customer_id'];
					$function_return=$this->JournalCreate_in_Sale($data,$paid,$account_head_id,$date,$cashhead);
					if($function_return['result']!='Success')
						throw new Exception($function_return['result'], 1);
					$datasource_Journal->commit();
				}
				$return['status']='success';
				$return['invoice_no']=$invoice_no;
			} catch (Exception $e) {
				$datasource_Sale->rollback();
				$datasource_SaleItem->rollback();
				$return['status']=$e->getMessage(); 
			}
		}

	}
	echo json_encode($return);
	exit;
}
//sale action  --ubm
public function api_multiple_order_place()
{
	$user_id=1;
	$sale_account_head_id=6;
	$discount_received_account_head_id=12;
	$discount_paid_account_head_id=13;
	$tax_on_sale_account_head_id=9;
	$return=[
	'status'=>'empty',
	];
	$data=$this->request->data['Sale'];

	if($data)
	{
		$date = date('Y-m-d');
		$executive=$this->request->data['executive_id'];
		$cashhead = $this->Executive->field(
			'Executive.account_head_id',
			array('id ' => $executive));
		$rowcount = $this->ClosingDay->find('count', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' => $date)));


		if($rowcount > 0 ){
			$return[ 'status' ]= 'Day Closed';
		}

		else{


			$register_row = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));

			if(!empty($register_row)){

				$route_code = $register_row['Route']['code'];
				$datasource_Sale = $this->Sale->getDataSource();
				$datasource_SaleItem = $this->SaleItem->getDataSource();
				$datasource_Stock = $this->Stock->getDataSource();
				$datasource_Journal = $this->Journal->getDataSource();

				try {

					for ($j=0; $j <count($data) ; $j++) {
						
		
						$invoice_no = $data[$j]['invoice_no'];
						$order_no = $data[$j]['invoice_no'];
						$grand_total=$data[$j]['total_amount'];
						$Sale_data=[
						'account_head_id'=>$data[$j]['customer_id'],
						'bill_id'=>'1',
						'executive_id'=>$executive,
						'customer_name'=>'',
						'customer_mobile'=>'',
						'invoice_no'=>$invoice_no,
						'order_no'=>$order_no,
						'date_of_order'=>$date,
						'date_of_delivered'=>$date,
						'total'=>$data[$j]['total_amount'],
						'other_name'=>'',
						'other_value'=>'0',
						'discount'=>'0',
						'status'=>'2',
						'is_erp'=>0,
						'sale_type'=>$data[$j]['sale_type'],
						'discount_amount'=>'0',
						'grand_total'=>$grand_total,
						'balance'=>$grand_total-$data[$j]['paid_amount'],
						'created_by'=>$user_id,
						'modified_by'=>$user_id,
						'created_at'=>$data[$j]['bill_date'],
						// 'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s'),
						];
						$this->Sale->create();
						if(!$this->Sale->save($Sale_data))
						{
							$errors = $this->Sale->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						$Executive=$this->Executive->findById($executive);
						$warehouse_id=$Executive['Executive']['warehouse_id'];
						$ordered_products=$data[$j]['ordered_products'];
						$sale_id=$this->Sale->getLastInsertId();
						$tax_Am =0;
						for ($i=0; $i <count($ordered_products) ; $i++) {
							$tax = $this->Product->field(
								'Product.tax',
								array('Product.id ' => $ordered_products[$i]['product_id']));

							$incl_tax=($ordered_products[$i]['unit_price']*($tax/100));
							$tax_rate=($incl_tax)/($ordered_products[$i]['unit_price']+$incl_tax)*100;

							$basic_rate=($ordered_products[$i]['unit_price'])-($ordered_products[$i]['unit_price']*($tax_rate/100));
							$tax_amount = $ordered_products[$i]['product_total']-($ordered_products[$i]['product_quantity']*$basic_rate);


							$tax_Am+=$tax_amount;
							$SaleItem_data=[
							'warehouse_id'=>$warehouse_id,
							'product_id'=>$ordered_products[$i]['product_id'],
							'sale_id'=>$sale_id,
							'unit_price'=>$basic_rate,
							'tax'=>$tax,
							'tax_amount'=>$tax_amount,
							'quantity'=>$ordered_products[$i]['product_quantity'],
							'net_value'=>$ordered_products[$i]['product_quantity']*$basic_rate,
							'total'=>$ordered_products[$i]['product_total'],
							];
							$this->SaleItem->create();
							if(!$this->SaleItem->save($SaleItem_data))
							{
								$errors = $this->SaleItem->validationErrors;
								foreach ($errors as $key => $value) {
									throw new Exception($value[0], 1);
								}
							}
						}
						$tax_value = $tax_Am;
						$date=date('Y-m-d');
						$datasource_Stock->begin();
						$Stock_function_return=$this->GeneralStock_Edit_Function_in_Purchase($sale_id,$date);

						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
						$datasource_Stock->commit();
						$datasource_Journal->begin();
						$credit=$sale_account_head_id;
						$debit=$data[$j]['customer_id'];
						$sale_amount=$data[$j]['total_amount'];
						$work_flow='From Executive app';
						$invoice_no=$invoice_no;
						$date=date('Y-m-d',strtotime($data[$j]['bill_date']));
						$remarks='Sale Invoice No :'.$invoice_no;
						$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
						if(!empty($Journals))
						{
							$JournalsNo = $Journals['Journal']['voucher_no'];
							$Journals_no=$JournalsNo+1;

						}
						else
						{
							$Journals_no=1;
						}
						$sale_net_value = $sale_amount - $tax_value;
						$Accountings_function_return=$this->JournalCreate($credit,$debit,$sale_net_value,$date,$remarks,$work_flow,$user_id,$Journals_no);
						$datasource_Journal->commit();

						if($Accountings_function_return['result']!='Success')
							throw new Exception($Stock_function_return['message']);

						// $datasource_Journal->commit();

						$discount = 0;
						$Accountings_function_return=$this->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id,$Journals_no);

						if($Accountings_function_return['result']!='Success')
							throw new Exception($Stock_function_return['message']);

						if($tax_value){
							$debit=$data[$j]['customer_id'];
							$credit=$tax_on_sale_account_head_id;
							$Accountings_function_return=$this->JournalCreate($credit,$debit,$tax_value,$date,$remarks,$work_flow,$user_id,$Journals_no);
							if($Accountings_function_return['result']!='Success')
								throw new Exception($Accountings_function_return['message']);
						}
						$paid=$data[$j]['paid_amount'];
						if($paid>0)
						{
							$datasource_Journal->begin();
							$account_head_id=$data[$j]['customer_id'];
							$function_return=$this->JournalCreate_in_Sale($invoice_no,$paid,$account_head_id,$date,$cashhead,$Journals_no);

							if($function_return['result']!='Success')
								throw new Exception($function_return['result'], 1);
						}
						$datasource_Journal->commit();
					}

					$return['status']='success';
				}
				catch (Exception $e) {
					$datasource_Sale->rollback();
					$datasource_SaleItem->rollback();
					$return['status']=$e->getMessage(); 
				}

			}else{
				$return[ 'status' ]= 'Day is not Registered';
			}

		}

	}
	echo json_encode($return);
	exit;
}

//sale action  --ubm
public function api_multiple_quotation()
{
	$user_id=1;

	$return=[
	'status'=>'empty',
	];

	$data=$this->request->data['Sale'];

	if($data)
	{

		$date = date('Y-m-d');
		$executive=$this->request->data['executive_id'];

		$rowcount = $this->ClosingDay->find('count', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' =>$date)));
		if($rowcount > 0 ){
			$return[ 'status' ]= 'Day Closed';
		}

		else{
			$register_row = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));

			if(!empty($register_row)){
				$route_code = $register_row['Route']['code'];
				$datasource_Sale = $this->Sale->getDataSource();
				$datasource_SaleItem = $this->SaleItem->getDataSource();

				try {

					for ($j=0; $j <count($data) ; $j++) {

						$Sale=$this->Sale->find('first',array('conditions'=>array('is_erp'=>0,'Sale.invoice_no LIKE'=>"%".$route_code."%"),'order' => 'Sale.id DESC',));

						if(!empty($Sale))
						{
							$SaleNostr = $Sale['Sale']['invoice_no'];
							$SaleNo = str_replace($route_code, '', $SaleNostr);
							$SaleNo = $SaleNo+1;
							$invoice_no=$route_code.$SaleNo;
							$order_no=$route_code.$SaleNo;

						}
						else
						{
							$invoice_no=$route_code.'111';
							$order_no=$route_code.'111';
						}

						$grand_total=$data[$j]['total_amount'];
						$Sale_data=[
						'account_head_id'=>$data[$j]['customer_id'],
						'bill_id'=>'1',
						'executive_id'=>$executive,
						'customer_name'=>'',
						'customer_mobile'=>'',
						'invoice_no'=>$invoice_no,
						'order_no'=>$order_no,
						'date_of_order'=>$date,
						'date_of_delivered'=>$date,
						'total'=>$data[$j]['total_amount'],
						'other_name'=>'',
						'other_value'=>'0',
						'discount'=>'0',
						'status'=>'1',
						'is_erp'=>'0',
						'sale_type'=>$data[$j]['sale_type'],
						'discount_amount'=>'0',
						'grand_total'=>$grand_total,
						'balance'=>$grand_total,
						'created_by'=>$user_id,
						'modified_by'=>$user_id,
						'created_at'=>$data[$j]['bill_date'],
						// 'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s'),
						];
						$this->Sale->create();
						if(!$this->Sale->save($Sale_data))
						{
							$errors = $this->Sale->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						$Executive=$this->Executive->findById($executive);
						$warehouse_id=$Executive['Executive']['warehouse_id'];
						$ordered_products=$data[$j]['ordered_products'];
						$sale_id=$this->Sale->getLastInsertId();

						for ($i=0; $i <count($ordered_products) ; $i++) {
							$tax = $this->Product->field(
								'Product.vat',
								array('Product.id ' => $ordered_products[$i]['product_id']));

							$incl_tax=($ordered_products[$i]['unit_price']*($tax/100));
							$tax_rate=($incl_tax)/($ordered_products[$i]['unit_price']+$incl_tax)*100;

							$basic_rate=($ordered_products[$i]['unit_price'])-($ordered_products[$i]['unit_price']*($tax_rate/100));
							$tax_amount = $ordered_products[$i]['product_total'] - ($basic_rate*$ordered_products[$i]['product_quantity']);

							$SaleItem_data=[
							'warehouse_id'=>$warehouse_id,
							'product_id'=>$ordered_products[$i]['product_id'],
							'sale_id'=>$sale_id,
							'unit_price'=>$basic_rate,
							'tax'=>$tax,
							'tax_amount'=>$tax_amount,
							'quantity'=>$ordered_products[$i]['product_quantity'],
							'net_value'=>$basic_rate*$ordered_products[$i]['product_quantity'],
							'total'=>$ordered_products[$i]['product_total'],
							];
							$this->SaleItem->create();
							if(!$this->SaleItem->save($SaleItem_data))
							{
								$errors = $this->SaleItem->validationErrors;
								foreach ($errors as $key => $value) {
									throw new Exception($value[0], 1);
								}
							}
						}

					}
					$return['status']='success';
				} catch (Exception $e) {
					$datasource_Sale->rollback();
					$datasource_SaleItem->rollback();
					$return['status']=$e->getMessage(); 
				}
			}else{
				$return[ 'status' ]= 'Day is not Registered';
			}
		}

	}
	echo json_encode($return);
	exit;
}


//receipt   --ubm
public function api_get_reciept()
{
	$return=[
	'status'=>'Empty',
	'receipt'=>'Empty',
	];
	$data=$this->request->data;
	if($data)
	{
		$customer_id=$data['customer_id'];
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$customer_id
				),
			'fields'=>array(
				'Sale.id',
				'Sale.invoice_no',
				'Sale.grand_total',
				'Sale.balance',
				'AccountHead.id',
				'AccountHead.name',
				),
			));
		$All_sale=['customer'=>[],'Sale'=>[],'total_sale'=>'0','total_balance'=>'0'];
		foreach ($Sale as $key => $value) {
			$Single_sale['sale_id']=$value['Sale']['id'];
			$Single_sale['invoice_no']=$value['Sale']['invoice_no'];
			$Single_sale['total']=$value['Sale']['grand_total'];
			$Single_sale['balance']=$value['Sale']['balance'];
			array_push($All_sale['Sale'], $Single_sale);
			$All_sale['customer']=[
			'id'=>$value['AccountHead']['id'],
			'name'=>$value['AccountHead']['name'],
			];
			$All_sale['total_sale']+=$value['Sale']['grand_total'];
			$All_sale['total_balance']+=$value['Sale']['balance'];
		}
		$return['receipt']=$All_sale;
	}
	$return['status']='Success';
	echo json_encode($return);
	exit;
}
//receipt 
//pending receipt   --ubm
public function api_get_pending_reciept()
{
	$return=[
	'status'=>'Empty',
	'receipt'=>'Empty',
	];
	$data=$this->request->data;
	if($data)
	{
		$customer_id=$data['customer_id'];
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$customer_id,
				'Sale.status'=>2
				),
			'fields'=>array(
				'Sale.id',
				'Sale.invoice_no',
				'Sale.grand_total',
				'Sale.total',
				'Sale.balance',
				'AccountHead.id',
				'AccountHead.name',
				),
			));
		$All_sale=['customer'=>[],'Sale'=>[],'total_sale'=>'0','total_balance'=>'0'];
		foreach ($Sale as $key => $value) {
			$Single_sale['sale_id']=$value['Sale']['id'];
			$Single_sale['invoice_no']=$value['Sale']['invoice_no'];
			$Single_sale['total']=$value['Sale']['grand_total'];
			$Single_sale['balance']=$value['Sale']['balance'];

			$Journal = $this->Journal->find('list', array(
				'conditions' => array(
					'Journal.remarks' => 'Sale Invoice No :' . $value['Sale']['invoice_no'],
					'Journal.credit' => $value['AccountHead']['id'],
					'Journal.flag=1',
					),
				'fields' => array(
					'Journal.amount',
					)
				));
			$paid_amount = 0;
			foreach ($Journal as $key0 => $amount) {
				$paid_amount+=$amount;
			}
			$balance = $value['Sale']['total'] - $paid_amount;
			$Single_sale['balance']=$balance;
			if($balance>0){
				array_push($All_sale['Sale'], $Single_sale);
			}
			$All_sale['customer']=[
			'id'=>$value['AccountHead']['id'],
			'name'=>$value['AccountHead']['name'],
			];
			$All_sale['total_sale']+=$value['Sale']['total'];
			$All_sale['total_balance']+=$balance;
		}
		$return['receipt']=$All_sale;
	}
	$return['status']='Success';
	echo json_encode($return);
	exit;
}
//pending receipt 
//receipt payment  --ubm
public function api_receipt_payment()
{
	$user_id=1;
	$return=[
	'status'=>'Empty',
	];
	$data=$this->request->data;
	if($data)
	{
		$date = date('Y-m-d');
		$executive=$data['executive_id'];
		$cashhead = $this->Executive->field(
			'Executive.account_head_id',
			array('id ' => $executive));
		if(empty($cashhead)){
			$cashhead =1;
		}
		$rowcount = $this->ClosingDay->find('count', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' => $date)));
		if($rowcount > 0 ){
			$return[ 'status' ]= 'Day Closed';
		}
		else{
			$datesourse_Sale=$this->Sale->getDataSource();
			$datesourse_Journal=$this->Journal->getDataSource();
			try {
				$datesourse_Sale->begin();
				$datesourse_Journal->begin();
				$PaidList = $data['PaidList'];

				foreach ($PaidList as $key => $value) {
					$invoice_no=$value['invoice_no'];
					$amount=$value['amount'];
					$Sale=$this->Sale->findByInvoiceNo($invoice_no);
					$this->Sale->id=$Sale['Sale']['id'];

					if(!$this->Sale->saveField('balance',$this->Sale->field('balance')-$amount))
						throw new Exception("Error Processing Request in Sale Updation", 1);
					$work_flow='From Mobile';
					$remarks='Sale Invoice No :'.$invoice_no;
					$date=date('Y-m-d');
					$debit=$cashhead;
					$credit=$Sale['Sale']['account_head_id'];
					$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);

					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}

				$datesourse_Sale->commit();
				$datesourse_Journal->commit();
				$openList = $data['openList'];

				foreach ($openList as $key => $value) {
					$invoice_no='0';
					$amount=$value['amount'];
					$work_flow='From Mobile';
					$remarks='Sale Invoice No :'.$invoice_no;
					$date=date('Y-m-d');
					$debit=$cashhead;
					$credit=$value['customer_id'];
					$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);

					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
				$return['status']='success';
			} catch (Exception $e) {
				$datesourse_Sale->rollback();
				$datesourse_Journal->rollback();
				$return['status']=$e->getMessage();
			}
		}

	}
	echo json_encode($return);
	exit;
}
//receipt payment

//sale return
public function api_sale_return()
{
	$sale_return_account_head_id=4;
	$tax_on_sale_return_account_head_id=11;
	$user_id=1;
	$return=[
	'status'=>'empty',
	];
	$SalesReturn=$this->SalesReturn->find('count');
	if(!empty($SalesReturn))
	{
		$invoice_no=$SalesReturn+1;
	}
	else
	{
		$invoice_no=1;
	}
	$executive=$this->request->data['executive_id'];
	$date = date('Y-m-d');
	$rowcount = $this->ClosingDay->find('count', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' => $date)));
	if($rowcount > 0 ){
		$return[ 'status' ]= 'Day Closed';
	}
	else{
		$datasource_SalesReturn = $this->SalesReturn->getDataSource();
		$datasource_SalesReturnItem = $this->SalesReturnItem->getDataSource();
		$datasource_Product = $this->Product->getDataSource();
		$datasource_Stock = $this->Stock->getDataSource();
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_SalesReturn->begin();
			$datasource_SalesReturnItem->begin();
			$datasource_Journal->begin();
			$data=$this->request->data;
			$data=$data['SalesReturn'];
			$SalesReturn_data=[
			'account_head_id'=>$data['customer_id'],
			'invoice_no'=>$invoice_no,
			'date'=>date('Y-m-d'),
			'total'=>$data['total'],
			'discount'=>'0',
			'grand_total'=>$data['grand_total'],
			'status'=>2,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
			];
			$this->SalesReturn->create();
			if(!$this->SalesReturn->save($SalesReturn_data))
			{
				$errors = $this->SalesReturn->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			if(empty($data['invoice_id'])){
				$data['invoice_id'] = 0;
			}
			$tax_amount = 0;
			$net_value = 0 ;
			$sales_return_id=$this->SalesReturn->getLastInsertId();
			$ReturnedProduct = $data['ReturnedProduct'];
			for ($i=0; $i <count($ReturnedProduct) ; $i++) {
				$tax = $ReturnedProduct[$i]['tax'];
				$incl_tax=($ReturnedProduct[$i]['unit_price']*($tax/100));
				$tax_rate=($incl_tax)/($ReturnedProduct[$i]['unit_price']+$incl_tax)*100;

				$basic_rate=($ReturnedProduct[$i]['unit_price'])-($ReturnedProduct[$i]['unit_price']*($tax_rate/100));
				$taxamount = $ReturnedProduct[$i]['refund_Amount']-($ReturnedProduct[$i]['return_quantity']*$basic_rate);
				$ReturnedProduct[$i]['net_value'] = $ReturnedProduct[$i]['refund_Amount'] - $taxamount;

				$tax_amount = $tax_amount + $taxamount;
				$net_value = $net_value + $ReturnedProduct[$i]['net_value'];
				$SalesReturnItem_data=[
				'product_id'=>$ReturnedProduct[$i]['product_id'],
				'sales_return_id'=>$sales_return_id,
				'invoice_no'=>$data['invoice_id'],
				'invoice_price'=>$ReturnedProduct[$i]['unit_price'],
				'unit_price'=>$ReturnedProduct[$i]['unit_price'],
				'quantity'=>$ReturnedProduct[$i]['return_quantity'],
				'net_value'=>$ReturnedProduct[$i]['net_value'],
				'cgst'=>0.00,
				'cgst_amount'=>0.00,
				'sgst'=>0.00,
				'sgst_amount'=>0.00,
				'tax'=>$ReturnedProduct[$i]['tax'],
				'tax_amount'=>$tax_amount,
				'total'=>$ReturnedProduct[$i]['refund_Amount'],
				];
				$this->SalesReturnItem->create();
				if(!$this->SalesReturnItem->save($SalesReturnItem_data))
				{
					$errors = $this->SalesReturnItem->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
			}
			$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
			if(!empty($Journals))
			{
				$JournalsNo = $Journals['Journal']['voucher_no'];
				$Journals_no=$JournalsNo+1;

			}
			else
			{
				$Journals_no=1;
			}
			$user_id = 1;
// $datasource_Journal->begin();
			$debit=$sale_return_account_head_id;
			$credit=$data['customer_id'];
			$amount=$net_value+$tax_amount;
			$discount=0;
			$work_flow='Sales Return From Executive app';
			$invoice_no= $invoice_no;
			$date=date('Y-m-d');
			$remarks='SalesReturn Invoice No :'.$invoice_no;
			$Accountings_function_return=$this->JournalCreate($credit,$debit,$net_value,$date,$remarks,$work_flow,$user_id,$Journals_no);
			if($Accountings_function_return['result']!='Success')
				throw new Exception($Stock_function_return['message']);

			if(floatval($tax_amount))
			{
				$credit=$data['customer_id'];
				$debit=$tax_on_sale_return_account_head_id;
				$Accountings_function_return=$this->JournalCreate($credit,$debit,$tax_amount,$date,$remarks,$work_flow,$user_id,$Journals_no);
				if($Accountings_function_return['result']!='Success')
					throw new Exception($Accountings_function_return['message']);
			}
			$credit= $this->Executive->field(
				'Executive.account_head_id',
				array('Executive.id ' => $executive));;
			$debit=$data['customer_id'];
			$Accountings_function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$Journals_no);
			if($Accountings_function_return['result']!='Success')
				throw new Exception($Stock_function_return['message']);
   
			$SalesReturnItem = $this->SalesReturnItem->find('all',array(
				'conditions' => array(
					'SalesReturnItem.sales_return_id' => $sales_return_id,
					),
				'fields' => array(
					'SalesReturnItem.product_id',
					'SalesReturnItem.quantity',
					'SalesReturnItem.unit_price',
					'SalesReturn.invoice_no',
					)
				));
			$datasource_Stock->begin();
			foreach ($SalesReturnItem as $key => $value) {
				$quantity = $value['SalesReturnItem']['quantity'];
				$unit_price = $value['SalesReturnItem']['unit_price'];
				$product_id=$value['SalesReturnItem']['product_id'];
				$Stock = $this->Stock->find('first',array(
					'conditions'=>array(
						'product_id'=>$product_id,
						'warehouse_id'=>1,
						)
					));
				$stock_id=$Stock['Stock']['id'];
				$stock_quantity=$Stock['Stock']['quantity'];
				$date=date('Y-m-d');
				$remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
				$quantity+=$stock_quantity;
				$Stock_function_return=$this->GeneralStock_Edit_Function($stock_id,$quantity,$date,$user_id,$remark);
				if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result']);
			}
			$return['status']='Success';
			$datasource_Stock->commit();
			$datasource_SalesReturn->commit();
			$datasource_SalesReturnItem->commit();
			$datasource_Journal->commit();
		}
		catch (Exception $e)
		{
			$datasource_SalesReturn->rollback();
			$datasource_SalesReturnItem->rollback();
			$datasource_Stock->rollback();
// $datasource_Journal->rollback();
			$return['status']=$e->getMessage();
		}
	}

	echo json_encode($return);
	exit;
}
//sale return

//customer search functionality
public function api_get_route_customer_search()
{
	$return=[
	'status'=>'Empty',
	'Customers'=>[],
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];

	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$CustomerList=$this->AccountHead->find('all',array(
			"joins"=>array(
				array(
					"table"=>'customers',
					"alias"=>'Customer',
					"type"=>'inner',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),

				),

			'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),

			'fields'=>array(
				'Customer.*',
				'AccountHead.id',
				'AccountHead.name',
				'AccountHead.opening_balance',

				),

			));

		if($CustomerList)
		{
			$return[ 'status' ]='success';
			$All_Customer=[];
			foreach ($CustomerList as $key => $value) {
				$Single_Customer['route_code'] = $this->Route->field(
					'Route.code',
					array('Route.id ' => $value['Customer']['route_id']));
				$Single_Customer['id']=$value['AccountHead']['id'];
				$Single_Customer['name']=$value['AccountHead']['name'];
				$Single_Customer['opening_balance']=$value['AccountHead']['opening_balance'];
				$Single_Customer['email']=$value['Customer']['email'];
				$Single_Customer['shope_code']=$value['Customer']['code'];
				$Single_Customer['credit_limit']=$value['Customer']['credit_limit'];
				$Single_Customer['address']=$value['Customer']['place'];
				$Single_Customer['mobile']=$value['Customer']
				['mobile'];
				$total = $value['AccountHead']['opening_balance'];
				$Recieved='0';
				$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
				$total+=$Debit_N_Credit_function['debit'];
				$Recieved+=$Debit_N_Credit_function['credit'];
				$Single_Customer['debit']=$total;
				$Single_Customer['credit']=$Recieved;
				$Sale=$this->Sale->find('all',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$value['AccountHead']['id'],
						'Sale.status'=>2
						),
					'fields'=>array(
						'Sale.id',
						'Sale.account_head_id',
						'Sale.invoice_no',
						'Sale.grand_total',
						'Sale.balance',
						'AccountHead.id',
						'AccountHead.name',
						),
					));
				$All_sale=[];
				foreach ($Sale as $key => $row) {
					$Single_sale['sale_id']=$row['Sale']['id'];
					$Single_sale['invoice_no']=$row['Sale']['invoice_no'];
					$Single_sale['total']=$row['Sale']['grand_total'];
					$Journal=$this->Journal->find('all',array('conditions'=>array(
						'credit'=>$row['Sale']['account_head_id'],
						'flag=1',
						'Journal.remarks '=>'Sale Invoice No :'.$row['Sale']['invoice_no'],
						)));
					$account_single=[];
					$credit = 0;

					foreach ($Journal as $key => $value) {
						$credit+=$value['Journal']['amount'];

					}
					$balance = 	$row['Sale']['grand_total'] - $credit;			
					$Single_sale['balance']= $balance;
					if($balance >0){
						array_push($All_sale, $Single_sale);
					}



				}
				$Single_Customer['receipt']=$All_sale;

//
				array_push($All_Customer, $Single_Customer);
			}

			$return[ 'Customers' ]=$All_Customer;
		}
	}


	echo json_encode($return);
	exit;
}

// customer list for executive routes --ubm
public function api_get_route_customer_list()
{
	$return=[
	'status'=>'Empty',
	'Customers'=>[],
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];

	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$CustomerList=$this->AccountHead->find('list',array(
			"joins"=>array(
				array(
					"table"=>'customers',
					"alias"=>'Customer',
					"type"=>'inner',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),

				),

			'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),

			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',

				),

			));
		if($CustomerList)
		{
			$return[ 'status' ]='success';
			$All_Customer=[];
			foreach ($CustomerList as $key => $value) {
				$Single_Customer['id']=$key;
				$Single_Customer['name']=$value;
				array_push($All_Customer, $Single_Customer);
			}
			$return[ 'Customers' ]=$All_Customer;
		}
	}


	echo json_encode($return);
	exit;
}
// route list for executive routes --ubm

// van stock for executives --ubm
public function api_get_van_stock()
{
	$return=[
	'status'=>'Empty',
	'Stocks'=>[],
	'total_stock'=>'0',
	'total_quantity'=>'0',
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];
		$warehouse_id = $this->Executive->field(
			'Executive.warehouse_id',
			array('Executive.id ' => $executive));
	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$Stock=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'stocks',
					"alias"=>'Stock',
					"type"=>'inner',
					"conditions"=>array('Product.id=Stock.product_id'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
					),
				),
			'conditions'=>array('Stock.flag'=>1,'Stock.warehouse_id'=>$warehouse_id),
			'limit'=>500,
			'fields'=>array(
				'Product.*',
				'Unit.name',
				'ProductType.name',
				'Warehouse.name',
				'Brand.name',
				'Stock.*',
				)
			));
		$All_Stock=[];
		$total_cost = 0;
		$total_quantity = 0;
		foreach($Stock as $value){
			if($value['Stock']['quantity']>0){
				$Single_stock['product_id']=$value['Product']['id'];
				$Single_stock['product_name']=$value['Product']['name'];

				$Single_stock['product_code']=$value['Product']['code'];
				$Single_stock['product_type_name']=$value['ProductType']['name'];
				$Single_stock['brand_name']=$value['Brand']['name'];
				$Single_stock['product_cost']=$value['Product']['cost'];
				$Single_stock['product_mrp']=$value['Product']['mrp'];
				$Single_stock['product_wholesale_price']=$value['Product']['wholesale_price'];
				$Single_stock['product_vat']=$value['Product']['tax'];
				$Single_stock['piece_per_cart']=$value['Product']['no_of_piece_per_unit'];
				$Single_stock['stock_quantity']=$value['Stock']['quantity'];
				$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
				$total_quantity+=$value['Stock']['quantity'];
				array_push($All_Stock, $Single_stock);
			}
		}
		if($Stock)
		{
			$return[ 'status' ]='success';
			$return[ 'Stocks' ]=$All_Stock;
			$return[ 'total_stock' ]=$total_cost;
			$return[ 'total_quantity' ]=$total_quantity;

		}
	}

	echo json_encode($return);
	exit;
}
// van stock for executives --ubm

// Current Sale Target for executives --ubm
public function api_get_sale_target()
{
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$return=[
	'status'=>'Empty',
	'SaleTargets'=>[],
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];

	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$target = $this->sale_target($executive,$first_date);

		if($target)
		{
			$return[ 'status' ]='success';

			$return[ 'SaleTargets' ]=$target;
		}
	}

	echo json_encode($return);
	exit;
}
// Current Sale Target for executives --ubm
// get product details of product_name/product_code
public function api_get_product_details()
{
	$data=$this->request->data;
	$product = $data['product'];
	$Product=$this->Product->find('first',array(
		'conditions'=>array(
			'OR' => array(
				array('Product.code' => $product),
				array('Product.name LIKE' => $product),
// array('Product.name LIKE' => "%".$product."%"),
				)
			)
		));
	if(!empty($Product))
	{
		$return['status']='Success';
		$return['Product']=$Product['Product'];
	}
	else
	{
		$return['status']='Empty';
	}
	echo json_encode($return);
	exit;
}
// get product details of product_name/product_code
// Customer credit limit and balance amount --ubm
public function api_get_credit_customer()
{
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$return=[
	'status'=>'Empty',
	'Customer'=>[],
	];
	$data=$this->request->data;
	if(isset($data['customer_id']))
	{
		$account_head_id=$data['customer_id'];
		$credit_limit = $this->Customer->field(
			'Customer.credit_limit',
			array('Customer.account_head_id ' => $account_head_id));
		$opening_balance = $this->AccountHead->field(
			'AccountHead.opening_balance',
			array('AccountHead.id ' => $account_head_id));

		$customer['credit_limit'] = $credit_limit;

	}
	else{
		$return[ 'status' ]= 'Customer Requered';
	}
	if(isset($account_head_id))
	{


		$total = $opening_balance;
		$Recieved='0';
		$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
		$total+=$Debit_N_Credit_function['debit'];
		$Recieved+=$Debit_N_Credit_function['credit'];

		$customer['balance'] = $total - $Recieved;
		if($customer)
		{
			$return[ 'status' ]='success';

			$return[ 'Customer' ]=$customer;
		}
	}

	echo json_encode($return);
	exit;
}
// Customer credit limit and balance amount
//invoice  detailes
public function api_get_invoice_details()
{
	$return=[
	'status'=>'Empty',
	'Sale'=>[],
	];
	$data=$this->request->data;
	if(isset($data['invoice_no']))
	{
		$invoice_no=$data['invoice_no'];
		$return['status']='success';
		$Sale=$this->Sale->find('first',array('conditions'=>array(
			'invoice_no'=>$invoice_no,
			),));
		if(!empty($Sale)){
			$return['Sale']['invoice_no']= $Sale['Sale']['invoice_no'];
			$return['Sale']['grand_total']= $Sale['Sale']['grand_total'];
			$return['Sale']['discount_amount']= $Sale['Sale']['discount_amount'];
			$return['Sale']['sale_type']= $Sale['Sale']['sale_type'];
			$return['Sale']['date']= $Sale['Sale']['date_of_delivered'];
			$return['Sale']['customer_name']= $Sale['AccountHead']['name'];

			$SaleItems=$this->Sale->find('all',array(
				"joins"=>array(
					array(
						"table"=>'sale_items',
						"alias"=>'SaleItem',
						"type"=>'inner',
						"conditions"=>array('Sale.id=SaleItem.sale_id'),
						),
					array(
						"table"=>'products',
						"alias"=>'Product',
						"type"=>'inner',
						"conditions"=>array('Product.id=SaleItem.product_id'),
						),
					),
				'conditions'=>array(
					'invoice_no'=>$invoice_no,
					),
				'fields'=>array(
					'SaleItem.unit_price',
					'SaleItem.quantity',
					'SaleItem.total',
			// 'Product.piecepercart',
					'Product.code',
					'Product.id',
					'Product.name',
					'SaleItem.tax',
					'SaleItem.tax_amount',
					),
				));
		}
		else{
			$return['status'] ="This invoice doesn't exist";
		}

	}
	if(isset($SaleItems))
	{
		$All_Sale=[];
		foreach ($SaleItems as $key => $value) {
			$Single_Sale['unit_price']=$value['SaleItem']['unit_price'];
			$Single_Sale['quantity']=$value['SaleItem']['quantity'];
			$Single_Sale['total']=$value['SaleItem']['total'];
			$Single_Sale['name']=$value['Product']['name'];
			$Single_Sale['code']=$value['Product']['code'];
			$Single_Sale['tax']=$value['SaleItem']['tax'];
			$Single_Sale['tax_amount']=$value['SaleItem']['tax_amount'];
			$Single_Sale['id']=$value['Product']['id'];
			array_push($All_Sale, $Single_Sale);
		}
		$return['Sale']['SaleItem']=$All_Sale;	
	}
	echo json_encode($return);
	exit;
}

//invoice  detailes
//invoice item detailes
public function api_get_invoice_item_details()
{
	$return=[
	'status'=>'Empty',
	'Sale'=>[],
	];
	$data=$this->request->data;
	if(isset($data['invoice_no']))
	{
		$invoice_no=$data['invoice_no'];
		$return['status']='success';
		$Sale=$this->Sale->find('all',array(
			"joins"=>array(
				array(
					"table"=>'sale_items',
					"alias"=>'SaleItem',
					"type"=>'inner',
					"conditions"=>array('Sale.id=SaleItem.sale_id'),
					),
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=SaleItem.product_id'),
					),
				),
			'conditions'=>array(
				'invoice_no'=>$invoice_no,
				),
			'fields'=>array(
				'SaleItem.unit_price',
				'SaleItem.quantity',
				'SaleItem.total',
				'SaleItem.tax',
				'SaleItem.tax_amount',
		// 'Product.piecepercart',
				'Product.code',
				'Product.id',
				'Product.name',
				'Product.cost',
				),
			));
	}
	if(isset($Sale))
	{
		$All_Sale=[];
		foreach ($Sale as $key => $value) {
			$Single_Sale['unit_price']=$value['SaleItem']['unit_price'];
			$Single_Sale['quantity']=$value['SaleItem']['quantity'];
			$Single_Sale['total']=$value['SaleItem']['total'];
			$Single_Sale['name']=$value['Product']['name'];
			$Single_Sale['code']=$value['Product']['code'];
			$Single_Sale['cost']=$value['Product']['cost'];
			$Single_Sale['tax']=$value['SaleItem']['tax'];
			$Single_Sale['tax_amount']=$value['SaleItem']['tax_amount'];
			// $Single_Sale['piecepercart']=$value['Product']['piecepercart'];
			$Single_Sale['id']=$value['Product']['id'];
			array_push($All_Sale, $Single_Sale);
		}
		$return['Sale']=$All_Sale;	
	}
	echo json_encode($return);
	exit;
}

//invoice item detailes
// Customer balance amount --ubm
public function api_get_customer_balance()
{
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$return=[
	'status'=>'Empty',
	'Customer'=>[],
	];
	$data=$this->request->data;
	if(isset($data['customer_id']))
	{
		$account_head_id=$data['customer_id'];

		$opening_balance = $this->AccountHead->field(
			'AccountHead.opening_balance',
			array('AccountHead.id ' => $account_head_id));


	}
	else{
		$return[ 'status' ]= 'Customer Requered';
	}
	if(isset($account_head_id))
	{


		$total = $opening_balance;
		$Recieved='0';
		$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
		$total+=$Debit_N_Credit_function['debit'];
		$Recieved+=$Debit_N_Credit_function['credit'];

		$customer['balance'] = $total - $Recieved;
		if($customer)
		{
			$return[ 'status' ]='success';

			$return[ 'Customer' ]=$customer;
		}
	}

	echo json_encode($return);
	exit;
}
// Customer balance amount
// Division/Type lists --ubm
public function api_get_division_type_list()
{
	$return=[
	'status'=>'Empty',
	'Divisions'=>[],
	'Types'=>[],
	];

	{
		$Divisions=$this->Division->find('list',array(

			'fields'=>
			'Division.name',
			'Division.id',

			));
		if($Divisions)
		{
			$return[ 'status' ]='success';
			$All_Division=[];
			foreach ($Divisions as $key => $value) {
				$Single_Division['id']=$key;
				$Single_Division['name']=$value;
				array_push($All_Division, $Single_Division);
			}
			$return[ 'Divisions' ]=$All_Division;
		}
		$CustomerTypes=$this->CustomerType->find('list',array(

			'fields'=>
			'CustomerType.name',
			'CustomerType.id',

			));
		if($CustomerTypes)
		{
			unset($CustomerTypes[1]);
			$return[ 'status' ]='success';
			$All_Type=[];
			foreach ($CustomerTypes as $key => $value) {
				$Single_Type['id']=$key;
				$Single_Type['name']=$value;
				array_push($All_Type, $Single_Type);
			}
			$return[ 'Types' ]=$All_Type;
		}
	}

	echo json_encode($return);
	exit;
}
// Division/Customer lists --ubm

// Division lists --ubm
public function api_get_division_list()
{
	$return=[
	'status'=>'Empty',
	'Divisions'=>[],
	];

	{
		$Divisions=$this->Division->find('list',array(

			'fields'=>
			'Division.name',
			'Division.id',

			));
		if($Divisions)
		{
			$return[ 'status' ]='success';
			$All_Division=[];
			foreach ($Divisions as $key => $value) {
				$Single_Division['id']=$key;
				$Single_Division['name']=$value;
				array_push($All_Division, $Single_Division);
			}
			$return[ 'Divisions' ]=$All_Division;
		}
	}

	echo json_encode($return);
	exit;
}
// Division lists --ubm
// Customer Type lists --ubm
public function api_get_customer_type_list()
{
	$return=[
	'status'=>'Empty',
	'Types'=>[],
	];

	{
		$CustomerTypes=$this->CustomerType->find('list',array(

			'fields'=>
			'CustomerType.name',
			'CustomerType.id',

			));
		if($CustomerTypes)
		{
			unset($CustomerTypes[1]);
			$return[ 'status' ]='success';
			$All_Type=[];
			foreach ($CustomerTypes as $key => $value) {
				$Single_Type['id']=$key;
				$Single_Type['name']=$value;
				array_push($All_Type, $Single_Type);
			}
			$return[ 'Types' ]=$All_Type;
		}
	}

	echo json_encode($return);
	exit;
}
// Customer Type lists --ubm
//Customer creation  --ubm
public function api_customer_creation()
{

	$user_id=1;
	$return=[
	'status'=>'Empty',
	];
	$sub_group_id = 3;

	$data=$this->request->data['Customer'];
	if($data)
	{
		$datasource_Customer = $this->Customer->getDataSource();
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_Customer->begin();
			$datasource_AccountHead->begin();
//check
// $opening_balance = $data['opening_balance'];
			$opening_balance = 0;
			$data['division_id'] = 1;
			$division_id = $data['division_id'];

			if ($division_id == '')
				throw new Exception("Empty Division", 1);

			if ($opening_balance == '')
				$opening_balance =0;

			$CustomerRow = $this->Customer->find('first', array(
				//'conditions'=>array('Customer.customer_type_id'=>$data['customer_type_id']),
				'order' => array('Customer.id' => 'DESC') ));
			$code = 1001;
			if(!empty($CustomerRow))
			{
				$code = $CustomerRow['Customer']['code']+1;
			}


			$name = trim($data['name']);
			if (!$name)
				throw new Exception("Empty Customer Name", 1);
			$date = date('Y-m-d');
			$place = $data['place'];
			$email = $data['email'];
			$customer_type_id = $data['customer_type_id'];
			$mobile = $data['mobile'];
			$description = $data['description'];
			$route_id = $data['route_id'];
			$customer_group_id = $data['customer_group_id'];
			$contact_person = $data['contact_person'];
			$credit_limit = $data['credit_limit'];
			$function_return = $this->AccountHeadCreate($sub_group_id, $name, $opening_balance, $date, $description);
			if ($function_return['result'] != 'Success')
				throw new Exception($function_return['message']);
			$AccountHead_id = $this->AccountHead->getLastInsertId();
			$Customer_date = [
			'account_head_id' => $AccountHead_id,
			'customer_type_id' => $customer_type_id,

			'place' => $place,
			'route_id' => $route_id,
			'division_id' => $division_id,
			'customer_group_id' => $customer_group_id,
			'code' => $code,
			'email' => $email,
			'mobile' => $mobile,
			'contact_person' => $contact_person,
			'credit_limit' => $credit_limit,
			'created_by' => $user_id,
			'modified_by' => $user_id,
			'created_at' => date('Y-m-d H:i:s', strtotime($date)),
			'updated_at' => date('Y-m-d H:i:s', strtotime($date)),
			];
			$this->Customer->create();
			if (!$this->Customer->save($Customer_date))
				throw new Exception("Error Customer Creation", 1);
			$return['status'] = 'Success';
			$last_iserted_customer = $this->Customer->findById($this->Customer->getLastInsertId());

// $All_Customer = [];
			$Single_Customer['route_code'] = $this->Route->field(
				'Route.code',
				array('Route.id ' => $last_iserted_customer['Customer']['route_id']));
			$Single_Customer['id']=$last_iserted_customer['AccountHead']['id'];
			$Single_Customer['name']=$last_iserted_customer['AccountHead']['name'];
			$Single_Customer['opening_balance']=$last_iserted_customer['AccountHead']['opening_balance'];
			$Single_Customer['email']=$last_iserted_customer['Customer']['email'];
			$Single_Customer['shope_code']=$last_iserted_customer['Customer']['code'];
			$Single_Customer['credit_limit']=$last_iserted_customer['Customer']['credit_limit'];
			$Single_Customer['address']=$last_iserted_customer['Customer']['place'];
			$Single_Customer['mobile']=$last_iserted_customer['Customer']
			['mobile'];

			$total = 0;
			$Recieved='0';

			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($last_iserted_customer['AccountHead']['id']);
			$total+=$Debit_N_Credit_function['debit'];
			$Recieved+=$Debit_N_Credit_function['credit'];
			$Single_Customer['debit']=$total;
			$Single_Customer['credit']=$Recieved;
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$last_iserted_customer['AccountHead']['id']
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					'Sale.balance',
					'AccountHead.id',
					'AccountHead.name',
					),
				));
			$All_sale=[];

			$Single_Customer['receipt']=$All_sale;
			$return['customer']= $Single_Customer;
			$return['key'] = $last_iserted_customer['AccountHead']['id'];
			$return['value'] = $last_iserted_customer['AccountHead']['name'];
			$datasource_Customer->commit();
			$datasource_AccountHead->commit();
			$return['status']='success';
		} catch (Exception $e) {
			$datasource_Customer->rollback();
			$datasource_AccountHead->rollback();
			$return['status']=$e->getMessage();
		}
	}
	echo json_encode($return);
	exit;
}
//Customer creation 


//Day Close for Executive
public function api_executive_day_close()
{
	$user_id=1;
	$return=[
	'status'=>'Empty',
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];

	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		$date = date('Y-m-d');
		$rowcount = $this->ClosingDay->find('count', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' => $date)));
		if($rowcount > 0 ){
			$return[ 'status' ]= 'Already Closed';
		}
		else
		{
			$datasource_ClosingDay = $this->ClosingDay->getDataSource();
			try {
				$datasource_ClosingDay->begin();
				if(isset($data['closing_stock']))
				{
					$closing_stock=$data['closing_stock'];

				}else{
					$closing_stock=0;
				}

				$tableData = [
				'executive_id' => $executive,
				'date' => $date,
				'closing_stock'=>$closing_stock,
				'status' => 1,

				];
				$this->ClosingDay->create();
				if (!$this->ClosingDay->save($tableData))
					throw new Exception("Error in day closing", 1);

				$datasource_ClosingDay->commit();
				$return['status']='success';
			} catch (Exception $e) {
				$datasource_ClosingDay->rollback();
				$return['status']=$e->getMessage();
			}
		}
	}
	echo json_encode($return);
	exit;
}
//Day Close for Executive

//No sale  --ubm
public function api_no_sale()
{

	$user_id=1;
	$return=[
	'status'=>'Empty',
	];
	$sub_group_id = 3;
	$data=$this->request->data;
	if($data)
	{
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];

			$datasource_NoSale = $this->NoSale->getDataSource();
			try {
				$datasource_NoSale->begin();
				$account_head_id=$data['customer_id'];
				if(empty($data['reason'])){
					$data['reason'] ="other";
				}
				$date = date('Y-m-d');

				$tableData = [
				'executive_id' => $executive,
				'customer_account_head_id' => $account_head_id,
				'date' => $date,
				'description' => $data['reason'],

				];
				$this->NoSale->create();
				if (!$this->NoSale->save($tableData))
					throw new Exception("Error in No Sale", 1);
				$return['status'] = 'Success';

				$datasource_NoSale->commit();
				$return['status']='success';
			} catch (Exception $e) {
				$datasource_NoSale->rollback();
				$return['status']=$e->getMessage();
			}

		}
		else{
			$return[ 'status' ]= 'Executive Requered';
		}

	}
	echo json_encode($return);
	exit;
}
//No sale

//Multiple No sale  --ubm
public function api_multiple_no_sale()
{

	$user_id=1;
	$return=[
	'status'=>'Empty',
	];
	$sub_group_id = 3;
	$data=$this->request->data['Visits'];
	if($data)
	{
		if(isset($this->request->data['executive_id']))
		{
			$executive=$this->request->data['executive_id'];

			$datasource_NoSale = $this->NoSale->getDataSource();
			try {
				for ($j=0; $j <count($data) ; $j++) {
					$datasource_NoSale->begin();
					$account_head_id=$data[$j]['customer_id'];
					if(empty($data[$j]['reason'])){
						$data[$j]['reason'] ="other";
					}
					$date = date('Y-m-d');

					$tableData = [
					'executive_id' => $executive,
					'customer_account_head_id' => $account_head_id,
					'date' => $date,
					'description' => $data[$j]['reason'],

					];
					$this->NoSale->create();
					if (!$this->NoSale->save($tableData))
						throw new Exception("Error in No Sale", 1);
					$return['status'] = 'Success';
					$datasource_NoSale->commit();
				}



				$return['status']='success';
			} catch (Exception $e) {
				$datasource_NoSale->rollback();
				$return['status']=$e->getMessage();
			}

		}
		else{
			$return[ 'status' ]= 'Executive Requered';
		}

	}
	echo json_encode($return);
	exit;
}
//Multiple No sale end

public function GeneralStock_Edit_Function_in_Purchase($id,$date)
{

	$user_id=1;
	try {
		$SaleItem = $this->SaleItem->find('all',array(
			'conditions' => array(
				'SaleItem.sale_id' => $id,
				),
			'fields' => array(
				'SaleItem.warehouse_id',
				'SaleItem.product_id',
				'SaleItem.quantity',
				'SaleItem.unit_price',
				'Sale.invoice_no',
				)
			));

		foreach ($SaleItem as $key => $value) {

			$quantity = $value['SaleItem']['quantity'];
			$unit_price = $value['SaleItem']['unit_price'];
			$product_id=$value['SaleItem']['product_id'];
			$warehouse_id=$value['SaleItem']['warehouse_id'];
			$Stock = $this->Stock->find('first',array(
				'conditions'=>array(
					'product_id'=>$product_id,
					'warehouse_id'=>$warehouse_id,
					)
				));
			$stock_id=$Stock['Stock']['id'];
			$stock_quantity=$Stock['Stock']['quantity'];
			$date=date('Y-m-d',strtotime($date));
			$remark='Sale --'.$value['Sale']['invoice_no'].'('.$quantity.')';
			$stock_quantity-=$quantity;
			$Stock_function_return=$this->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);

			if($Stock_function_return['result']!='Success')
				throw new Exception($Stock_function_return['result']);
		}
		$return['result']='Success';

	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function JournalCreate_in_Sale($invoice_no,$paid,$account_head_id,$date,$cashhead=1,$voucher_no=null)
{
	$user_id=1;
	try {
		$credit=$account_head_id;
		// $debit=1;
		$debit=$cashhead;
		$amount=$paid;
		$date=$date;
		$remarks='Sale Invoice No :'.$invoice_no;
		// $remarks='Direct Payment';
		$work_flow='Sales';
		$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no);
		if($function_return['result']!='Success')
			throw new Exception($function_return['message'], 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no=null)
{
	try {
		if(empty($voucher_no)){
			$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
			if(!empty($Journals))
			{
				$JournalsNo = $Journals['Journal']['voucher_no'];
				$voucher_no=$JournalsNo+1;

			}
			else
			{
				$voucher_no=1;
			}
		}
		if($amount)
		{
			$Journal_data=[
			'remarks'=>$remarks,
			'credit'=>$credit,
			'debit'=>$debit,
			'amount'=>$amount,
			'date'=>date('Y-m-d',strtotime($date)),
			'work_flow'=>$work_flow,
			'voucher_no'=>$voucher_no,
			'flag'=>'1',
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
			];
			$this->Journal->create();
			if(!$this->Journal->save($Journal_data))
			{
				$errors = $this->Journal->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function GeneralStock_Edit_Function($stock_id,$quantity,$date,$user_id,$remark)
{
	try {
		if(!$user_id)
			throw new Exception("Empty user Id", 1);
		if(!$stock_id)
			throw new Exception("Empty Stock Id", 1);
		if(!$date)
			throw new Exception("Empty date", 1);
		if(!$quantity && $quantity!=0)
			throw new Exception("Empty quantity Id", 1);
		if(!$remark)
			throw new Exception("Empty remark", 1);
		$this->Stock->id=$stock_id;
		if(!$this->Stock->saveField('quantity',$quantity ))
			throw new Exception("Error Processing Stock quantity Updation", 1);
		if(!$this->Stock->saveField('modified_by',$user_id ))
			throw new Exception("Error Processing Stock modified_by Updation", 1);
		if(!$this->Stock->saveField('flag','1' ))
			throw new Exception("Error Processing Stock flag Updation", 1);
		if(!$this->Stock->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)) ))
			throw new Exception("Error Processing Stock updated_at Updation", 1);
		$Stock=$this->Stock->read();
		unset($Stock['Stock']['id']);
		unset($Stock['Stock']['flag']);
		unset($Stock['Stock']['modified_by']);
		$Stock['Stock']['remark']=$remark;
		$function_stockLog_return=$this->stock_logs($Stock['Stock']);
		if($function_stockLog_return['result']!='Success')
			throw new Exception($function_stockLog_return['result'], 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}

function sale_target($id,$first_date){

	$SaleTargetList=$this->SaleTarget->find('first',array(
		'conditions'=>array('(SaleTarget.wef_date) <='=>$first_date,'executive_id'=>$id),
		'fields'=>'max(SaleTarget.wef_date) as date',

		));
	$target = $this->SaleTarget->field(

		'SaleTarget.target',

		array('SaleTarget.wef_date' => $SaleTargetList[0]['date'],'executive_id'=>$id));
	return $target;
}

public function General_Journal_Debit_N_Credit_function($account_head_id)
{
	$bank=2;
	$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
	$modes=['1'];
	foreach ($bank_list as $key => $value) {
		array_push($modes, $key);
	}
	$account_single=[];
	$account_single['debit']=0;
	$account_single['credit']=0;
	$Journal_Debit=$this->Journal->find('all',array('conditions'=>array(
		'debit'=>$account_head_id,
		'flag=1',
		)));
	foreach ($Journal_Debit as $key => $value) {
		$account_single['debit']+=$value['Journal']['amount'];
	}
	$Journal_Credit=$this->Journal->find('all',array('conditions'=>array(
		'credit'=>$account_head_id,
		'flag=1',
		)));
	foreach ($Journal_Credit as $key => $value) {
		$account_single['credit']+=$value['Journal']['amount'];
	}
	return $account_single;
}
public function AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description)
{
	try {
		$AccountHead_date=[
		'name'=>strtoupper($name),
		'opening_balance'=>$opening_balance,
		'description'=>$description,
		'sub_group_id'=>$sub_group_id,
		'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
		'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
		];
		$this->AccountHead->create();
		if(!$this->AccountHead->save($AccountHead_date))
		{
			$errors = $this->AccountHead->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function AccountHead_Option_ListBySubGroupId($sub_group_id)
{
	$AccountHeads=$this->AccountHead->find('all',array(
		'conditions'=>array(
			'SubGroup.id'=>$sub_group_id,
			),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'SubGroup.id',
			)
		));
	$AccountHead_list=[];
	foreach ($AccountHeads as $key => $value) {
		$AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
	}
	return $AccountHead_list;
}
public function api_get_customer_info()
{
	$return=[
	'status'=>'Empty',
	'Customers'=>[],
	];
	$data=$this->request->data;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];
		$date = date('Y-m-d');

		$register_row = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));
		if(!empty($register_row)){
			$customer_group_id = $register_row['DayRegister']['customer_group_id'];
			$route_id = $register_row['DayRegister']['route_id'];
		}

	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($customer_group_id))
	{
		$CustomerList=$this->AccountHead->find('all',array(
			"joins"=>array(
				array(
					"table"=>'customers',
					"alias"=>'Customer',
					"type"=>'inner',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),

				),
			'conditions'=>array('Customer.customer_group_id'=>$customer_group_id,
				'Customer.route_id'=>$route_id
				),
			'group' => array('Customer.id'),

			'fields'=>array(
				'Customer.*',
				'AccountHead.id',
				'AccountHead.name',
				'AccountHead.opening_balance',

				),

			));

		if($CustomerList)
		{
			$return[ 'status' ]='success';
			$All_Customer=[];
			foreach ($CustomerList as $key => $value) {
				$Single_Customer['route_code'] = $this->Route->field(
					'Route.code',
					array('Route.id ' => $value['Customer']['route_id']));
				$Single_Customer['id']=$value['AccountHead']['id'];
				$Single_Customer['name']=$value['AccountHead']['name'];
				$Single_Customer['opening_balance']=$value['AccountHead']['opening_balance'];
				$Single_Customer['email']=$value['Customer']['email'];
				$Single_Customer['shope_code']=$value['Customer']['code'];
				$Single_Customer['credit_limit']=$value['Customer']['credit_limit'];
				$Single_Customer['address']=$value['Customer']['place'];
				$Single_Customer['mobile']=$value['Customer']
				['mobile'];

				$total = $value['AccountHead']['opening_balance'];
				$Recieved='0';
				$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
				$total+=$Debit_N_Credit_function['debit'];
				$Recieved+=$Debit_N_Credit_function['credit'];
				$Single_Customer['debit']=$total;
				$Single_Customer['credit']=$Recieved;
				$Sale=$this->Sale->find('all',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$value['AccountHead']['id'],
						'Sale.status'=>2
						),
					'fields'=>array(
						'Sale.id',
						'Sale.invoice_no',
						'Sale.grand_total',
						'Sale.balance',
						'AccountHead.id',
						'AccountHead.name',
						),
					));
				$All_sale=[];
				foreach ($Sale as $key => $row) {
					$Single_sale['sale_id']=$row['Sale']['id'];
					$Single_sale['invoice_no']=$row['Sale']['invoice_no'];
					$Single_sale['total']=$row['Sale']['grand_total'];
					$Single_sale['balance']=$row['Sale']['balance'];
					array_push($All_sale, $Single_sale);
				}
				$Single_Customer['receipt']=$All_sale;
				array_push($All_Customer, $Single_Customer);
			}
			$return[ 'Customers' ]=$All_Customer;
		}
	}
	else{
		$return[ 'status' ]= 'Group is not registerd';
	}
	echo json_encode($return);
	exit;
}
public function api_get_customer_info_by_search()
{

	$conditions=[];
	$return=[
	'status'=>'Empty',
	'Customers'=>[],
	];
	$data=$this->request->data;
	if(isset($data['route_id']))
	{

		$conditions['Customer.route_id']=$data['route_id'];


	}
	if(isset($data['customer_group_id']))
	{

		$conditions['Customer.customer_group_id']=$data['customer_group_id'];


	}

	{
		$CustomerList=$this->AccountHead->find('all',array(
			"joins"=>array(
				array(
					"table"=>'customers',
					"alias"=>'Customer',
					"type"=>'inner',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),

				),
			'conditions'=>$conditions,
			'group' => array('Customer.id'),

			'fields'=>array(
				'Customer.*',
				'AccountHead.id',
				'AccountHead.name',
				'AccountHead.opening_balance',

				),

			));

		if($CustomerList)
		{
			$return[ 'status' ]='success';
			$All_Customer=[];
			foreach ($CustomerList as $key => $value) {
				$Single_Customer['route_code'] = $this->Route->field(
					'Route.code',
					array('Route.id ' => $value['Customer']['route_id']));
				$Single_Customer['id']=$value['AccountHead']['id'];
				$Single_Customer['name']=$value['AccountHead']['name'];
				$Single_Customer['opening_balance']=$value['AccountHead']['opening_balance'];
				$Single_Customer['email']=$value['Customer']['email'];
				$Single_Customer['shope_code']=$value['Customer']['code'];
				$Single_Customer['credit_limit']=$value['Customer']['credit_limit'];
				$Single_Customer['address']=$value['Customer']['place'];
				$Single_Customer['mobile']=$value['Customer']
				['mobile'];

				$total = $value['AccountHead']['opening_balance'];
				$Recieved='0';

				$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
				$total+=$Debit_N_Credit_function['debit'];
				$Recieved+=$Debit_N_Credit_function['credit'];
				$Single_Customer['debit']=$total;
				$Single_Customer['credit']=$Recieved;
				$Sale=$this->Sale->find('all',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$value['AccountHead']['id']
						),
					'fields'=>array(
						'Sale.id',
						'Sale.account_head_id',
						'Sale.invoice_no',
						'Sale.grand_total',
						'Sale.balance',
						'AccountHead.id',
						'AccountHead.name',
						),
					));
				$All_sale=[];
				foreach ($Sale as $key => $row) {

					$Single_sale['sale_id']=$row['Sale']['id'];
					$Single_sale['invoice_no']=$row['Sale']['invoice_no'];

					$Single_sale['total']=$row['Sale']['grand_total'];

					$Journal=$this->Journal->find('all',array('conditions'=>array(
						'credit'=>$row['Sale']['account_head_id'],
						'flag=1',
						'Journal.remarks '=>'Sale Invoice No :'.$row['Sale']['invoice_no'],
						)));
					$account_single=[];
					$credit = 0;

					foreach ($Journal as $key => $value) {
						$credit+=$value['Journal']['amount'];

					}
					$balance = 	$row['Sale']['grand_total'] - $credit;			
					$Single_sale['balance']= $balance;
					if($balance >0){
						array_push($All_sale, $Single_sale);
					}


				}
				$Single_Customer['receipt']=$All_sale;

//
				array_push($All_Customer, $Single_Customer);
			}
			$return[ 'Customers' ]=$All_Customer;
		}
	}
	echo json_encode($return);
	exit;
}
public function stock_logs($stock_logs_array)
{
	try {
		$this->StockLog->create();
		if(!$this->StockLog->save($stock_logs_array))
			throw new Exception("Cant Create Stock Log Data", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function api_get_product_type()
{
	$return=[
	'status'=>'Empty',
	'ProductTypes'=>[],
	];
	$ProductType = $this->ProductType->find('list',array(
		"joins"=>array(
			array(
				"table"=>'products',
				"alias"=>'Product',
				"type"=>'inner',
				"conditions"=>array('ProductType.id=Product.product_type_id'),
				),
			),
		'group'=>array('ProductType.id'),
		'fields'=>array('id','name'),'order'=>array('name ASC'),));
	if($ProductType)
	{
		$return[ 'status' ]='success';
		$All_Type=[];
		foreach ($ProductType as $key => $value) {
			$Single_Type['id']=$key;
			$Single_Type['name']=$value;
			$ProductTypeBrandMapping = $this->ProductTypeBrandMapping->find('all', array(
				'conditions' => array('ProductTypeBrandMapping.product_type_id' => $key),
				'order'=>array('Brand.name ASC'),
				'fields' => array(
					'Brand.id',
					'Brand.name',
					)
				)
			);
			$All_Brand=[];
			$singl_Brand['id'] = 0;
			$singl_Brand['name'] = 'GENERAL';
			array_push($All_Brand, $singl_Brand);
			if(!empty($ProductTypeBrandMapping)){
				foreach ($ProductTypeBrandMapping as $keys => $row) {
					$singl_Brand['id'] = $row['Brand']['id'];
					$singl_Brand['name'] = $row['Brand']['name'];
					array_push($All_Brand, $singl_Brand);
				}

			}
			$Single_Type['Brands']=$All_Brand;
			array_push($All_Type, $Single_Type);
		}
		$return[ 'ProductTypes' ]=$All_Type;
	}


	echo json_encode($return);
	exit;
}
//search stock  by product type id and brand id
public function api_get_van_stock_search()
{

	$return=[
	'status'=>'Empty',
	'Stocks'=>[],
	'total_stock'=>'0',
	'total_quantity'=>'0',
	];
	$data=$this->request->data;
	$data['executive_id']=1;
	if(isset($data['executive_id']))
	{
		$executive=$data['executive_id'];
		$warehouse_id = $this->Executive->field(
			'Executive.warehouse_id',
			array('Executive.id ' => $executive));

		$conditions['Stock.flag'] = 1;
		$conditions['Stock.warehouse_id'] = $warehouse_id;
	}
	else{
		$return[ 'status' ]= 'Executive Requered';
	}
	if(isset($executive))
	{
		if(isset($data['product_type_id'])){
			$conditions['Product.product_type_id'] = $data['product_type_id'];
		}
		if(isset($data['brand_id'])){
			$conditions['Product.brand_id'] = $data['brand_id'];
		}

		$Stock=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'stocks',
					"alias"=>'Stock',
					"type"=>'inner',
					"conditions"=>array('Product.id=Stock.product_id'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
					),
				),
			'conditions'=>$conditions,
			'limit'=>500,
			'fields'=>array(
				'Product.*',
				'Unit.name',
				'ProductType.name',
				'Warehouse.name',
				'Brand.name',
				'Stock.*',
				)
			));
		$All_Stock=[];
		$total_cost = 0;
		$total_quantity = 0;
		foreach($Stock as $value){
			if($value['Stock']['quantity']>0){
				$Single_stock['product_id']=$value['Product']['id'];
				$Single_stock['product_name']=$value['Product']['name'];
				$Single_stock['product_code']=$value['Product']['code'];
				$Single_stock['product_barcode']=$value['Product']['barcode'];
				
				$Single_stock['product_type_name']=$value['ProductType']['name'];
				$Single_stock['brand_name']=$value['Brand']['name'];
				$Single_stock['product_cost']=$value['Product']['cost'];
				$Single_stock['product_mrp']=$value['Product']['mrp'];
				$Single_stock['product_wholesale_price']=$value['Product']['wholesale_price'];
				$Single_stock['product_vat']=$value['Product']['tax'];
				$Single_stock['piece_per_cart']=$value['Product']['no_of_piece_per_unit'];
				$Single_stock['stock_quantity']=$value['Stock']['quantity'];
				$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
				$total_quantity+=$value['Stock']['quantity'];
				array_push($All_Stock, $Single_stock);
			}
		}
		if($Stock)
		{
			$return[ 'status' ]='success';
			$return[ 'Stocks' ]=$All_Stock;
			$return[ 'total_stock' ]=$total_cost;
			$return[ 'total_quantity' ]=$total_quantity;



		}
		else{
			$return[ 'status' ]='No Stock';

		}
	}
pr($return);exit;
	echo json_encode($return);
	exit;
}
//search stock  by product type id and brand id

//stock transfer cost
public function stock_transfer_cost($id)
{
	$total_stock= 0;
	$StockTransferitems=$this->StockTransferItem->find('all',array(
		"joins"=>array(
			array(
				"table"=>'products',
				"alias"=>'Product',
				"type"=>'inner',
				"conditions"=>array('Product.id=StockTransferItem.product_id'),
				),

			),
		"conditions"=>array('stock_transfer_id'=>$id),
		"fields"=>array(
			'StockTransferItem.quantity',
			'Product.mrp',
			),
		));

	$total_cost = 0;
	$total_quantity = 0;
	foreach($StockTransferitems as $value){
		if($value['StockTransferItem']['quantity']>0){

			$total_cost+=$value['Product']['mrp']*$value['StockTransferItem']['quantity'];
			$total_quantity+=$value['StockTransferItem']['quantity'];
		}
	}
	$total_stock=$total_cost;

	return $total_stock;
}

// current van stock value
public function current_van_stock($executive)
{
	$total_stock= 0;

	$warehouse_id = $this->Executive->field(
		'Executive.warehouse_id',
		array('Executive.id ' => $executive));

	if(isset($executive))
	{
		$Stock=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'stocks',
					"alias"=>'Stock',
					"type"=>'inner',
					"conditions"=>array('Product.id=Stock.product_id'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
					),
				),
			'conditions'=>array('Stock.flag'=>1,'Stock.warehouse_id'=>$warehouse_id),
			'fields'=>array(
				'Product.mrp',
				'Stock.quantity',
				)
			));
		$All_Stock=[];
		$total_cost = 0;
		$total_quantity = 0;
		foreach($Stock as $value){
			if($value['Stock']['quantity']>0){

				$total_cost+=$value['Product']['mrp']*$value['Stock']['quantity'];
				$total_quantity+=$value['Stock']['quantity'];
			}
		}
		if($Stock)
		{

			$total_stock=$total_cost;


		}
	}

	return $total_stock;
}

}