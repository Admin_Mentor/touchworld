<section class="content-header">
	<h1> Bonus Configuration</h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?php echo $this->Form->create('BonusConfiguration', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'BonusConfiguration_Form']); ?>
			<div class="col-md-12">
				<div class="form-group">
						<div class="col-md-1 col-lg-1 col-sm-1">
							<?php echo $this->Form->input('wef_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'WEF Date')); ?>
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<?php echo $this->Form->input('executive_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'type','style'=>'width: 100%;','id'=>'executive_id','empty'=>[''=>'Select'],'required','label'=>'Executive')); ?>
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<?= $this->Form->input('bonus_sales',array('class'=>'form-control number','type'=>'text','required','id'=>'bonus_sales','label'=>'Bonus Amount Sales %')); ?>
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<?= $this->Form->input('bonus_collection',array('class'=>'form-control number','type'=>'text','required','id'=>'bonus_collection','label'=>'Bonus Amount Collection %')); ?>
						</div>
					    <div class="col-md-2 col-lg-2 col-sm-2">
							<?= $this->Form->input('eligibility_sale_target',array('class'=>'form-control number','type'=>'text','required','id'=>'eligibility_sale_target','label'=>'Bonus Eligibility Sale Target %')); ?>
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2">
							<?= $this->Form->input('eligibility_collection_target',array('class'=>'form-control number','type'=>'text','required','id'=>'eligibility_collection_target','label'=>'Bonus Eligibility Colection Target%'));?>
						</div>
						<div class="col-md-1 col-sm-1 col-sm-1"> <br>
							<button style="margin-top: 5%;" class="save pull-right" id='save_button' >Save</button>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body">
			<table class="boder table table-condensed" id="table_data">
				<thead>
					<tr class="blue-bg">
						<th class="padding_left">WEF Date</th>
						<th style="width: 26%">Executive</th>
						<th>Bonus Amount Sale %</th>
						<th>Bonus Amount Collection %</th>
						<th>Bonus Elgblty: Sale Target %</th>
						<th>Bonus Elgblty: Collection Target %</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('input').click(function(){
		$(this).select();
	});
</script>
 <script type="text/javascript">
	$('#table_data').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/get_bonus_configuration_ajax",
			"type": "POST",
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "BonusConfiguration.wef_date" },
		{ "data" : "Executive.name" },
		{ "data" : "BonusConfiguration.bonus_sales" },
		{ "data" : "BonusConfiguration.bonus_collection" },
	    { "data" : "BonusConfiguration.eligibility_sale_target" },
		{ "data" : "BonusConfiguration.eligibility_collection_target" },
		{ "data" : "BonusConfiguration.action" },
		],
		"columnDefs": [
		],
	});
</script>