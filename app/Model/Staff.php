<?php
App::uses('AppModel', 'Model');
class Staff extends AppModel {

	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'name required',
			),
		),
		'code' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'code required',
			),
		),
		'contact_no' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'contact_no required',
			),
		),
		'is_executive' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'is_executive required',
			),
		),
	);
	public $belongsTo = array(
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AccountHead' => array(
			'className' => 'AccountHead',
			'foreignKey' => 'account_head_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

