<?php
App::uses('AppModel', 'Model');
/**
 * AccountHead Model
 *
 * @property SubGroup $SubGroup
 */
class AccountHead extends AppModel {
/**
 * Validation rules
 *
 * @var array
 */
public $validate = array(
	'name' => array(
		'notBlank' => array(
			'rule' => array('notBlank'),
			'message' => 'Name is Required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'isUnique' => array(
			'rule' => array('isUnique'),
			'message' => 'Name Already Registered',
			),
		),
	'opening_balance' => array(
		'numeric' => array(
			'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	'sub_group_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'notBlank' => array(
			'rule' => array('notBlank'),
			'message' => 'sub_group_id is Required',
			),
		),
	);
	// The Associations below have been created with all possible keys, those that are not needed can be removed
/**
 * belongsTo associations
 *
 * @var array
 */
public $belongsTo = array(
	'SubGroup' => array(
		'className' => 'SubGroup',
		'foreignKey' => 'sub_group_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		),
	);
}
