$(document).on('change','#customer_type',function(){
  var customer_type_id=$(this).val();
  $.post( "<?= $this->webroot ?>Customer/get_customer_by_customer_type_ajax/"+customer_type_id ,function( data ) {
    $('#account_head').html('');
    $.each(data.options,function(key,value){
      $('#account_head').append($("<option></option>").attr("value",key).text(value));
    })
  }, "json");
});
$(document).on('click','#ledger_pdf',function(){
  var account_head=$('#account_head').val();
    url='<?= $this->webroot."Accountings/ledger_print_ajax/"; ?>'+account_head;
    window.open(url);
  
});