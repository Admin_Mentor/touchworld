<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Customer Profit</h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-header">
      <div class="col-md-12">
        <div class="col-md-2">
          <?= $this->Form->input('customer_type',array('type'=>'select','options'=>$CustomerType,'class'=>'form-control select_two_class','label'=>'Customer Type','empty'=>'All','id'=>'customer_type_id')) ?>
        </div>
        <div class="col-md-2">
          <?= $this->Form->input('from_date',array('type'=>'text','id'=>'from_date','class'=>'form-control date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
        </div>
        <div class="col-md-2">
          <?= $this->Form->input('to_date',array('type'=>'text','id'=>'to_date','class'=>'form-control date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
        </div>
        <div class="col-md-2"><br>
          <button class='btn' type='button' id='fetch_button'>Fetch</button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <table class="table table-condensed" id='table_data' data-page-length='-1'>
        <thead>
          <tr class="blue-bg">
            <th>Customer Type</th>
            <th>Customer</th>
            <th class="dt-head-right">Sales Amount</th>
            <th class="dt-head-right">Sales Cost</th>
            <th class="dt-head-right">Return Amount</th>
            <th class="dt-head-right">Return Cost</th> 
            <th class="dt-head-right">Profit</th> 
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
          <tr>
            <td style="color: #dd4b39" colspan="2"><h4>Total</h4></td>
            <td align="right" style="color: #dd4b39"></td>
            <td align="right" style="color: #dd4b39"></td>
            <td align="right" style="color: #dd4b39"></td>
            <td align="right" style="color: #dd4b39"></td>
            <td align="right" style="color: #dd4b39"></td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#table_data').DataTable({
    "processing": false,
    "serverSide": true,
    "searching": false,
    "paging": false,
    // "info": false,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/customer_performance_ajax_new",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
        d.customer_type_id= $('#customer_type_id').val();
      },
      "dataSrc": "records",
    },
     dom: 'Bfrtip',
    // lengthMenu: [
    // [25, 50,100,-1],
    // ['25 rows', '50 rows','100 rows','Show all' ]
    // ],
    buttons: [
    //{ extend: 'colvis', },
      {
          extend: 'print',
          // text: 'Print' ,
          // title: 'Executive Brand Wise Sale Report',
           footer: true,
       //exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    // '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
                    // '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
                    // '<h5 align="center">Thiruvangoor</h5',
                    // '<h5 align="center">Calicut-673 304</h5>',
                    // '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
           '<h3 align="center">Customer Profit</h3>',
            //'<h5>CustomerType :'+$('#customer_type_id').val()+'</h5>',
                    '<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              }
            },
            {
              extend: 'csv',
              // text: 'CSV' ,
            title:'Customer Profit'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

               footer: true,
               customize: function (csv) {
                 return "\tCustomer Profit\n"+"\t(Period : From "+$("#from_date").val()+"    To : "+$("#to_date").val()+")\n"+  csv ;
               },
      // exportOptions: { columns: ':visible'},
            },
            {
              extend: 'excel',
              // text: 'Excel' ,
              // title:'Executive Brand Wise Sale Report',
               title:'Customer Profit'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

               footer: true,
      // exportOptions: { columns: ':visible'},
            },
       //      {
       //        extend: 'pdf',
       //       // text: 'Pdf' ,
       //        //title:'Executive Brand Wise Sale Report',
       //        title:'Customer Profit'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

       //         footer: true,
       // //exportOptions: { columns: ':visible'},
       //      },
    // { extend: 'excel', title: 'Advance Order Report', exportOptions: { columns: ':visible' } }, 
    // { extend: 'pdf',   title: 'Advance Order Report', exportOptions: { columns: ':visible' } }, 
    // { extend: 'csv',   title: 'Advance Order Report', exportOptions: { columns: ':visible' } },
    //'pageLength',
    ],
    "columns": [
    { "data" : "CustomerType" },
    { "data" : "AccountHead" },
    { "data" : "sale_cost" },
    { "data" : "sale_product_cost" },
    { "data" : "sale_return_cost" },
    { "data" : "return_product_cost" },
    { "data" : "profit" },
    ],
    rowCallback: function (row, data) {
      $(row).hide();
      $.fn.show_alert('Success');
    },
    "drawCallback": function (settings) {
      var api = this.api();
      var rows = api.rows({ page: 'current' }).nodes();
      var last = null;
      var groupadmin = [];
      api.column(0, { page: 'current' }).data().each(function (group, i) {
        if (last !== group) {
          $(rows).eq(i).before(
            '<tr style="cursor:pointer" class="group " id="' + i + '">\
            <td colspan="7"><b style="color:#dd4b39">'+group+'</b></td>\
            </tr>'
            );
          groupadmin.push(i);
          last = group;
        }
      });
      for (var k = 0; k < groupadmin.length; k++) {
        $("#" + groupadmin[k]).nextUntil("#" + groupadmin[k + 1]).addClass(' group_' + groupadmin[k]);
        $("#" + groupadmin[k]).click(function () {
          var gid = $(this).attr("id");
          $(".group_" + gid).slideToggle(100);
        });
      }
    },
    rowGroup: {
      startRender: function ( rows, group ) {
        var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
        var sale_cost           = rows.data().pluck('sale_cost' ).reduce(function(a,b){return intVal(a)+intVal(b);},0);
        var sale_product_cost   = rows.data().pluck('sale_product_cost' ).reduce(function(a,b){return intVal(a)+intVal(b);},0);
        var sale_return_cost    = rows.data().pluck('sale_return_cost').reduce(function(a,b){return intVal(a)+intVal(b);},0);
        var return_product_cost = rows.data().pluck('return_product_cost').reduce(function(a,b){return intVal(a)+intVal(b);},0);
        var profit              = rows.data().pluck('profit').reduce(function(a,b){return intVal(a)+intVal(b);},0);
        return $('<tr/>')
        .append( '<td class="details-control" colspan="2"></td>' )
        .append( '<td class="dt-body-right"><span style="color:#dd4b39">'+Math.round(sale_cost)+'</span></td>' )         
        .append( '<td class="dt-body-right"><span style="color:#dd4b39">'+Math.round(sale_product_cost)+'</span></td>' )         
        .append( '<td class="dt-body-right"><span style="color:#dd4b39">'+Math.round(sale_return_cost)+'</span></td>' )         
        .append( '<td class="dt-body-right"><span style="color:#dd4b39">'+Math.round(return_product_cost)+'</span></td>' )         
        .append( '<td class="dt-body-right"><span style="color:#dd4b39">'+Math.round(profit)+'</span></td>' )         
        .append('<tr></tr>') 
      },
      dataSrc: "CustomerType"
    }, 
    "footerCallback":function(row,data,start,end,display){ 
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      pageTotal=api.column(2,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(2).footer()).html('<h4>'+Math.round(pageTotal)+'</h4>');
      pageTotal=api.column(3,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(3).footer()).html('<h4>'+Math.round(pageTotal)+'</h4>');
      pageTotal=api.column(4,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(4).footer()).html('<h4>'+Math.round(pageTotal)+'</h4>');
      pageTotal=api.column(5,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(5).footer()).html('<h4>'+Math.round(pageTotal)+'</h4>');
      pageTotal=api.column(6,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(6).footer()).html('<h4>'+Math.round(pageTotal)+'</h4>');
    },
    "columnDefs": [
    {"targets": [ 0 ],"orderable": false ,                          },
    {"targets": [ 1 ],"orderable": false ,                          },
    {"targets": [ 2 ],"orderable": false ,className: "dt-body-right"},
    {"targets": [ 3 ],"orderable": false ,className: "dt-body-right"},
    {"targets": [ 4 ],"orderable": false ,className: "dt-body-right"},
    {"targets": [ 5 ],"orderable": false ,className: "dt-body-right"},
    {"targets": [ 6 ],"orderable": false ,className: "dt-body-right"},
    ],
   // "dom": 'Bfrtip',

  });
$('#fetch_button').click(function(){
  table = $('#table_data').dataTable();
  table.fnDraw();
});
$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }

</script>