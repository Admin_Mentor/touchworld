<section class="content-header">
	<h1> Basic Pay Structure </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="row-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<?php echo $this->Form->create('BasicPay', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'BasicPay_Form']); ?>
								<div class="box-body">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'WEF Date')); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('staff_id',array('type'=>'select','class'=>'form-control  select2','id'=>'staff_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>$Staff,'required','label'=>'Staff')); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?= $this->Form->input('amount',array('class'=>'form-control','type'=>'number','step'=>'any','required','id'=>'amount',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
													</div>
												</div>
											</div>
											
										</div>
									</div>
									<div class="col-md-12">
										<div class="row">
											
											<div class="col-md-6 col-lg-3 col-sm-3 col-xs-12">
												
											</div>
											
											<div class="col-md-6 col-sm-6 col-sm-12"> 
												<button style="margin-top: 5%;" class="save pull-right" id='save_button' >Save</button>
          <button style="margin-top: 5%; display:none" id='edit_button' value='' class="save pull-right">Edit</button>
											</div>
										</div>
									</div>
								</div>
								<?= $this->Form->end(); ?>
							</div>
							<div class="row-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="box-body table-responsive no-padding">
											<table class="boder table table-condensed table datatable boder" id="myTable">
												<thead>
													<tr class="blue-bg">
														<th class="padding_left">Date</th>
														<th>Staff</th>
														<th>Amount</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<?php $total=0; foreach ($BasicPays as $key => $value): ?>
													<tr class="blue-pddng view_transaction">
														<td><?= date('d-m-Y',strtotime($value['BasicPay']['wef_date'])); ?></td>
														<td class='name'><?= $value['Staff']['name']; ?></td>
														<td><?= $value['BasicPay']['amount'] ?></td>
														<td>
														<a class="edit_basicpay" data-id="<?= $value['BasicPay']['id']?>" href="#"><i class="fa fa-edit " aria-hidden="true"  style="color: #3c8dbc;"></i></a>
														<a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Hr/DeleteBasicPay/<?= $value['BasicPay']['id']; ?>"><i class="fa fa-trash blue-col blue-col"></i></a>
														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
											
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>
</section>

<script type="text/javascript">
	$(document).on('click','.edit_basicpay',function(){
    var pay_id = $(this).data('id');
    $.post( "<?= $this->webroot ?>Hr/get_basicpay_details_ajax/"+pay_id,function( data ) {
    	var dateAr = data.BasicPay.wef_date.split('-');
    	var date = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
    	$('#staff_id').val(data.BasicPay.staff_id).trigger('change');
      $('#amount').val(data.BasicPay.amount);
      $('#remarks').val(data.BasicPay.remarks);
      $('#date').val(date);
      
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#amount").append("<input type='text' id='pay_id' name='data[BasicPay][id]' value='"+data.BasicPay.id+"'>");
      $('#BasicPay_Form').attr('action','<?= $this->webroot; ?>Hr/EditBasicPay');
    }, "json");
  });
</script>
<script type="text/javascript">
	
</script>