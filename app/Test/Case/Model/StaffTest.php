<?php
App::uses('Staff', 'Model');

/**
 * Staff Test Case
 */
class StaffTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.staff',
		'app.prepaid_account_head',
		'app.outstanding_account_head',
		'app.paid_account_head',
		'app.account_head',
		'app.sub_group',
		'app.group',
		'app.master_group',
		'app.type',
		'app.area_manager',
		'app.basic_pay',
		'app.director',
		'app.executive',
		'app.warehouse',
		'app.stock_log',
		'app.product',
		'app.product_type',
		'app.product_type_brand_mapping',
		'app.brand',
		'app.sub_brand',
		'app.category',
		'app.colour',
		'app.unit',
		'app.purchase_return_item',
		'app.purchase_return',
		'app.purchased_item',
		'app.purchase',
		'app.grn_order',
		'app.grn_order_item',
		'app.sale_item',
		'app.sale',
		'app.sales_return_item',
		'app.sales_return',
		'app.stock',
		'app.unwanted_list',
		'app.warehouse_rol',
		'app.leave_management',
		'app.pay_structure',
		'app.technician'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Staff = ClassRegistry::init('Staff');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Staff);

		parent::tearDown();
	}

}
