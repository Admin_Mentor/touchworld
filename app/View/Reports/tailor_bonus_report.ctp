<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<!-- <style type="text/css">
.box{
  width: 88%;
}
}
</style> -->
<section class="content-header">
  <h1>Tailor Bonus Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box" width="84%">
      <div class="box-header" >
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-3">
              <?php echo $this->Form->input('tailor_id',array('type'=>'select','id'=>'tailor_id','style'=>'width:100%','class'=>'form-control select_two_class search_field','empty'=>'ALL',)); ?>
            </div>
            <div class="col-md-2">
              <div class="col-md-12">
                <?php echo $this->Form->input('from_date',array('type'=>'text','id'=>'from_date','class'=>'form-control search_field date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="col-md-2">
              <div class="col-md-12">
                <?php echo $this->Form->input('to_date',array('type'=>'text','id'=>'to_date','class'=>'form-control  search_field date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="col-md-1"><br>
              <button class='btn' type='button' id='fetch_button'>Fetch</button>
            </div>
            <div class="col-md-4 pd-lt-0" hidden> <br>
              <label>Export</label> <span class="print-span"><a id="exportExcel" target="_blank"><i class="fa fa-download fa fa-2x" aria-hidden="true" style="padding-left: 10px;"></i></a></span>
            </div>
          </div>
        </div>
      </div>
      <div class="box-body" style="overflow:auto">
        <table class="table table-condensed" id='tailor_bonus_table' data-page-length="10" data-order='[[ 2, "asc" ]]' border="1" >
          <thead>
            <tr class="blue-bg">
              <th>Tailor</th>
              <th>Production No</th>
              <th>Date</th>
              <th>Product</th>
              <th>Production Cost</th>
              <th>Quantity</th>
              <th>Bonus Amount</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="4" style="font-size:20px; color:red;text-align:right">Total:</th>
              <th style="font-size:20px; color:red;">0</th>
              <th style="font-size:20px; color:red;">0</th>
              <th style="font-size:20px; color:red;">0</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#tailor_bonus_table').dataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/tailor_bonus_report_ajax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
        d.tailor_id= $('#tailor_id').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "Staff.name" },
    { "data" : "Production.production_no" },
    { "data" : "ExecutiveBonusDetails.receipt_date" },
    { "data" : "Product.name" },
    { "data" : "Production.production_cost" },
    { "data" : "Production.quantity" },
    { "data" : "ExecutiveBonusDetails.bonus_amount" },
    ],
    "dom": 'Bfrtip',
    "buttons": [
    { extend: 'colvis' },
    {
      extend: 'print',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {
      extend: 'csv',
      title:'Tailor Bonus Report of '+$('#tailor_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {
      extend: 'excel',
      title:'Tailor Bonus Report of '+$('#tailor_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {
      extend: 'pdf',
      title:'Tailor Bonus Report of '+$('#tailor_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {extend: 'pageLength'},
    ],
    "columnDefs": [
    //{ className: "text-right", "targets": [ 9 ] },
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      var pagecount=4;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(3))+'');
      pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(3))+'');
      pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(3))+'');
    },
  });
  $('#fetch_button').on('click',function(){
    table = $('#tailor_bonus_table').dataTable();
    table.fnDraw();
  });
</script>