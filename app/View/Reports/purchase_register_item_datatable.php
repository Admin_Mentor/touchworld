<script type="text/javascript">
	$.fn.product_type_change=function(product_type_id){
		var data={product_type_id:product_type_id};
		var url_address= '<?php echo $this->webroot; ?>'+'Reports/product_type_select_ajax';
		$.ajax({
			type: "post",  
			url:url_address,
			data: data, 
			dataType:'json',
			success: function(response) {
				$('#product_id').html(response.row);
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}
	$(document).on('change','#product_type_id',function(){
		var product_type_id=$('#product_type_id').val();
		$.fn.product_type_change(product_type_id);
	});
	$('#table_product_wise_list').DataTable({
		"processing": false,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Reports/purchase_item_wise_ajax",
			"type": "POST",
			 data:function( d ) {
        d.from= $('#from_date').val();
        d.product_type_id= $('#product_type_id').val();
        d.to= $('#to_date').val();
        d.product_id= $('#product_id').val();
      },
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "Product.name" },
		{ "data" : "Purchase.invoice_no" },
		{ "data" : "Purchase.delivered_date" },
		{ "data" : "Purchase.unit_price"},
		{ "data" : "PurchasedItem.quantity" },
		{ "data" : "PurchaseReturnItem.return_qty" },
		{ "data" : "PurchasedItem.total" },
		],
		rowCallback: function (row, data) {
      $.fn.show_alert('Success');
    },
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api();
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};    
			var total_mrp = api
			.column( 6)
			.data()
			.reduce( function (a, b) {
				return intVal(a) + intVal(b);
			} ,0);
			var total_mrp=total_mrp.toFixed(2);

			$( api.column( 6 ).footer() ).html(
				total_mrp
				);
		},

		"fnDrawCallback": function( oSettings ) {
		}
	});
	$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
	$(document).on('click','#fetch_data',function(){
    table = $('#table_product_wise_list').dataTable();
    table.fnDraw();
  });
	// $(document).on('click','#fetch_data',function(){
	// 	var product_type_id=$('#product_type_id option:selected').val();
	// 	var product_id=$('#product_id option:selected').val();
	// 	var from=$('#from_date').val();
	// 	var to=$('#to_date').val();
	// 	var data={
	// 		product_type_id:product_type_id,
	// 		product_id:product_id,
	// 		from:from,
	// 		to:to
	// 	};
	// 	var url_address= '<?php echo $this->webroot; ?>'+'Reports/PurchaseItemWiseFilter';
	// 	$.ajax({
	// 		type: "post",
	// 		url:url_address,
	// 		data: data,
	// 		dataType:'json',
	// 		success: function(response) {
	// 			$.fn.product_filter_function_datatable();
	// 		},
	// 		error:function (XMLHttpRequest, textStatus, errorThrown) {
	// 			alert(textStatus);
	// 		}
	// 	});
	// });
</script>