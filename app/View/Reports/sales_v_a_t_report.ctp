<section class="content-header">
<h1>Sales VAT Report</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('SalesVATReport'); ?>
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                 <?= $this->Form->input('from_date',array(
                    'type'=>'text',
                    'id'=>'from_date',
                    'value'=>$final,
                    'class'=>'form-control cls_label_all date_field search_field date_picker datepicker',
// 'label'=>false,
                    'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                    )); ?>
               </div>
             </div>
           </div>
         </div>
         <div class="col-md-2">
          <div class="form-horizontal">
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <?= $this->Form->input('to_date',array(
                      'type'=>'text',
                      'id'=>'to_date',
                      'value'=>$date1,
                      'class'=>'form-control cls_label_all date_field search_field date_picker datepicker',
// 'label'=>false,
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
              </div>
            </div>
          </div>
        </div>
         <div class="col-md-2">
            <div class="col-md-12 col-lg-12 col-sm-12">
              <br>
              <input type="submit"  class="print" id="getdata" value="Get"></a>
            </div>
         </div>
          <div class="col-md-2">
              <div class="col-md-12 col-lg-12 col-sm-12">
                <br>
                <input type="button"  class="save" name="button" id="exportExcel" value="Export"></a>
              </div> 
          </div>
          <div class="col-md-2">
              <div class="col-md-12 col-lg-12 col-sm-12">
                <br>
                <button type="button" id="report_print" class="print" >Print</button>
              </div> 
          </div>
      </div>
      <div class="col-md-1 col-sm-1 pull-right mar-tp-5">
        <br>
         <?php echo $this->Form->input('flag',array('type'=>'hidden','class'=>'form-control pull-right ','id'=>'flag','data-mask'=>'data-mask',)); ?>
        
      </div>
    </div>
  </div>
  <?= $this->Form->end(); ?>
  <div class="row">
    <div class="col-md-12">
      <div class="box-body table-responsive no-padding">
        <table class="table datatable table-hover boder text-center table-bordered" id='GSTReport_table' data-order='[[ 0, "asc" ]]'>
          <thead>
            <tr class="blue-bg">
              <th>Date</th>
              <th>Inv.No</th>
              <!-- <th>Customer</th> -->
             <!--  <th>Invoice Amount</th> -->
              <th>Taxable Value</th>
              <th>Gross Total</th>
              <th>VAT SALES 5%</th>
              <th>VAT Amount</th>
              <!-- <th>SGST</th> -->
              <!-- <th>IGST</th> -->
              <!-- <th>GST in No</th> -->
            </tr>
          </thead>
          <tbody>
            <?php foreach ($SalesGSTReport['list'] as $key => $value) :?>
              <tr>
                <td><?= $value['date']; ?></td>
                <td><?= $value['invoice_no']; ?></td>
                <!-- <td><?= $value['customer_name']; ?></td> -->
                <!-- <td><?= $value['invoice_amount']; ?></td> -->
                <td><?= $value['taxable_value']; ?></td>
                <td><?= $value['gross_total']; ?></td>
                <td><?= $value['vat_5_total']; ?></td>
               <!--  <td><?= $value['18_total']; ?></td>
                <td><?= $value['28_total']; ?></td> -->
                <td><?= $value['vat_amount'];?></td>
                <!-- <td><?= $value['sgst']; ?></td> -->
                <!-- <td><?= $value['igst']; ?></td> -->
               <!--  <td><?= $value['customer_gstin']; ?></td>
 -->
              </tr>
            <?php endforeach; ?>
          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $('#report_print').click(function(){
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    // var customer=$('#customer_id').val();
    // var customer_type=$('#customer_type_id').val();
    // var flag=$('#flag').val();
    // if(customer == ''){
    //   customer=0;
    // }
    // if(customer_type == ''){
    //   customer_type=0;
    // }
    url='<?= $this->webroot."Reports/sales_offline_vat_print/"; ?>'+from_date+'/'+to_date;
    window.open(url);

  });
 
  // $('.Search').change(function(){
  //   $.fn.Search();
  // });
  $.fn.Search=function(){
    //var customer_type_id=$('#customer_type_id').val();
    //var customer_id=$('#customer_id').val();
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var data={
      //customer_type_id:customer_type_id,
      //customer_id:customer_id,
      from_date:from_date,
      to_date:to_date,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/SalesGSTReport_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $('#GSTReport_table').DataTable().destroy();
        $('#GSTReport_table tbody').html(response.row);
        
        $('#GSTReport_table').DataTable();
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
  $('#exportExcel').click(function()   { 
      var date_from= $('#from_date').val();
      var date_to=$('#to_date').val();
      if(!date_from)
      {
        alert("Take From Date");
        return false;
      }
      if(!date_to)
      {
        alert("Take To Date");
        return false;
      }
      var website_url3='<?php echo $this->webroot; ?>Reports/export_sales_vat_report/'+date_from+'/'+date_to;
      $(location).attr("href", website_url3);
    });
  $('.main-sidebar').ready(function(){
    $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
  });
</script>