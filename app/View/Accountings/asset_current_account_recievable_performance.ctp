<section class="content-header">
    <h1> Debters Create </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary box_tp_brdr">
                <div class="row-wrapper">
                    <div class="row">
                        <?php echo $this->Form->create('AccountHead', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'AccountHead_Form']); ?>	
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('executive_id',array('class'=>'form-control select2','type'=>'select','empty'=>'select','options'=>$Executive_list,'id'=>'executive_view_id',)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('route_id',array('class'=>'form-control select2 routes','type'=>'select','empty'=>'select','options'=>$Route_list,'id'=>'route_view_id',)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('group',array('class'=>'form-control select2 customergroup','type'=>'select','empty'=>'select','options'=>$CustomerGroup_list,'id'=>'group_view',)); ?>
                                            </div>
                                            <div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#group_add_modal"></i></div>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('customer_type',array('class'=>'form-control select2','type'=>'select','empty'=>'select','required','options'=>$CustomerType_list,'id'=>'customer_type',)); ?>
                                            </div>
                                            <br>
                                            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-6"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_type_modal"></i></a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('name',array('class'=>'form-control select2 name','type'=>'select','required','empty'=>'select','options'=>$Customer_list,'id'=>'name','label'=>'Customer')); ?>
                                            </div>
                                            <br>
                                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_modal_add"></i></a></div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <?= $this->Form->end(); ?>
                        </div>
                        <br>
                        <div class="row-wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-condensed table table datatable boder boder" id="AccountHead_table">
                                            <thead>
                                                <tr class="blue-bg">
                                                    <!-- <th>Executive </th> -->
                                                    <th>Route</th>
                                                    <th>Group</th>
                                                    <th>Customer Type</th>
                                                    <th>Customer</th>
                                                    <th>Code</th>
                                                    <th>Opening Balance</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($Customer as $key => $value): ?>
                                                    <tr class="blue-pddng">
                                                        <!--   <td><?= $value['Executive']['name']; ?></td> -->
                                                        <td><?= $value['Route']['name']; ?></td>
                                                        <td><?= $value['CustomerGroup']['name']; ?></td>
                                                        <td><?= $value['CustomerType']['name']; ?></td>
                                                        <td><span style="display:none" class='AccountHead_id'><?= $value['AccountHead']['id']; ?></span><span><?= $value['AccountHead']['name']; ?></span></td>
                                                        <td><?= $value['Customer']['code']; ?></td>
                                                        <td><?= $value['AccountHead']['opening_balance']; ?></td>
                                                        <td><i class="fa fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#Account_modal"></i></td>
                                                        <td><a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Accountings/AccountHead_delete/<?= $value['AccountHead']['id']; ?>"><i class="fa fa-trash blue-col blue-col"></i></a></td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="customer_type_modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Customer Type</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">Customer Type</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" id="customer_type_name" type="text">
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id='add_customer_type'>Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require('customer_modal.php') ?> 
    <?php require('customer_add_modal.php') ?> 
    <?php require('state_modal.php') ?>
    <?php require('route_modal.php') ?>
    <?php require('customer_group_modal.php') ?>
    <?php require('Division_Add_modal.php') ?>
</section>
<script type="text/javascript">
    <?php require('division.js') ?>
    $('.opening_balance').on('keyup change', function () {
        $('#opening_balance').val($(this).val());
        $('#opening_balance_modal').val($(this).val());
    });
    $(document).on('change', '#customer_type,#modal_customer_type', function () {
        $('#modal_customer_type').val($(this).val()).trigger('change.select2');
        $('#customer_type').val($(this).val()).trigger('change.select2');
    });
</script>
<!-- for edit  -->
<script type="text/javascript">
    $(document).on('click', '.edit_head', function () {
        var id = $(this).closest('tr').find('td span.AccountHead_id').text();
        $.post("<?= $this->webroot ?>Accountings/Customer_get_ajax/" + id, function (responds) {
            console.log(responds);
            if (responds.result != 'Success')
            {
                alert(responds.message);
                return false;
            }

            $('#name_edit').val(responds.data.AccountHead.name);
            $('#opening_balance_edit').val(responds.data.AccountHead.opening_balance).trigger('change');
            $('#customer_type_edit').val(responds.data.CustomerType.id).trigger('change');
// $('#state_id_edit').val(responds.data.Customer.state_id).trigger('change');
// $('#executive_id_edit').val(responds.data.Customer.executive_id).trigger('change');
$('#route_id_edit').val(responds.data.Customer.route_id).trigger('change');
$('#customer_group_id_edit').val(responds.data.Customer.customer_group_id).trigger('change');
// $('#day_edit').val(responds.data.Customer.day).trigger('change');
$('#code_edit').val(responds.data.Customer.code);
$('#email_edit').val(responds.data.Customer.email);
$('#mobile_edit').val(responds.data.Customer.mobile);
// $('#gstin_edit').val(responds.data.Customer.gstin);
$('#place_edit').val(responds.data.Customer.place);
$('#description_edit').val(responds.data.AccountHead.description);
// $('#state_code_edit').val(responds.data.Customer.state_code);
$('#customer_id_hide').val(responds.data.Customer.id);
$('#AccountHead_id').val(responds.data.AccountHead.id);


$('#contact_person_edit').val(responds.data.Customer.contact_person);
$('#credit_limit_edit').val(responds.data.Customer.credit_limit);
$('#division_edit').val(responds.data.Customer.division_id);
$('#vat_no').val(responds.data.Customer.vat_no);



}, "json");
    });
    $(document).on('click', '#edit_customer_button', function () {
        var name_edit = $('#name_edit').val();
        var Customer_id = $('#customer_id_hide').val();
        var AccountHead_id = $('#AccountHead_id').val();
        var opening_balance_edit = $('#opening_balance_edit').val();
        var customer_type_edit = $('#customer_type_edit').val();
// var state_id_edit=$('#state_id_edit').val();
var executive_id_edit = $('#executive_id_edit').val();
var route_id_edit = $('#route_id_edit').val();
var customer_group_id_edit = $('#customer_group_id_edit').val();
// var day_edit=$('#day_edit').val();
// var code_edit=$('#code_edit').val();
var email_edit = $('#email_edit').val();
var mobile_edit = $('#mobile_edit').val();
// var gstin_edit=$('#gstin_edit').val();
var place_edit = $('#place_edit').val();
var description_edit = $('#description_edit').val();
var contact_person_edit = $('#contact_person_edit').val();
var credit_limit_edit = $('#credit_limit_edit').val();
var division_edit = $('#division_edit').val();
var vat_no = $('#vat_no').val();

// var state_code_edit=$('#state_code_edit').val();

// $('#AccountHead_Form').attr('action', '<?= $this->webroot; ?>Accountings/Customer_account_edit');

var website_url = '<?php echo $this->webroot; ?>Accountings/Customer_account_edit';
var data = {
    name_edit: name_edit,
    Customer_id: Customer_id,
    AccountHead_id: AccountHead_id,
    opening_balance_edit: opening_balance_edit,
    customer_type_edit: customer_type_edit,
// state_id_edit:state_id_edit,
executive_id_edit: executive_id_edit,
route_id_edit: route_id_edit,
customer_group_id_edit: customer_group_id_edit,
// day_edit:day_edit,
// code_edit:code_edit,
email_edit: email_edit,
mobile_edit: mobile_edit,
// gstin_edit:gstin_edit,
place_edit: place_edit,
description_edit: description_edit,
contact_person_edit: contact_person_edit,
credit_limit_edit: credit_limit_edit,
division_edit: division_edit,
vat_no : vat_no
};
$.ajax({
    method: "POST",
    url: website_url,
    data: data,
//dataType:'json',
}).done(function (data) {
// location.reload();
});
});
</script>
<!-- For delete -->
<script type="text/javascript">
    $(document).on('click', '.delete', function () {
        if (!confirm("Are you sure?"))
        {
            return false;
        }
        var id = $(this).closest('tr').find('td span.AccountHead_id').text();
        var rowindex = $(this).closest('tr').index();
        $.post("<?= $this->webroot ?>Accountings/customer_delete/" + id, function (data) {
            if (data.result != 'Success')
            {
                alert(data.message);
                return false;
            }
            $('#AccountHead_table tbody tr:eq(' + rowindex + ')').remove();
        }, "json");
    });
</script>
<script type="text/javascript">
    $('#add_customer_type').click(function () {
        var name = $('#customer_type_name').val();
        if (name == '')
        {
            $('#customer_type_name').focus();
            return false;
        }
        var website_url = '<?php echo $this->webroot; ?>CustomerType/add_customer_type_ajax';
        var data = {
            name: name,
        };
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
            dataType: 'json',
        }).done(function (data) {
            if (data.result != 'Success')
            {
                alert(data.result);
                return false;
            }
            $('#customer_type_modal').modal('toggle');
            $('#customer_type_name').val('');
            $('#customer_type').append($("<option></option>").attr("value", data.key).text(data.value));
            $('#modal_customer_type').append($("<option></option>").attr("value", data.key).text(data.value));
            $('#customer_type').val(data.key).trigger('change');
            $('#modal_customer_type').val(data.key).trigger('change');
        });
    });
</script>
<script type="text/javascript">
    $('#executive_view_id').change(function () {
        var executive_id = $(this).val();
        $.fn.executive_change(executive_id);
        $.fn.table_search();
    });
    $.fn.executive_change = function (executive_id) {
        var data = {executive_id: executive_id};
        var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/executive_select_ajax';
        $.ajax({
type: "post", // Request method: post, get
url: url_address,
data: data, // post data
success: function (response) {
    $('#route_view_id').html(response);
},
error: function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
}
});
    }
    $('#route_view_id').change(function () {
        var route_id = $(this).val();
        $.fn.route_change(route_id);
        $.fn.table_search();
    });
    $.fn.route_change = function (route_id) {
        var data = {route_id: route_id};
        var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/route_select_ajax';
        $.ajax({
            type: "post",
            url: url_address,
            data: data,
            success: function (response) {
//  $('#day_view').html(response);
$('#group_view').html(response);
},
error: function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
}
});
    }
// $('#day_view').change(function () {
//     var day = $('#day_view option:selected').text();
//     var route_id = $('#route_view_id option:selected').val();
//     $.fn.day_change(day, route_id);
//     $.fn.table_search();
// });
//
$('#group_view').change(function () {
    var group = $('#group_view option:selected').val();
    var route_id = $('#route_view_id option:selected').val();
    $.fn.group_change(group, route_id);
    $.fn.table_search();
});
$.fn.group_change = function (group, route_id) {
    var data = {group: group, route_id: route_id};
    var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/group_select_ajax';
    $.ajax({
        type: "post",
        url: url_address,
        data: data,
        success: function (response) {
            $('#customer_type').html(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}
//
$.fn.day_change = function (day, route_id) {
    var data = {day: day, route_id: route_id};
    var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/day_select_ajax';
    $.ajax({
        type: "post",
        url: url_address,
        data: data,
        success: function (response) {
            $('#customer_type').html(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}
$('#customer_type').change(function () {
    var customer_type = $(this).val();
    var executive_id = $('#executive_view_id option:selected').val();
    var route_id = $('#route_view_id option:selected').val();
    var day = $('#day_view option:selected').text();
    $.fn.customer_type_change(customer_type, executive_id, route_id, day);
    $.fn.table_search();
});
$.fn.customer_type_change = function (customer_type, executive_id, route_id, day) {
    var data = {customer_type: customer_type, executive_id: executive_id, route_id: route_id,
// day: day
};
var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/customer_type_select_ajax';
$.ajax({
    type: "post",
    url: url_address,
    data: data,
    success: function (response) {
        $('#name').html(response);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
    }
});
}
$('#name').change(function () {
    $.fn.table_search();
});
$.fn.table_search = function () {
    var executive_id = $('#executive_view_id option:selected').val();
    var route_id = $('#route_view_id option:selected').val();
    var group = $('#group_view option:selected').val();
    var day = $('#day_view option:selected').text();
    var customer_type = $('#customer_type option:selected').val();
    var name = $('#name option:selected').text();
    var data = {
        executive_id: executive_id,
        route_id: route_id,
        day: day,
        group:group,
        customer_type: customer_type,
        name: name,
    };
    var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/customer_search_ajax';
    $.ajax({
        type: "post",
        url: url_address,
        data: data,
//dataType:'json',
success: function (response) {
    console.log(response);
    $('#AccountHead_table').DataTable().destroy();
    $('#AccountHead_table tbody').html(response.row);
    $('#AccountHead_table').DataTable();
},
error: function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
}
});
}
</script>
<script type="text/javascript">
    <?php require('state.js'); ?>
    <?php require('customer.js'); ?>
</script>