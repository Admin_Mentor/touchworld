<section class="content-header">
	<h1>Bank Transaction 
		<a href="<?= $this->webroot ?>Accountings/AssetCurrentBank"><input type="button" class="btn btn-success save pull-right" value="Create Bank"></input></a>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Cash_Form']); ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
						<?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'account_head','style'=>'width: 100%;','options'=>$AccountHeads,'empty'=>'ALL','required','label'=>'Bank Name',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body table-responsive no-padding">
			<table class="boder table table-condensed table datatable" id="transaction_detailsss_table">
				<thead>
					<tr class="blue-bg">
						<th width="10%">Date</th>
						<th>Credit Account</th>
						<th>Debit Account</th>
						<th>Remark</th>

						<th>Credit</th>
						<th>Debit</th>

						<th>Closing Balance</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $credit=0; $debit=0; $closing_balanc=0; 
					foreach ($All_Accounts as $key => $value): ?>
					<tr class="blue-pddng view_transaction">
						<td><span style="display:none" class='journal_id'><?= $value['id']; ?></span><span><?= date('d-m-Y',strtotime($value['date'])); ?></span></td>
						<td class='name'><?= $value['Credit_name']; ?></td>
						<td><?= $value['Debit_name']; ?></td>
						<td><?= $value['remarks']; ?></td>
						<td class="text-right"><?= floatval($value['credit']);  $credit+=$value['credit']; ?></td>
						<td class="text-right"><?= floatval($value['debit']); $debit+=$value['debit']; ?></td>
						<td class="text-right"><?= floatval($value['credit']-$value['debit']); $closing_balanc+=($value['credit']-$value['debit']); ?></td>
						<td><i class="fa fa-pencil-square-o blue-col edit"></i></td>
						<td><a href="#"><i class="fa fa-trash blue-col delete"></i></a></td>
					</tr>
				<?php endforeach ?>
			</tbody>
			<tfoot>
				<tr class="blue-pddng">
					<td></td>
					<td></td>
					<td></td>
					<td class="total_amount text-right">Total</td>
					<td class="total_amount text-right"><?= $credit; ?></td>
					<td class="total_amount text-right"><?= $debit; ?></td>
					<td class="total_amount text-right"><?= $closing_balanc; ?></td>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
	<?php require('asset_current_bank_transaction.js'); ?>
</script>