<div id="group_add_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Group</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('Group', ['class'=>'form-horizontal','id'=>'Group_Form']); ?>
				<div class="form-group">
					<label class="col-sm-4 control-label">Name</label>
					<div class="col-sm-6">
						<?= $this->Form->input('name',array('class'=>'form-control','type'=>'text','required','id'=>'group_name_modal','label'=>false,)); ?>
					</div>
				</div>
				
				<?= $this->Form->end(); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" id='add_group_button'>Save</button>
			</div>
		</div>
	</div>
</div>