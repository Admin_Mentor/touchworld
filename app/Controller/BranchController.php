<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
/**
* Executive Controller
*/
class BranchController extends AppController {
public $uses=[
'Type',
'MasterGroup',
'Group',
'SubGroup',
'AccountHead',
'Executive',
'Location',
'Route',
'Customer',
'Warehouse',
'Branch',
'BranchRouteMapping',
];
public $components = array('RequestHandler');
public function AddBranch()
{
	$BranchAll=$this->Branch->find('all');
    $this->set(compact('BranchAll'));
$Warehouse=$this->Warehouse->find('list');
$this->set(compact('Warehouse'));
$Location=$this->Route->find('list',
array("joins"=>array(
// array(
// 	"table"=>'executive_route_mappings',
// 	"alias"=>'ExecutiveRouteMapping',
// 	"type"=>'left',
// 	"conditions"=>array('ExecutiveRouteMapping.route_id=Route.id'),
// 	),
),
));

$this->set(compact('Location'));
if($this->request->data)
{
$data=$this->request->data['Branch'];
$datasource_Branch = $this->Branch->getDataSource();
$datasource_BranchRouteMapping = $this->BranchRouteMapping->getDataSource();

try {

$name=strtoupper(trim($data['name']));
$Branch=$this->Branch->find('first',array(
'conditions'=>array('Branch.name'=>$name)
));
if(empty($Branch))
{
$branch_data=[
'name'=>trim(strtoupper($data['name'])),
'warehouse_id'=>$data['warehouse_id'],
'mobile'=>$data['mobile'],
'created_at'=>date('Y-m-d H:i:s'),
'updated_at'=>date('Y-m-d H:i:s'),
];
$datasource_Branch->begin();
$this->Branch->create();
if(!$this->Branch->save($branch_data))
{
$errors = $this->Branch->validationErrors;
foreach ($errors as $key => $value) {
throw new Exception($value[0], 1);
}
$branch_id=$this->Branch->getLastInsertId();
// $userdata=[
//             'user_role_id'=>1,
//             'branch_id'=>$branch_id,
//             'mobile'=>$data['mobile'],
//              'flag'=>1,
//             'password'=>$data['name'],
//             'name'=>$data['name'],

//           ];
// $this->User->create();
//     if(!$this->User->save($user_data))
//     {
//       $errors = $this->User->validationErrors;
//       foreach ($errors as $key => $value) {
//         throw new Exception($value[0], 1);
//       }
//     }
}
$datasource_Branch->commit();
$branch_id=$this->Branch->getLastInsertId();
foreach ($data['location_id'] as $key => $value) {
$route_id=$this->BranchRouteMapping->find('first',array('conditions'=>array('BranchRouteMapping.route_id'=>$value)));
if(empty($route_id))
{
$mapping_data=[
'branch_id'=>$branch_id,
'route_id'=>$value,
];
$datasource_BranchRouteMapping->begin();
$this->BranchRouteMapping->create();
if(!$this->BranchRouteMapping->save($mapping_data))
{
$errors = $this->BranchRouteMapping->validationErrors;
foreach ($errors as $key => $value) {
throw new Exception($value[0], 1);
}
}
$datasource_BranchRouteMapping->commit();
}
}
$return['result']='success';
$this->Session->setFlash(__($return['result']));
}
else
{
$return['result']='Branch Already Inserted';
$this->Session->setFlash(__($return['result']));
}
} catch (Exception $e) {
$return['result']=$e->getMessage();
$datasource_Branch->rollback();
// $datasource_Staff->rollback();
$datasource_BranchRouteMapping->rollback();
}
$this->redirect( Router::url( $this->referer(), true ) );
}
}
public function get_branch_details_ajax($id)
	{
		$return=[
			'status'=>'Empty',
			'Branch'=>[],
			'BranchRoute'=>[],
		];
		if(isset($id))
		{
			$Branch=$this->Branch->findById($id);
			$return['Branch']=$Branch['Branch'];
			$BranchRoute=$this->BranchRouteMapping->find('list',array(
				'conditions'=>array('BranchRouteMapping.branch_id'=>$id,),

				'fields'=>'BranchRouteMapping.route_id'
			));
			$return['BranchRoute']=$BranchRoute;

		}
		echo json_encode($return);
		exit;
	}
	public function EditBranch()
{
	if($this->request->data)
	{
		$data=$this->request->data['Branch'];
		$datasource_Branch = $this->Branch->getDataSource();
		$datasource_BranchRouteMapping = $this->BranchRouteMapping->getDataSource();
		try {

			$branch_data=[
'name'=>trim(strtoupper($data['name'])),
'warehouse_id'=>$data['warehouse_id'],
'mobile'=>$data['mobile'],
'updated_at'=>date('Y-m-d H:i:s'),
];
          $datasource_Branch->begin();
 			$this->Branch->id=$data['id'];
			if(!$this->Branch->save($branch_data))
			{
				$errors = $this->Branch->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
				$datasource_Branch->commit();
			$branch_id = $data['id']; 
			$routelist = $this->BranchRouteMapping->find('list', array(
				'conditions' => array('BranchRouteMapping.branch_id' => $branch_id,),
				 'fields' => array('BranchRouteMapping.id', 'BranchRouteMapping.route_id')
				 ));
			// pr($routelist);exit;
			foreach ($data['location_id'] as $key => $value) {
				$route_id=$this->BranchRouteMapping->find('first',array('conditions'=>array('BranchRouteMapping.route_id'=>$value)));
         if(empty($route_id))
            {
				$rowcount = $this->BranchRouteMapping->find('count', array('conditions' => array('BranchRouteMapping.branch_id' => $branch_id,'BranchRouteMapping.route_id' => $value)));
					if($rowcount <1)
					{
					$mapping_data=[
		'branch_id'=>$branch_id,
		'route_id'=>$value,
		];
		$datasource_BranchRouteMapping->begin();
			$this->BranchRouteMapping->create();
			if(!$this->BranchRouteMapping->save($mapping_data))
			{
				$errors = $this->BranchRouteMapping->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$datasource_BranchRouteMapping->commit();
		}
		}
	    }
		
			$return['result']='success';
					$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
			$datasource_BranchRouteMapping->rollback();

			$datasource_Branch->rollback();
		}

		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function get_branch_routes($id)
{
	$conditions=array();
	// $conditions['Stock.flag']=1;
	
	
		$conditions['BranchRouteMapping.branch_id']=$id;
	$branch_id_List = $this->BranchRouteMapping->find('all',array('fields'=>'BranchRouteMapping.branch_id,Route.name,BranchRouteMapping.id',
		'conditions'=>$conditions));
	
	
	$data['row']='';
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$i=1;
	foreach ($branch_id_List as $key => $value) {
		$data['row']= $data['row'].'<tr class="blue-pd">';
		$data['row']= $data['row'].'<td>'.$i.'</td>';
		$data['row']= $data['row'].'<td>'.$value["Route"]["name"].'</td>';
		$data['row']= $data['row'].'<td><a data-id="'.$value["BranchRouteMapping"]["id"].'" class="del-route"><i class="fa fa-trash blue-col blue-col"></i></a></td>';
	
		$data['row']= $data['row'].'</tr>';
	$data['branch_id'] = $value['BranchRouteMapping']['branch_id'];
	$i++;
	
}
	
	echo json_encode($data);
	exit;
}
public function DeleteBranchRoute($id)
{
	try {
	
		
		if(!$this->BranchRouteMapping->delete($id))
			throw new Exception("Cant Delete This Route", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	$this->Session->setFlash(__($return['result']));
	echo json_encode($return);
	exit;
	
}
}