<?php
App::uses('AppModel', 'Model');
/**
 * PurchasedItem Model
 *
 * @property Product $Product
 * @property Purchase $Purchase
 */
class PurchaseExpense extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Expense' => array(
			'className' => 'Expense',
			'foreignKey' => 'expense_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Purchase' => array(
			'className' => 'Purchase',
			'foreignKey' => 'purchase_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
