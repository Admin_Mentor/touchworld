$('#prdct_type').keyup(function(){
  var prdct_type=$(this).val();
  var data={
    prdct_type:prdct_type
  };
  var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_type_search';
  $.ajax({
    type: "post",
    url:url_address,
    data: data,
    success: function(response) {
      if(response=="Yes")
      {
        $('#prdct_type_error').html('This Product Type is already taken');
        $('#add_prdct_type').attr('disabled',true);
      }
      else
      {
        $('#prdct_type_error').html('');
      }
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});
$('.product_type_disable').keyup(function(){
  var product_type_id=$('#prdct_type').val().trim();
  if(product_type_id!="")
  {
    $('#add_prdct_type').attr('disabled',false);
  }
  else{
    $('#add_prdct_type').attr('disabled',true);
  }
});
$.fn.product_type_append=function(result){
  $('#product_type_id').append(result);
  var latest_value = $("#product_type_id option:last").val();
  $('#product_type_id').val(latest_value).change();
  $('#product_type_id_modal').append(result);
  $('#product_type_id_modal').val(latest_value).change();
}
$('#product_type_id').change(function(){
  var id=$(this).val();
  $('#product_type_id_modal').val(id).trigger('change.select2');;
});
$('#product_type_id_modal').change(function(){
  var id=$(this).val();
  $('#product_type_id').val(id);
});
$('#add_Product_Type').click(function(){
  var data=$('#ProductType_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>ProductType/product_type_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $('#product_type_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#product_type_id').val($('#product_type_id option:last-child').val()).trigger('change');
      $('#product_type_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#product_type_id_modal').val($('#product_type_id_modal option:last-child').val()).trigger('change.select2');
      $('.product_type_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('.product_type_id_modal').val($('#product_type_id_modal option:last-child').val()).trigger('change.select2');
      $('#ProductType_Add_Form')[0].reset();
      $('#Product_Type_Add_Modal').modal('toggle');
    }
  }, "json");
});
$('#Product_Type_Delete_Btn').click(function(){
  var product_type_id=$('#product_type_id').val();
  if(!product_type_id)
  {
    $('#product_type_id').select2('open');
    return false; 
  }
  if(!confirm("Are you sure?"))
  {
    return false;
  }
  var url_address= "<?= $this->webroot; ?>ProductType/product_type_delete_ajax/"+product_type_id;
  $.get( url_address, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $("#product_type_id option[value='" + product_type_id+ "']").remove();
    }
  }, "json");
});
$('#Product_Type_Edit_Btn').click(function(){
  var product_type_id=$('#product_type_id').val();
  if(!product_type_id)
  {
    $('#product_type_id').select2('open');
    return false; 
  }
  var url_address= "<?= $this->webroot; ?>ProductType/product_type_get_ajax/"+product_type_id;
  $.get( url_address, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $('#ProductTypeEditName').val(response.data.name);
      $('#ProductTypeEditId').val(response.data.id);
      $('#ProductTypeEditCode').val(response.data.code);
      $('#Product_Type_Edit_Modal').modal('show');
    }
  }, "json");
});
$('#edit_Product_Type').click(function(){
  var data=$('#ProductType_Edit_Form').serialize();
  var product_type_id=$('#product_type_id').val();
  var url_address= "<?= $this->webroot; ?>ProductType/product_type_edit_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $("#product_type_id option[value='" + product_type_id+ "']").remove();
      $("#product_type_id_modal option[value='" + product_type_id+ "']").remove();
      $('#product_type_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#product_type_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#product_type_id').val($('#product_type_id option:last-child').val()).trigger('change');
      $('#ProductType_Edit_Form')[0].reset();
      $('#Product_Type_Edit_Modal').modal('toggle');
    }
  }, "json");
});