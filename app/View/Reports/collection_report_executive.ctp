<style type="text/css">

.cls_label_all {
  padding-top: 5%;
}
.row_top_row{

  margin-top: 5%;
}
.deaf_btn_btn {
  margin-left: 4%;
}

.row_new_add{
  margin-top: 2%;
}
#radio_butto_add {
  margin-top: 7px;
  margin-left: -51%;
}
.check_bx_rght{
  white-space: nowrap;
  padding-top: 20px;
}
.chkbx_icn_clk {
    position: absolute;
    margin-left: -16px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Collection Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="row" style="margin-top: 2%;">
                  <div class="col-md-3">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('branch_id',array(
                      'type'=>'select',
                      'id'=>'branch_id',
                      'style'=>'width:100%',
                      'options'=>$branch_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'MAIN',
                      )); ?>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('route_id',array(
                      'type'=>'select',
                      'id'=>'route_id',
                      'style'=>'width:100%',
                      'options'=>$route_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'ALL',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('executive_id',array(
                      'type'=>'select',
                      'id'=>'executive_id',
                      'style'=>'width:100%',
                      'options'=>$executive_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'ALL',
                      )); ?>
                    </div>
                  </div>
                 
                  <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('from_date',array(
                      'type'=>'text',
                      'id'=>'from_date',
                      'class'=>'form-control search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                      <?php echo $this->Form->input('to_date',array(
                        'type'=>'text',
                        'id'=>'to_date',
                        'class'=>'form-control  search_field date_picker datepicker',
                        'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                        )); ?>
                      </div>
                    </div>
                    
                    <div class="col-md-2">
                      <div class="col-md-12">
                        <br>
                        <button class='btn' type='button' id='collection_report_button'>Fetch</button>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="col-md-12">
                        <div class="checkbox">
                          <label class="label_chk_bx check_bx_rght"><input type="checkbox" name="data[Report]['check']" id="checkbox" value="" class="chkbx_icn_clk">Show Zero Balance Customers</label>
                        </div>
                       <!--  <button class='btn' type='button' id='collection_report_button'>Fetch</button> -->
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="box-body">
                    <div class="col-md-4 col-xs-12">
                      <h3 class="muted "></h3>
                    </div>
                    <table class="table table-condensed table boder no-footer" id='table_collection_list'>
                      <thead>
                        <tr class="blue-bg">
                          <!-- <th>Executive</th> -->
                          <!-- <th>Circle</th> -->
                          <th>Date</th>
                          <th>Customer</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $('#table_collection_list').DataTable();
      $('#collection_report_button').on('click',function(){
        $.fn.table_search();
      });
      $('#checkbox').click(function(){
        $.fn.table_search();
      });
      $.fn.table_search=function(){   
        var from_date=$('#from_date').val();
        var to_date=$('#to_date').val();
       // var branch_id=$('#branch_id').val();
        var route_id=$('#route_id').val();
        var executive_id=$('#executive_id').val();
        
        if($('#checkbox').is(':checked')){
          var check_id=0;
        }

        else

        {
          var check_id=1;
        }
        var data={
          from_date:from_date,
          to_date:to_date,
         // branch_id:branch_id,
          route_id:route_id,
          executive_id:executive_id,
          check_id:check_id,
        };

        var url_address= '<?php echo $this->webroot; ?>'+'Reports/collection_report_executive_ajax';
        $.ajax({
          type: "post",
          url:url_address,
          data: data,
          dataType:'json',
          success: function(response) {
            $('#table_collection_list').DataTable().destroy();
            $('#table_collection_list tbody').html(response.row);
            $('#table_collection_list tfoot').html(response.tfoot);
            $('#table_collection_list').DataTable({
         "ordering": false,
         dom: 'Bfrtip',
         buttons: [
          {
               extend: 'colvis',
             },
         {
          extend: 'print',
         // text: 'Print' ,
          //title: 'Sales Register Summary',
          footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
             .prepend(
            //   '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
            //   '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
            //   '<h5 align="center">Thiruvangoor</h5',
            //   '<h5 align="center">Calicut-673 304</h5>',
            //   '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
              '<h3 align="center">Collection Report</h3>',
               '<h5>Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+'</h5>'
               );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
            .prepend(
              '<tr><td colspan="8"></td></tr>'
              );
          }
        },
        {
          //extend: 'csv',
          //title:'Collection Report'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
          //text: 'CSV' ,
         // footer: true,
         extend: 'csvHtml5',
            title:'Collection Report'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

               footer: true,
               customize: function (csv) {
                 return "\t  Collection Report\n"+"\t(Period : From "+$("#from_date").val()+"    To : "+$("#to_date").val()+")\n"+  csv ;
               },
       exportOptions: { columns: ':visible'},
        },
        {
          extend: 'excel',
          //text: 'Excel' ,
          //title:'Sales Register Summary',
          title:'Collection Report'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
          footer: true,
       exportOptions: { columns: ':visible'},
        },
        {
          extend: 'pdf',
          //text: 'Pdf' ,
          //title:'Sales Register Summary',
          title:'Collection Report'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
          footer: true,
       exportOptions: { columns: ':visible'},
        },
         {
               extend: 'pageLength',
             },
        ]
      } );
            $.fn.show_alert('Success');
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      }
// $.fn.table_search();

$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>
