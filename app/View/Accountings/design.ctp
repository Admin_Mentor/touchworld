
<style type="text/css">

  .tp_frm_mrg {
    margin-top: 20px;
  }

  .mdl_in_ser_icn {
    padding-left: 6px;
  }

</style>

<section class="content-header">
  <h1> Sale </h1>
</section>
<section class="content">
  <div class="row">
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary box_tp_brdr">
            <div class="row-wrapper">
              <div class="row">
                <div class="col-md-12">


                  <div class="box-body">



                    <div class="row">                   
                      <div class="col-md-12">
                        <div class="col-md-8 col-lg-8 col-sm-8">
                          <div class="row"><form class="form-horizontal">
                            <div class="box-body">
                              <div class="form-group">
                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                                  <label for="inputEmail3" class="control-label">Customer</label>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                 <select class="form-control">
                                  <option>option 1</option>
                                  <option>option 2</option>
                                  <option>option 3</option>
                                  <option>option 4</option>
                                  <option>option 5</option>
                                </select>
                              </div>
                              <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><i class="fa fa-plus-circle fa-2x ad-mar modal_color" data-toggle="modal" data-target="#myModal48"></i></div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                                <label for="inputEmail3" class="control-label">Customer Type</label>
                              </div>
                              <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                               <select class="form-control">
                                <option>option 1</option>
                                <option>option 2</option>
                                <option>option 3</option>
                                <option>option 4</option>
                                <option>option 5</option>
                              </select>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><i class="fa fa-plus-circle fa-2x ad-mar modal_color" data-toggle="modal" data-target="#myModal48"></i></div>
                          </div>
                        </div>
                      </form></div>
                      <div class="row">
                        <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12"><button class="btn btn-success nav-toggle tabs-anchor btn_detls_btm" href="#collapse1">Show/Hide Customer<i class="fa fa fa-expand crcl_dwn" aria-hidden="true"></i></button></div>                         
                      </div>
                      <div id="collapse1" style="display: none;">

                        <div class="row">
                         <form class="form-horizontal">
                          <div class="box-body">
                            <div class="form-group">
                              <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                                <label for="inputEmail3" class="control-label">Bill Type</label>
                              </div>
                              <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                               <select class="form-control">
                                <option>option 1</option>
                                <option>option 2</option>
                                <option>option 3</option>
                                <option>option 4</option>
                                <option>option 5</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                    <div class="row">
                     <form class="form-horizontal">
                      <div class="box-body">
                        <div class="form-group">
                          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                            <label for="inputEmail3" class="control-label" style="white-space: nowrap;">Customer Address</label>
                          </div>
                          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="row">
                   <form class="form-horizontal">
                    <div class="box-body">
                      <div class="form-group">
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                          <label for="inputEmail3" class="control-label">Executive</label>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                         <select class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                      <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><i class="fa fa-plus-circle fa-2x ad-mar modal_color" data-toggle="modal" data-target="#add_executive"></i></div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>   

          <div class="col-md-4 col-lg-4 col-sm-4">
            <div class="row">
              <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12"><label for="inputEmail3" class="control-label">Customer Category</label></div>
              <div class="col-md-3 col-lg-3 col-sm-6"><label for="inputEmail3" class="control-label">A</label></div>
            </div>
            <div class="row tp_frm_mrg">
              <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12"><label for="inputEmail3" class="control-label">Credit Limit</label></div>
              <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12">
                <div class="form-group"><input type="text" class="form-control" id="inputEmail3"></div></div>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-6 col-lg-4 col-xs-12"><label for="inputEmail3" class="control-label">Balance</label></div>
                <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12">
                  <div class="form-group"><input type="text" class="form-control" id="inputEmail3"></div></div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 col-md-offset-4">
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                          Hide Price
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 col-md-offset-4">
                  <button class="btn btn-primary tp_mrg_rght brdr_non" data-toggle="modal" data-target="#credit_limit_2">Increase Credit Limit</button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 col-md-offset-4">
                    <button class="btn btn-primary tp_btm_spc brdr_non" data-toggle="modal" data-target="#product_search">Product Search</button>
                  </div>
                </div>


              </div>

            </div>
          </div>


          <div class="row-wrapper">
            <div class="row">
              <div class="col-md-12">
                <div class="box-body table-responsive no-padding">
                  <table class="table border_table">
                    <tbody>

                      <tr class="blue-bg">
                        <th style="width: 10%;">IMEI</th>
                        <th style="width: 23%;">Product</th>                    
                        <th style="width: 8%;">MRP</th>
                        <th style="width: 8%;">Unit Price</th>
                        <th style="width: 8%;">Quantity</th>             
                        <th style="width: 8%;">Total Amount</th>
                        <th style="width: 3%;"></th>
                      </tr>

                      <tr>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td><i class="fa fa-plus-circle fa-2x ad-mar tab_mdl_clr"></i></td>
                      </tr>

                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><label for="inputEmail3" class="control-label" style="float: right;">Grand Total</label></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td></td>
                      </tr>

                      <tr>
                        <td></td>
                        <td></td>
                        <td><label for="inputEmail3" class="control-label" style="float: right;">Discount</label></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td><input type="text" class="form-control" id="inputEmail3" placeholder="Percentage"></td>
                        <td><input type="text" class="form-control" id="inputEmail3" placeholder="Amount"></td>
                        <td></td>
                      </tr>

                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><label for="inputEmail3" class="control-label" style="float: right;">Net Value</label></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td></td>
                      </tr>

                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><label for="inputEmail3" class="control-label" style="float: right;">Paid</label></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td></td>
                      </tr>

                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><label for="inputEmail3" class="control-label" style="float: right;">Balance</label></td>
                        <td><input type="text" class="form-control" id="inputEmail3"></td>
                        <td></td>
                      </tr>



                    </tbody>
                  </table>
                </div> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>

</div>
</div>
</section>

<div id="add_executive" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">

        <form class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                <label for="inputEmail3" class="control-label">Executive Name</label>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                <input type="email" class="form-control" id="inputEmail3">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                <label for="inputEmail3" class="control-label">Executive Address</label>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                <input type="email" class="form-control" id="inputEmail3">
              </div>
            </div>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
      </div>
    </div>

  </div>
</div>

<div id="credit_limit_2" class="modal fade" role="dialog">
  <div class="modal-dialog">

   
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">

        <div class="row">

          <div class="col-md-12">

            <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-3">
              <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12"><label for="inputEmail3" class="control-label">Old Limit</label></div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="150000">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12"><label for="inputEmail3" class="control-label" style="white-space: nowrap;">New Limit</label></div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="30000">
                  </div>
                </div>
              </div>
              
            </div>

          </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
      </div>
    </div>

  </div>
</div>


<div id="product_search" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">

       <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
          <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="control-label">Product Type</label></div>
          <div class="col-md-7 col-lg-7 col-sm-7 col-xs-12"><div class="form-group"><select class="form-control">
            <option>option 1</option>
            <option>option 2</option>
            <option>option 3</option>
            <option>option 4</option>
            <option>option 5</option>
          </select></div></div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
          <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="control-label">Product </label></div>
          <div class="col-md-7 col-lg-7 col-sm-7 col-xs-12"><div class="form-group"><input type="text" class="form-control" id="inputEmail3"></div></div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
          <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="control-label">Shop </label></div>
          <div class="col-md-7 col-lg-7 col-sm-7 col-xs-12"><div class="form-group"><select class="form-control">
            <option>option 1</option>
            <option>option 2</option>
            <option>option 3</option>
            <option>option 4</option>
            <option>option 5</option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12"><div class="form-group"><button class="btn btn-success">Search<i class="fa fa-search mdl_in_ser_icn" aria-hidden="true"></i></button></div></div>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>

</div>
</div>



<!-- <div id="credit_limit_2" class="modal fade" role="dialog">
  <div class="modal-dialog">

   
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">

        <div class="row">

          <div class="col-md-12">

            <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-3">
              <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12"><label for="inputEmail3" class="control-label">Old Limit</label></div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="150000">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12"><label for="inputEmail3" class="control-label" style="white-space: nowrap;">New Limit</label></div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="30000">
                  </div>
                </div>
              </div>
              
            </div>

          </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
      </div>
    </div>

  </div>
</div> -->




<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle').click(function () {
      var collapse_content_selector = $(this).attr('href');
      var toggle_switch = $(this);
      $(collapse_content_selector).toggle(function () {
        if ($(this).css('display') == 'none') {
          toggle_switch.html('Show/Hide Customer<i class="fa fa fa-expand crcl_dwn" aria-hidden="true"></i>');
        } else {
          toggle_switch.html('Show/Hide Customer<i class="fa fa fa-expand crcl_dwn" aria-hidden="true"></i>');
        }
      });
    });
  });
</script>


