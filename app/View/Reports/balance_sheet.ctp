<section class="content-header">
  <div class="box box-primary">
    <div class="box-header">
      <div class="row">
        <div class="form-group">
          <div class="col-md-6">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
              <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
              <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><br>
              <button type='button' class='btn btn-primary' id='get_button'>GET</button>
            </div>
          </div>
          <div class="col-md-6"><h1>Balance Sheet</h1></div>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-6">
            <div class="panel panel-default">
              <div class="panel-heading">
                <b>Particulars</b>
              </div>
              <div class="panel-body" id='Left-Particulars'>
                <div class="panel-group" id="Particulars_Left">
                  <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #9295d3">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#Particulars_Left" href="#collapseOne" class="collapsed" aria-expanded="false">
                          <b>Liabilities</b><b id='type_id_5' class='pull-right type_amount-left'>0</b>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                        <div class="panel-group" id="Liabilities">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #9295d3">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#Particulars_Left" href="#collapseTwo" class="collapsed" aria-expanded="false">
                          <b>Capital</b><b id='type_id_2' class='pull-right type_amount-left'>0</b>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                        <div class="panel-group" id="Capital">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="panel panel-default">
              <div class="panel-heading">
                <b>Particulars</b>
              </div>
              <div class="panel-body" id='Right-Particulars'>
                <div class="panel-group" id="Particulars_Right">
                  <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #9295d3">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#Particulars_Right" href="#collapseThree" class="collapsed" aria-expanded="false">
                          <b>Asset</b><b id='type_id_1' class='pull-right type_amount-right'>0</b>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                        <div class="panel-group" id="Asset">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <div class="col-md-12">
        <div class="col-md-6" id='diffrence_in_opening-left' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading diffrence_opening">
              <h4 style="color: red"><b>Difference In Opening Balance : <span class='pull-right' id='diffrence-left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-md-offset-6" id='diffrence_in_opening-right' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading diffrence_opening" >
              <h4 style="color: red"><b>Difference In Opening Balance : <span class='pull-right' id='diffrence-right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total : <span class='pull-right' id='grand_total-left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total : <span class='pull-right' id='grand_total-right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_modal.php'); ?>
<section class="content"></section>
<script type="text/javascript">
  $('#get_button').click(function(){
    $('#wait').show();
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    type={
      1:'Asset',
      2:'Capital',
      3:'Expense',
      4:'Income',
      5:'Liabilities',
    };
    var activeAjaxConnections=0;
    $.each(type,function(type_id,type_name){
      var data={
        from_date:from_date,
        to_date:to_date,
        type_id:type_id,
      };
      $.ajax({
        method: "POST",
        url: "<?= $this->webroot; ?>Reports/Get_Master_Group_Ajax",
        beforeSend: function(xhr) {
          activeAjaxConnections++;
        },
        data: data,
        dataType: "json",
      }).done(function(data) {
        activeAjaxConnections--;
        $("#"+type_name+"").empty();
        $.each(data.MasterGroup,function(master_group_id,master_group_name){
          $("#"+type_name+"").append(
            '<div class="panel panel-default">\
            <div class="panel-heading" style="background-color: #c4b6fc">\
            <h4 class="panel-title">\
            <a data-toggle="collapse" data-parent="#'+type_name+'" href="#MasterGroup_'+master_group_id+'" aria-expanded="false" class="collapsed"><b>'+master_group_name+'</b><b class="pull-right master_group_amount" data-parent=type_id_'+type_id+' id="master_group_id_'+master_group_id+'">0</b></a>\
            </h4>\
            </div>\
            <div id="MasterGroup_'+master_group_id+'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">\
            <div class="panel-body">\
            </div>\
            </div>\
            </div>'
            );  
          var data={
            from_date:from_date,
            to_date:to_date,
            master_group_id:master_group_id,
          };
          $.ajax({
            method: "POST",
            url: "<?= $this->webroot; ?>Reports/Get_Group_Ajax",
            beforeSend: function(xhr) {
              activeAjaxConnections++;
            },
            data: data,
            dataType: "json",
          }).done(function(data) {
            activeAjaxConnections--;
            $('#MasterGroup_'+master_group_id+' > .panel-body').empty();
            $.each(data.Group,function(group_id,group_name){
              $('#MasterGroup_'+master_group_id+' > .panel-body').append(
                '<div class="panel panel-default">\
                <div class="panel-heading" style="background-color: #e9b6fc">\
                <h4 class="panel-title">\
                <a data-toggle="collapse" data-parent="#MasterGroup_'+master_group_id+'" href="#Group_'+group_id+'" aria-expanded="false" class="collapsed"><b>'+group_name+'</b><b class="pull-right group_amount" data-parent=master_group_id_'+master_group_id+' id="group_id_'+group_id+'">0</b></a>\
                </h4>\
                </div>\
                <div id="Group_'+group_id+'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">\
                <div class="panel-body">\
                </div>\
                </div>\
                </div>'
                );  
              var data={
                from_date:from_date,
                to_date:to_date,
                group_id:group_id,
              };
              $.ajax({
                method: "POST",
                url: "<?= $this->webroot; ?>Reports/Get_SubGroup_Ajax",
                beforeSend: function(xhr) {
                  activeAjaxConnections++;
                },
                data: data,
                dataType: "json",
              }).done(function(data) {
                activeAjaxConnections--;
                $('#Group_'+group_id+' > .panel-body').empty();
                $.each(data.SubGroup,function(sub_group_id,sub_group_name){
                  $('#Group_'+group_id+' > .panel-body').append(
                    '<div class="panel panel-default">\
                    <div class="panel-heading" style="background-color: #fecded">\
                    <h4 class="panel-title">\
                    <a data-toggle="collapse" data-parent="#Group_'+group_id+'" href="#SubGroup_'+sub_group_id+'" aria-expanded="false" class="collapsed"><b>'+sub_group_name+'</b><b class="pull-right sub_group_amount" data-parent=group_id_'+group_id+' id="sub_group_id_'+sub_group_id+'">0</b></a>\
                    </h4>\
                    </div>\
                    <div id="SubGroup_'+sub_group_id+'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">\
                    <div class="panel-body">\
                    </div>\
                    </div>\
                    </div>'
                    );  
                  var data={
                    from_date:from_date,
                    to_date:to_date,
                    sub_group_id:sub_group_id,
                    type_id:type_id,
                  };
                  $.ajax({
                    method: "POST",
                    url: "<?= $this->webroot; ?>Reports/Get_AccountHead_Ajax",
                    beforeSend: function(xhr) {
                      activeAjaxConnections++;
                    },
                    data: data,
                    dataType: "json",
                  }).done(function(data) {
                    activeAjaxConnections--;
                    if(activeAjaxConnections==0)
                    {
                      $('#wait').hide();
                    }
                    console.log(activeAjaxConnections);
                    $('#SubGroup_'+sub_group_id+' > .panel-body').empty();
                    $.each(data.AccountHead,function(key,account){
                      if(account.amount)
                      {
                        $('#SubGroup_'+sub_group_id+' > .panel-body').append(
                          '<h4>\
                          <u><b class="name">'+account.name+'</b>&nbsp<b class="pull-right journal_amount" data-parent="sub_group_id_'+sub_group_id+'">'+account.amount+'</b></u>\
                          </h4>'
                          );
                      }
                    });
                  }).fail(function(){ activeAjaxConnections-- });
                });
              }).fail(function(){ activeAjaxConnections-- });
            });
          }).fail(function(){ activeAjaxConnections-- });
        });
}).fail(function() { activeAjaxConnections-- });
});
$(document).ajaxStop(function(){
 // $(document).ajaxComplete(function(){});
  $(".journal_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text(0);
  });
  $(".journal_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text((parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(3));
  });
  $(".sub_group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text(0);
  });
  $(".sub_group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text((parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(3));
  });
  $(".group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text(0);
  });
  $(".group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text((parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(3));
  });
  $(".master_group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text(0);
  });
  $(".master_group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text((parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(3));
  });
  $('#grand_total-left').text(0);
  $(".type_amount-left").each(function(){
    var amount=$(this).closest('b').text();
    $('#grand_total-left').text((parseFloat($('#grand_total-left').text())+parseFloat(amount)).toFixed(3));
  });
  $('#grand_total-right').text(0);
  $(".type_amount-right").each(function(){
    var amount=$(this).closest('b').text();
    $('#grand_total-right').text((parseFloat($('#grand_total-right').text())+parseFloat(amount)).toFixed(3));
  });
  var grand_total_left=$('#grand_total-left').text();
  var grand_total_right=$('#grand_total-right').text();
  $('#diffrence-left').text(0);
  $('#diffrence-right').text(0);00
  if(grand_total_left>grand_total_right)
  {
    var diffrence=grand_total_left-grand_total_right;
    $('#diffrence-right').text(diffrence.toFixed(3));
    if(diffrence)
    {
      $('#diffrence_in_opening-left').hide();
      $('#diffrence_in_opening-right').show();
    }
    $('#grand_total-right').text((parseFloat(grand_total_right)+parseFloat(diffrence)).toFixed(3));
  }

  else
  { 
    var diffrence=grand_total_right-grand_total_left;
    $('#diffrence-left').text(diffrence.toFixed(3));
    if(diffrence!=0)
    {
      $('#diffrence_in_opening-left').show();
      $('#diffrence_in_opening-right').hide();
      $('#grand_total-left').text((parseFloat(grand_total_left)+parseFloat(diffrence)).toFixed(3));
    }
    else
    {
      $('#diffrence_in_opening-right').show();
    }
  }
});
$('#modal_from_date').val(from_date);
$('#modal_to_date').val(to_date);
});
</script>
<script type="text/javascript">
  <?php require('general_journal_transaction_ajax.js'); ?>
</script>