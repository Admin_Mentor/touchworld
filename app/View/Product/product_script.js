$('select').select2();
$('#product_table').DataTable();
$('.input_number').on('click',function(){
  $(this).select();
});

$('.input_number').on('keyup',function(){
  if(isNaN($(this).val())){
    $(this).val('0');
  }
});

$('#flavours_check_id').click(function(){

  if($('#flavours_check_id').prop('checked')==false)
  {
    $('#flavours_hidden_field').attr("style","display:none");
  }
  else {
    $('#flavours_hidden_field').removeAttr('style');
  }
});


$('#flavours_check_id_edit').click(function(){

  if($('#flavours_check_id_edit').prop('checked')==false)
  {
    $('#flavours_hidden_field_edit').attr("style","display:none");
  }
  else {
    $('#flavours_hidden_field_edit').removeAttr('style');
  }
});


$('#addflavours_edit').click(function(){

  var flavour_name=$('#flavour_name_edit').val();
  if(flavour_name=='')
  {
    $('#flavour_name_edit').focus();
    return false;
  }
  var flavour_name_field='<input name="" class="form-control toUpperCase flavour_name_in_table" value="'+flavour_name+'">';
  $('#flavour_table_edit tbody').append('<tr class="blue-pd flavours_tr">\
    <td>'+flavour_name_field+'</td>\
    <td><i class="fa fa-minus-circle fa-2x remove_flavour" aria-hidden="true"  ></i></td>\
    </tr>');
  $('#flavour_name_edit').val('');
});

$(document).on('click','.remove_flavour',function(){
  $(this).closest('tr').remove();
});


$('#ProductProductName').keyup(function(){
  var product_name=$('#ProductProductName').val();
  var data={product_name:product_name};
  var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_name_search';
  $.ajax({
      type: "post",  // Request method: post, get
      url:url_address,
      data: data,  // post data
      success: function(response) {
        if(response=="Yes")
        {
          $('#product_name_error').text('This Product Name is already taken');
        }
        else
        {
          $('#product_name_error').text('');  
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
});

$('#prdct_name').keyup(function(){
  var product_name=$('#prdct_name').val();
  var data={product_name:product_name};
  var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_name_search';
  $.ajax({
      type: "post",  // Request method: post, get
      url:url_address,
      data: data,  // post data
      success: function(response) {
        if(response=="Yes")
        {          
          $('#check_product_name_status').attr('class','fa fa-times');
        }
        else
        {
          $('#check_product_name_status').attr('class','fa fa-check'); 
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
});



$('#product_add_button').click(function(){
  var product_name_error=$('#product_name_error').text();
  var name=$('#ProductProductName').val();
  if($.trim(name)=='' || product_name_error!='')
  {
    $('#ProductProductName').focus();
    return false;
  }
  var product_id_error=$('#product_id_error').text();
  var product_id=$('#ProductProductId').val();
  if($.trim(product_id)=='' || product_id_error!='')
  {
    $('#ProductProductId').focus();
    return false;
  }
  var product_type_id=$('#ProductProductTypeId').val();
  if(product_type_id=='' || product_type_id=='empty')
  {
    return false;
  }
  
  var price=$('#ProductPrice').val();
  var mrp=$('#ProductSalePrice').val();

  if(parseFloat(mrp)<parseFloat(price))
  {
    $('#product_error').text("* MRP must be greater than or equal to Cost value. ");
    $('#ProductSalePrice').focus();
    return false;
  }else
  {
    $('#product_error').text("");
  }

  var data=$('#ProductIndexForm').serialize();

  var url_address= '<?php echo $this->webroot; ?>Product/product_add_ajax';
  $.ajax({
    method: "POST",
    url:url_address,
    data: data,  // post data
    dataType:'json',
  }).done(function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
    }
    else
    {
      $('#product_name').append(response.option);
      $('#product_name').select2('val',$('#product_name option:last-child').val()).trigger('change');
      $('#ProductProductName').val('');
      $('#ProductProductId').val('');
      $('#ProductThreshold').val('0');
      $('#ProductPrice').val('0');
      $('#ProductSalePrice').val('0');
      $('#ProductTax').val('0');
      $('#ProductQuantity').val('0');
      $.fn.search();
    }
  });

});



$('#ProductProductId').keyup(function(){
  var product_id=$(this).val();
  var data={product_id:product_id};
  var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_id_search';
  $.ajax({
      type: "post",  // Request method: post, get
      url:url_address,
      data: data,  // post data
      success: function(response) {
        if(response=="Yes")
        {
          $('#product_id_error').text('This Product Id is already taken');
        }
        else
        {
          $('#product_id_error').text('');
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
});

$('#prdct_id').keyup(function(){
  var product_id=$(this).val();
  var data={product_id:product_id};
  var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_id_search';
  $.ajax({
      type: "post",  // Request method: post, get
      url:url_address,
      data: data,  // post data
      success: function(response) {
        if(response=="Yes")
        {
          $('#check_product_id_status').attr('class','fa fa-times');
        }
        else
        {
          $('#check_product_id_status').attr('class','fa fa-check');
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
});

$('#ProductSalePrice').keyup(function(){
  var cost=$('#ProductPrice').val();
  var mrp=$('#ProductSalePrice').val();
  if(mrp!=0)
  {
    if(parseFloat(mrp)<parseFloat(cost))
    {
      $('#product_error').text("* MRP must be greater than or equal to Cost value. ");
    }
    else
    {
      $('#product_error').text("");
    }
  }
  else
  {
    $('#product_error').text("");
  }
});


$('#mrp').keyup(function(){
  var cost=$('#cost').val();
  var mrp=$('#mrp').val();
  if(mrp!=0)
  {
    if(parseFloat(mrp)<parseFloat(cost))
    {
      $('#mrp_cost_error_modal').text("* MRP must be greater than or equal to Cost value. ");
    }
    else
    {
      $('#mrp_cost_error_modal').text("");
    }
  }
  else
  {
    $('#mrp_cost_error_modal').text("");
  }
});






$('#ProductPrice,#ProductSalePrice,#ProductQuantity,#ProductTax').click(function(){
  if($(this).val() == 0)
  {
    $(this).val('');
  }
});

$('#flavours_check_id').click(function(){

  if($('#flavours_check_id').prop('checked')==false)
  {
    $('#flavours_hidden_field').attr("style","display:none");
    $('#flavour_table tbody tr.flavours_tr').remove();

  }
  else {
    $('#flavours_hidden_field').removeAttr('style');
  }
});

$('#addflavours').click(function(){
  var flavour_name=$('#flavour_name').val();
  var flag=0;
  $('#flavour_table tbody tr.flavours_tr').each(function(){
    var name=$(this).closest('tr').find('td input.flavour_name_in_table').val();
    if(name==flavour_name)
    {
      flag=1;
    }
  }); 

  if(flavour_name=='' || flag==1)
  {
    $('#flavour_name').focus();
    return false;
  }

  var length=$('#flavour_table tbody tr').length;
  var flavour_name_field='<input name="data[Product][flavours][name]['+length+']" class="form-control flavour_name_in_table" value="'+flavour_name+'">';
  $('#flavour_table tbody').append('<tr class="blue-pd flavours_tr">\
    <td>'+flavour_name_field+'</td>\
    <td><i class="fa fa-minus-circle fa-2x remove_flavour" aria-hidden="true"></i></td>\
    </tr>');
  $('#flavour_name').val('');
});

$(document).on('click','.remove_flavour',function(){
  $(this).parent().parent().remove();
});




// edit_product

$(document).on('click', '.product_edit_modal', function (event) {
  var id=$(this).closest('tr').find('span.product_hidden_id').text();
  $('#hidden_product_id').val(id);
  var url_address='<?php echo $this->webroot; ?>'+'Product/get_product_ajax/'+id;
  $.ajax({
    type: "POST",  // Request method: post, get
    url:url_address,
    //  data: data,  // post data
    dataType:'json',
    success: function(response) {
      $('#prdct_type').select2('val',response.Product.product_type_id_fk).trigger('change');
      $('#unit_name').select2('val',response.Product.unit_id_fk).trigger('change');
      $('#prdct_name').val(response.Product.product_name);
      $('#prdct_id').val(response.Product.product_id);
      $('#threshold').val(response.Product.threashold);
      $('#cost').val(response.Product.cost);
      $('#mrp').val(response.Product.mrp);
      $('#flavour_table_edit tbody').empty();
      var flag=0;
      if(response.ProductFlavour)
      {
        flag=1;
        $.each( response.ProductFlavour, function(i, name){
          name=response.ProductFlavour[i].ProductFlavour.name;
          id=response.ProductFlavour[i].ProductFlavour.id;
          $.fn.flavour_table(name,id);
        });

      }
      if(flag==1)
      {
        $('#flavours_check_id_edit').prop('checked',true);
        $('#flavours_hidden_field_edit').attr("style","");
      }
      else
      {
        $('#flavours_check_id_edit').prop('checked',false);
        $('#flavours_hidden_field_edit').attr("style","display:none");
      }
      $("#product_edit_modal").modal();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});




$.fn.flavour_table=function(name,id)
{
  var flavour_name_field='<input name="" class="form-control flavour_name_in_modal_table toUpperCase" value="'+name+'">\
  <input type="hidden" class="flavour_id_in_modal_table" value="'+id+'">';
  $('#flavour_table_edit tbody').append('<tr class="blue-pd flavours_tr_old">\
    <td>'+flavour_name_field+'</td>\
    <td><i class="fa fa-minus-circle fa-2x remove_flavour_old" aria-hidden="true"  ></i></td>\
    </tr>');
}

//  ajax call for : Edit Product Type

$('#product_edit').click(function(){

  var p_id=$('#hidden_product_id').val();
  var prdct_type=$('#prdct_type').val();
  var prdct_name=$('#prdct_name').val();

  var check_product_name_status=$('#check_product_name_status').attr('class');
  if(prdct_name=='' || check_product_name_status!='fa fa-check')
  {
    $('#prdct_name').focus();
    return false;
  }

  var check_product_id_status=$('#check_product_id_status').attr('class');
  var prdct_id=$('#prdct_id').val();
  if(check_product_id_status!='fa fa-check' || prdct_id=='')
  {
    $('#prdct_id').select();
    return false;
  }
  
  var unit_name=$('#unit_name').val();
  if(unit_name==null)
  {
    $('#unit_name').select2('open');
    return false;
  }
  var threshold=$('#threshold').val();

  var cost=$('#cost').val();
  var mrp=$('#mrp').val();
  var vat=$('#vat').val();


  if(parseFloat(mrp)<parseFloat(cost))
  {
    $('#mrp_cost_error_modal').text('MRP must be greater than or equal to Cost value');
    $('#mrp').focus();
    return false;
  }
  else
  {
    $('#mrp_cost_error_modal').text('');
  }
  // var tax=$('#tax').val();
  var url_address= '<?php echo $this->webroot;?>'+'Product/product_edit_ajax';
  var data={
    p_id:p_id,
    prdct_type:prdct_type,
    prdct_name:prdct_name,
    prdct_id:prdct_id,
    unit_name:unit_name,
    threshold:threshold,
    cost:cost,
    mrp:mrp,
    vat:vat
  };

  if($('#flavours_check_id_edit').prop('checked')==true)
  {
    var i=0;
    data['flavoursname_old']=[];
    data['flavoursid_old']=[];
    $('#flavour_table_edit tbody tr.flavours_tr_old').each(function(){
      name=$(this).closest('tr').find('td:nth-child(1) input:nth-child(1)').val();
      id=$(this).closest('tr').find('td:nth-child(1) input:nth-child(2)').val();
      data['flavoursname_old'].push(''+name+'');
      data['flavoursid_old'].push(''+id+'');
      i++;
    });
    var j=0;
    data['flavoursname_new']=[];
    $('#flavour_table_edit tbody tr.flavours_tr').each(function(){
      name=$(this).closest('tr').find('td:nth-child(1) input:nth-child(1)').val();
      data['flavoursname_new'].push(''+name+'');
    });
  }

  $.ajax({
    type: "post",  // Request method: post, get
    url:url_address,
    data: data, // post data
    // dataType:'json',
    success: function(response) {
      var tableRow=$('#product_table tbody tr td:nth-child(3) span.product_hidden_id:contains("' + p_id + '")').closest("tr").index()+1;
      $('table#product_table tr:nth-child(' + tableRow + ') td:nth-child(3) span:nth-child(1)').text(prdct_name);
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});






$(document).on('click', '.product_delete', function (event) {
  var id=$(this).closest('tr').find('span.product_hidden_id').text();
  var tableRow=$(this).closest('tr').index();
  var url_address='<?php echo $this->webroot; ?>'+'Product/product_delete_ajax/'+id;
  $.ajax({
    type: "POST",  // Request method: post, get
    url:url_address,
    //  data: data,  // post data
    dataType:'json',
    success: function(response) {
      if(response.result=='Yes')
      {
        alert('This Product Cant be Deleted');
      }
      else 
      {
        $('#product_table tbody tr:eq(' + tableRow + ')').remove();
      }
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});


$(document).on('click', '.remove_flavour_old', function (event) {

  var id=$(this).closest('tr').find('td input.flavour_id_in_modal_table').val();
  
  var tableRow=$(this).closest('tr').index();

  var url_address='<?php echo $this->webroot; ?>'+'Product/flavour_delete_ajax/'+id;
  $.ajax({
    type: "POST",  // Request method: post, get
    url:url_address,
    //  data: data,  // post data
    dataType:'json',
    success: function(response) {
      if(response.result=='Yes')
      {
        alert('This Flavour Cant be Deleted');
      }
      else 
      {
        $('#flavour_table_edit tbody tr:eq(' + tableRow + ')').remove();
      }
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });

});
