<?php
App::uses('AppController', 'Controller');
class UnitController extends AppController {
	public $uses=array(
		'Unit',
		'Product',
		
	);
	public function index()
	{
		$Unit_list=$this->Unit->find('list',array('fields'=>array('id','name')));
		$this->Unit->unbindModel(array('hasMany'=>array('Product')));
		$Unit=$this->Unit->find('all',array('fields'=>array('id','name')));
		$this->Unit->unbindModel(array('hasMany'=>array('Product')));
		$Unit_Conversion_list=$this->Unit->find('all',array(
			'conditions'=>array('Unit.conversion >1'),
			'fields'=>array('Unit.id','Unit.name','Unit.sub_unit_id','Unit.conversion','SubUnit.name'),
		));
		//pr($Unit_Conversion_list);exit;
		$this->set(compact('Unit_list','Unit','Unit_Conversion_list'));

	}
	public function unit_add_ajax()
	{
		try {
			$data=$this->request->data['Unit'];
			$userid = 1;
			if(empty($userid))
				throw new Exception("Please Login first", 1);
			$name=$data['name'];
			if(empty($name))
				throw new Exception("Empty name", 2);
			$unit_level=1;
			if(!empty($data['unit_level']))
			{
				$unit_level+=$data['unit_level'];
			}
			$Unit=$this->Unit->find('first',array('conditions'=>array('Unit.name'=>$name)));
			if(empty($Unit))
			{
				$Table_data=array(
					'name'=>$name,
					'level'=>$unit_level,
				);
				$this->Unit->create();
				if(!$this->Unit->save($Table_data))
				{
					$errors = $this->Unit->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$id=$this->Unit->getLastInsertId();
				$Unit=$this->Unit->findById($id);
				$return['result']="Success";
			}
			else
			{
				$return['result']="Already Added";
			}
			$Unit_id=$Unit['Unit']['id'];
			
			$return['key']=$Unit['Unit']['id'];
			$return['value']=$Unit['Unit']['name'];
			//pr($return);exit;
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}

	public function unit_delete_ajax($id)
	{
		try {
			$Product_list=$this->Product->findByUnitId($id);
			if(!empty($Product_list))
				throw new Exception("Can't delete, It is used in Product", 1);
			$sub_unit=$this->Unit->findBySubUnitId($id);
			if(!empty($sub_unit))
				throw new Exception("Can't delete, It is a Sub Unit", 1);
			if(!$this->Unit->delete($id))
				throw new Exception("Error Processing While delete", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function Unit_settings_update()
	{
		try {
			$data=$this->request->data;
			$unit_level=1;
			$SubUnit=$this->Unit->findById($data['sub_unit_id']);
			$unit_level+=$SubUnit['Unit']['level'];
			$Unit_data=[
			'sub_unit_id'=>$data['sub_unit_id'],
			'conversion'=>$data['conversion'],
			'level'=>$unit_level,
			];
			$this->Unit->id=$data['unit_id'];
			if(!$this->Unit->save($Unit_data))
		    {
		      throw new Exception("Error in setting Unit"); 
		    }
			
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function get_Unit_ajax($id)
	{
		$Unit=$this->Unit->findById($id);
		if(!empty($Unit))
		{
			$return['Unit']=$Unit['Unit'];
			$return['result']='Success';
		}
		else
		{
			$return['result']='Empty';
		}
		echo json_encode($return);
 		exit;
	}
	public function unit_edit_ajax()
	{

		$data=$this->request->data['UnitEdit'];
		try {
			$id=$data['id'];
			$unit_name=$data['name'];
    		if(!$unit_name)
      			throw new Exception("Empty Unit name", 1);
      		$Unit_data=[
      		'name'=>$unit_name,
      		];
      		$this->Unit->id=$id;
		    if(!$this->Unit->save($Unit_data))
		    	throw new Exception("Error in Unit edit", 1);
		    
			$return['result']='Success';
		}
		catch (Exception $e) {
		    $return['result']=$e->getMessage();
		}
		echo json_encode($return);
  		exit;
	}
	public function unit_conversion_add_ajax()
	{

		$datasource_Unit = $this->Unit->getDataSource();
		try {
      		$datasource_Unit->begin();
      		$data=$this->request->data;
      		$SubUnit=$this->Unit->findById($data['sub_unit_id']);
      		$unit_level=1;
			$unit_level+=$SubUnit['Unit']['level'];
      		$Unit_data=[
      		'sub_unit_id'=>$data['sub_unit_id'],
      		'conversion'=>$data['conversion'],
      		'level'=>$unit_level,
      		];
      		$this->Unit->id=$data['unit_id'];
      		if(!$this->Unit->save($Unit_data))
		    	throw new Exception("Error in Unit Conversion edit", 1);
      		$datasource_Unit->commit();
      		$return['result']='Success';
	        //$return['key']=$sale_id;
	        $return['website']=$this->webroot.'Unit/';

      	} catch (Exception $e) {
	      $datasource_Unit->rollback();
	      $return['result']=$e->getMessage();
	    }
	    echo json_encode($return); exit;
	}
	public function unit_conversion_delete($id)
	{

		$datasource_Unit = $this->Unit->getDataSource();
		try {
      		$datasource_Unit->begin();
      		$data=$this->request->data;
      		$Unit_data=[
      		'sub_unit_id'=>0,
      		'conversion'=>1,
      		];
      		$this->Unit->id=$id;
      		if(!$this->Unit->save($Unit_data))
		    	throw new Exception("Error in Unit Conversion delete", 1);
      		$datasource_Unit->commit();
      		$return['result']='Success';
	        //$return['key']=$sale_id;
	        //$return['website']=$this->webroot.'Unit/';

      	} catch (Exception $e) {
	      $datasource_Unit->rollback();
	      $return['result']=$e->getMessage();
	    }
	    echo json_encode($return); exit;
	}



}