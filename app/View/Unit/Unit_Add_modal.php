<div id="Unit_Add_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add  Unit</h4>
      </div>
      <?php echo $this->Form->create('Unit',array('id'=>'Unit_Add_Form')); ?>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('name',array('class'=>'form-control toUpperCase','label'=>'Name')); ?>
             <!--  <?php echo $this->Form->input('product_type_id',array('type'=>'hidden')); ?> -->
            </div>
          </div>
    <!--       <div class="form-group">
            <div class="col-sm-10">
            <?php echo $this->Form->input('unit_level',array('type'=>'text','class'=>'form-control','label'=>'Unit Level')); ?>
            </div>
          </div> -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id='add_Unit'>Add</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>