<?php
App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');
date_default_timezone_set('Asia/Riyadh');
class AppController extends Controller {
	public $uses=[
		'Type',
		'MasterGroup',
		'Group',
		'SubGroup',
		'AccountHead',
		'Journal',
		'BankDetail',
		'Profile',
		'Module',
		'Menu',
		'UserRole',
		'UserMenuPermission',
	];
	// public $components = array('Flash','Session');
	public $components = array(
		'Flash','Session',
	);
	public $Global_Var_Profile;
	//public $User_Menu_Permissions;
	function beforeFilter()
	{
		$user_id=$this->Session->read('User.id');
		$user_role_id=$this->Session->read('UserRole.id');
	    $view_flag=1;
	    $Profile=$this->Profile->find('first');
		$this->Global_Var_Profile=$Profile;
		$this->set('Profile',$Profile);

		$controller=$this->request->params['controller'];
	    $action=$this->request->params['action'];
	    $menu_action=$controller.'/'.$action;
	    $this->set('menu_action',$menu_action);
		if($controller!='Settings' && $action!='userpass'){
			$userid = $this->Session->read('User.id');
			if(!isset($userid))
			{
				$this->Session->setFlash(__('Please Login!.'));
				$action=$this->request->params['action'];
				if($controller!='Settings' && $action!='login')
				{
					return $this->redirect(array('controller'=>'User','action' => 'login'));	
				}
			}
			else
			{
				$mode_catagory=[
					'1'=>'Cash',
					'2'=>'Bank',
				];
				$this->set('mode_catagory',$mode_catagory);
				$AccountHead_cash=$this->AccountHead->find('list',array('conditions'=>array('sub_group_id'=>1)));
				$this->set('Mode',$AccountHead_cash);
			}
		}
		// $UserMenuPermissions=array(
		// 	'view_permission'=>0,
		// 	'edit_permission'=>0,
		// 	'create_permission'=>0,
		// 	'delete_permission'=>0,
		// 	);
		// $menu_id=$this->Menu->field('Menu.id',array('action'=>$menu_action));
		// if(!empty($menu_id) && !empty($user_role_id))
	 //    {
	 //      $UserMenuPermission=$this->UserMenuPermission->findByUserRoleIdAndMenuId($user_role_id,$menu_id);

	 //      if(!empty($UserMenuPermission))
	 //      {
	      	
	 //        if($UserMenuPermission['UserMenuPermission']['view_permission']==0)
	 //        {
	 //          $view_flag=0;
	 //        }
	 //      }
	 //      else
	 //      {
	 //        $view_flag=0;
	 //      }
	 //    }
	 //    if(!empty($UserMenuPermission))
	 //    {
	 //    	$UserMenuPermissions=$UserMenuPermission['UserMenuPermission'];
	 //    }
	 //    $this->User_Menu_Permissions=$menu_action;
	 //    $this->set('create_permission_flag',$UserMenuPermissions['create_permission']);
		// $this->set('edit_permission_flag',$UserMenuPermissions['edit_permission']);
		// $this->set('delete_permission_flag',$UserMenuPermissions['delete_permission']);
	 //    if($view_flag==0)
	 //    {
	 //      $this->Session->setFlash("Permission denied");
	 //      return $this->redirect( Router::url( $this->referer(), true ) );
	 //    }
	}
}