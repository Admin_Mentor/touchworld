<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<section class="content-header">
  <h1>Product Log</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row" style="margin-top: 2%;">
         <div class="col-md-3">
           <?php echo $this->Form->input('product_id',array('id'=>'product_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
         </div>
         <div class="col-md-3">
           <?php echo $this->Form->input('warehouse_id',array('id'=>'warehouse_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
         </div>
         <div class="col-md-2" hidden>
           <?php echo $this->Form->input('date',array('type'=>'text','id'=>'date','class'=>'form-control date_range_picker','value'=>"01-01-2018 / 01-15-2018")); ?>
         </div>
         <div class="col-md-1"><br>
          <button class='btn' type='button' id='fetch_button'>Fetch</button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
      <table class="table table-condensed datatable table boder no-footer" id='table_list'>
        <thead>
          <tr class="blue-bg">
            <th>LOG ID</th>
            <th>Code</th>
            <th>Product</th>
            <th>Warehouse</th>
            <th>Quantity_In</th>
            <th>Quantity_Out</th>
            <th>Quantity</th>
            <th>Remark</th>
            <th>date</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $('#fetch_button').click(function(){
    var product_id=$('#product_id').val();
    var warehouse_id=$('#warehouse_id').val();
    var date=$('#date').val();
    var data={
      product_id:product_id,
      warehouse_id:warehouse_id,
      date:date,
    };
    var url_address= "<?= $this->webroot; ?>Reports/GetProductLogAjax";
    $.post( url_address,data, function( response ) {
      $('#table_list').DataTable().destroy();
      $('#table_list tbody').empty();
      $.each(response,function(key,value){
        var newDate = value.StockLog.updated_at.split(' ');
        var reverse_date=newDate[0].split('-').reverse().join('-');
        var created_date=reverse_date;
 
    // var newDate = value.StockLog.created_at.split(' ').reverse().join('.');

        var tr='<tr>';
        tr+='<td>'+value.StockLog.id+'</td>';
         tr+='<td>'+value.Product.code+'</td>';
        tr+='<td>'+value.Product.name+'</td>';
        tr+='<td>'+value.Warehouse.name+'</td>';
        tr+='<td>'+value.StockLog.quantity_in+'</td>';
        tr+='<td>'+value.StockLog.quantity_out+'</td>';
        tr+='<td>'+value.StockLog.quantity+'</td>';
        tr+='<td>'+value.StockLog.remark+'</td>';
        tr+='<td>'+created_date+'</td>';
        tr+='</tr>';
        $('#table_list tbody').append(tr);
      });
      // $('#table_list').DataTable();
       $('#table_list').DataTable({

        "ordering": false,
        dom: 'Bfrtip',
        buttons: [
        
        {
          extend: 'print',
           footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    '<h3 align="center">Stock Movement Report</h3>',
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
              }
            },
            {
              extend: 'csv',
              footer: true,
       exportOptions: { columns: ':visible'},
            },
            {
              extend: 'excel',
              footer: true,
       exportOptions: { columns: ':visible'},
            },
            {
              extend: 'pdf',
              footer: true,
       exportOptions: { columns: ':visible'},
            },
            {
               extend: 'pageLength',
             },
            ]
            
          });
       $.fn.show_alert('Success');
    }, "json");
  });

  $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>
<script>
  $('.date_range_picker').daterangepicker({
    opens: 'left',
    locale: { format: 'DD.MM.YYYY'}
  });

  $('.date_range_picker').on('blur', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });
</script>