$(document).ready(function()
{
  $('#add_button').show();
});
$('#add_button').click(function()
{
  if($('#amount').val()==0 && $('#paid').val()==0)
  {
    alert("Enter any amount");
    return false;
  }
  if($('#category').val()==null || $('#category').val()=='')
  {
    $('#category').focus();
//return false;
}
else if($('#account_head').val()==null || $('#account_head').val()=='')
{
  $('#account_head').focus();
//return false;
}
else
{
  $('#add_button').hide();
}

});
$(document).on('change','#route_visibility',function(){
  $('.route-field').toggle();
//var toggle_button_for_cost=$('#cost_visibility').prop('checked');
});
$("input[name='data[Journal][group]']").change(function(){
  var group=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/SubGroup_Option_ListByGroupId_ajax/"+group, function( data ) {
    $('#category').html('');
    $('#account_head').html('');
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
    $.each(data.options,function(key,value){
      var priliminary_expense_written_off_sub_group_id='<?= $priliminary_expense_written_off_sub_group_id; ?>';
      if(priliminary_expense_written_off_sub_group_id!=key)
        $('#category').append($("<option></option>").attr("value",key).text(value));
    })
    $('#category').trigger('change');
  }, "json");
  $.post( "<?= $this->webroot ?>Accountings/expense_table_list_by_group_id_ajax/"+group, function( data ) {
    $('#journal_table').DataTable().destroy();
    $('#journal_table tbody').empty();
    $('#journal_table tfoot').empty();
    if(data.result=='success')
    {
      $('#journal_table tbody').html(data.row.body);
    }
    $('#journal_table tfoot').html(data.row.foot);  
    $('#journal_table').DataTable();
  }, "json");
});
$("input[name='data[Journal][group]']:checked").change();
$('#category').change(function(){
  var category=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/AccountHead_Option_ListBySubGroupId_ajax/"+category, function( data ) {
    $('#account_head').html('');
    $('#outstanding_hidden').val('0');
    $('#outstanding').val('0');
    $.each(data.options,function(key,value){
      $('#account_head').append($("<option></option>").attr("value",key).text(value));
    });
    $('#account_head').change();
  }, "json");
});
//  $('.expense_amount').keyup(function(){
//   var amount=$(this).val();
//   var outstanding=$('#outstanding_hidden').val();
//   $('#paid').val(parseFloat(amount)+parseFloat(outstanding));
//   var paid=$('#paid').val();
//   var outstanding_hidden=$('#outstanding_hidden').val();
//   $('#outstanding').val(parseFloat(outstanding_hidden)+parseFloat(amount)-parseFloat(paid));
// });
$('#amount').keyup(function(){
  var amount=$('#amount').val();
  $('#paid').val(amount);
});
$('.expense_amount').keyup(function(){
  var amount=$('#amount').val();
  var paid=$('#paid').val();
  $('#outstanding').val(0);  
  $('#prepaid').val(0);  
  var existing_outstanding=$('#existing_outstanding').val();
  var existing_prepaid=$('#existing_prepaid').val();
  var balance=parseFloat(amount)+parseFloat(existing_outstanding)-parseFloat(paid);
  var first_stage_value=parseFloat(amount)-parseFloat(paid);
  if(first_stage_value>=0) {
    $('#outstanding').val(parseFloat(first_stage_value));  
  } else {
    $('#outstanding').val(0);
    final_stage_value=parseFloat(amount)+parseFloat(existing_outstanding)-parseFloat(paid);
    $('#prepaid').val(final_stage_value*-1);  
  }
});
$('#tax_amount').keyup(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (event.shiftKey &&  keycode== '53') {
        var amount=$('#amount').val();
        var tax_amount=$('#tax_amount').val().replace('%', '');
        tax_amount=amount*tax_amount/100;
        $('#tax_amount').val(tax_amount);
    }
    if(isNaN($(this).val()) || $(this).val()=='') { $(this).val('0').keyup(); }
});
$('.expense_amount').click(function(){
  $(this).select();
});
$('#account_head').change(function(){
  var account_head=$('#account_head option:selected').text();
  $('#outstanding').val(0);
  $('#prepaid').val(0);
  $('#existing_prepaid').val(0);
  $('#existing_outstanding').val(0);
  if(!account_head){
    return false;
  }
  $.post( "<?= $this->webroot ?>Accountings/get_outstanding_balance_from_expense/"+account_head, function( data ) {  
    $('#tax_amount').val(data.tax);
    $('#supplier_name').val(data.supplier);
    $('#trn_number').val(data.trn_number);

    if(parseFloat(data.outstanding)>0)
    {
      $('#existing_outstanding').val(data.outstanding);
    }
    else
    {
      $('#existing_prepaid').val(parseFloat(data.outstanding)*-1);
    }
  }, "json");
});
$('#account_head').change();