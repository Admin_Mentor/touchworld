<style type="text/css">
  .algn_lft{
    text-align: center !important;
  }
  .pdng_tp{
    margin-top: 15px;
  }
  .clr_star {
    color: #a29da2;
  }
  .cart_clr {
    color: #a29da2;
  }
  .pgn_btm_rht{
    margin-bottom: 15px;
  }
  .color_of{
    color :red !important;
  }
  .size_of
  {
    font-size:25px !important;
    padding-left: 55px !important;
  }
  .gst_head_alignment
  {
    position: relative;
    align-items: center;
  }
</style>
<section class="content-header">
  <h1> Pay Structure </h1>
</section>
<section class="content">
  <div class="box">
     <form id="pay_form" name="pay_form" method="post" action="<?= $this->webroot?>Hr/PayStructure">
    <div class="row" style="margin-top: 2%;">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
             <?php 
                    $date = date('m-Y',strtotime("-1 month"));
                    ?>
             <?php echo $this->Form->input('month',array('type'=>'text','class'=>'form-control pull-right datepicker_month evaluation-search','id'=>'date','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask','value'=> $date)); ?>
          </div>
        </div>
      </div>
    </div>
 <div class="row" style="margin-top: 2%;">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
           <a href="<?= $this->webroot; ?>Hr/PayList"><button   type="button"  class="btn btn-success pull-right">PayList</button></a>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="row" style="margin-top: 2%;">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
          <?php echo $this->Form->input('staff_id',array('type'=>'select','empty'=>'select','options'=>$Staff_list,'class'=>'form-control select2 evaluation-search','style'=>'width:100%','label'=>'Staff','id'=>'staff_id',)) ?>
         
          </div>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
            <label>Basic Salary</label>
            <input id="basicsalary" name="basicsalary" readonly="" type="text" class="form-control">
            <input id="basic_salary" name="basic_salary" type="hidden" class="form-control">
          </div>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
            <label>Bonus</label>
            
             <input id="bonusspan" name="bonusspan" readonly type="text" class="form-control">
            <input id="bonus" name="bonus" type="hidden" class="form-control">
          </div>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
            <label>Allowance</label>
            <input id="allowance" name="allowance" value="0" type="text" class="form-control calc-field">
          </div>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
            <label>Deduction</label>
            <input id="deduction" name="deduction" value="0" type="text" class="form-control calc-field">
          </div>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
            <label>Net Salary</label>
            <input id="net_salary" name="net_salary" type="hidden" class="form-control">
             <input id="netsalary" readonly="" name="netsalary" type="text" class="form-control">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
      <div class="col-md-2 col-md-2 col-sm-2 col-md-offset-10">
          <button  id="save" type="submit" disabled="" class="btn btn-success pull-right" style="margin-bottom: 20px; margin-top: 10px;">Save</button>
        </div>
      </div>

    </div>
</form>
 <div class="row">
          <div class="col-md-offset-10 col-sm-12 col-xs-12">
            <h3 class="total-amt"> Salary : <span id='main_total_cost'><?php echo $totalSalary; ?></span></h3>
          </div>
        </div>
        <hr/>
        <div class="inn_tabl_wrap">
          <div class="box-body">
            <table class="table table-condensed datatable text-center table-bordered" id='table_target_list' data-page-length='100'>
              <thead>
                <tr class="blue-bg">
                <th>Staff</th>
                  <th>Basic</th>
                  <th>Bonus</th>
                  <th>Allowance</th>
                   <th>Deduction</th>
                   <th>Net Salary</th>
                   <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($evaluationList
                 as $key => $value) : ?>
                  <tr class="blue-pd flash">
                  <td class="staff_name"><?php echo $value['staff_name']; ?></td>
                    <td><?php echo $value['basic_salary']; ?></td>
                    
                   
                      <td class="bonus" ><?php echo $value['bonus']; ?></td>
                       <td class="cost"><?php echo $value['allowance']; ?></td>
                       <td class="cost"><?php echo $value['deduction']; ?></td>
                       <td class="cost"><?php echo $value['net_salary']; ?></td>
                      <td class="cost"><a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Hr/DeletePaystructure/<?= $value['id'];?>"><i class='fa fa-2x fa-trash blue-col'></i></a></td>
                    
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
  </div>
</section>
 
<script type="text/javascript">
  $('.calc-field').on('keyup',function(){
    var allow = $('#allowance').val() ? $('#allowance').val() :0;
    $('#allowance').val(allow);
    var bonus = $('#bonus').val() ? $('#bonus').val() :0;
    $('#bonus').val(bonus);
    var dedu = $('#deduction').val() ? $('#deduction').val() :0;
    $('#deduction').val(dedu);
    var basic_salary = $('#basic_salary').val() ? $('#basic_salary').val() :0;
    var netsalary = parseFloat(basic_salary) + parseFloat(allow) +parseFloat(bonus) - parseFloat(dedu);
    $('#net_salary').val(netsalary);
    $('#netsalary').val(netsalary);


  })
  $('#date').inputmask('99.9999',{placeholder:"mm/yyyy"});
  $(".datepicker_month").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
});
    $(document).on('change','#date',function(){
      $('#staff_id').change();
  });
$(document).on('change','#staff_id',function(){
   // $('#staff_id').change(function () {
    var Staff_id = $(this).val();
    var month = $('#date').val();

     $('#basic_salary').val('');
                  $('#bonus').val('');
                   $('#basicsalary').val('');
                  $('#bonusspan').val('');
                  $('#netsalary').val('');
                  $('#net_salary').val('');
                  
    if(!Staff_id){
      alert('choose Staff');
      $('#staff_id').select2('open');
    return false;
  }
  if(!month){
      alert('choose Date');
      $('#date').focus();
    return false;
  }
      $.ajax({
            url: '<?= $this->webroot ?>Hr/get_staff_pay_ajax/',
            dataType: 'json',
            method: 'post',
            data:  {
              'Staff_id':Staff_id,
              'month':month
            },
            
        })
            .done(function(response) {
                //show result
                if(response.status === 'Success') {
                 $('#basic_salary').val(response.basic_pay_amount);
                  $('#bonus').val(response.bonus_amount);
                    $('#basicsalary').val(response.basic_pay_amount);
                  $('#bonusspan').val(response.bonus_amount);
                  $('.calc-field').keyup();
                  $('#save').attr('disabled',false);

                }  else {
                  alert(response.status)
                  $('#staff_id').select2('open');
                  return false;
                  $('#save').attr('disabled',true);
                    //show default message
                }
            })
            .fail(function(jqXHR) {
                if (jqXHR.status == 403) {
                    window.location = '/';
                } else {
                    console.log(jqXHR);

                }
            });
    
    
  });
$(document).on('change','.evaluation-search',function(){
     $.fn.table_search();
    // $('#staff_id').change();

  });
  $.fn.table_search=function(){
    var staff_id=$('#staff_id option:selected').val();
    // var performance_id=$('#performance_id option:selected').val();
   var month=$('#date ').val();
    var data={
      staff_id:staff_id,
      month : month,
      // performance_id: performance_id
        };
    var url_address= '<?php echo $this->webroot; ?>'+'Hr/pay_search_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $('#table_target_list').DataTable().destroy();
        $('#table_target_list tbody').html(response.row);
        $('#main_total_cost').text($.fn.indian_number_format(response.totalSalary));
        if(response.result=='Success')
        {
          // $('#main_total_cost').text($.fn.indian_number_format(response.total_cost));
        }
        else
        {
          // $('#main_total_cost').text('0');
        }
        $('#table_target_list').DataTable();
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
</script>