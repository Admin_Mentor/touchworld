<section class="content-header">
  <h1> Ledger </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body table-responsive no-padding">
      <table class="table table-condensed table datatable table boder boder" id="journal_table" data-page-length='100'>
        <thead>
          <tr class="blue-bg">
            <th>Account id</th>
            <th>Account Name</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($AccountHead as $key => $value) :?>
            <tr class="blue-pddng view_transaction">
              <td><?= $key; ?></td>
              <td class='name'><?= $value; ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_modal.php'); ?>