<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<section class="content-header">
  <h1>Sale  Report (Customer Wise)</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
           <div class="col-md-3">
           <?php echo $this->Form->input('branch_id',array('type'=>'select', 'empty' =>'MAIN','options'=>$Branch,'class'=>'form-control select2 rec_select_box search_field','label'=>'Branch')) ?>
         </div>
          <div class="col-md-3">
           <?php echo $this->Form->input('route_id',array('id'=>'route_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
         </div>
         <div class="col-md-3">
           <?php echo $this->Form->input('customer_id',array('id'=>'customer_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
         </div>
          <div class="col-md-3">
            <?php echo $this->Form->input('customer_group_id',array('type'=>'select', 'empty' =>'ALL','options'=>$CustomerGroup,'class'=>'form-control select2 rec_select_box search_field','label'=>'Group')) ?>
         </div>
          <div class="col-md-3">
           <?php echo $this->Form->input('executive_id',array('id'=>'executive_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'All')); ?>
         </div>
          <div class="col-md-3">
                  <?php echo $this->Form->input('product_type_id',array('type'=>'select','empty' =>'All','options'=>$ProductType,'class'=>'form-control select2','label'=>'Product Type')) ?>
                </div>
                <div class="col-md-3">
                  <?php echo $this->Form->input('brand_id',array('type'=>'select', 'empty' =>' All','options'=>$Brand,'class'=>'form-control select2 rec_select_box search_field','label'=>'Brand')) ?>
                </div>
                <div class="col-md-3">
                  <?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>' Select','options'=>$Product,'class'=>'form-control select2 rec_select_box search_field','label'=>'Product')) ?>
                </div>
               
               
        
         
          
          <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('from_date',array(
                      'type'=>'text',
                      'id'=>'from_date',
                      'class'=>'form-control search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                      <?php echo $this->Form->input('to_date',array(
                        'type'=>'text',
                        'id'=>'to_date',
                        'class'=>'form-control  search_field date_picker datepicker',
                        'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                        )); ?>
                      </div>
                    </div>
         <div class="col-md-1"><br>
          <button class='btn' type='button' id='fetch_button'>Fetch</button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
      <table class="table table-condensed datatable table boder no-footer" id='table_collection_list'>
        <thead>
          <tr class="blue-bg">
            <th>Executive</th>
            <th>Customer</th>
            <th>Route</th>
            <th>Net Amount</th>
<!--        <th>Round Off</th>
 -->        <th>Tax Amount</th>
            <th>Discount Amount</th>
            <th>Total</th>
           

          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">

  $('#fetch_button').click(function(){
    //alert("k");
    var branch_id=$('#branch_id').val();
    var route_id=$('#route_id').val();
    var customer_id=$('#customer_id').val();
    var customer_group_id=$('#customer_group_id').val();
    var executive_id=$('#executive_id').val();
    var product_type_id=$('#product_type_id').val();
    var brand_id=$('#brand_id').val();
    var product_id=$('#product_id').val();
     
   
    
    
   
   // alert(customer_id);
    
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();;
    var data={
      branch_id:branch_id,
      route_id:route_id,
      customer_id:customer_id,
      customer_group_id:customer_group_id,
      executive_id:executive_id,
      product_type_id:product_type_id,
      brand_id:brand_id,
      product_id:product_id,
      from_date:from_date,
      to_date:to_date,
    };
    //console.log(data);
    //if(executive_id!=""){
    var url_address= "<?= $this->webroot; ?>Reports/SaleCollectionReportCustomerwiseAjax";
    $.post( url_address,data, function( response ) {
      $('#table_collection_list').DataTable().destroy();
      $('#table_collection_list tbody').empty();
      $.each(response,function(key,value){
        var tr='<tr>';
        tr+='<td>'+value.executive+'</td>';
        tr+='<td>'+value.name+'</td>';
        tr+='<td>'+value.route+'</td>';
        tr+='<td>'+value.net_value+'</td>';
        tr+='<td>'+value.tax_amount+'</td>';
        tr+='<td>'+value.dicount_amount+'</td>';
        tr+='<td>'+value.grandtotal+'</td>';
        tr+='</tr>';
        $('#table_collection_list tbody').append(tr);
      });
      $('#table_collection_list').DataTable();
    }, "json");
 // }
  });
</script>
