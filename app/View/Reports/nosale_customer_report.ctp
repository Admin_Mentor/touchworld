<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<style type="text/css">

.cls_label_all {
  padding-top: 5%;
}
.row_top_row{

  margin-top: 5%;
}
.deaf_btn_btn {
  margin-left: 4%;
}

.row_new_add{
  margin-top: 2%;
}
#radio_butto_add {
  margin-top: 7px;
  margin-left: -51%;
}
.check_bx_rght{
  white-space: nowrap;
  padding-top: 20px;
}
.chkbx_icn_clk {
    position: absolute;
    margin-left: -16px !important;
}
</style>

<section class="content-header">
  <h1>Non Buying Customer Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="row" style="margin-top: 2%;">
                  
                  <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('route_id',array(
                      'type'=>'select',
                      'id'=>'route_id',
                      'style'=>'width:100%',
                      'options'=>$route_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'ALL',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('from_date',array(
                      'type'=>'text',
                      'id'=>'from_date',
                      'class'=>'form-control search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                      <?php echo $this->Form->input('to_date',array(
                        'type'=>'text',
                        'id'=>'to_date',
                        'class'=>'form-control  search_field date_picker datepicker',
                        'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                        )); ?>
                      </div>
                    </div>
                    <div class="col-md-2">
                    <div class="col-md-12">
                     <?= $this->Form->input('Amount',array('class'=>'form-control','type'=>'text','required','id'=>'Amount','required')); ?>

                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="col-md-12">
                        <br>
                        <button class='btn' type='button' id='nosale_report_button'>Fetch</button>
                      </div>
                    </div>
                     
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="col-md-4 col-xs-12">
                      <h3 class="muted "></h3>
                    </div>
                    <table class="table table-condensed table boder no-footer" id='table_nosale_list'>
                      <thead>
                        <tr class="blue-bg">
                          <!-- <th>Executive</th> -->
                           <th>Code</th>
                          <th>Customer</th>
                          <th>Sale Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $('#table_nosale_list').DataTable();
      $('#nosale_report_button').on('click',function(){
        $.fn.table_search();
      });
      $('#checkbox').click(function(){
        $.fn.table_search();
      });
      $.fn.table_search=function(){   
        var from_date=$('#from_date').val();
        var to_date=$('#to_date').val();
        var Amount=$('#Amount').val();
       var route_id=$('#route_id').val();
        if(!Amount)
        {
         $('#Amount').focus();
         return false();
        }
        var data={
          from_date:from_date,
          to_date:to_date,
          Amount:Amount,
         route_id:route_id,
        };
        var url_address= '<?php echo $this->webroot; ?>'+'Reports/nosale_report_ajax';
        $.ajax({
          type: "post",
          url:url_address,
          data: data,
          dataType:'json',
          success: function(response) {
            $('#table_nosale_list').DataTable().destroy();
            $('#table_nosale_list tbody').html(response.row);
            $('#table_nosale_list').DataTable(
              {
                dom: 'Bfrtip',
    lengthMenu: [
    [25, 50,100,-1],
    ['25 rows', '50 rows','100 rows','Show all' ]
    ],
    buttons: [
    { extend: 'print',
     //title: 'NoSale Customer Report ('+from_date+' to'+to_date+', Amount<'+Amount+')',
      customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    // '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
                    // '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
                    // '<h5 align="center">Thiruvangoor</h5',
                    // '<h5 align="center">Calicut-673 304</h5>',
                    // '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
                    '<h3 align="center">Non Buying Customer Report</h3>',
            //'<h5>CustomerType :'+$('#customer_type_id').val()+'</h5>',
                    '<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+' Amount :'+$('#Amount').val()+' </h5>'
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              },
      exportOptions: { columns: ':visible' } },
    // { extend: 'excel', title: 'NoSale Customer Report', exportOptions: { columns: ':visible' } }, 
    // { extend: 'pdf',   title: 'NoSale Customer Report', exportOptions: { columns: ':visible' } }, 
    'pageLength',
    ],
              });
            $.fn.show_alert('Success');
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      }
// $.fn.table_search();
$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>
