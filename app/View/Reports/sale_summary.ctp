<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Sales Register Summary</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
             <div class="col-md-12">
              <div class="row">
                <div class="col-md-2">
                  <div class="col-md-12">
                   <?php echo $this->Form->input('from_date',array(
                    'type'=>'text',
                    'id'=>'from_date',
                    'value'=>$from,
                    'class'=>'form-control cls_label_all date_field search_field date_picker datepicker',
                    'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                    )); ?>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="col-md-12">
                    <?php echo $this->Form->input('to_date',array(
                      'type'=>'text',
                      'id'=>'to_date',
                      'value'=>$to,
                      'class'=>'form-control cls_label_all date_field search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <br>
                    <button class='btn' type='button' id='fetch_button'>Fetch</button>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="col-md-4 col-xs-12">
                  <h3 class="muted "></h3>
                </div>
                <table class="table table-condensed table " id='table_product_wise_list' data-page-length="-1">
                  <thead>
                    <tr class="blue-bg">
                      <th>No</th>
                      <th>Date</th>
                      <th>Customer Name</th>
                      <th>Net Amount</th>
                      <th>Discount</th>
                      <th>Taxable Value</th>
                      <th>Vat</th>
                      <th>Grand Total</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $('#table_product_wise_list').DataTable( {
      dom: 'Bfrtip',
      ordering: false,
      buttons: [
      {
               extend: 'colvis',
             },
      {
        extend: 'print',
       // text: 'Print' ,
       // title: 'SaleSummary',
        footer: true,
       exportOptions: { columns: ':visible'},
        customize: function ( win ) {
          $(win.document.body)
          .css( 'font-size', '10pt' )
          .prepend(
            // '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
            // '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
            // '<h5 align="center">Thiruvangoor</h5',
            // '<h5 align="center">Calicut-673 304</h5>',
            // '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
            '<h3 align="center">Sales Register Summary</h3>',
         //    '<h5>Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+'</h5>'
            '<h5></h5>'
            );
          $(win.document.body).find( 'table' )
          .addClass( 'compact' )
          .css( 'font-size', 'inherit' )
          .prepend(
            '<tr><td colspan="8"><h4></h4></td></tr>'
            );
        }
      },
      {
        extend: 'csv',
       // text: 'CSV' ,
        footer: true,
      //title:'Sales Register Summary (Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
       exportOptions: { columns: ':visible'},
      },
      {
        extend: 'excel',
        // text: 'Excel' ,
       // title:'Sales Register Summary',
        footer: true,
          //  title:'>Sales Register Summary (Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
       exportOptions: { columns: ':visible'},
      },
      {
        extend: 'pdf',
        //text: 'Pdf' ,
        //title:'Sales Register Summary',
     // title:'Sales Register Summary (Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
         footer: true,
       exportOptions: { columns: ':visible'},
      },
      {
               extend: 'pageLength',
             },
      ],
    } );
  } );
  $.fn.search = function() {
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var data={
      to_date:to_date,
      from_date:from_date
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/SaleSummary_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $('#table_product_wise_list').DataTable().destroy();
        $('#table_product_wise_list tbody').html(response.row);
        $('#table_product_wise_list').DataTable( {
         "ordering": false,
         dom: 'Bfrtip',
         buttons: [
          {
               extend: 'colvis',
             },
         {
          extend: 'print',
         // text: 'Print' ,
          //title: 'Sales Register Summary',
          footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
             .prepend(
            //   '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
            //   '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
            //   '<h5 align="center">Thiruvangoor</h5',
            //   '<h5 align="center">Calicut-673 304</h5>',
            //   '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
              '<h3 align="center">Sales Register Summary</h3>',
              '<h5>Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+'</h5>'
               );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
            .prepend(
              '<tr><td colspan="8"></td></tr>'
              );
          }
        },
        {
          extend: 'csv',
          //text: 'CSV' ,         
          title:'Sales Register Summary (Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

          footer: true,
       exportOptions: { columns: ':visible'},
        },
        {
          extend: 'excel',
          //text: 'Excel' ,
         title:'Sales Register Summary (Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
          footer: true,
       exportOptions: { columns: ':visible'},
        },
        {
          extend: 'pdf',
          //text: 'Pdf' ,
                  title:'Sales Register Summary (Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
          footer: true,
       exportOptions: { columns: ':visible'},
        },
         {
               extend: 'pageLength',
             },
        ]
      } );
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };
  $('#fetch_button').on('click',function(){
    $.fn.search();
  });
</script>