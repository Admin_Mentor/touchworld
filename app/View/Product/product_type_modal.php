<div id="Product_Type_edit" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Product Type</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3"><label class="form-group">Product Type</label></div>
          <div class="col-md-9">
            <input type="text" style="display:none" id="prdct_type_id">
            <input type="text" class="form-control form-group toUpperCase" placeholder="" id="prdct_typ">
            <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" id="product_type_edit">Update</button>
      </div>
    </div>
  </div>
</div>