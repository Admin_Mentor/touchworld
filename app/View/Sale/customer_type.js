$('#add_customer_type').click(function(){
	var name=$('#customer_type_name').val();
	if(name=='')
	{
		$('#customer_type_name').focus();
		return false;
	}
	var website_url='<?php echo $this->webroot; ?>CustomerType/add_customer_type_ajax';
	var data={
		name:name,
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#customer_type_modal').modal('toggle');
		$('#customer_type_name').val('');
		$('#customer_type_id').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#modal_customer_type_id').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#customer_type_id').val(data.key).change();
		$('#modal_customer_type_id').val(data.key).change();
	});
});
$(document).on('change','#customer_type_id,#modal_customer_type_id',function(){
	$('#modal_customer_type_id').val($(this).val()).trigger('change.select2');
	$('#customer_type_id').val($(this).val()).trigger('change.select2');
	var customer_type_id=$('#customer_type_id').val();
	// if(customer_type_id==0 || customer_type_id==1)
	// 	$('.non_general_customer').hide();
	// else
	// 	$('.non_general_customer').show();
});

// $("#modal_customer_type_id option[value*='1']").prop('disabled',true);
