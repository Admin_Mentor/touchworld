<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Settings</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="Sale_Modal_Style">
        <?php echo $this->Form->create('CustomerSettings'); ?>
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-6">
                <?php
                echo $this->Form->input('unit_price', array('type' => 'checkbox','class'=>'checkbox'));
                echo $this->Form->input('net_value', array('type' => 'checkbox','class'=>'checkbox'));
                echo $this->Form->input('tax_amount', array('type' => 'checkbox','class'=>'checkbox')); 
                echo $this->Form->input('show_margin', array('type' => 'checkbox','class'=>'checkbox')); 
                ?>
              </div>
              <div class="col-md-6">
                <?php
                echo $this->Form->input('executive', array('type' => 'checkbox','class'=>'checkbox'));
                echo $this->Form->input('warehouse', array('type' => 'checkbox','class'=>'checkbox'));
                echo $this->Form->input('discount', array('type' => 'checkbox','class'=>'checkbox')); 
                echo $this->Form->input('customer_type', array('type' => 'checkbox','class'=>'checkbox')); 
                ?>
              </div>
            </div>
          </div>
        </div>
        <?php echo $this->Form->end(); ?>
        </div>
      </div>
   <!--    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).on('change','#CustomerSettingsSaleForm input',function(){
    var data=$('#CustomerSettingsSaleForm').serialize();
    $.post( "<?= $this->webroot ?>Sale/UpdateCustomerSettings",data,function( response ) {
      if(response.result != 'Success')
      {
        alert('An Error Occured Plz Try again...');
        return false;
      }else{
        $.fn.SetCustomerSettings();
      }
    }, "json");
  });
  $.fn.SetCustomerSettings = function(){
    $.post( "<?= $this->webroot ?>Sale/GetCustomerSettings/Sale",function( response ) {
      if(response.unit_price == 1){
        $('#CustomerSettingsUnitPrice').prop('checked', true);
        $('.settings_unit_price').show();
      }
      else{
       $('#CustomerSettingsUnitPrice').prop('checked', false);
       $('.settings_unit_price').hide();
     }
     if(response.net_value == 1){
      $('#CustomerSettingsNetValue').prop('checked', true);
      $('.settings_net_value').show();
    }
    else{
      $('#CustomerSettingsNetValue').prop('checked', false);
      $('.settings_net_value').hide();
    }
    if(response.tax_amount == 1){
      $('#CustomerSettingsTaxAmount').prop('checked', true);
      $('.settings_tax_amount').show();
    }
    else{
      $('#CustomerSettingsTaxAmount').prop('checked', false);
      $('.settings_tax_amount').hide();
    }

    if(response.executive == 1){
      $('#CustomerSettingsExecutive').prop('checked', true);
      $('.settings_executive').show();
    }
    else{
      $('#CustomerSettingsExecutive').prop('checked', false);
      $('.settings_executive').hide();
    }
     if(response.warehouse == 1){
      $('#CustomerSettingsWarehouse').prop('checked', true);
      $('.settings_warehouse').show();
    }
    else{
      $('#CustomerSettingsWarehouse').prop('checked', false);
      $('.settings_warehouse').hide();
    }
     if(response.discount == 1){
      $('#CustomerSettingsDiscount').prop('checked', true);
      $('.settings_discount').show();
    }
    else{
      $('#CustomerSettingsDiscount').prop('checked', false);
      $('.settings_discount').hide();
    }
      if(response.show_margin == 1){
      $('#CustomerSettingsShowMargin').prop('checked', true);
      $('.settings_show_margin').show();
    }
    else{
      $('#CustomerSettingsShowMargin').prop('checked', false);
      $('.settings_show_margin').hide();
    }
      if(response.customer_type == 1){
      $('#CustomerSettingsCustomerType').prop('checked', true);
      $('.settings_customer_type').show();
    }
    else{
      $('#CustomerSettingsCustomerType').prop('checked', false);
      $('.settings_customer_type').hide();
    }
  }, "json");
  }
  $.fn.SetCustomerSettings();
</script>

