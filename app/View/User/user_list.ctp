<section class="content-header">
  <h1> <span>User Management</span> <span class='pull-right'><button class='btn btn-primary' data-toggle="modal" data-target="#User_modal"><i class="fa fa-plus-circle"></i> Add User</button></span></h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
              </div>
              <div class="row-wrapper">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box-body table-responsive no-padding">
                      <table class="table table-condensed table table boder boder datatable" id="myTable">
                        <thead>
                          <tr class="blue-bg">
                            <th>Name</th>
                            <th>Role</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Date</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($UserList as $key => $value): ?>
                            <tr>
                              <td>
                                <span><?= $value['User']['name']; ?></span>
                                <span style='display: none' class='user_id'><?= $value['User']['id']; ?></span>
                              </td>
                                <td><?= $value['UserRole']['role_name']; ?></td>
                              <td><?= $value['User']['email']; ?></td>
                              <td><?= $value['User']['name']; ?></td>
                              <td><?= $value['User']['password']; ?></td>
                              <td><?= date('d-m-Y',strtotime($value['User']['created_at'])); ?></td>
                              <td><?php $flag=$value['User']['flag']; if($flag==1) {$button_value="Active"; } else { $button_value="Deactive"; } ?> <a href="<?php echo $this->webroot.'User/status_updation_ajax/'.$value['User']['id']; ?>"><button class='btn'><?= $button_value; ?></button></a></td>
                            </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<div class="modal fade" id="User_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add User</h4>
      </div>
      <?= $this->Form->create('User', array('url' => array('controller' => 'User', 'action' => 'Add')));?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3">
              <div class="col-md-12">
                <?php echo $this->Form->input('name',array('type'=>'text','class'=>'form-control','label'=>'User Name','autocomplete' => 'off')); ?>
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('password',array('type'=>'password','class'=>'form-control','label'=>'Password','autocomplete' => 'off')); ?>
              </div>
               <div class="col-md-12">
               <?= $this->Form->input('user_role_id',array('class'=>'form-control select2','type'=>'select','options'=>$RoleList,'style'=>"width: 100%;",'id'=>'user_role_id','label'=>'Role',)); ?>
               
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('mobile',array('type'=>'text','class'=>'form-control','autocomplete' => 'off')); ?>
              </div>
              
              <div class="col-md-12">
                <?php echo $this->Form->input('email',array('type'=>'text','class'=>'form-control')); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id='Add_user_btn'>Add User</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).on('click','.status_updation',function(){
    var user_id=$(this).closest('tr').find('td span.user_id').text();
    $.post( "<?= $this->webroot ?>User/status_updation_ajax/"+user_id,function( data ) {
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }
    }, "json");
  });
</script>