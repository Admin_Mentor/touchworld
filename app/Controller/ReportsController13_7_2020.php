
<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
ini_set('memory_limit', '1024M');
set_time_limit(0); 
class ReportsController extends AppController {
	public $scaffold;
	public $uses=[
	'Type',
	'MasterGroup',
	'Group',
	'SubGroup',
	'AccountHead',
	'Journal',
	'BankDetail',
	'Purchase',
	'PurchasedItem',
	'AssetPurchase',
	'Sale',
	'SaleItem',
	'Vendor',
	'Party',
	'CustomerType',
	'Customer',
	'SalesReturn',
	'SalesReturnItem',
	'PurchaseReturn',
	'PurchaseReturnItem',
	'StockLog',
	'Stock',
	'ProductType',
	'Product',
	'ExecutiveRouteMapping',
	'NoSale',
	'Executive',
	'StockTransfer',
	'State',
	'Unit',
	'DayRegister',
	'ClosingDay',
	'Warehouse',
	'Route',
	'CustomerGroup',
	'Brand',
	'ProductTypeBrandMapping',
	'Branch',
	'SaleTarget',
	'ExecutiveBonusDetails',
	'Staff',
	'Role',
	'Production',
	'Cheque',
	'ExpenseVatDetail',
	'ExpenseVat'
	];
	public function ProfitLossReport()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/ProfitLossReport'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			$from_date=date('1-m-Y');
			$to_date=date('d-m-Y');
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
			$ProfitLoss=$this->profit_loss_calculator($from_date,$to_date);
			$this->set('ProfitLoss',$ProfitLoss);
		}
	}
	public function RouteWiseProfitLossReport()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/ProfitLossReport'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			$from_date=date('1-m-Y');
			$to_date=date('d-m-Y');
			$routes=$this->Route->find('list',array('fields'=>array('id','name')));
			$this->set(compact('routes'));
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
			$ProfitLoss=$this->profit_loss_calculator_route($from_date,$to_date,1);
			$this->set('ProfitLoss',$ProfitLoss);
		}
	}
	public function ProfitLossReport_ajax()
	{
		$from_date=$this->request->data['from_date'];
		$to_date=$this->request->data['to_date'];
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$route_id=null;
		if(!empty($this->request->data['route_id']))
			$route_id=$this->request->data['route_id'];
		if(empty($route_id))
		{
		$ProfitLoss=$this->profit_loss_calculator($from_date,$to_date,$route_id);
	    }
	    else
	    {
	    $ProfitLoss=$this->profit_loss_calculator_route($from_date,$to_date,$route_id);
	    }
		echo json_encode($ProfitLoss);
		exit;
	}
	public function StockCalculator($from_date,$to_date,$route_id=null)
	{
		$stock_account_head_id=19;
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$account_single_opening=[];
		$account_single_opening['debit']=0;
		$account_single_opening['credit']=0;
		$AccountHead=$this->AccountHead->findById($stock_account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$conditions=[];
		$conditions_Debit_opening=[];
		$conditions_Debit_opening['Journal.flag']=1;
		$conditions_Debit_opening['Journal.debit']=$stock_account_head_id;
		$conditions_Debit_opening['Journal.date <']=date('Y-m-d',strtotime($from_date));
		if(!empty($route_id))
		{
		$conditions_Debit_opening['Journal.route_id']=$route_id;	
		}
		$conditions_Credit_opening=[];
		$conditions_Credit_opening['Journal.flag']=1;
		$conditions_Credit_opening['Journal.credit']=$stock_account_head_id;
		$conditions_Credit_opening['Journal.date <']=date('Y-m-d',strtotime($from_date));
		if(!empty($route_id))
		{
		$conditions_Credit_opening['Journal.route_id']=$route_id;	
		}
		$conditions_Journal_Debit=[];
		$conditions_Journal_Debit['Journal.flag']=1;
		$conditions_Journal_Debit['Journal.debit']=$stock_account_head_id;
		$conditions_Journal_Debit['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
		if(!empty($route_id))
		{
		$conditions_Journal_Debit['Journal.route_id']=$route_id;	
		}
		$conditions_Journal_Credit=[];
		$conditions_Journal_Credit['Journal.flag']=1;
		$conditions_Journal_Credit['Journal.credit']=$stock_account_head_id;
		$conditions_Journal_Credit['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
		if(!empty($route_id))
		{
		$conditions_Journal_Credit['Journal.route_id']=$route_id;	
		}
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Debit_opening));
		$account_single_opening['debit']+=$Journal_Debit_opening['Journal']['total_amount'];
		$Journal_Credit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Credit_opening));
		$account_single_opening['credit']+=$Journal_Credit_opening['Journal']['total_amount'];
		$Journal_Debit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Debit));
		$account_single['debit']+=$Journal_Debit['Journal']['total_amount'];
		$Journal_Credit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Credit));
		$account_single['credit']+=$Journal_Credit['Journal']['total_amount'];
		if(!empty($route_id))
		{
			$Stock=$this->Stock->find('all',
				array(
				"joins"=>array(
					array(
						"table"=>'executives',
						"alias"=>'Executive',
						"type"=>'inner',
						"conditions"=>array('Executive.warehouse_id=Stock.warehouse_id'),
						),
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
						),
					array(
						"table"=>'routes',
						"alias"=>'Route',
						"type"=>'inner',
						"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array(
					'Route.id'=>$route_id,
					),
				'fields'=>array(
					'Stock.quantity',
					'Product.cost',
					),
				));
			$total_cost=0;
			foreach ($Stock as $key_stock => $value_stock) {
				$total_cost+=$value_stock['Product']['cost']*$value_stock['Stock']['quantity'];
			}
			$return['open']=$total_cost+$account_single_opening['debit']-$account_single_opening['credit'];
			$return['close']=$total_cost;
		}
		else
		{
			$return['open']=floatval($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit']);
			$return['close']=floatval($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit']+$account_single['debit']-$account_single['credit']);
		}
		return $return;
	}
	public function CostCalculator($from_date,$to_date,$route_id=null)
	{
		$sale_account_head_id=6;
		$return['costValue']=0;
		$Sale_AccountHead=$this->AccountHead->findById($sale_account_head_id);
		$SaleValue=$this->General_Journal_Debit_N_Credit_function_route_cost_sale($sale_account_head_id,$from_date,$to_date,$route_id);
		$return['costValue']=$SaleValue['credit'];		
		return $return;
	}
	public function StockCalculatorProfit($from_date,$to_date,$route_id=null)
	{
		$stock_account_head_id=19;
		$final_product=22;
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$account_single_opening=[];
		$account_single_opening['debit']=0;
		$account_single_opening['credit']=0;
		$AccountHead=$this->AccountHead->findById($stock_account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$conditions=[];
		$conditions_Debit_opening=[];
		$conditions_Debit_opening['Journal.flag']=1;
		$conditions_Debit_opening['Journal.debit']=$stock_account_head_id;
		$conditions_Debit_opening['Journal.date <']=date('Y-m-d',strtotime($from_date));

		$conditions_Credit_opening=[];
		$conditions_Credit_opening['Journal.flag']=1;
		$conditions_Credit_opening['Journal.credit']=$stock_account_head_id;
		$conditions_Credit_opening['Journal.date <']=date('Y-m-d',strtotime($from_date));

		$conditions_Journal_Debit=[];
		$conditions_Journal_Debit['Journal.flag']=1;
		$conditions_Journal_Debit['Journal.debit']=$stock_account_head_id;
		$conditions_Journal_Debit['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];

		$conditions_Journal_Credit=[];
		$conditions_Journal_Credit['Journal.flag']=1;
		$conditions_Journal_Credit['Journal.credit']=$stock_account_head_id;
		$conditions_Journal_Credit['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];

		
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Debit_opening));
		$account_single_opening['debit']+=$Journal_Debit_opening['Journal']['total_amount'];
		$Journal_Credit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Credit_opening));
		$account_single_opening['credit']+=$Journal_Credit_opening['Journal']['total_amount'];
		$Journal_Debit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Debit));
		$account_single['debit']+=$Journal_Debit['Journal']['total_amount'];
		$Journal_Credit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Credit));
		$account_single['credit']+=$Journal_Credit['Journal']['total_amount'];
		/////**** start final product********/////////////
       $conditions_Debit_opening_final=[];
		$conditions_Debit_opening_final['Journal.flag']=1;
		$conditions_Debit_opening_final['Journal.debit']=$final_product;
		$conditions_Debit_opening_final['Journal.date <']=date('Y-m-d',strtotime($from_date));

		$conditions_Credit_opening_final=[];
		$conditions_Credit_opening_final['Journal.flag']=1;
		$conditions_Credit_opening_final['Journal.credit']=$final_product;
		$conditions_Credit_opening_final['Journal.date <']=date('Y-m-d',strtotime($from_date));

		$conditions_Journal_Debit_final=[];
		$conditions_Journal_Debit_final['Journal.flag']=1;
		$conditions_Journal_Debit_final['Journal.debit']=$final_product;
		$conditions_Journal_Debit_final['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
		
		$conditions_Journal_Credit_final=[];
		$conditions_Journal_Credit_final['Journal.flag']=1;
		$conditions_Journal_Credit_final['Journal.credit']=$final_product;
		$conditions_Journal_Credit_final['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];

         $Journal_Debit_opening_final=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Debit_opening_final));
		$account_single_opening['debit']+=$Journal_Debit_opening_final['Journal']['total_amount'];
		$Journal_Credit_opening_final=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Credit_opening_final));
		$account_single_opening['credit']+=$Journal_Credit_opening_final['Journal']['total_amount'];
		$Journal_Debit_final=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Debit_final));
		$account_single['debit']+=$Journal_Debit_final['Journal']['total_amount'];
		$Journal_Credit_final=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Credit_final));
		$account_single['credit']+=$Journal_Credit_final['Journal']['total_amount'];
      	/////**** End final product********/////////////
			$return['open']=floatval($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit']);
			$return['close']=floatval($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit']+$account_single['debit']-$account_single['credit']);
		return $return;
	}
	public function profit_loss_calculator($from_date,$to_date,$route_id=null)
	{
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$ProfitLoss['result']='Success';
		$ProfitLoss['Stock']=$this->StockCalculatorProfit($from_date,$to_date,$route_id);
		$ProfitLoss['Income']=$this->IncomeCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Expense']=$this->ExpenseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Purchase']=$this->PurchaseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Sale']=$this->SaleCalculator($from_date,$to_date,$route_id);
		$Product=$this->Product_id_list_function($from_date,$to_date);
		$GSTReport=['total'=>[]];
		$gst_in=0;
		$gst_out=0;
		$total_in=0;
		$total_out=0;
		$GSTReport['total']['out']=0;
		$GSTReport['total']['in']=0;
		// $single_GSTReport=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date,$gst='All',$type='All',$route_id);
		// $GSTReport['total']['in']=$single_GSTReport['tax']['in'];
		// $GSTReport['total']['out']=$single_GSTReport['tax']['out'];
		// $gst=round($GSTReport['total']['out']-$GSTReport['total']['in']);
		// if($gst>0)
		// {
		// 	$tax_amount['left']=$gst;
		// 	$tax_amount['right']=0;
		// }
		// else
		// {
		// 	$tax_amount['left']=0;
		// 	$tax_amount['right']=$gst*-1;
		// }
		$tax_amount['left']=0;
		$tax_amount['right']=0;
		$ProfitLoss['Expense']['IndirectExpense']['amount']+=$tax_amount['left'];
		$ProfitLoss['Income']['IndirectIncome']['amount']+=$tax_amount['right'];
		$IndirectExpense_gst_array['name']='tax';
		$IndirectExpense_gst_array['PrePaid']=0;
		$IndirectExpense_gst_array['paid']=0;
		$IndirectExpense_gst_array['Outstanding']=$tax_amount['left'];
		$IndirectExpense_gst_array['total']=$tax_amount['left'];
		$ProfitLoss['Expense']['IndirectExpense']['single']['TAX']=$IndirectExpense_gst_array;
		$IndirectIncome_gst_array['name']='tax';
		$IndirectIncome_gst_array['Received']=0;
		$IndirectIncome_gst_array['Advance']=0;
		$IndirectIncome_gst_array['Accrued']=$tax_amount['right'];
		$IndirectIncome_gst_array['Total']=$tax_amount['right'];
		$ProfitLoss['Income']['IndirectIncome']['single']['TAX']=$IndirectIncome_gst_array;
		$ProfitLoss['First']['Left']=0;
		$ProfitLoss['First']['Right']=0;
		$ProfitLoss['Second']['Left']=0;
		$ProfitLoss['Second']['Right']=0;
		$ProfitLoss['FirstTotal']['Left']=0;
		$ProfitLoss['FirstTotal']['Right']=0;
		$ProfitLoss['Gross']['Left']=0;
		$ProfitLoss['Gross']['Right']=0;
		$ProfitLoss['Net']['Right']=0;
		$ProfitLoss['Net']['Left']=0;
		$ProfitLoss['SecondTotal']['Left']=0;
		$ProfitLoss['SecondTotal']['Right']=0;
		$FirstLeft=$ProfitLoss['Purchase']['PurchaseValue']-$ProfitLoss['Purchase']['PurchaseReturn']+$ProfitLoss['Expense']['DirectExpense']['amount']+$ProfitLoss['Stock']['open']+$tax_amount['right'];
		$FirstLeft=number_format($FirstLeft,4,'.','');
		$FirstRight=$ProfitLoss['Sale']['SaleValue']-$ProfitLoss['Sale']['SalesReturn']+$ProfitLoss['Income']['DirectIncome']['amount']+$ProfitLoss['Stock']['close']+$tax_amount['left'];
		$FirstRight=number_format($FirstRight,4,'.','');
		if($FirstLeft>$FirstRight)
		{
			$ProfitLoss['First']['Right']=number_format(($FirstLeft-$FirstRight),3,'.','');
			$ProfitLoss['Gross']['Left']=number_format(($FirstLeft-$FirstRight),3,'.','');
		}
		else
		{
			$ProfitLoss['First']['Left']=number_format(($FirstRight-$FirstLeft),3,'.','');
			$ProfitLoss['Gross']['Right']=number_format(($FirstRight-$FirstLeft),3,'.','');
		}
		$ProfitLoss['FirstTotal']['Left']+=$FirstLeft+$ProfitLoss['First']['Left'];
		$ProfitLoss['FirstTotal']['Right']+=$FirstRight+$ProfitLoss['First']['Right'];
		$SecondLeft=$ProfitLoss['Expense']['IndirectExpense']['amount']+$ProfitLoss['Gross']['Left'];
		$SecondRight=$ProfitLoss['Income']['IndirectIncome']['amount']+$ProfitLoss['Gross']['Right'];
		if($SecondLeft>$SecondRight)
		{
			$ProfitLoss['Second']['Right']=$SecondLeft-$SecondRight;
			$ProfitLoss['Net']['Right']=$SecondLeft-$SecondRight;
		}
		else
		{
			$ProfitLoss['Second']['Left']=$SecondRight-$SecondLeft;
			$ProfitLoss['Net']['Left']=$SecondRight-$SecondLeft;
		}
		$ProfitLoss['SecondTotal']['Left']+=$ProfitLoss['Net']['Left']+$SecondLeft;
		$ProfitLoss['SecondTotal']['Right']+=$ProfitLoss['Net']['Right']+$SecondRight;
		return $ProfitLoss;
	}
	public function profit_loss_calculator_route($from_date,$to_date,$route_id=null)
	{
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$ProfitLoss['result']='Success';
		$ProfitLoss['CostofSale']=$this->CostCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Income']=$this->IncomeCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Expense']=$this->ExpenseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Purchase']=$this->PurchaseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Cost']=$this->PurchaseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Sale']=$this->SaleCalculator($from_date,$to_date,$route_id);
		$Product=$this->Product_id_list_function($from_date,$to_date);
		$GSTReport=['total'=>[]];
		$gst_in=0;
		$gst_out=0;
		$total_in=0;
		$total_out=0;
		$GSTReport['total']['out']=0;
		$GSTReport['total']['in']=0;
		$tax_amount['left']=0;
		$tax_amount['right']=0;
		$ProfitLoss['Expense']['IndirectExpense']['amount']+=$tax_amount['left'];
		$ProfitLoss['Income']['IndirectIncome']['amount']+=$tax_amount['right'];
		$IndirectExpense_gst_array['name']='tax';
		$IndirectExpense_gst_array['PrePaid']=0;
		$IndirectExpense_gst_array['paid']=0;
		$IndirectExpense_gst_array['Outstanding']=$tax_amount['left'];
		$IndirectExpense_gst_array['total']=$tax_amount['left'];
		$ProfitLoss['Expense']['IndirectExpense']['single']['TAX']=$IndirectExpense_gst_array;
		$IndirectIncome_gst_array['name']='tax';
		$IndirectIncome_gst_array['Received']=0;
		$IndirectIncome_gst_array['Advance']=0;
		$IndirectIncome_gst_array['Accrued']=$tax_amount['right'];
		$IndirectIncome_gst_array['Total']=$tax_amount['right'];
		$ProfitLoss['Income']['IndirectIncome']['single']['TAX']=$IndirectIncome_gst_array;
		$ProfitLoss['First']['Left']=0;
		$ProfitLoss['First']['Right']=0;
		$ProfitLoss['Second']['Left']=0;
		$ProfitLoss['Second']['Right']=0;
		$ProfitLoss['FirstTotal']['Left']=0;
		$ProfitLoss['FirstTotal']['Right']=0;
		$ProfitLoss['Gross']['Left']=0;
		$ProfitLoss['Gross']['Right']=0;
		$ProfitLoss['Net']['Right']=0;
		$ProfitLoss['Net']['Left']=0;
		$ProfitLoss['SecondTotal']['Left']=0;
		$ProfitLoss['SecondTotal']['Right']=0;
		$FirstLeft=$ProfitLoss['CostofSale']['costValue']+$ProfitLoss['Expense']['DirectExpense']['amount']+$tax_amount['right'];
		$FirstLeft=number_format($FirstLeft,4,'.','');
		$FirstRight=$ProfitLoss['Sale']['SaleValue']-$ProfitLoss['Sale']['SalesReturn']+$ProfitLoss['Income']['DirectIncome']['amount']+$tax_amount['left'];
		$FirstRight=number_format($FirstRight,4,'.','');
		if($FirstLeft>$FirstRight)
		{
			$ProfitLoss['First']['Right']=number_format(($FirstLeft-$FirstRight),3,'.','');
			$ProfitLoss['Gross']['Left']=number_format(($FirstLeft-$FirstRight),3,'.','');
		}
		else
		{
			$ProfitLoss['First']['Left']=number_format(($FirstRight-$FirstLeft),3,'.','');
			$ProfitLoss['Gross']['Right']=number_format(($FirstRight-$FirstLeft),3,'.','');
		}
		$ProfitLoss['FirstTotal']['Left']+=$FirstLeft+$ProfitLoss['First']['Left'];
		$ProfitLoss['FirstTotal']['Right']+=$FirstRight+$ProfitLoss['First']['Right'];
		$SecondLeft=$ProfitLoss['Expense']['IndirectExpense']['amount']+$ProfitLoss['Gross']['Left'];
		$SecondRight=$ProfitLoss['Income']['IndirectIncome']['amount']+$ProfitLoss['Gross']['Right'];
		if($SecondLeft>$SecondRight)
		{
			$ProfitLoss['Second']['Right']=$SecondLeft-$SecondRight;
			$ProfitLoss['Net']['Right']=$SecondLeft-$SecondRight;
		}
		else
		{
			$ProfitLoss['Second']['Left']=$SecondRight-$SecondLeft;
			$ProfitLoss['Net']['Left']=$SecondRight-$SecondLeft;
		}
		$ProfitLoss['SecondTotal']['Left']+=$ProfitLoss['Net']['Left']+$SecondLeft;
		$ProfitLoss['SecondTotal']['Right']+=$ProfitLoss['Net']['Right']+$SecondRight;
		return $ProfitLoss;
	}
	public function ExpenseCalculator($from_date,$to_date,$route_id=null)
	{
		$group_id_indirect=13;
		$group_id_direct=12;
		$outstanding_group_id=20;
		$prepaid_group_id=5;
		$AccountingsController = new AccountingsController;
		$excluding_ids=['4','5','8',];
		$AccountingsController = new AccountingsController;
		$group=[$group_id_direct=>'DirectExpense',$group_id_indirect=>'IndirectExpense'];
		foreach ($group as $group_key => $group_value) {
			$All_Accounts_table_list=$AccountingsController->AccountHead_Table_ListByGroupId($group_key);
			$account_all=[];
			$Total['DirectExpense']=0;
			$Total['IndirectExpense']=0;
			$Total['PrePaid']=0;
			$Total['Outstanding']=0;
			foreach ($All_Accounts_table_list as $keyA => $valueA) {
				$single=[];
				if(!in_array($valueA['AccountHead']['id'], $excluding_ids))
				{
					$single['SubGroup']=$valueA['SubGroup']['name'];
					if(!isset($account_all[$single['SubGroup']]))
					{
						$account_all[$single['SubGroup']]['DirectExpense']=0;
						$account_all[$single['SubGroup']]['IndirectExpense']=0;
						$account_all[$single['SubGroup']]['PrePaid']=0;
						$account_all[$single['SubGroup']]['Outstanding']=0;
						$account_all[$single['SubGroup']]['total']=0;
						$account_all[$single['SubGroup']]['paid']=0;
					}
					$name=explode(' ',$valueA['AccountHead']['name']);
					array_pop($name);
					$name=implode(' ', $name);
					$expense_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id'],$from_date,$to_date,$route_id);
					if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])))
					{
						$expense_function['debit']+=$valueA['AccountHead']['opening_balance'];
					}
					$account_all[$single['SubGroup']][$group_value]+=$expense_function['debit'];
					$account_all[$single['SubGroup']]['paid']+=      $expense_function['debit'];
					$account_all[$single['SubGroup']]['total']+=     $expense_function['debit'];
					$Total[$group_value]+=                           $expense_function['debit'];
					$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
					if(!empty($PrePaid))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($PrePaid['AccountHead']['id'],$from_date,$to_date,$route_id);
						$PrePaid_amount=0;
						if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($PrePaid['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($PrePaid['AccountHead']['created_at'])))
						{
							$PrePaid_amount=$PrePaid['AccountHead']['opening_balance'];
						}
						$PrePaid_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['PrePaid']+=$PrePaid_amount;
// $account_all[$single['SubGroup']]['total']-=  $PrePaid_amount;
						$Total['PrePaid']+=                           $PrePaid_amount;
					}
					$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
					if(!empty($Outstanding))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Outstanding['AccountHead']['id'],$from_date,$to_date,$route_id);						
						$Outstanding_amount=0;
						if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($Outstanding['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Outstanding['AccountHead']['created_at'])))
						{
							$Outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
						}
						$account_all[$single['SubGroup']][$group_value]+$Outstanding['AccountHead']['opening_balance'];
						$account_all[$single['SubGroup']]['paid']+= $Outstanding_amount;
						$account_all[$single['SubGroup']]['total']+=$Outstanding_amount;
						$Total[$group_value]+=                      $Outstanding_amount;
						$Outstanding_amount+=$return_function['credit']-$return_function['debit'];
						$account_all[$single['SubGroup']]['Outstanding']+=$Outstanding_amount;
// $account_all[$single['SubGroup']]['paid']-=       $Outstanding_amount;
// $account_all[$single['SubGroup']]['total']+=      $Outstanding_amount;
						$Total['Outstanding']+=                           $Outstanding_amount;
					}				
				}
			}
			$return[$group_value]['amount']=$Total[$group_value];
// $return[$group_value]['amount']+=$Total['Outstanding'];
// $return[$group_value]['amount']-=$Total['PrePaid'];
			$return[$group_value]['single']=$account_all;
		}
		return $return;	
	}
	public function ExpenseCalculatorForBalansheet($from_date,$to_date)
	{
		$group_id_indirect=13;
		$group_id_direct=12;
		$outstanding_group_id=20;
		$prepaid_group_id=5;
		$AccountingsController = new AccountingsController;
		$excluding_ids=['4','5','8',];
		$AccountingsController = new AccountingsController;
		$group=[$group_id_direct=>'DirectExpense',$group_id_indirect=>'IndirectExpense'];
		foreach ($group as $group_key => $group_value) {
			$All_Accounts_table_list=$AccountingsController->AccountHead_Table_ListByGroupId($group_key);
			$account_all=[];
			$Total['DirectExpense']=0;
			$Total['IndirectExpense']=0;
			$Total['PrePaid']=0;
			$Total['Outstanding']=0;
			foreach ($All_Accounts_table_list as $keyA => $valueA) {
				$single=[];
				if(!in_array($valueA['AccountHead']['id'], $excluding_ids))
				{
					$single['SubGroup']=$valueA['SubGroup']['name'];
					if(!isset($account_all[$single['SubGroup']]))
					{
						$account_all[$single['SubGroup']]['DirectExpense']=0;
						$account_all[$single['SubGroup']]['IndirectExpense']=0;
						$account_all[$single['SubGroup']]['PrePaid']=0;
						$account_all[$single['SubGroup']]['Outstanding']=0;
						$account_all[$single['SubGroup']]['total']=0;
						$account_all[$single['SubGroup']]['paid']=0;
					}
					$name=explode(' ',$valueA['AccountHead']['name']);
					array_pop($name);
					$name=implode(' ', $name);
					$expense_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id'],$from_date,$to_date);
					if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])))
					{
						$expense_function['debit']+=$valueA['AccountHead']['opening_balance'];
					}
					$account_all[$single['SubGroup']][$group_value]+=$expense_function['debit'];
					$account_all[$single['SubGroup']]['paid']+=      $expense_function['debit'];
					$account_all[$single['SubGroup']]['total']+=     $expense_function['debit'];
					$Total[$group_value]+=                           $expense_function['debit'];
					$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
					if(!empty($PrePaid))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($PrePaid['AccountHead']['id'],$from_date,$to_date);
						$PrePaid_amount=$PrePaid['AccountHead']['opening_balance'];
						$PrePaid_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['PrePaid']+=$PrePaid_amount;
						$account_all[$single['SubGroup']]['total']-=  $PrePaid_amount;
						$Total['PrePaid']+=                           $PrePaid_amount;
					}
					$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
					if(!empty($Outstanding))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Outstanding['AccountHead']['id'],$from_date,$to_date);						
						$Outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
						$Outstanding_amount+=$return_function['credit']-$return_function['debit'];
						$account_all[$single['SubGroup']]['Outstanding']+=$Outstanding_amount;
						$account_all[$single['SubGroup']]['paid']-=       $Outstanding_amount;
// $account_all[$single['SubGroup']]['total']+=      $Outstanding_amount;
						$Total['Outstanding']+=                           $Outstanding_amount;
					}				
				}
			}
			$return[$group_value]['amount']=$Total[$group_value];
// $return[$group_value]['amount']+=$Total['Outstanding'];
			//$return[$group_value]['amount']-=$Total['PrePaid'];
		}
		return $return;	
	}
	public function IncomeCalculator($from_date,$to_date,$route_id=null)
	{
		$group_id_indirect=15;
		$group_id_direct=14;
		$advance_group_id=21;
		$paccrued_group_id=6;
		$master_group_id=6;
		$excluding_ids=['6','7','9'];
		$AccountingsController = new AccountingsController;
		$group=[$group_id_indirect=>'IndirectIncome',$group_id_direct=>'DirectIncome'];
		foreach ($group as $group_key => $group_value) {
			$All_Accounts_table_list=$AccountingsController->AccountHead_Table_ListByGroupId($group_key);
			$account_all=[];
			$Total['DirectIncome']=0;
			$Total['IndirectIncome']=0;
			$Total['Advance']=0;
			$Total['Accrued']=0;
			foreach ($All_Accounts_table_list as $keyA => $valueA) {
				$single=[];
				if(!in_array($valueA['AccountHead']['id'], $excluding_ids))
				{
					$single['SubGroup']=$valueA['SubGroup']['name'];
					if(!isset($account_all[$single['SubGroup']]))
					{
						$account_all[$single['SubGroup']]['DirectIncome']=0;
						$account_all[$single['SubGroup']]['IndirectIncome']=0;
						$account_all[$single['SubGroup']]['Advance']=0;
						$account_all[$single['SubGroup']]['Accrued']=0;
						$account_all[$single['SubGroup']]['Total']=0;
						$account_all[$single['SubGroup']]['Received']=0;
					}
					$name=explode(' ',$valueA['AccountHead']['name']);
					array_pop($name);
					$name=implode(' ', $name);
					$main_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id'],$from_date,$to_date,$route_id);
					$main_function['credit']+=$valueA['AccountHead']['opening_balance'];
					$account_all[$single['SubGroup']][$group_value]+=$main_function['credit'];
					$account_all[$single['SubGroup']]['Received']+=  $main_function['credit'];
					$account_all[$single['SubGroup']]['Total']+=     $main_function['credit'];
					$Total[$group_value]+=                           $main_function['credit'];
					$Advance=$this->AccountHead->findByName($name.' ADVANCE');
					if(!empty($Advance))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Advance['AccountHead']['id'],$from_date,$to_date,$route_id);
						$Advance_amount=$Advance['AccountHead']['opening_balance'];
						$Advance_amount+=$return_function['credit']-$return_function['debit'];
						$account_all[$single['SubGroup']]['Advance']+=$Advance_amount;
						$account_all[$single['SubGroup']]['Total']-=  $Advance_amount;
						$Total['Advance']+=                           $Advance_amount;
					}
					$Accrued=$this->AccountHead->findByName($name.' ACCRUED');
					if(!empty($Accrued))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Accrued['AccountHead']['id'],$from_date,$to_date,$route_id);
						$Accrued_amount=$Accrued['AccountHead']['opening_balance'];
						$Accrued_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['Accrued']+= $Accrued_amount;
						$account_all[$single['SubGroup']]['Received']-=$Accrued_amount;
// $account_all[$single['SubGroup']]['Total']+=   $Accrued_amount;
						$Total['Accrued']+=                            $Accrued_amount;
					}
				}
			}
			$return[$group_value]['amount']=$Total[$group_value];
// $return[$group_value]['amount']+=$Total['Accrued'];
			$return[$group_value]['amount']-=$Total['Advance'];
			$return[$group_value]['single']=$account_all;
		}
		return $return;	
	}
	public function IncomeCalculatorForBalansheet($from_date,$to_date)
	{
		$group_id_indirect=15;
		$group_id_direct=14;
		$advance_group_id=21;
		$paccrued_group_id=6;
		$master_group_id=6;
		$excluding_ids=['6','7','9'];
		$AccountingsController = new AccountingsController;
		$group=[$group_id_indirect=>'IndirectIncome',$group_id_direct=>'DirectIncome'];
		foreach ($group as $group_key => $group_value) {
			$All_Accounts_table_list=$AccountingsController->AccountHead_Table_ListByGroupId($group_key);
			$account_all=[];
			$Total['DirectIncome']=0;
			$Total['IndirectIncome']=0;
			$Total['Advance']=0;
			$Total['Accrued']=0;
			foreach ($All_Accounts_table_list as $keyA => $valueA) {
				$single=[];
				if(!in_array($valueA['AccountHead']['id'], $excluding_ids))
				{
					$single['SubGroup']=$valueA['SubGroup']['name'];
					if(!isset($account_all[$single['SubGroup']]))
					{
						$account_all[$single['SubGroup']]['DirectIncome']=0;
						$account_all[$single['SubGroup']]['IndirectIncome']=0;
						$account_all[$single['SubGroup']]['Advance']=0;
						$account_all[$single['SubGroup']]['Accrued']=0;
						$account_all[$single['SubGroup']]['Total']=0;
						$account_all[$single['SubGroup']]['Received']=0;
					}
					$name=explode(' ',$valueA['AccountHead']['name']);
					array_pop($name);
					$name=implode(' ', $name);
					$main_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id'],$from_date,$to_date);
					$main_function['credit']+=$valueA['AccountHead']['opening_balance'];
					$account_all[$single['SubGroup']][$group_value]+=$main_function['credit'];
					$account_all[$single['SubGroup']]['Received']+=  $main_function['credit'];
					$account_all[$single['SubGroup']]['Total']+=     $main_function['credit'];
					$Total[$group_value]+=                           $main_function['credit'];
					$Advance=$this->AccountHead->findByName($name.' ADVANCE');
					if(!empty($Advance))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Advance['AccountHead']['id'],$from_date,$to_date);
						$Advance_amount=$Advance['AccountHead']['opening_balance'];
						$Advance_amount+=$return_function['credit']-$return_function['debit'];
						$account_all[$single['SubGroup']]['Advance']+=$Advance_amount;
						$account_all[$single['SubGroup']]['Total']-=  $Advance_amount;
						$Total['Advance']+=                           $Advance_amount;
					}
					$Accrued=$this->AccountHead->findByName($name.' ACCRUED');
					if(!empty($Accrued))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Accrued['AccountHead']['id'],$from_date,$to_date);
						$Accrued_amount=$Accrued['AccountHead']['opening_balance'];
						$Accrued_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['Accrued']+= $Accrued_amount;
						$account_all[$single['SubGroup']]['Received']-=$Accrued_amount;
// $account_all[$single['SubGroup']]['Total']+=   $Accrued_amount;
						$Total['Accrued']+=                            $Accrued_amount;
					}
				}
			}
			$return[$group_value]['amount']=$Total[$group_value];
// $return[$group_value]['amount']+=$Total['Accrued'];
			$return[$group_value]['amount']-=$Total['Advance'];
		}
		return $return;	
	}
	public function PurchaseCalculator($from_date,$to_date,$route_id=null)
	{
		$purchase_account_head_id=5;
		$purchase_return_account_head_id=7;
		$gst_on_purchase_account_head_id=8;
		$return['PurchaseValue']=0;
		$return['PurchaseReturn']=0;
		$Purchase_AccountHead=$this->AccountHead->findById($purchase_account_head_id);
		$Purchase_Return_AccountHead=$this->AccountHead->findById($purchase_return_account_head_id);
		$PurchaseValue=$this->General_Journal_Debit_N_Credit_function($purchase_account_head_id,$from_date,$to_date,$route_id);
// $PurchaseValue['debit']+=$Purchase_AccountHead['AccountHead']['opening_balance'];
		$GstOnPurchase=$this->General_Journal_Debit_N_Credit_function($gst_on_purchase_account_head_id,$from_date,$to_date,$route_id);
		$return['PurchaseValue']=$PurchaseValue['debit'];
		$return['GstOnPurchase']=$GstOnPurchase;
		$PurchaseReturn=$this->General_Journal_Debit_N_Credit_function($purchase_return_account_head_id,$from_date,$to_date,$route_id);
// $PurchaseReturn['credit']+=$Purchase_Return_AccountHead['AccountHead']['opening_balance'];
		$return['PurchaseReturn']=$PurchaseReturn['credit'];
		return $return;
	}
	public function SaleCalculator($from_date,$to_date,$route_id=null)
	{
		$sale_account_head_id=6;
		$sale_return_account_head_id=4;
		$gst_on_sale_account_head_id=9;
		$return['SaleValue']=0;
		$return['SalesReturn']=0;
		$Sale_AccountHead=$this->AccountHead->findById($sale_account_head_id);
		$Sale_Return_AccountHead=$this->AccountHead->findById($sale_return_account_head_id);
		$SaleValue=$this->General_Journal_Debit_N_Credit_function($sale_account_head_id,$from_date,$to_date,$route_id);
		if(!empty($route_id))
		{
		$SaleValue=$this->General_Journal_Debit_N_Credit_function_route($sale_account_head_id,$from_date,$to_date,$route_id);
	    }
// $SaleValue['credit']+=$Sale_AccountHead['AccountHead']['opening_balance'];
		$GstOnSale=$this->General_Journal_Debit_N_Credit_function($gst_on_sale_account_head_id,$from_date,$to_date,$route_id);
		$return['SaleValue']=$SaleValue['credit'];
		$return['GstOnSale']=$GstOnSale;
		$SalesReturn=$this->General_Journal_Debit_N_Credit_function($sale_return_account_head_id,$from_date,$to_date,$route_id);
		if(!empty($route_id))
		{
		$SalesReturn=$this->General_Journal_Debit_N_Credit_function_route($sale_return_account_head_id,$from_date,$to_date,$route_id);
	    }
// $SalesReturn['debit']+=$Sale_Return_AccountHead['AccountHead']['opening_balance'];
		$return['SalesReturn']=$SalesReturn['debit'];
		return $return;
	}
	public function General_Journal_Debit_N_Credit_function($account_head_id,$from_date,$to_date,$route_id=null)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$debit_conditions=[];
		$credit_conditions=[];
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$debit_conditions['Journal.debit']=$account_head_id;
		$debit_conditions['Journal.flag']=1;
		$debit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
			$debit_conditions['Journal.route_id']=$route_id;
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>$debit_conditions,
			'fields'=>array( 'Journal.total_amount'),
			));
		$credit_conditions['Journal.credit']=$account_head_id;
		$credit_conditions['Journal.flag']=1;
		$credit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
			$credit_conditions['Journal.route_id']=$route_id;
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>$credit_conditions,
			'fields'=>array( 'Journal.total_amount'),
			));
		$account_single['credit']=round($Journal_Credit['Journal']['total_amount'],3);
		$account_single['debit']=round($Journal_Debit['Journal']['total_amount'],3);
		return $account_single;
	}
	public function General_Journal_Debit_N_Credit_function_route($account_head_id,$from_date,$to_date,$route_id=null)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$debit_conditions=[];
		$credit_conditions=[];$sale=0;$sale_return=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$debit_conditions['Journal.debit']=$account_head_id;
		$debit_conditions['Journal.flag']=1;
		$debit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
		    $Customer=$this->Customer->find('all',array('conditions'=>array('Customer.route_id'=>$route_id),'fields'=>'Customer.account_head_id'));
		    foreach ($Customer as $key => $value) {
		    		$debit_conditions['Journal.credit']=$value['Customer']['account_head_id'];
		    	$Journal_Debit1=$this->Journal->find('first',array(
			'conditions'=>$debit_conditions,
			'fields'=>array( 'Journal.total_amount'),
			));
		    	$sale_return+=$Journal_Debit1['Journal']['total_amount'];
		    }
		$credit_conditions['Journal.credit']=$account_head_id;
		$credit_conditions['Journal.flag']=1;
		$credit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
		    foreach ($Customer as $key => $value1) {
		    	$credit_conditions['Journal.debit']=$value1['Customer']['account_head_id'];
		    	$Journal_Credit1=$this->Journal->find('first',array(
			'conditions'=>$credit_conditions,
			'fields'=>array( 'Journal.total_amount'),
			));
		    	$sale+=$Journal_Credit1['Journal']['total_amount'];
		    }
		$account_single['credit']=round($sale,3);
		$account_single['debit']=round($sale_return,3);
		return $account_single;
	}
	public function General_Journal_Debit_N_Credit_function_route_cost_sale($account_head_id,$from_date,$to_date,$route_id=null)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$debit_conditions=[];
		$credit_conditions=[];$sale=0;$sale_return=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		//$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		// $customer_condition=[];
		// if(!empty($route_id))
		// {
  //    $customer_condition['Customer.route_id']=$route_id;
		// }
  //        $Customer=$this->Customer->find('all',array('conditions'=>$customer_condition,'fields'=>'Customer.account_head_id'));
		   $Customer=$this->Customer->find('all',array('conditions'=>array('Customer.route_id'=>$route_id),'fields'=>'Customer.account_head_id'));
		$credit_conditions['Journal.credit']=$account_head_id;
		$credit_conditions['Journal.flag']=1;
		$credit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
		    foreach ($Customer as $key => $value1) {
		    	$credit_conditions['Journal.debit']=$value1['Customer']['account_head_id'];
		    	$Journal_Credit=$this->Journal->find('all',array(
			'conditions'=>$credit_conditions,
			'fields'=>array( 'Journal.remarks'),
			));
		    	if(!empty($Journal_Credit))
		    	{
		    	foreach ($Journal_Credit as $key => $value2) {
		    		  $remarks=$value2['Journal']['remarks'];
                      // pr($remarks);
		    	$conditions=[];
		       $conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		    	$conditions['Journal.flag']=1;
		    	$conditions['Journal.remarks']=$remarks;
		    	$conditions['Journal.credit']=19;
		    
		    	$Journal=$this->Journal->find('all',array(
			'conditions'=>$conditions,
			'fields'=>array( 'Journal.amount'),
			));
		    foreach ($Journal as $key => $value3) {
		    	$sale+=$value3['Journal']['amount'];
		    }

		    	}
		    	
		    		}
		    	}
//pr($Journal);
		 
		$account_single['credit']=round($sale,3);
		return $account_single;
	}
	public function General_Journal_Openging_Debit_N_Credit_function($account_head_id,$from_date)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$account_head_id,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'credit'=>$account_head_id,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$account_single['credit']=round($Journal_Credit['Journal']['total_amount'],3);
		$account_single['debit']=round($Journal_Debit['Journal']['total_amount'],3);
		return $account_single;
	}
	public function ReportTrialBalance()
	{
		$PermissionList=$this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/ReportTrialBalance'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			$from_date=date('1-m-Y');
			$to_date=date('d-m-Y');
			//$from_date=date('d-m-Y',strtotime(date('Y').'-04-01'));
			//$to_date=date('d-m-Y');
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
		}
	}
	public function ReportTrialBalance_ajax()
	{
		$from_date=date('d-m-Y',strtotime($this->request->data['from_date']));
		$to_date=date('d-m-Y',strtotime($this->request->data['to_date']));
		$TrialBalance=$this->ReportTrialBalancecalculator($from_date,$to_date);
		$TrialBalance['Total']['Left']=$TrialBalance['Asset']+$TrialBalance['Expense'];
		$TrialBalance['Total']['Right']=$TrialBalance['Liabilities']+$TrialBalance['Capital']+$TrialBalance['Income'];
		echo json_encode($TrialBalance);
		exit;
	}
	public function ReportTrialBalancecalculator($from_date,$to_date)
	{
		$return['result']='empty';
		$Type_ids=[
		'1'=>'Asset',
		'2'=>'Capital',
		'3'=>'Expense',
		'4'=>'Income',
		'5'=>'Liabilities',
		];
		$return['Asset']['amount']=0;
		$return['Capital']['amount']=0;
		$return['Expense']['amount']=0;
		$return['Income']['amount']=0;
		$return['Liabilities']['amount']=0;
		foreach ($Type_ids as $id => $name) {
			$AccoutHead_list=$this->AccountHead_list_By_Type_Id($id);
			foreach ($AccoutHead_list as $key => $value) {
				$AccountHead=$this->AccountHead->find('first',[
					'joins'=>array(
						array(
							'table'=>'groups',
							'alias'=>'Group',
							'type'=>'INNER',
							'conditions'=>array('Group.id=SubGroup.group_id')
							),
						array(
							'table'=>'master_groups',
							'alias'=>'MasterGroup',
							'type'=>'INNER',
							'conditions'=>array('MasterGroup.id=Group.master_group_id')
							),
						array(
							'table'=>'types',
							'alias'=>'Type',
							'type'=>'INNER',
							'conditions'=>array('MasterGroup.type_id=Type.id')
							),
						),
					'conditions'=>array('AccountHead.id'=>$key),
					'fields'=>array(
						'AccountHead.id',
						'AccountHead.name',
						'AccountHead.opening_balance',
						'SubGroup.id',
						'SubGroup.name',
						'Group.id',
						'Group.name',
						'MasterGroup.id',
						'MasterGroup.name',
						'Type.id',
						'Type.name',
						),
					]);
				$return[$name]['amount']+=$AccountHead['AccountHead']['opening_balance'];
				$function_value_return=$this->General_Journal_Debit_N_Credit_function($key,$from_date,$to_date);
				if($name=='Asset')
				{
					$return[$name]['amount']+=$function_value_return['debit']-$function_value_return['credit'];
					$single_data['name']=$function_value_return['name'];
					$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
					$single_data['single_amount']+=$function_value_return['debit']-$function_value_return['credit'];
					$return[$name]['single'][]=$single_data;
				}
				if($name=='Liabilities')
				{
					$return[$name]['amount']+=$function_value_return['credit']-$function_value_return['debit'];
					$single_data['name']=$function_value_return['name'];
					$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
					$single_data['single_amount']+=$function_value_return['credit']-$function_value_return['debit'];
					$return[$name]['single'][]=$single_data;
				}
				if($name=='Income')
				{
					$return[$name]['amount']+=$function_value_return['credit'];
					$single_data['name']=$function_value_return['name'];
					$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
					$single_data['single_amount']+=$function_value_return['credit'];
					$return[$name]['single'][]=$single_data;
				}
				if($name=='Expense')
				{
					$return[$name]['amount']+=$function_value_return['debit'];
					$single_data['name']=$function_value_return['name'];
					$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
					$single_data['single_amount']+=$function_value_return['debit'];
					$return[$name]['single'][]=$single_data;
				}
				if($name=='Capital')
				{
					$AccountHead_Name=$AccountHead['AccountHead']['name'];
					$AccountHead_Name_split=explode(' ',$AccountHead_Name);
					if($AccountHead_Name_split[1]=='CAPITAL')
					{
						$return[$name]['amount']+=$function_value_return['credit']-$function_value_return['debit'];
						$single_data['name']=$function_value_return['name'];
						$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
						$single_data['single_amount']+=$function_value_return['credit']-$function_value_return['debit'];
						$return[$name]['single'][]=$single_data;
					}
					else
					{
						$return[$name]['amount']-=$function_value_return['debit']-$function_value_return['credit'];
						$single_data['name']=$function_value_return['name'];
						$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
						$single_data['single_amount']+=$function_value_return['debit']-$function_value_return['credit'];
						$return[$name]['single'][]=$single_data;
					}
				}
			}
			$return['result']='Success';
		}
		return $return;
	}
	public function AccountHead_list_By_Type_Id($type_id)
	{
		$AccountHead=$this->AccountHead->find('all',[
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				array(
					'table'=>'master_groups',
					'alias'=>'MasterGroup',
					'type'=>'INNER',
					'conditions'=>array('MasterGroup.id=Group.master_group_id')
					),
				array(
					'table'=>'types',
					'alias'=>'Type',
					'type'=>'INNER',
					'conditions'=>array('MasterGroup.type_id=Type.id')
					),
				),
			'conditions'=>array('Type.id'=>$type_id),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
// 'SubGroup.id',
// 'SubGroup.name',
// 'Group.id',
// 'Group.name',
// 'MasterGroup.id',
// 'MasterGroup.name',
				'Type.id',
				'Type.name',
				),
			]);
		$AccountHead_list=[];
		foreach ($AccountHead as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
		}
		return $AccountHead_list;
	}
	public function AccountHead_list_By_SubGroup_id($sub_group_id)
	{
		$data=$this->AccountHead->find('list',[
			'conditions'=>array('AccountHead.sub_group_id'=>$sub_group_id),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				),
			]);
		return $data;
	}
	public function BalanceSheet()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/BalanceSheet'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			$from_date=date('1-m-Y');
			$to_date=date('d-m-Y');
			//$from_date=date('d-m-Y',strtotime(date('Y').'-04-01'));
			//$to_date=date('d-m-Y',strtotime('+1 day'));
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
		}
	}
	public function BalanceSheet_ajax()
	{
		$from_date=date('d-m-Y',strtotime($this->request->data['from_date']));
		$to_date=date('d-m-Y',strtotime($this->request->data['to_date']));
		$BalanceSheet=$this->ReportTrialBalancecalculator($from_date,$to_date);
		$ProfitLoss=$this->profit_loss_calculator($from_date,$to_date);
		$BalanceSheet['Profit']=$ProfitLoss['Net']['Left'];
		$BalanceSheet['Loss']=$ProfitLoss['Net']['Right'];
		$BalanceSheet['Total']['Left']=$BalanceSheet['Capital']+$BalanceSheet['Liabilities']+$ProfitLoss['Net']['Left']-$ProfitLoss['Net']['Right'];
		$BalanceSheet['Total']['Right']=$BalanceSheet['Asset'];
		echo json_encode($BalanceSheet);
		exit;
	}
	public function DayBook()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/DayBook'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		if(!$this->request->data)
		{
			$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$this->request->data['Reports']['from_date']=$from_date;
			$this->request->data['Reports']['to_date']=$to_date;
		}
	}
	public function DayBook_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Journal.id';
		$columns[]='Journal.date';
		$columns[]='Journal.work_flow';
		$columns[]='AccountHeadCredit.name';
		$columns[]='AccountHeadDebit.name';
		$columns[]='Journal.amount';
		$columns[]='Journal.remarks';
		$columns[]='Journal.voucher_no';
		$columns[]='Journal.voucher_no';
		$conditions=[];
		$conditions['Journal.flag']=1;
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		$conditions['Journal.date between ? and ?']=[$from_date,$to_date];
		$totalData=$this->Journal->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Journal.id LIKE' =>'%'. $q . '%',
				'Journal.date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'Journal.work_flow LIKE' =>'%'. $q . '%',
				'AccountHeadCredit.name LIKE' =>'%'. $q . '%',
				'AccountHeadDebit.name LIKE' =>'%'. $q . '%',
				'Journal.amount LIKE' =>'%'. $q . '%',
				'Journal.remarks LIKE' =>'%'. $q . '%',
				'Journal.voucher_no LIKE' =>'%'. $q . '%',
				);
			$totalFiltered=$this->Journal->find('count',[
				'conditions'=>$conditions,
				]);
		}
		$Data=$this->Journal->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Journal.*',
				'AccountHeadCredit.name',
				'AccountHeadDebit.name',
				)
			));
		foreach ($Data as $key => $value) {
			$Data[$key]['Journal']['date']=date('d-m-Y',strtotime($value['Journal']['date']));
			$Data[$key]['Journal']['amount']=floatval($value['Journal']['amount']);
			$Data[$key]['Journal']['action']='<span hidden class="journal_id">'.$value['Journal']['id'].'</span><span><a><i class="fa fa-trash fa daybook_delete"></i></a></span>';
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	}
	public function Ledger()
	{
		$user_id=1;
		if(!$this->request->data)
		{
			$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
			$AccountHead=$this->AccountHead->find('list',array('order'=>['AccountHead.id DESC']));
			$this->set(compact('AccountHead'));
		}
	}
	public function StockPurchaseReport()
	{
		$ProductType=$this->ProductType->find('list');
		$this->set(compact('ProductType'));
		$Product=$this->Product->find('list');
		$this->set(compact('Product'));
	}
	public function Get_All_GSTReportForBalanceSheet($products,$from_date,$to_date,$route_id=null)
	{
		$single_GSTReport=[];
		$all_GSTReport=[];
		$single_GSTReport['tax']['out']=0;
		$single_GSTReport['tax']['in']=0;
		$this->PurchasedItem->virtualFields=['total_tax_amount'=>"SUM(PurchasedItem.tax_amount)"];
		$PurchasedItem=$this->PurchasedItem->find('first',array(
			'conditions'=>array(
				'Purchase.status=2',
				'Product.id'=>$products,
				'Purchase.date_of_delivered between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
				),
			'fields'=>array('PurchasedItem.total_tax_amount'),
			));
		$single_GSTReport['tax']['in']+=$PurchasedItem['PurchasedItem']['total_tax_amount'];;
		$this->PurchaseReturnItem->virtualFields=['total_tax_amount'=>"SUM(PurchaseReturnItem.tax_amount)"];
		$PurchaseReturnItem=$this->PurchaseReturnItem->find('first',array(
			'conditions'=>array(
				'PurchaseReturn.status=2',
				'Product.id'=>$products,
				'PurchaseReturn.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
				),
			'fields'=>array('PurchaseReturnItem.total_tax_amount'),
			));
		$single_GSTReport['tax']['out']+=$PurchaseReturnItem['PurchaseReturnItem']['total_tax_amount'];
		$this->SaleItem->virtualFields=['total_tax_amount'=>"SUM(SaleItem.tax_amount)"];
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.status=2',
				'Product.id'=>$products,
				'Sale.date_of_delivered between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
				),
			'fields'=>array('SaleItem.total_tax_amount'),
			));
		$single_GSTReport['tax']['out']+=$SaleItem['SaleItem']['total_tax_amount'];
		$this->SalesReturnItem->virtualFields=['total_tax_amount'=>"SUM(SalesReturnItem.tax_amount)"];
		$SalesReturnItem=$this->SalesReturnItem->find('first',array(
			'conditions'=>array(
				'SalesReturn.status=2',
				'Product.id'=>$products,
				'SalesReturn.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
				),
			'fields'=>array('SalesReturnItem.total_tax_amount'),
			));
		$single_GSTReport['tax']['in']+=$SalesReturnItem['SalesReturnItem']['total_tax_amount'];
		return $single_GSTReport;
	}
	public function GSTReport()
	{
		$data['GSTReport']['from_date']=$from_date=date('d-m-Y',strtotime('-1 month'));
		$data['GSTReport']['to_date']=$to_date=date('d-m-Y');
		$GST_RATE=array('0'=>'0 %','5'=>'5 %','12'=>'12 %','18'=>'18 %','Others'=>'Others %');
		$this->set(compact('GST_RATE'));
		$this->request->data=$data;
		$ProductType=$this->ProductType->find('list');
		$this->set(compact('ProductType'));
		$Product=$this->Product->find('list');
		$this->set(compact('Product'));
		$GSTReport=['list'=>[],'total'=>[]];
		$cgst_in=0;
		$cgst_out=0;
		$sgst_in=0;
		$sgst_out=0;
		$igst_in=0;
		$igst_out=0;
		$total_in=0;
		$total_out=0;
		foreach ($Product as $key => $value) {
			$single_GSTReport=$this->get_GSTReport($key,$from_date,$to_date);
			if($single_GSTReport)
			{
				foreach ($single_GSTReport as $keyG => $valueG) {
					if($valueG['total']['in'] || $valueG['total']['out'])
					{
						$cgst_out+=$valueG['cgst']['out'];
						$cgst_in+=$valueG['cgst']['in'];
						$sgst_out+=$valueG['sgst']['out'];
						$sgst_in+=$valueG['sgst']['in'];
						$igst_out+=$valueG['igst']['out'];
						$igst_in+=$valueG['igst']['in'];
						$GSTReport['list'][]=$valueG;
					}
				}
			}
			$GSTReport['total']['cgst']['in']=$cgst_in;
			$GSTReport['total']['cgst']['out']=$cgst_out;
			$GSTReport['total']['sgst']['in']=$sgst_in;
			$GSTReport['total']['sgst']['out']=$sgst_out;
			$GSTReport['total']['igst']['in']=$igst_in;
			$GSTReport['total']['igst']['out']=$igst_out;
			$GSTReport['total']['total']['in']=$cgst_in+$sgst_in+$igst_in;
			$GSTReport['total']['total']['out']=$cgst_out+$sgst_out+$igst_out;
		}
		$this->set(compact('GSTReport'));
	}
	public function GSTReport_ajax()
	{
		$data=$this->request->data;
		$conditions=[];
		if($data['product_type_id'])
		{
			$product_type_id=$data['product_type_id'];	
			$conditions['Product.product_type_id']=$product_type_id;
		}
		if($data['product_id'])
		{
			$product_id=$data['product_id'];	
			$conditions['Product.id']=$product_id;
		}
		$from_date=$data['from_date'];
		$to_date=$data['to_date'];
		$GSTReport=['list'=>[],'total'=>[]];
		$cgst_in=0;
		$cgst_out=0;
		$sgst_in=0;
		$sgst_out=0;
		$igst_in=0;
		$igst_out=0;
		$total_in=0;
		$total_out=0;
		$Product=$this->Product->find('all',array(
			'conditions'=>$conditions,
			));
		foreach ($Product as $key => $value) {
			$single_GSTReport=$this->get_GSTReport($key,$from_date,$to_date);
			if($single_GSTReport)
			{
				if($single_GSTReport['total']['in'] || $single_GSTReport['total']['out'])
				{
					$cgst_out+=$single_GSTReport['cgst']['out'];
					$cgst_in+=$single_GSTReport['cgst']['in'];
					$sgst_out+=$single_GSTReport['sgst']['out'];
					$sgst_in+=$single_GSTReport['sgst']['in'];
					$igst_out+=$single_GSTReport['igst']['out'];
					$igst_in+=$single_GSTReport['igst']['in'];
					$GSTReport['list'][]=$single_GSTReport;
				}
			}
			$GSTReport['total']['cgst']['in']=$cgst_in;
			$GSTReport['total']['cgst']['out']=$cgst_out;
			$GSTReport['total']['sgst']['in']=$sgst_in;
			$GSTReport['total']['sgst']['out']=$sgst_out;
			$GSTReport['total']['igst']['in']=$igst_in;
			$GSTReport['total']['igst']['out']=$igst_out;
			$GSTReport['total']['total']['in']=$cgst_in+$sgst_in+$igst_in;
			$GSTReport['total']['total']['out']=$cgst_out+$sgst_out+$igst_out;
		}
		echo json_encode($GSTReport);
		exit;
	}
	public function get_GSTReport($product_id,$from_date,$to_date,$gst=null,$type=null,$product_type_id=null)
	{
		$profile_state_id=$this->Global_Var_Profile['State']['id'];
		$Product=$this->Product->findById($product_id);
		$single_GSTReport=[];
		$all_GSTReport=[];
		$single_GSTReport['cgst']['out']=0;
		$single_GSTReport['cgst']['in']=0;
		$single_GSTReport['sgst']['out']=0;
		$single_GSTReport['sgst']['in']=0;
		$single_GSTReport['igst']['out']=0;
		$single_GSTReport['igst']['in']=0;
		$single_GSTReport['total']['out']=0;
		$single_GSTReport['total']['in']=0;
		if($Product)
		{
			$single_GSTReport['product']=$Product['Product']['name'];
			if($type=='Purchase' || !$type)
			{
				$conditions=[];
				$conditions['Purchase.status']=2;
				if($product_type_id)
				{
					$conditions['ProductType.id']=$product_type_id;
				}
				if($product_id)
				{
					$conditions['Product.id']=$product_id;
				}
				$conditions['Purchase.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
				if($gst)
				{
//$conditions['OR']['PurchasedItem.cgst']=$gst/2;
					$conditions['OR']['PurchasedItem.gst']=$gst;
				}
				if($gst=='Zero')
				{
					unset($conditions['OR']);
					$conditions['PurchasedItem.gst']=0;
//$conditions['PurchasedItem.gst']=0;
				}
//pr($conditions);exit;
				$PurchasedItem=$this->PurchasedItem->find('all',array(
					'joins'=>array(
						array(
							'table'=>'product_types',
							'alias'=>'ProductType',
							'type'=>'INNER',
							'conditions'=>array('ProductType.id=Product.product_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>array(
// 'PurchasedItem.cgst_amount',
// 'PurchasedItem.sgst_amount',
						'PurchasedItem.gst_amount',
						'PurchasedItem.gst',
// 'PurchasedItem.sgst',
// 'PurchasedItem.igst',
						'Purchase.date_of_delivered',
						'PurchasedItem.taxable_value',
						'Purchase.invoice_no',
						'Purchase.account_head_id',
						'Purchase.date_of_delivered',
						'ProductType.id',
						'Product.id',
						'ProductType.name',
						'Product.hsn_code',
						),
					));
				if($PurchasedItem)
				{
					foreach ($PurchasedItem as $keyP => $valueP) {
						$single_GSTReport['date']=date('d-m-Y',strtotime($valueP['Purchase']['date_of_delivered']));
						$single_GSTReport['product_type']=$valueP['ProductType']['name'];
						$single_GSTReport['hsn']=$valueP['Product']['hsn_code'];
						$single_GSTReport['type']='Purchase';
						$single_GSTReport['invoice_no']=$valueP['Purchase']['invoice_no'];
						$single_GSTReport['taxable_value']=$valueP['PurchasedItem']['taxable_value'];
						$account_head_id=$valueP['Purchase']['account_head_id'];
						$Party=$this->Party->findByAccountHeadId($account_head_id);
						$state_id=$Party['Party']['state_id'];
						if($profile_state_id==$state_id)
						{
							$single_GSTReport['cgst']['rate_percentage']=$valueP['PurchasedItem']['gst']/2;
							$single_GSTReport['cgst']['out']=$valueP['PurchasedItem']['gst_amount']/2;
							$single_GSTReport['sgst']['rate_percentage']=$valueP['PurchasedItem']['gst']/2;
							$single_GSTReport['sgst']['out']=$valueP['PurchasedItem']['gst_amount']/2;	
							$single_GSTReport['igst']['rate_percentage']=0;
							$single_GSTReport['igst']['out']=0;
						}
						else
						{
							$single_GSTReport['cgst']['rate_percentage']=0;
							$single_GSTReport['cgst']['out']=0;
							$single_GSTReport['sgst']['rate_percentage']=0;
							$single_GSTReport['sgst']['out']=0;
							$single_GSTReport['igst']['rate_percentage']=$valueP['PurchasedItem']['gst'];
							$single_GSTReport['igst']['out']=$valueP['PurchasedItem']['gst_amount'];
						}
						$single_GSTReport['total']['out']=$single_GSTReport['cgst']['out']+$single_GSTReport['sgst']['out']+$single_GSTReport['igst']['out'];
						$all_GSTReport[]=$single_GSTReport;
					}
				}
			}
			if($type=='Sale' || !$type)
			{
				$conditions=[];
				$conditions['Sale.status']=2;
				if($product_type_id)
				{
					$conditions['ProductType.id']=$product_type_id;
				}
				if($product_id)
				{
					$conditions['Product.id']=$product_id;
				}
				$conditions['Sale.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
				if($gst)
				{
//$conditions['OR']['SaleItem.cgst']=$gst/2;
					$conditions['OR']['SaleItem.gst']=$gst;
				}
				if($gst=='Zero')
				{
					unset($conditions['OR']);
//$conditions['SaleItem.cgst']=0;
					$conditions['SaleItem.gst']=0;
				}
				$SaleItem=$this->SaleItem->find('all',array(
					'joins'=>array(
						array(
							'table'=>'product_types',
							'alias'=>'ProductType',
							'type'=>'INNER',
							'conditions'=>array('ProductType.id=Product.product_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>array(
// 'SaleItem.cgst_amount',
// 'SaleItem.sgst_amount',
						'SaleItem.gst_amount',
// 'SaleItem.cgst',
// 'SaleItem.sgst',
						'SaleItem.gst',
						'SaleItem.taxable_value',
						'Sale.invoice_no',
						'Sale.account_head_id',
						'Sale.date_of_delivered',
						'ProductType.id',
						'ProductType.name',
						'Product.hsn_code',
						),
					));
// exit;
				if($SaleItem)
				{
					$single_GSTReport['cgst']['out']=0;
					$single_GSTReport['cgst']['in']=0;
					$single_GSTReport['sgst']['out']=0;
					$single_GSTReport['sgst']['in']=0;
					$single_GSTReport['igst']['out']=0;
					$single_GSTReport['igst']['in']=0;
					$single_GSTReport['total']['out']=0;
					$single_GSTReport['total']['in']=0;
					foreach ($SaleItem as $keyS => $valueS) {
						$single_GSTReport['date']=date('d-m-Y',strtotime($valueS['Sale']['date_of_delivered']));
						$single_GSTReport['product_type']=$valueS['ProductType']['name'];
						$single_GSTReport['hsn']=$valueS['Product']['hsn_code'];
						$single_GSTReport['type']='Sale';
						$single_GSTReport['invoice_no']=$valueS['Sale']['invoice_no'];
						$single_GSTReport['taxable_value']=$valueS['SaleItem']['taxable_value'];
						$account_head_id=$valueS['Sale']['account_head_id'];
						$Customer=$this->Customer->findByAccountHeadId($account_head_id);
						$state_id=$Customer['Customer']['state_id'];
						if($profile_state_id==$state_id)
						{
							$single_GSTReport['cgst']['rate_percentage']=$valueS['SaleItem']['gst']/2;
							$single_GSTReport['cgst']['in']=$valueS['SaleItem']['gst_amount']/2;
							$single_GSTReport['sgst']['rate_percentage']=$valueS['SaleItem']['gst']/2;
							$single_GSTReport['sgst']['in']=$valueS['SaleItem']['gst_amount']/2;	
							$single_GSTReport['igst']['rate_percentage']=0;
							$single_GSTReport['igst']['in']=0;
						}
						else
						{
							$single_GSTReport['cgst']['rate_percentage']=0;
							$single_GSTReport['cgst']['in']=0;
							$single_GSTReport['sgst']['rate_percentage']=0;
							$single_GSTReport['sgst']['in']=0;
							$single_GSTReport['igst']['rate_percentage']=$valueS['SaleItem']['gst'];
							$single_GSTReport['igst']['in']=$valueS['SaleItem']['gst_amount'];
						}
						$single_GSTReport['total']['in']=$single_GSTReport['cgst']['in']+$single_GSTReport['sgst']['in']+$single_GSTReport['igst']['in'];
						$all_GSTReport[]=$single_GSTReport;
					}
// 	if($SaleItem){
// pr($SaleItem);
// }
				}
			}
		}
		return $all_GSTReport;
	}
	public function get_VATReport($product_id,$from_date,$to_date,$gst=null,$type=null,$product_type_id=null)
	{
		$profile_state_id=$this->Global_Var_Profile['State']['id'];
		$Product=$this->Product->findById($product_id);
		$single_GSTReport=[];
		$all_GSTReport=[];
		$single_GSTReport['cgst']['out']=0;
		$single_GSTReport['cgst']['in']=0;
		$single_GSTReport['sgst']['out']=0;
		$single_GSTReport['sgst']['in']=0;
		$single_GSTReport['igst']['out']=0;
		$single_GSTReport['igst']['in']=0;
		$single_GSTReport['total']['out']=0;
		$single_GSTReport['total']['in']=0;
		if($Product)
		{
			$single_GSTReport['product']=$Product['Product']['name'];
			if($type=='Purchase' || !$type)
			{
				$conditions=[];
				$conditions['Purchase.status']=2;
				if($product_type_id)
				{
					$conditions['ProductType.id']=$product_type_id;
				}
				if($product_id)
				{
					$conditions['Product.id']=$product_id;
				}
				$conditions['Purchase.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
				if($gst)
				{
//$conditions['OR']['PurchasedItem.cgst']=$gst/2;
					$conditions['OR']['PurchasedItem.vat']=$gst;
				}
				if($gst=='Zero')
				{
					unset($conditions['OR']);
					$conditions['PurchasedItem.vat']=0;
//$conditions['PurchasedItem.gst']=0;
				}
//pr($conditions);exit;
				$PurchasedItem=$this->PurchasedItem->find('all',array(
					'joins'=>array(
						array(
							'table'=>'product_types',
							'alias'=>'ProductType',
							'type'=>'INNER',
							'conditions'=>array('ProductType.id=Product.product_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>array(
// 'PurchasedItem.cgst_amount',
// 'PurchasedItem.sgst_amount',
						'PurchasedItem.vat_amount',
						'PurchasedItem.vat',
// 'PurchasedItem.sgst',
// 'PurchasedItem.igst',
						'Purchase.date_of_delivered',
						'PurchasedItem.net_value',
						'Purchase.invoice_no',
						'Purchase.account_head_id',
						'Purchase.date_of_delivered',
						'ProductType.id',
						'Product.id',
						'ProductType.name',
						'Product.hsn_code',
						),
					));
				if($PurchasedItem)
				{
					foreach ($PurchasedItem as $keyP => $valueP) {
						$single_GSTReport['date']=date('d-m-Y',strtotime($valueP['Purchase']['date_of_delivered']));
						$single_GSTReport['product_type']=$valueP['ProductType']['name'];
						$single_GSTReport['hsn']=$valueP['Product']['hsn_code'];
						$single_GSTReport['type']='Purchase';
						$single_GSTReport['invoice_no']=$valueP['Purchase']['invoice_no'];
						$single_GSTReport['taxable_value']=$valueP['PurchasedItem']['net_value'];
						$account_head_id=$valueP['Purchase']['account_head_id'];
						$Party=$this->Party->findByAccountHeadId($account_head_id);
						$state_id=$Party['Party']['state_id'];
						$single_GSTReport['cgst']['rate_percentage']=0;
						$single_GSTReport['cgst']['out']=0;
						$single_GSTReport['sgst']['rate_percentage']=0;
						$single_GSTReport['sgst']['out']=0;
						$single_GSTReport['igst']['rate_percentage']=$valueP['PurchasedItem']['vat'];
						$single_GSTReport['igst']['out']=$valueP['PurchasedItem']['vat_amount'];
						$single_GSTReport['total']['out']=$single_GSTReport['cgst']['out']+$single_GSTReport['sgst']['out']+$single_GSTReport['igst']['out'];
						$all_GSTReport[]=$single_GSTReport;
					}
				}
			}
			if($type=='Sale' || !$type)
			{
				$conditions=[];
				$conditions['Sale.status']=2;
				if($product_type_id)
				{
					$conditions['ProductType.id']=$product_type_id;
				}
				if($product_id)
				{
					$conditions['Product.id']=$product_id;
				}
				$conditions['Sale.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
				if($gst)
				{
//$conditions['OR']['SaleItem.cgst']=$gst/2;
					$conditions['OR']['SaleItem.tax']=$gst;
				}
				if($gst=='Zero')
				{
					unset($conditions['OR']);
//$conditions['SaleItem.cgst']=0;
					$conditions['SaleItem.tax']=0;
				}
				$SaleItem=$this->SaleItem->find('all',array(
					'joins'=>array(
						array(
							'table'=>'product_types',
							'alias'=>'ProductType',
							'type'=>'INNER',
							'conditions'=>array('ProductType.id=Product.product_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>array(
// 'SaleItem.cgst_amount',
// 'SaleItem.sgst_amount',
						'SaleItem.tax_amount',
// 'SaleItem.cgst',
// 'SaleItem.sgst',
						'SaleItem.tax',
						'SaleItem.net_value',
						'Sale.invoice_no',
						'Sale.account_head_id',
						'Sale.date_of_delivered',
						'ProductType.id',
						'ProductType.name',
						'Product.hsn_code',
						),
					));
// exit;
				if($SaleItem)
				{
					$single_GSTReport['cgst']['out']=0;
					$single_GSTReport['cgst']['in']=0;
					$single_GSTReport['sgst']['out']=0;
					$single_GSTReport['sgst']['in']=0;
					$single_GSTReport['igst']['out']=0;
					$single_GSTReport['igst']['in']=0;
					$single_GSTReport['total']['out']=0;
					$single_GSTReport['total']['in']=0;
					foreach ($SaleItem as $keyS => $valueS) {
						$single_GSTReport['date']=date('d-m-Y',strtotime($valueS['Sale']['date_of_delivered']));
						$single_GSTReport['product_type']=$valueS['ProductType']['name'];
						$single_GSTReport['hsn']=$valueS['Product']['hsn_code'];
						$single_GSTReport['type']='Sale';
						$single_GSTReport['invoice_no']=$valueS['Sale']['invoice_no'];
						$single_GSTReport['taxable_value']=$valueS['SaleItem']['net_value'];
						$account_head_id=$valueS['Sale']['account_head_id'];
						$Customer=$this->Customer->findByAccountHeadId($account_head_id);
						$state_id=$Customer['Customer']['state_id'];
						$single_GSTReport['cgst']['rate_percentage']=0;
						$single_GSTReport['cgst']['in']=0;
						$single_GSTReport['sgst']['rate_percentage']=0;
						$single_GSTReport['sgst']['in']=0;
						$single_GSTReport['igst']['rate_percentage']=$valueS['SaleItem']['tax'];
						$single_GSTReport['igst']['in']=$valueS['SaleItem']['tax_amount'];
						$single_GSTReport['total']['in']=$single_GSTReport['cgst']['in']+$single_GSTReport['sgst']['in']+$single_GSTReport['igst']['in'];
						$all_GSTReport[]=$single_GSTReport;
					}
// 	if($SaleItem){
// pr($SaleItem);
// }
				}
			}
		}
		return $all_GSTReport;
	}
	public function stock_report_function()
	{
	}
	public function SaleItemWise()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Reports/SaleItemWise'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		$this->set('from',date("d-m-Y", strtotime('-1 day')));
		$this->set('to',date("d-m-Y"));
		$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'))));
		$this->set('Product',$this->Product->find('list',array('fields'=>array('id','name'))));

	}
	public function SaleItemWise_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Product.name';
		$columns[]='Sale.invoice_no';
		$columns[]='Sale.date_of_delivered';
		$columns[]='SaleItem.unit_price';
		$columns[]='SaleItem.quantity';
		$columns[]='SaleItem.tax_amount';
		$columns[]='SaleItem.total';
		$conditions=[];
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		if(!empty($requestData['product_id']))
		{
			if($requestData['product_id'])
				$conditions['Product.id']=$requestData['product_id'];	
		}
		if(!empty($requestData['product_type_id']))
		{
			if($requestData['product_type_id'])
				$conditions['ProductType.id']=$requestData['product_type_id'];	
		}
		$conditions['Sale.flag']=1;
		$conditions['Sale.status']=2;
		$conditions['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
		$totalData=$this->Product->find('count',array(
			"joins"=>array(
				array(
					"table"=>'sale_items',
					"alias"=>'SaleItem',
					"type"=>'INNER',
					"conditions"=>array('SaleItem.product_id=Product.id'),
					),
				array(
					"table"=>'sales',
					"alias"=>'Sale',
					"type"=>'INNER',
					"conditions"=>array('Sale.id=SaleItem.sale_id'),
					),
				),
			'conditions'=>$conditions,
			));

		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Product.name LIKE' =>'%'. $q . '%',
				'Sale.invoice_no LIKE' =>'%'. $q . '%',
				'Sale.date_of_delivered LIKE' =>'%'. $q . '%',
				'SaleItem.unit_price LIKE' =>'%'. $q . '%',
				'SaleItem.tax_amount LIKE' =>'%'. $q . '%',
				'SaleItem.total LIKE' =>'%'. $q . '%',
				);
			$totalFiltered=$this->Product->find('count',array(
				"joins"=>array(
					array(
						"table"=>'sale_items',
						"alias"=>'SaleItem',
						"type"=>'INNER',
						"conditions"=>array('SaleItem.product_id=Product.id'),
						),
					array(
						"table"=>'sales',
						"alias"=>'Sale',
						"type"=>'INNER',
						"conditions"=>array('Sale.id=SaleItem.sale_id'),
						),
					),
				'conditions'=>$conditions,
				));
		}

		$Data=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'sale_items',
					"alias"=>'SaleItem',
					"type"=>'INNER',
					"conditions"=>array('SaleItem.product_id=Product.id'),
					),
				array(
					"table"=>'sales',
					"alias"=>'Sale',
					"type"=>'INNER',
					"conditions"=>array('Sale.id=SaleItem.sale_id'),
					),
				),
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Product.name',
				'Sale.id',
				'Sale.invoice_no',
				'Sale.date_of_delivered',
				'SaleItem.unit_price',
				'SaleItem.quantity',
				'SaleItem.total',
				'SaleItem.tax_amount',
				)
			));
//pr($Data);
		foreach ($Data as $key => $value) {
			$Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
		}

		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        => $Data
			);
		echo json_encode($json_data);
		exit;

	}
	public function Summary()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Reports/Summary'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
// $from=date('Y-m-d', strtotime("-1 Year"));
		$from=date('Y-m-d', strtotime("now"));
		$to=date('Y-m-d', strtotime("now"));
		$SaleCompletedTotal=$this->SaleCompletedTotal($from,$to);
		$this->set('SaleTotal',$SaleCompletedTotal);
		$SaleSparePartTotal=$this->SaleSparePartTotal($from,$to);
		$this->set('SaleSparePartTotal',$SaleSparePartTotal);
		$SaleReturnSparePartTotal=$this->SaleReturnSparePartTotal($from,$to);
		$this->set('SaleReturnSparePartTotal',$SaleReturnSparePartTotal);
		$SaleReturnCompletedTotal=$this->SaleReturnCompletedTotal($from,$to);
		$this->set('SaleReturnTotal',$SaleReturnCompletedTotal);
// $LabourChargeTotal=$this->LabourChargeTotal($from,$to);
// $this->set('LabourChargeTotal',$LabourChargeTotal);
// $ServiceDiscountTotal=$this->ServiceDiscountTotal($from,$to);
// $this->set('ServiceAmountTotal',$ServiceDiscountTotal);
// $ServiceCompletedTotal=$this->ServiceCompletedTotal($from,$to);
// $this->set('ServiceTotal',$ServiceCompletedTotal);
// $ServiceCashTotal=$this->ServiceCashTotal($from,$to);
// $this->set('ServiceCashTotal',$ServiceCashTotal);
// $PainterCharge=$this->PainterCharge($from,$to);
// $this->set('PainterCharge',$PainterCharge);
// $ServiceSparePartTotal=$this->ServiceSparePartTotal($from,$to);
// $this->set('ServiceSparePartTotal',$ServiceSparePartTotal);
// $ExpanceAmountTotal=$this->ExpanceAmountTotal($from,$to);
//             $this->set('ExpanceAmountTotal',$ExpanceAmountTotal);
// $ExpanceTotal=$this->ExpanceTotal($from,$to); 
// $this->set('ExpanceTotal',$ExpanceTotal); 
// $IncomeTotal=$this->IncomeTotal($from,$to);
// $this->set('IncomeTotal',$IncomeTotal);
// $ServiceTotalProfit=$ServiceSparePartTotal['SparePartsListAmount']-$ServiceSparePartTotal['ProductCost']+$LabourChargeTotal-$ServiceDiscountTotal['ServiceDiscountTotal'];
// $this->set('ServiceTotalProfit',$ServiceTotalProfit);
		$PurchaseTotal=$this->PurchaseTotal($from,$to);
		$this->set('PurchaseTotal',$PurchaseTotal);
		$PurchaseReturnTotal=$this->PurchaseReturnTotal($from,$to);
		$this->set('PurchaseReturnTotal',$PurchaseReturnTotal);
// $AccountPayable=$this->AccountPayable($from,$to);
// $this->set('AccountPayable',$AccountPayable);
		$opening_balance=$this->opening_balance($from);
		$this->set('opening_balance',$opening_balance);
		$closing_balance=$this->closing_balance($from,$to);
		$this->set('closing_balance',$closing_balance);
		$ReceiptCash=$this->ReceiptCash($from,$to);
		$this->set('ReceiptCash',$ReceiptCash);
// $ServiceReceiptCash=$this->ServiceReceiptCash($from,$to);
// $this->set('ServiceReceiptCash',$ServiceReceiptCash);
		$PaymentCash=$this->PaymentCash($from,$to);
		$this->set('PaymentCash',$PaymentCash);
	}
	public function SummaryAjax()
	{
		$data=$this->request->data;
		$from=$data['from'];
		$to=$data['to'];
		$SaleCompletedTotal=$this->SaleCompletedTotal($from,$to);
		$return['SaleCompletedTotal']=$SaleCompletedTotal;
		$SaleSparePartTotal=$this->SaleSparePartTotal($from,$to);
		$return['SaleSparePartTotal']=$SaleSparePartTotal;
		$SaleReturnSparePartTotal=$this->SaleReturnSparePartTotal($from,$to);
		$return['SaleReturnSparePartTotal']=$SaleReturnSparePartTotal;
		$SaleReturnCompletedTotal=$this->SaleReturnCompletedTotal($from,$to);
		$return['SaleReturnTotal']=$SaleReturnCompletedTotal;
// $LabourChargeTotal=$this->LabourChargeTotal($from,$to);
// $return['LabourChargeTotal']=$LabourChargeTotal;
// $ServiceDiscountTotal=$this->ServiceDiscountTotal($from,$to);
// $return['ServiceAmountTotal']=$ServiceDiscountTotal;
// $ServiceCashTotal=$this->ServiceCashTotal($from,$to);
// $return['ServiceCashTotal']=$ServiceCashTotal;
// $PainterCharge=$this->PainterCharge($from,$to);
// $return['PainterCharge']=$PainterCharge;
// $ServiceCompletedTotal=$this->ServiceCompletedTotal($from,$to);
// $return['ServiceTotal']=$ServiceCompletedTotal;
// $ServiceSparePartTotal=$this->ServiceSparePartTotal($from,$to);
// $return['ServiceSparePartTotal']=$ServiceSparePartTotal;
// $ExpanceAmountTotal=$this->ExpanceAmountTotal($from,$to);
// $return['ExpanceAmountTotal']=$ExpanceAmountTotal;
// $ExpanceTotal=$this->ExpanceTotal($from,$to);
// $return['ExpanceTotal']=$ExpanceTotal;
// $IncomeTotal=$this->IncomeTotal($from,$to);
// $return['IncomeTotal']=$IncomeTotal;
// $ServiceTotalProfit=$ServiceSparePartTotal['SparePartsListAmount']-$ServiceSparePartTotal['ProductCost']+$LabourChargeTotal-$ServiceDiscountTotal['ServiceDiscountTotal'];
// $return['ServiceTotalProfit']=$ServiceTotalProfit;
		$PurchaseTotal=$this->PurchaseTotal($from,$to);
		$return['PurchaseTotal']=$PurchaseTotal;
		$PurchaseReturnTotal=$this->PurchaseReturnTotal($from,$to);
		$return['PurchaseReturnTotal']=$PurchaseReturnTotal;
// $AccountPayable=$this->AccountPayable($from,$to);
// $return['AccountPayable']=$AccountPayable;
		$opening_balance=$this->opening_balance($from);
		$return['opening_balance']=$opening_balance;
		$ReceiptCash=$this->ReceiptCash($from,$to);
		$return['ReceiptCash']=$ReceiptCash;
// $ServiceReceiptCash=$this->ServiceReceiptCash($from,$to);
// $return['ServiceReceiptCash']=$ServiceReceiptCash;
		$PaymentCash=$this->PaymentCash($from,$to);
		$return['PaymentCash']=$PaymentCash;
		echo json_encode($return);
		exit;
	}
	public function SaleCompletedTotal($from,$to)
	{
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array(
				'Sale.status=2','Sale.flag=1',
				'Sale.date_of_delivered between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));
		$grand_total=0;
		$paid_total=0;
		$topay_total=0;
		foreach ($Sale as $key => $value) {
			$grand_total+=$value['Sale']['total'];
		}
		$return['SaleCompletedTotal']=$grand_total;
		return $return;
	}
	public function SaleSparePartTotal($from,$to)
	{
		$Sale=$this->Sale->find('all',array(
			"joins"=>array(
				array(
					"table"=>'sale_items',
					"alias"=>'SaleItem',
					"type"=>'inner',
					"conditions"=>array('Sale.id=SaleItem.sale_id'),
					),
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=SaleItem.product_id'),
					),
				),
			'conditions'=>array(
				'Sale.status=2','Sale.flag=1',
				'Sale.date_of_delivered between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			'fields'=>array(
				'Sale.*',
				'SaleItem.quantity',
				'SaleItem.total',
				'Product.name',
				'Product.cost',
				)
			)
		);
// pr($SalesOffline);
		$SaleSparePartsAmount=0;
		$SaleProductCost=0;
		foreach ($Sale as $key => $value) {
			$SaleSparePartsAmount+=$value['SaleItem']['total'];
			$SparePartsListCostAmount=$value['Product']['cost']*$value['SaleItem']['quantity'];
			$SaleProductCost+=$SparePartsListCostAmount;
		}
		$return['SaleSparePartsAmount']=$SaleSparePartsAmount;
		$return['SaleProductCost']=$SaleProductCost;
		return $return;
	}
	public function SaleReturnCompletedTotal($from,$to)
	{
		$SalesReturn=$this->SalesReturn->find('all',array(
			'conditions'=>array(
				'SalesReturn.date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));
		$grand_total=0;
		foreach ($SalesReturn as $key => $value) {
			$grand_total+=$value['SalesReturn']['grand_total'];
		}
		return $grand_total;
	}
	public function SaleReturnSparePartTotal($from,$to)
	{
		$SalesReturn=$this->SalesReturn->find('all',array(
			"joins"=>array(
				array(
					"table"=>'sales_return_items',
					"alias"=>'SalesReturnItem',
					"type"=>'inner',
					"conditions"=>array('SalesReturn.id=SalesReturnItem.sales_return_id'),
					),
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=SalesReturnItem.product_id'),
					),
				),
			'conditions'=>array(
				'SalesReturn.date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			'fields'=>array(
				'SalesReturn.*',
				'SalesReturnItem.quantity',
				'SalesReturnItem.total',
				'Product.name',
				'Product.cost',
				)
			)
		);
		$SaleReturnSparePartsAmount=0;
		$SaleReturnProductCost=0;
		foreach ($SalesReturn as $key => $value) {
			$SaleReturnSparePartsAmount+=$value['SalesReturnItem']['total'];
			$SparePartsListCostAmount=$value['Product']['cost']*$value['SalesReturnItem']['quantity'];
			$SaleReturnProductCost+=$SparePartsListCostAmount;
		}
		$return['SaleReturnSparePartsAmount']=$SaleReturnSparePartsAmount;
		$return['SaleReturnProductCost']=$SaleReturnProductCost;
		return $return;
	}
// public function LabourChargeTotal($from,$to)
// {
// 	$LabourCharge=$this->Service->find('all',array(
// 		"joins"=>array(
// 			array(
// 				"table"=>'labour_charge_lists',
// 				"alias"=>'LabourChargeList',
// 				"type"=>'inner',
// 				"conditions"=>array('Service.id=LabourChargeList.service_id_fk'),
// 				),
// 			array(
// 				'table'=>'work_codes',
// 				'alias'=>'WorkCode',
// 				'type'=>'INNER',
// 				'conditions'=>array('WorkCode.id=LabourChargeList.labour_work_code_id_fk')
// 				),
// 			),
// 		'conditions'=>array(
// 			'WorkCode.name!="ZA99L0"',
// 			'Service.status>=2',
// 			'Service.created between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
// 			),
// 		'fields'=>array(
// 			'LabourChargeList.*',
// 			'WorkCode.*',
// 			)
// 		)
// 	);
// 	$grand_total=0;
// 	foreach ($LabourCharge as $key => $value) {
// 		$grand_total+=$value['LabourChargeList']['price'];
// 	}
// 	return $grand_total;
// }
// public function ServiceDiscountTotal($from,$to)
// {
// 	$Service=$this->Service->find('all',array(
// 		'joins'=>array(
// 			array(
// 				"table"=>'painter_lists',
// 				"alias"=>'PainterList',
// 				"type"=>'left',
// 				"conditions"=>array('Service.id=PainterList.service_id_fk'),
// 				),
// 			),
// 		'conditions'=>array(
// 			'Service.status>=2',
// 			'Service.created between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
// 			),
// 		'fields'=>array(
// 			'Service.grand_total',
// 			'Service.paid',
// 			'Service.balance',
// 			'Service.discount',
// 			'PainterList.id',
// 			'PainterList.painting_charge',
// 			),
// 		));
// 	$service_total=0;
// 	$amount_recieved=0;
// 	$balance_total=0;
// 	$dicount_total=0;
// 	foreach ($Service as $key => $value) {
// 		$service_total+=$value['Service']['grand_total'];
// 		$amount_recieved+=$value['Service']['paid'];
// 		$balance_total+=$value['Service']['balance'];
// 		$dicount_total+=$value['Service']['discount'];
// 		if(!empty($value['PainterList']['id']))
// 		{
// 			$service_total-=$value['PainterList']['painting_charge'];
// 			// $amount_recieved-=$value['PainterList']['painting_charge'];
// 			// $balance_total-=$value['PainterList']['painting_charge'];
// 			// $dicount_total-=$value['PainterList']['painting_charge'];
// 		}
// 	}
// 	$return['ServiceGrandTotal']=$service_total;
// 	$return['ServiceRecievedTotal']=$amount_recieved;
// 	$return['ServiceBalanceTotal']=$balance_total;
// 	$return['ServiceDiscountTotal']=$dicount_total;
// 	return $return;
// }
	public function PurchaseTotal($from,$to)
	{
		$Purchase=$this->Purchase->find('all',array(
			'conditions'=>array(
				'status '=>2,
				'date_of_delivered between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			)
		);
		$grand_total=0;
		foreach ($Purchase as $key => $value) {
			$grand_total+=$value['Purchase']['grand_total'];
		}
		return $grand_total;	
	}
	public function PurchaseReturnTotal($from,$to)
	{
		$PurchaseReturn=$this->PurchaseReturn->find('all',array(
			'conditions'=>array(
				'date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			)
		);
		$grand_total=0;
		foreach ($PurchaseReturn as $key => $value) {
			$grand_total+=$value['PurchaseReturn']['grand_total'];
		}
		return $grand_total;	
	}
	public function opening_balance($from)
	{
		$conditions_opening['date <']=date('Y-m-d',strtotime($from));
		$Journalopening=$this->Journal->find('all',array(
			'conditions'=>$conditions_opening
			)
		);
		$opening_balance=0;
		foreach ($Journalopening as $key => $value) {
			$opening_balance+=$value['Journal']['credit']-$value['Journal']['debit'];
		}
		return $opening_balance;
	}
	public function closing_balance($from,$to)
	{
		$Journalclosing=$this->Journal->find('all',array(
			'conditions'=>array(
				'date between ? and ?' => array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to)))
				)
			)
		);
		$closing_balance=0;
		foreach ($Journalclosing as $key => $value) {
			$closing_balance+=$value['Journal']['credit']-$value['Journal']['debit'];
		}
		return $closing_balance;
	}
	public function ReceiptCash($from,$to)
	{
		$ReceiptCash=$this->Journal->find('all',array(
			'conditions'=>array(
				'date between ? and ?' => array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				'work_flow="Receipt"',
				),
			)
		);
		$ReceiptCashtotal=0;
		foreach ($ReceiptCash as $keyRC => $valueRC) {
			$ReceiptCashtotal+=$valueRC['Journal']['credit']-$valueRC['Journal']['debit'];
		}
		return $ReceiptCashtotal;
	}
	public function PaymentCash($from,$to)
	{
		$PaymentCash=$this->Journal->find('all',array(
			'conditions'=>array(
				'date between ? and ?' => array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				'work_flow="Payment"',
				),
			)
		);
		$total=0;
		foreach ($PaymentCash as $key => $value) {
			$total+=$value['Journal']['debit']-$value['Journal']['credit'];
		}
		return $total;
	}
	public function ServiceSparePartTotal($from,$to)
	{
		$Service=$this->Service->find('all',array(
			"joins"=>array(
				array(
					"table"=>'spare_parts_lists',
					"alias"=>'SparePartsList',
					"type"=>'inner',
					"conditions"=>array('Service.id=SparePartsList.service_id_fk'),
					),
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=SparePartsList.product_id_fk'),
					),
				),
			'conditions'=>array(
				'status>=2',
				'Service.created between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			'fields'=>array(
				'Service.*',
				'SparePartsList.quantity',
				'SparePartsList.amount',
				'Product.product_name',
				'Product.cost',
				)
			)
		);
		$SparePartsListAmount=0;
		$ProductCost=0;
		foreach ($Service as $key => $value) {
			$SparePartsListAmount+=$value['SparePartsList']['amount'];
			$SparePartsListCostAmount=$value['Product']['cost']*$value['SparePartsList']['quantity'];
			$ProductCost+=$SparePartsListCostAmount;
		}
		$return['SparePartsListAmount']=$SparePartsListAmount;
		$return['ProductCost']=$ProductCost;
		return $return;
	}
	public function ExpanceTotal($from,$to)
	{
		$Expense_amount=0;
		$Expense=$this->Expense->find('all',array(
			'conditions'=>array(
				'exp_date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));  
		foreach ($Expense as $key => $value) {
			$Expense_amount+=$value['Expense']['exp_paid'];
		}
		$DeliveryExpense=$this->DeliveryExpense->find('all',array(
			'conditions'=>array(
				'exp_date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));
		$DeliveryExpense_paid=0;
		foreach ($DeliveryExpense as $key => $value) {
			$DeliveryExpense_paid+=$value['DeliveryExpense']['exp_paid'];
		}
		$Expense_amount+=$DeliveryExpense_paid;
		$ExpencesInPurchase=$this->ExpensesSumup->find('all',array(
			'conditions'=>array(
				'created between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));  
		$ExpencesInPurchase_Amount=0;
		foreach ($ExpencesInPurchase as $key => $value) {
			$ExpencesInPurchase_Amount+=$value['ExpensesSumup']['exp_paid'];
		}
		$Expense_amount+=$ExpencesInPurchase_Amount;
		$Expense['Total']=$Expense_amount;
		$Expense['PurchaseTotal']=$ExpencesInPurchase_Amount; 
		return $Expense;
	}

	public function product_type_select_ajax($product_type_id=null)
	{
//$data=$this->request->data;
//$product_type_id =$data['product_type_id'];
		$product=$this->Product->find('list',array('conditions'=>array('product_type_id'=>$product_type_id)));
//pr($product);
// $return['row']='<option value="">Select</option>';
// foreach ($product as $key => $value) {
// 	$return['row']=$return['row'].'<option value='.$key.'>'.$value.'</option>';;
// }
		echo json_encode($product); exit;

	}
	public function product_type_select_ajax_purchaseitem()
	{
		$product_type_id=$this->request->data['product_type_id'];
		$product=$this->Product->find('all',array('conditions'=>array('product_type_id'=>$product_type_id)));
		echo '<option value="">Select</option>';
		foreach ($product as $key => $value) {
			echo '<option value='.$value['Product']['id'].'>'.$value['Product']['name'].'</option>';
		}
		exit;
	}
	public function SalesBar($user_branch_id=null){
//pr($Sale_date);
//exit;
		$data_month_wise_final=array();
		$data_month_wise=array();
		$data_month_wise['jan']=0;
		$data_month_wise['feb']=0;
		$data_month_wise['mar']=0;
		$data_month_wise['apr']=0;
		$data_month_wise['may']=0;
		$data_month_wise['jun']=0;
		$data_month_wise['jul']=0;
		$data_month_wise['aug']=0;
		$data_month_wise['sep']=0;
		$data_month_wise['oct']=0;
		$data_month_wise['nov']=0;
		$data_month_wise['dec']=0;
		$total_jan_off=0;
		$total_feb_off=0;
		$total_mar_off=0;
		$total_apr_off=0;
		$total_may_off=0;
		$total_jun_off=0;
		$total_jul_off=0;
		$total_aug_off=0;
		$total_sep_off=0;
		$total_oct_off=0;
		$total_nov_off=0;
		$total_dec_off=0;
//$data_month_wise_off=array();
		$data_month_wise_selected_off=array();
		$data_month_wise_selected_off['jan']=0;
		$data_month_wise_selected_off['feb']=0;
		$data_month_wise_selected_off['mar']=0;
		$data_month_wise_selected_off['apr']=0;
		$data_month_wise_selected_off['may']=0;
		$data_month_wise_selected_off['jun']=0;
		$data_month_wise_selected_off['jul']=0;
		$data_month_wise_selected_off['aug']=0;
		$data_month_wise_selected_off['sep']=0;
		$data_month_wise_selected_off['oct']=0;
		$data_month_wise_selected_off['nov']=0;
		$data_month_wise_selected_off['dec']=0;
		$conditions=[];
		if(!empty($user_branch_id))
		{
			$conditions['Sale.branch_id']=$user_branch_id;
		}
		$conditions['Sale.status']=2;
		$conditions['Sale.flag']=1;
		$Sale_date = date('Y-m-d');
		$date_build = date_parse_from_format("Y-m-d", $Sale_date);
		$year = $date_build["year"];

		$conditions['year(Sale.date_of_delivered)']=$year;
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$sales_offline_total=$this->Sale->find('all',array('fields'=>array('id','grand_total','date_of_delivered','discount_amount','invoice_no'),
			'conditions'=> $conditions));
// pr($sales_offline_total);exit;
		foreach ($sales_offline_total as $key => $value) {
			$date2=$value['Sale']['date_of_delivered'];
			$SaleItem=$this->SaleItem->find('all',[
				'conditions'=>array(
					'Sale.id'=>$value['Sale']['id'],
				),
				'fields'=>array(
					'SaleItem.net_value',
				),
			]);
          $sale=0;
			foreach ($SaleItem as $key => $value1) {
				 $sale+=$value1['SaleItem']['net_value'];
			}
			$sale=$sale+$value['Sale']['discount_amount'];
			//pr($value['Sale']['invoice_no'].'****'.$sale.'****'.$value['Sale']['discount_amount']);
			$date_explode2=explode("-",$date2);
			$mnth_off= $date_explode2[1];
			if($mnth_off=='01')
			{
				$total_jan_off+=$sale;
				$data_month_wise_selected_off['jan']=$total_jan_off;
			}
			if($mnth_off=='02')
			{
				$total_feb_off+=$sale;
				$data_month_wise_selected_off['feb']=$total_feb_off;
			}
			if($mnth_off=='03')
			{
				$total_mar_off+=$sale;
				$data_month_wise_selected_off['mar']=$total_mar_off;
			}
			if($mnth_off=='04')
			{
				$total_apr_off+=$sale;
				$data_month_wise_selected_off['apr']=$total_apr_off;
			}
			if($mnth_off=='05')
			{
				$total_may_off+=$sale;
				$data_month_wise_selected_off['may']=$total_may_off;
			}
			if($mnth_off=='06')
			{
				$total_jun_off+=$sale;
				$data_month_wise_selected_off['jun']=$total_jun_off;
			}
			if($mnth_off=='07')
			{
				$total_jul_off+=$sale;
				$data_month_wise_selected_off['jul']=$total_jul_off;
			}
			if($mnth_off=='08')
			{
				$total_aug_off+=$sale;
				$data_month_wise_selected_off['aug']=$total_aug_off;
			}
			if($mnth_off=='09')
			{
				$total_sep_off+=$sale;
				$data_month_wise_selected_off['sep']=$total_sep_off;
			}
			if($mnth_off=='10')
			{
				$total_oct_off+=$sale;
				$data_month_wise_selected_off['oct']=$total_oct_off;
			}
			if($mnth_off=='11')
			{
				$total_nov_off+=$sale;
				$data_month_wise_selected_off['nov']=$total_nov_off;
			}
			if($mnth_off=='12')
			{
				$total_dec_off+=$sale;
				$data_month_wise_selected_off['dec']=$total_dec_off;
			}
		}
		$data_month_wise['jan']=$data_month_wise_selected_off['jan'];
		$data_month_wise['feb']=$data_month_wise_selected_off['feb'];
		$data_month_wise['mar']=$data_month_wise_selected_off['mar'];
		$data_month_wise['apr']=$data_month_wise_selected_off['apr'];
		$data_month_wise['may']=$data_month_wise_selected_off['may'];
		$data_month_wise['jun']=$data_month_wise_selected_off['jun'];
		$data_month_wise['jul']=$data_month_wise_selected_off['jul'];
		$data_month_wise['aug']=$data_month_wise_selected_off['aug'];
		$data_month_wise['sep']=$data_month_wise_selected_off['sep'];
		$data_month_wise['oct']=$data_month_wise_selected_off['oct'];
		$data_month_wise['nov']=$data_month_wise_selected_off['nov'];
		$data_month_wise['dec']=$data_month_wise_selected_off['dec'];
		array_push($data_month_wise_final, $data_month_wise);
		return $data_month_wise_final;
		$this->set('data_month_wise_final',$data_month_wise_final);
	}
// ---------------------------end of Sales year wise report  -------------------
// ------------------------------ Purchase Rrepot--------------------
	public function PurchaseBarGraph()
	{
		$data_month_wise_final=array();
		$total_jan=0;
		$total_feb=0;
		$total_mar=0;
		$total_apr=0;
		$total_may=0;
		$total_jun=0;
		$total_jul=0;
		$total_aug=0;
		$total_sep=0;
		$total_oct=0;
		$total_nov=0;
		$total_dec=0;
		$data_month_wise_selected=array();
		$data_month_wise_selected['jan']=0;
		$data_month_wise_selected['feb']=0;
		$data_month_wise_selected['mar']=0;
		$data_month_wise_selected['apr']=0;
		$data_month_wise_selected['may']=0;
		$data_month_wise_selected['jun']=0;
		$data_month_wise_selected['jul']=0;
		$data_month_wise_selected['aug']=0;
		$data_month_wise_selected['sep']=0;
		$data_month_wise_selected['oct']=0;
		$data_month_wise_selected['nov']=0;
		$data_month_wise_selected['dec']=0;

		$Purchase_date = date('Y-m-d');
		$date_build = date_parse_from_format("Y-m-d", $Purchase_date);
		$year = $date_build["year"];

// $conditions['year(Sale.date_of_delivered)']=$year;
		$this->Purchase->unbindModel(array('hasMany' => array('PurchasedItem')));
		$purchases_total=$this->Purchase->find('all',array(
			'fields'=>array('id','total','grand_total','date_of_delivered'),
			'conditions'=>array(
				'status=2',
				'flag=1',
				'year(Purchase.date_of_delivered)'=>$year,
				)
			));
//pr($purchases_total);exit;
		foreach ($purchases_total as $key => $value) {
			$date1=$value['Purchase']['date_of_delivered'];
			$date_explode1=explode("-",$date1);
			$mnth= $date_explode1[1];
			if($mnth=='01')
			{
				$total_jan+=$value['Purchase']['total'];
				$data_month_wise_selected['jan']=$total_jan;
			}
			if($mnth=='02')
			{
				$total_feb+=$value['Purchase']['total'];
				$data_month_wise_selected['feb']=$total_feb;
			}
			if($mnth=='03')
			{
				$total_mar+=$value['Purchase']['total'];
				$data_month_wise_selected['mar']=$total_mar;
			}
			if($mnth=='04')
			{
				$total_apr+=$value['Purchase']['total'];
				$data_month_wise_selected['apr']=$total_apr;
			}
			if($mnth=='05')
			{
				$total_may+=$value['Purchase']['total'];
				$data_month_wise_selected['may']=$total_may;
			}
			if($mnth=='06')
			{
				$total_jun+=$value['Purchase']['total'];
				$data_month_wise_selected['jun']=$total_jun;
			}
			if($mnth=='07')
			{
				$total_jul+=$value['Purchase']['total'];
				$data_month_wise_selected['jul']=$total_jul;
			}
			if($mnth=='08')
			{
				$total_aug+=$value['Purchase']['total'];
				$data_month_wise_selected['aug']=$total_aug;
			}
			if($mnth=='09')
			{
				$total_sep+=$value['Purchase']['total'];
				$data_month_wise_selected['sep']=$total_sep;
			}
			if($mnth=='10')
			{
				$total_oct+=$value['Purchase']['total'];
				$data_month_wise_selected['oct']=$total_oct;
			}
			if($mnth=='11')
			{
				$total_nov+=$value['Purchase']['total'];
				$data_month_wise_selected['nov']=$total_nov;
			}
			if($mnth=='12')
			{
				$total_dec+=$value['Purchase']['total'];
				$data_month_wise_selected['dec']=$total_dec;
			}
		}
		array_push($data_month_wise_final, $data_month_wise_selected);
		return $data_month_wise_final;
		$this->set('data_month_wise_final',$data_month_wise_final);
	}
	public function BrandGraph()
	{
		$Sale_total=$this->SaleItem->find('all',array(
			'joins'=>array(
				array(
					'table'=>'brands',
					'alias'=>'Brand',
					'type'=>'INNER',
					'conditions'=>array('Brand.id=Product.brand_id')
					),
				),
			'fields'=>array(
				'SaleItem.quantity',
				'Product.id',
				'Product.name',
				'Brand.id',
				'Brand.name',
				'Product.brand_id',
				),
			'conditions'=>array('Sale.status=2','Sale.flag=1')
			));
		$Product=$this->SaleItem->find('list',array(
			'joins'=>array(
				array(
					'table'=>'products',
					'alias'=>'Product',
					'type'=>'INNER',
					'conditions'=>array('Product.id=SaleItem.product_id')
					),
				),
			'fields'=>array('Product.id','Product.name'),
			));

		$product_array=[];
		foreach ($Sale_total as $key => $value) {
			$product_id=$value['Product']['id'];
			$brand_id=$value['Brand']['id'];
			$quantity=$value['SaleItem']['quantity'];
			$singl_product_array['brand_id']=$value['Brand']['id'];
			$singl_product_array['brand_name']=$value['Brand']['name'];
			$singl_product_array['brand_name']=$value['Brand']['name'];
			$singl_product_array['quantity']=$quantity;
			if($product_array)
			{
				if(isset($product_array[$brand_id]))
				{
					$product_array[$brand_id]['quantity']+=$quantity;
				}	
				else
				{
					$product_array[$brand_id]=$singl_product_array;
				}
			}
			else
			{
				$product_array[$brand_id]=$singl_product_array;
			}
		}
		$sum_sale_quantity = 0;
		foreach ($product_array as $key => $value){
			$sum_sale_quantity += $value['quantity'];
		}
		$limit=5;
		$brand_array=[];
		$i=0;
		$colors=['prgrs_color_1','prgrs_color_2','prgrs_color_3','prgrs_color_4','prgrs_color_5','prgrs_color_6','prgrs_color_7','info','danger','info'];
		$price = array();
		foreach ($product_array as $key => $value){
			$price[$key] =  $value['quantity'];

		}
		array_multisort($price, SORT_DESC, $product_array);
		foreach ($product_array as $keyPR => $value)
		{
			$i++;
			if($i<=$limit) {
				$single_sale_quantity = $value['quantity']/$sum_sale_quantity*100;
				$single['name']=$value['brand_name'];
				$single['quantity']=round($single_sale_quantity,3);
				$single['color']=$colors[$i];
				$brand_array[]=$single;
			}
			else
			{
				break;
			}
		}
		return $brand_array;

	}
//...............Pie Chart................//
	public function pieChart($user_branch_id=null)
	{
		$piechart_all=array();
		$from = date('Y-m-d',strtotime('first day of this month'));
		$to = date('Y-m-d',strtotime('last day of this month'));
		$Route=$this->Route->find('list',array(
			'order'=>array('id ASC'),
			'fields'=>[
			'Route.id',
			'Route.name'
			]
			));
		$conditions=[];
		if(!empty($user_branch_id))
		{
			$conditions['Sale.branch_id']=$user_branch_id;
		}
		$conditions['Sale.status']=2;
		$conditions['Sale.date_of_delivered between ? and ?']=array($from,$to);

		foreach ($Route as $key => $value) {
			$this->SaleItem->virtualFields = array(
				//'total_net_value' => "SUM(SaleItem.net_value)",
				'total_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
				'total_sale' => "SUM(SaleItem.total)",
				);
			$conditions['Customer.route_id']=$key;
			$SaleItem=$this->SaleItem->find('first',array(
				"joins" => array(
					array(
						"table" => 'customers',
						"alias" => 'Customer',
						"type" => 'inner',
						"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
						),
					array(
						"table" => 'routes',
						"alias" => 'Route',
						"type" => 'inner',
						"conditions" => array('Customer.route_id=Route.id'),
						),
					),
				'conditions'=>$conditions,
				'fields'=>array('total_net_value','total_sale'),
				));
			$net_value=floatval($SaleItem['SaleItem']['total_net_value']?$SaleItem['SaleItem']['total_net_value']:0);
			$single['net_value']=round($net_value);
			$single['route']=$value;
			$piechart_all[]=$single;

		}

		return $piechart_all;

	}
	public function SaleSummary()
	{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Reports/SaleSummary'));
	if(!in_array($menu_id, $PermissionList))
	{
		$this->Session->setFlash("Permission denied");
		return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	}
		$from=date("d-m-Y");
		$this->set('from',$from);
		$to=date("d-m-Y");
		$this->set('to',$to);
	}
	public function SaleSummary_ajax()
	{
		$data=$this->request->data;
		$from_date=$data['from_date'];
		$to_date=$data['to_date'];
		$from_date=date('Y-m-d',strtotime(($from_date)));
		$to_date=date('Y-m-d',strtotime(($to_date)));
		$list_array=array();
		//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array('Sale.invoice_no IS NOT NULL',
				'Sale.status=2',
				'Sale.flag=1',
				'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date),
			),
			'fields'=>array(
				'Sale.id',
				'Sale.invoice_no',
				'Sale.date_of_delivered',
				'Sale.grand_total',
				'Sale.account_head_id',
				'Sale.discount_amount',
			),
			'order'=>array('Sale.id DESC'),
		));
		if(!empty($Sale))
		{
			foreach ($Sale as $key => $value) {
				$list_single_offline['invoice_no']=$value['Sale']['invoice_no'];
				$list_single_offline['delivered_date']=$value['Sale']['date_of_delivered'];
				
				$AccountHead=$this->AccountHead->find('first',array(
					'conditions'=>array(
						'AccountHead.id' =>$value['Sale']['account_head_id']
					)));
				if(!empty($AccountHead))
				{
				$list_single_offline['customer_name']=$AccountHead['AccountHead']['name'];	
				}
				else
				{
$list_single_offline['customer_name']="";
			     }
				$list_single_offline['total']=$value['Sale']['grand_total'];
				$list_single_offline['discount_amount']=$value['Sale']['discount_amount'];
				$this->SaleItem->virtualFields = array( 
					'SaleItem_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
					//'SaleItem_net_value' => "SUM(SaleItem.net_value)",
					'SaleItem_tax' => "SUM(SaleItem.tax_amount)",
					//'SaleItem_total' => "SUM(SaleItem.total)",
					'SaleItem_total' => "SUM((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount)",
				);
				$SaleItem=$this->SaleItem->find('first',array(
					'conditions'=>array('sale_id'=>$value['Sale']['id']),
					'fields'=>array(
						'SaleItem_net_value',
						'SaleItem_tax',
						'SaleItem_total',
					)
				));
				$list_single_offline['net_amount']=$SaleItem['SaleItem']['SaleItem_net_value']?$SaleItem['SaleItem']['SaleItem_net_value']:0;
				$list_single_offline['taxable_amount']=$SaleItem['SaleItem']['SaleItem_net_value']-$value['Sale']['discount_amount'];
				$list_single_offline['amount']=$SaleItem['SaleItem']['SaleItem_total']?$SaleItem['SaleItem']['SaleItem_total']:0;
				$list_single_offline['tax']=$SaleItem['SaleItem']['SaleItem_tax']?$SaleItem['SaleItem']['SaleItem_tax']:0;
				array_push($list_array, $list_single_offline);
			}
		}
		$return=array();
		$return['row']='';
		$grand_total=0; $amount=0;$net_amount=0;$roundoff=0;$discount=0;$tax=0;
		foreach ($list_array as $key => $value) {
			$return['row'].='<tr class="blue-pddng toggle_class">';
			$return['row'].='<td>'.$value['invoice_no'].'</td>';
			$return['row'].='<td>'.date('d-m-Y',strtotime($value['delivered_date'])).'</td>';
			$return['row'].='<td>'.$value['customer_name'].'</td>';
			$return['row'].='<td>'.round($value['net_amount'],3).'</td>';
			$return['row'].='<td>'.round($value['discount_amount'],3).'</td>';
		    $return['row'].='<td>'.round($value['taxable_amount'],3).'</td>';
			$return['row'].='<td>'.round($value['tax'],3).'</td>';
			$return['row'].='<td>'.round($value['total'],3).'</td>';
			$net_amount+=$value['net_amount'];
			$amount+=$value['taxable_amount'];
			$discount+=$value['discount_amount'];
			$tax+=$value['tax'];
			$grand_total+=$value['total'];
			$return['row'].='</tr>';
		}
		$return['row'].='<tr class="blue-pddng toggle_class">';
		$return['row'].='<td></td>';
		$return['row'].='<td></td>';
		$return['row'].='<td class="total_amount"><b>Total</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($net_amount,3).'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($discount,3).'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($amount,3).'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($tax,3).'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($grand_total,3).'</b></td>';
		$return['row'].='</tr>';
		echo json_encode($return);
		exit;
	}
	public function PurchaseItemWise()
	{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Reports/PurchaseItemWise'));
	if(!in_array($menu_id, $PermissionList))
	{
		$this->Session->setFlash("Permission denied");
		return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	}
		$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'))));
		$this->set('Product',$this->Product->find('list',array('fields'=>array('id','name'))));
		$first_product=$this->Product->find('first');
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0) );
		$this->set('date1',$date1);
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 month", $time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('final',$final);
		$date2=date("Y-m-d", strtotime($date1) );
		$final2=date("Y-m-d", strtotime($final) );
		$Product_list=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'purchased_items',
					"alias"=>'PurchasedItem',
					"type"=>'INNER',
					"conditions"=>array('PurchasedItem.product_id=Product.id'),
				),
				array(
					"table"=>'purchases',
					"alias"=>'Purchase',
					"type"=>'INNER',
					"conditions"=>array('Purchase.id=PurchasedItem.purchase_id'),
				),
			),
			'conditions'=>array('Product.id'=>$first_product['Product']['id']),
			'order'=>array('Purchase.date_of_delivered ASC'),
			'fields'=>array(
				'Product.name',
				'Purchase.invoice_no',
				'Purchase.date_of_delivered',
				'PurchasedItem.unit_price',
				'PurchasedItem.quantity',
				'PurchasedItem.total',
			)
		)
	);
		$Product_list_array=array();
		foreach ($Product_list as $key => $value) {
			if(!empty($value['Purchase']))
			{
				$product_list_single['invoice_no']=$value['Purchase']['invoice_no'];
				$product_list_single['delivered_date']=$value['Purchase']['date_of_delivered'];
				$product_list_single['rate']=$value['PurchasedItem']['unit_price'];
				$product_list_single['product_qty']=$value['PurchasedItem']['quantity'];
				$product_list_single['total']=$value['PurchasedItem']['total'];
			}
			array_push($Product_list_array, $product_list_single);
		}
		$this->set('Product_list',$Product_list_array);
	}
	public function purchase_item_wise_ajax()
	{

		$requestData=$this->request->data;
		$conditions=[];
		$conditions['Purchase.flag']=1;
		$conditions['Purchase.status']=2;
		if($requestData['product_type_id']) $conditions['Product.product_type_id']=$requestData['product_type_id'];
		if($requestData['product_id']) $conditions['PurchasedItem.product_id']=$requestData['product_id'];
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		$conditions['Purchase.date_of_delivered between ? and ?']=[$from_date,$to_date];
		$totalData=$this->PurchasedItem->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Purchase.invoice_no LIKE'    =>'%'. $q . '%',
				'Product.name LIKE'           =>'%'. $q . '%',
				'PurchasedItem.quantity LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->PurchasedItem->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->PurchasedItem->find('all',array(
			"joins"=>array(
				array(
					"table"=>'product_types',
					"alias"=>'ProductType',
					"type"=>'INNER',
					"conditions"=>array('ProductType.id=Product.product_type_id'),
				),
			),
			'conditions'=>$conditions,
			'order'=>array('Purchase.date_of_delivered'),
			'fields'=>array(
				'Purchase.invoice_no',
				'Purchase.date_of_delivered',
				'PurchasedItem.unit_price',
				'Product.name',
				'ProductType.name',
				'PurchasedItem.quantity',
				'PurchasedItem.product_id',
				'PurchasedItem.total',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['Purchase']['date_of_delivered']=date('d-m-Y',strtotime($value['Purchase']['date_of_delivered']));
			$Data[$key]['PurchasedItem']['quantity']=floatval($value['PurchasedItem']['quantity']);
			$Data[$key]['PurchasedItem']['unit_price']=floatval($value['PurchasedItem']['unit_price']);
			$Data[$key]['PurchasedItem']['total']=floatval($value['PurchasedItem']['total']);
			$Data[$key]['PurchasedItem']['return_qty']=0;
			$credit_note_no_list=$this->PurchaseReturnItem->find('all',array(
				'conditions'=>array(
					'PurchaseReturnItem.invoice_no'=>$value['Purchase']['invoice_no'],
					'PurchaseReturnItem.product_id'=>$value['PurchasedItem']['product_id'],
				)
			));
			foreach ($credit_note_no_list as $val) {
				$Data[$key]['PurchasedItem']['return_qty']+=$val['PurchaseReturnItem']['quantity'];
				$Data[$key]['PurchasedItem']['total']     -=$val['PurchaseReturnItem']['total'];
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData), 
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	
	}
	public function GraphicalReport()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Reports/GraphicalReport'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		$countOFF=$this->Sale->find('count',array('conditions'=>array('status=1','flag=1')));
		$countP=$this->Purchase->find('count',array('conditions'=>array('status=1','flag=1')));
		$totalPending_count=$countOFF;
		$Sales_month=$this->SalesBar();
		$Purchase_month=$this->PurchaseBarGraph();
		$this->set('Sales_month',$Sales_month);
		$this->set('Purchase_month',$Purchase_month);
		$this->set('Pending_Saleorders',$totalPending_count);
		$this->set('Pending_Purchaseorders',$countP);
	}
	public function PurchaseSummary()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Reports/PurchaseSummary'));
		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0) );
		$this->set('date1',$date1);
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 month", $time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('final',$final);
		$date2=date("Y-m-d", strtotime($date1) );
		$final2=date("Y-m-d", strtotime($final) );
		$purchases_array=array();
		$purchases=$this->Purchase->find('all',
			array(
				'conditions'=>array(
					'Purchase.status=2',
					'Purchase.flag=1',
					'Purchase.date_of_delivered between ? and ?' => array($final2,$date2)
					),
				'fields'=>array('id','date_of_delivered','invoice_no','status','grand_total','other_value','account_head_id'),
				'order'=>array('Purchase.date_of_delivered ASC'),
				));
		if(!empty($purchases))
		{
			foreach ($purchases as $key => $value) {
				$purchases_array_selected=array();
				$purchases_array_selected['party_invoice_no']=$value['Purchase']['invoice_no'];
				$purchases_array_selected['date_of_delivered']=$value['Purchase']['date_of_delivered'];
				$AccountHead=$this->AccountHead->find('first',array(
					'conditions'=>array(
						'AccountHead.id' =>$value['Purchase']['account_head_id']
						)));
				$purchases_array_selected['party_name']=$AccountHead['AccountHead']['name'];
				$purchases_array_selected['other_value']=$value['Purchase']['other_value'];
				$purchases_array_selected['grand_total']=$value['Purchase']['grand_total'];
				$tax_amount_total=0;
				$total=0;
				$PurchasedItem_grouped=$this->PurchasedItem->find('all',array('conditions'=>array('purchase_id'=>$value['Purchase']['id'])));
				if(!empty($PurchasedItem_grouped))
				{
					foreach ($PurchasedItem_grouped as $key => $value1) {
						$tax_amount_total+=$value1['PurchasedItem']['cgst_amount']+$value1['PurchasedItem']['sgst_amount']+$value1['PurchasedItem']['igst_amount'];
						$total+=$value1['PurchasedItem']['net_value'];
					}
					$purchases_array_selected['tax_amount']=$tax_amount_total;
					$purchases_array_selected['net_value']=$total;
				}
				array_push($purchases_array, $purchases_array_selected);
			}
		}
		$this->set('purchases_array',$purchases_array);
	}
	public function purchase_summary_ajax()
	{
// 	$data=$this->request->data;
// 	$from_date=$data['from_date'];
// 	$to_date=$data['to_date'];
// 	$explode_from_date=explode('-', $from_date);
// 	$from_date=$explode_from_date[2].'-'.$explode_from_date[1].'-'.$explode_from_date[0];
// 	$explode_to_date=explode('-', $to_date);
// 	$to_date=$explode_to_date[2].'-'.$explode_to_date[1].'-'.$explode_to_date[0];
// 	$purchases_array=array();
// 	$purchases=$this->Purchase->find('all',
// 		array(
// 			'conditions'=>array(
// 				'Purchase.status=2',
// 				'Purchase.flag=1',
// 				'Purchase.date_of_delivered between ? and ?' => array($from_date,$to_date)
// 			),
// 			'fields'=>array('id','date_of_delivered','invoice_no','status','grand_total','other_value','account_head_id'),
// 			'order'=>array('Purchase.date_of_delivered ASC'),
// 		));
// 	if(!empty($purchases))
// 	{
// 		foreach ($purchases as $key => $value) {
// 			$purchases_array_selected=array();
// 			$purchases_array_selected['party_invoice_no']=$value['Purchase']['invoice_no'];
// 			$purchases_array_selected['date_of_delivered']=$value['Purchase']['date_of_delivered'];
// 			$AccountHead=$this->AccountHead->find('first',array(
// 				'conditions'=>array(
// 					'AccountHead.id' =>$value['Purchase']['account_head_id']
// 				)));
// 			$purchases_array_selected['party_name']=$AccountHead['AccountHead']['name'];
// 			$purchases_array_selected['other_value']=$value['Purchase']['other_value'];
// 			$purchases_array_selected['grand_total']=$value['Purchase']['grand_total'];
// 			$tax_amount_total=0;
// 			$total=0;
// 			$PurchasedItem_grouped=$this->PurchasedItem->find('all',array('conditions'=>array('purchase_id'=>$value['Purchase']['id'])));
// 			if(!empty($PurchasedItem_grouped))
// 			{
// 				foreach ($PurchasedItem_grouped as $key => $value1) {
// 					$tax_amount_total+=$value1['PurchasedItem']['cgst_amount']+$value1['PurchasedItem']['sgst_amount']+$value1['PurchasedItem']['igst_amount'];
// 					$total+=$value1['PurchasedItem']['net_value'];
// 				}
// 				$purchases_array_selected['tax_amount']=$tax_amount_total;
// 				$purchases_array_selected['net_value']=$total;
// 			}
// 			array_push($purchases_array, $purchases_array_selected);
// 		}
// 	}
// 	$return=array();
// 	$return['row']='';
// 	$grand_total=0;
// 	$tax=0; $amount=0;$roundoff=0;
// 	if(!empty($purchases_array)){
// 		foreach ($purchases_array as $key => $value) {
// 			$return['row']=$return['row'].'<tr class="blue-pddng toggle_class">';
// 			$return['row']=$return['row'].'<td>'.$value['party_invoice_no'].'</td>';
// 			$return['row']=$return['row'].'<td>'.date('d-m-Y',strtotime($value['date_of_delivered'])).'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['party_name'].'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['net_value'].'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['tax_amount'].'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['other_value'].'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['grand_total'].'</td>';
// 			$amount+=$value['net_value'];
// 			$tax+=$value['tax_amount'];
// 			$roundoff+=$value['other_value'];
// 			$grand_total+=$value['grand_total'];
// 			$return['row']=$return['row'].'</tr>';
// 		}
// 		$return['row']=$return['row'].'<tr class="blue-pddng toggle_class">';
// 		$return['row']=$return['row'].'<td></td>';
// 		$return['row']=$return['row'].'<td></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>Total</b></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>'.$amount.'</b></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>'.$tax.'</b></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>'.$roundoff.'</b></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>'.$grand_total.'</b></td>';
// 		$return['row']=$return['row'].'</tr>';
// 	}
// 	echo json_encode($return);
// 	exit;
//
		$data=$this->request->data;
		$from_date=$data['from_date'];
		$to_date=$data['to_date'];
		$explode_from_date=explode('-', $from_date);
		$from_date=$explode_from_date[2].'-'.$explode_from_date[1].'-'.$explode_from_date[0];
		$explode_to_date=explode('-', $to_date);
		$to_date=$explode_to_date[2].'-'.$explode_to_date[1].'-'.$explode_to_date[0];
		$purchases_array=array();
		$purchases=$this->Purchase->find('all',
			array(
				'conditions'=>array(
					'Purchase.status>=2',
					'Purchase.flag=1',
					'Purchase.date_of_delivered between ? and ?' => array($from_date,$to_date)
					),
				'fields'=>array('id','date_of_delivered','invoice_no','status','grand_total','other_value','account_head_id','total_tax_amount','discount_amount'),
				'order'=>array('Purchase.date_of_delivered DESC'),
				));
		if(!empty($purchases))
		{
			foreach ($purchases as $key => $value) {
				$purchases_array_selected=array();
				$purchases_array_selected['party_invoice_no']=$value['Purchase']['invoice_no'];
				$purchases_array_selected['date_of_delivered']=$value['Purchase']['date_of_delivered'];
				$AccountHead=$this->AccountHead->find('first',array(
					'conditions'=>array(
						'AccountHead.id' =>$value['Purchase']['account_head_id']
						)));
				$purchases_array_selected['party_name']=$AccountHead['AccountHead']['name'];
				$purchases_array_selected['other_value']=$value['Purchase']['other_value'];
				$purchases_array_selected['grand_total']=$value['Purchase']['grand_total'];
				$tax_amount_total=0;
				$total=0;
				$discount=0;
				$PurchasedItem_grouped=$this->PurchasedItem->find('all',array('conditions'=>array('purchase_id'=>$value['Purchase']['id'])));
				if(!empty($PurchasedItem_grouped))
				{
					foreach ($PurchasedItem_grouped as $key => $value1) {
						$total+=$value1['PurchasedItem']['quantity']*$value1['PurchasedItem']['unit_price'];
						$discount+=$value1['PurchasedItem']['discount'];
					}
					$purchases_array_selected['net_value']=$total;
					$purchases_array_selected['discount']=$value['Purchase']['discount_amount'];
					$purchases_array_selected['taxable_value']=$total-$value['Purchase']['discount_amount'];
					$purchases_array_selected['tax_amount']=$value['Purchase']['total_tax_amount'];
				}
				array_push($purchases_array, $purchases_array_selected);
			}
		}
		$return=array();
		$return['row']='';
		$grand_total=0;
		$tax=0;
		$net_value=0;
		$discount=0;
		$taxable_value=0;
		$roundoff=0;
		$other_Expense=0;
		if(!empty($purchases_array)){
			foreach ($purchases_array as $key => $value) {
				$return['row']=$return['row'].'<tr class="blue-pddng toggle_class">';
				$return['row']=$return['row'].'<td>'.$value['party_invoice_no'].'</td>';
				$return['row']=$return['row'].'<td>'.date('d-m-Y',strtotime($value['date_of_delivered'])).'</td>';
				$return['row']=$return['row'].'<td>'.$value['party_name'].'</td>';
				$return['row']=$return['row'].'<td>'.round($value['net_value'],3).'</td>';
				$return['row']=$return['row'].'<td>'.$value['discount'].'</td>';
				$return['row']=$return['row'].'<td>'.round($value['taxable_value'],3).'</td>';
				$return['row']=$return['row'].'<td>'.round($value['tax_amount'],3).'</td>';
				// $return['row']=$return['row'].'<td>'.$value['other_value'].'</td>';
				$return['row']=$return['row'].'<td>'.round($value['grand_total'],3).'</td>';
				$net_value+=$value['net_value'];
				$discount+=$value['discount'];
				$taxable_value+=$value['net_value'];
				$taxable_value-=$value['discount'];
				$tax+=$value['tax_amount'];
				$roundoff+=$value['other_value'];
				$grand_total+=$value['grand_total'];
				$return['row']=$return['row'].'</tr>';
			}
			$return['row']=$return['row'].'<tr class="blue-pddng toggle_class">';
			$return['row']=$return['row'].'<td></td>';
			$return['row']=$return['row'].'<td></td>';
			$return['row']=$return['row'].'<td class="total_amount"><b>Total</b></td>';
			$return['row']=$return['row'].'<td class="total_amount"><b>'.round($net_value,3).'</b></td>';
			$return['row']=$return['row'].'<td class="total_amount"><b>'.round($discount,3).'</b></td>';
			$return['row']=$return['row'].'<td class="total_amount"><b>'.round($taxable_value,3).'</b></td>';
			$return['row']=$return['row'].'<td class="total_amount"><b>'.round($tax,3).'</b></td>';
			// $return['row']=$return['row'].'<td class="total_amount"><b>'.$roundoff.'</b></td>';
			$return['row']=$return['row'].'<td class="total_amount"><b>'.round($grand_total,3).'</b></td>';
			$return['row']=$return['row'].'</tr>';
		}
		echo json_encode($return);
		exit;

	}

	public function DayRegisterSummary()
	{
		$Executive_list=$this->Executive->find('list');
		$this->set(compact('Executive_list'));
	}
//new day register report
	public function DayRegisterReport()
	{
		if(!empty($this->request->data['date'])){
			$date = date('Y-m-d',strtotime($this->request->data['date']));
		}
		$executive =$this->request->data['executive'];
		$return['row']['tbody'] ="";
		$return['date']=$this->request->data['date'];
		$return['transfer_print'] ="";
		$warehouse_id = $this->Executive->field(
			'Executive.warehouse_id',
			array('Executive.id ' => $executive));
		$TransferList=$this->StockTransfer->find('first',array(
			'conditions'=>array('(StockTransfer.date) <='=>$date,'warehouse_to'=>$warehouse_id),
			'fields'=>'max(StockTransfer.date) as date,StockTransfer.id as id',
			));
		if(!empty($TransferList)){
			if(!empty($TransferList['StockTransfer']['id'])){
				$return['transfer_print'] = '<span style=""><a target="_blank" href="'.$this->webroot.'Print/stockfpdf/'.$TransferList['StockTransfer']['id'].'" title="VanStock"><i class="fa fa-cart-plus fa-2x" aria-hidden="true"></i></a><a></a></span><a>';
			}
		}
		$return['executive']=$this->request->data['executive'];
		$CustomerList=$this->AccountHead->find('all',array(
			"joins"=>array(
				array(
					"table"=>'customers',
					"alias"=>'Customer',
					"type"=>'inner',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),
				),
			'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
			'fields'=>array(
				'Customer.*',
				'AccountHead.id',
				'AccountHead.name',
				'AccountHead.opening_balance',
				),
			));
		if($CustomerList)
		{
			$All_Customer=[];
			foreach ($CustomerList as $key => $value) {
				$Single_Customer['id']=$value['AccountHead']['id'];
				$Single_Customer['name']=$value['AccountHead']['name'];
				$Single_Customer['shope_code']=$value['Customer']['code'];
				$Single_Customer['action'] = '';
				$Single_Customer['remarks'] = '';
				$Single_Customer['sort'] = 0;
				$Single_Customer['print'] = "";
//
				$Sale=$this->Sale->find('first',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$value['AccountHead']['id'],
						'Sale.executive_id'=>$executive,
						'Sale.date_of_delivered'=>$date,
						'Sale.status'=>2
						),
					'fields'=>array(
						'Sale.id',
						'Sale.invoice_no',
						),
					));
				$All_sale=[];
				if(!empty($Sale)){
					$All_sale['sale_id']=$Sale['Sale']['id'];
					$All_sale['invoice_no']=$Sale['Sale']['invoice_no'];
					$Single_Customer['action'] = 'Sale';
					$Single_Customer['remarks'] = 'Invoice No:'.$Sale['Sale']['invoice_no'];
					$Single_Customer['sort'] = 3;
					$Single_Customer['print'] = '<span style=""><a target="_blank" href="'.$this->webroot.'Print/fpdf/'.$Sale['Sale']['id'].'"><i class="fa fa-print" aria-hidden="true"></i></a><a></a></span><a>';
				}
				$Single_Customer['sale']=$All_sale;
				$Saleqtn=$this->Sale->find('first',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$value['AccountHead']['id'],
						'Sale.executive_id'=>$executive,
						'Sale.date_of_order'=>$date,
						'Sale.status'=>1,
						),
					'fields'=>array(
						'Sale.id',
						'Sale.invoice_no',
						),
					));
				$All_qtn =[];
				if(!empty($Saleqtn)){
					$All_qtn['sale_id']=$Saleqtn['Sale']['id'];
					$All_qtn['invoice_no']=$Saleqtn['Sale']['invoice_no'];
					$Single_Customer['action'] = 'Quotation';
					$Single_Customer['remarks'] = 'Quotation No:'.$Saleqtn['Sale']['invoice_no'];
					$Single_Customer['sort'] = 2;
					$Single_Customer['print'] = '<span style=""><a target="_blank" href="'.$this->webroot.'Print/qutofpdf/'.$Saleqtn['Sale']['id'].'"><i class="fa fa-print" aria-hidden="true"></i></a><a></a></span><a>';
				}
				$Single_Customer['quotation']=$All_qtn;
				$NoSale=$this->NoSale->find('first',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
						'NoSale.executive_id'=>$executive,
						'NoSale.date'=>$date
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',
						),
					));
				$All_nosale =[];
				if(!empty($NoSale)){
					$All_nosale['no_sale_id']=$NoSale['NoSale']['id'];
					$All_nosale['description']=$NoSale['NoSale']['description'];
					$Single_Customer['action'] = 'No Sale';
					$Single_Customer['remarks'] = $NoSale['NoSale']['description'];
					$Single_Customer['sort'] = 1;
					$Single_Customer['print'] = "";
				}
				$Single_Customer['nosale']=$All_nosale;
				if($Single_Customer['action']!=''){
					array_push($All_Customer, $Single_Customer);
				}
//
			}
			array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
			$return['row']['tbody'] ="";
			foreach ($All_Customer as $key => $value) {
				$return['row']['tbody'].='<tr class="blue-pddng">';
				$return['row']['tbody'].='<td class="color_label ">'.$value['name'].'('.$value['shope_code'].')'.'</td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['action'].'</td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['print'].'</td>';
				$return['row']['tbody'].='</tr>';
			}
		}
		echo json_encode($return);
		exit;
	}
	public function DayRegisterDetailFunction($executive,$date)
	{
		try {
			$date=date('Y-m-d',strtotime($date));
// $Sale=$this->Sale->find('all',array(
// 	'conditions'=>array(
// 		'Sale.status>1',
// 		'Sale.account_head_id'=>$account_head_id,
// 		'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date)
// 		),
// 	'fields'=>array(
// 	// 'Sale.id',
// 	// 'Sale.total',
// 	// 'Sale.grand_total',
// 	// 'Sale.other_value',
// 	// 'Sale.other_value',
// 		),
// 	));
// $AccountHead=$this->AccountHead->findById($account_head_id);
			require('fpdf/fpdf.php');
			$pdf = new FPDF('p', 'mm', [297, 210]);
			$x=8;
			$y=8;
			function header_section($pdf) {
				$x=8;
				$y=8;
				$pdf->AddPage();
				$pdf->SetFont('Arial','B',12);
				$pdf->rect(5, 5, 200, 290);
				$pdf->Text($x, $y+3, "#");
				$Date_line_x=25;
				$pdf->Line($Date_line_x, $y-3,$Date_line_x, 295);
				$pdf->Text($Date_line_x+10, $y+5, "Customer");
				$AccountHead_line_x=$Date_line_x+65;
				$pdf->Line($AccountHead_line_x, $y-3,$AccountHead_line_x, 295);
				$pdf->Text($AccountHead_line_x+10, $y+5, "Action");
				$Remark_line_x=$AccountHead_line_x+50;
				$pdf->Line($Remark_line_x, $y-3,$Remark_line_x, 295);
				$pdf->Text($Remark_line_x+5, $y+5, "Remark");
				$Credit_line_x=$Remark_line_x+20;
// $pdf->Line($Credit_line_x, $y-3,$Credit_line_x, 295);
// $pdf->Text($Credit_line_x+5, $y+5, "Debit");
				$Debit_line_x=$Credit_line_x+20;
// $pdf->Line($Debit_line_x, $y-3,$Debit_line_x, 295);
// $pdf->Text($Debit_line_x+5, $y+5, "Balance");
				$pdf->Line(5, $x+8, 205, $x+8); 
				$pdf->SetFont('Arial','B',8);
			}
			$Date_line_x=25;
			$AccountHead_line_x=$Date_line_x+65;
			$Remark_line_x=$AccountHead_line_x+50;
			$Credit_line_x=$Remark_line_x+20;
			$Debit_line_x=$Credit_line_x+20;
// header_section($pdf,$Sale);
			header_section($pdf);
// $Journal_all=array(
// 	array(
// 		'date'=>$AccountHead['AccountHead']['created_at'],
// 		'name'=>$AccountHead['AccountHead']['name'],
// 		'remarks'=>'openning Balance',
// 		'debit'=>$AccountHead['AccountHead']['opening_balance'],
// 		'credit'=>0
// 		)
// 	);
// $Journal_credit=$this->Journal->find('all',array(
// 	'conditions'=>array(
// 		'Journal.credit'=>$account_head_id,
// 		'AccountHeadDebit.sub_group_id'=>array('1','2'),
// 		'Journal.flag=1',
// 		'Journal.date between ? and ?'=>array($from_date,$to_date),
// 		)
// 	));
//new lines
			$CustomerList=$this->AccountHead->find('all',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
				'fields'=>array(
					'Customer.*',
					'AccountHead.id',
					'AccountHead.name',
					'AccountHead.opening_balance',
					),
				));
			if($CustomerList)
			{
				$All_Customer=[];
				foreach ($CustomerList as $key => $value) {
					$Single_Customer['id']=$value['AccountHead']['id'];
					$Single_Customer['name']=$value['AccountHead']['name'];
					$Single_Customer['shope_code']=$value['Customer']['code'];
					$Single_Customer['action'] = '';
					$Single_Customer['remarks'] = '';
					$Single_Customer['sort'] = 0;
//
					$Sale=$this->Sale->find('first',array(
						'conditions'=>array(
							'Sale.account_head_id'=>$value['AccountHead']['id'],
							'Sale.executive_id'=>$executive,
							'Sale.date_of_delivered'=>$date,
							'Sale.status'=>2,
							),
						'fields'=>array(
							'Sale.id',
							'Sale.invoice_no',
							),
						));
					$All_sale=[];
					if(!empty($Sale)){
						$All_sale['sale_id']=$Sale['Sale']['id'];
						$All_sale['invoice_no']=$Sale['Sale']['invoice_no'];
						$Single_Customer['action'] = 'Sale';
						$Single_Customer['remarks'] = 'Invoice No:'.$Sale['Sale']['invoice_no'];
						$Single_Customer['sort'] = 3;
					}
					$Single_Customer['sale']=$All_sale;
					$Saleqtn=$this->Sale->find('first',array(
						'conditions'=>array(
							'Sale.account_head_id'=>$value['AccountHead']['id'],
							'Sale.executive_id'=>$executive,
							'Sale.date_of_order'=>$date,
							'Sale.status'=>1,
							),
						'fields'=>array(
							'Sale.id',
							'Sale.invoice_no',
							),
						));
					$All_qtn =[];
					if(!empty($Saleqtn)){
						$All_qtn['sale_id']=$Saleqtn['Sale']['id'];
						$All_qtn['invoice_no']=$Saleqtn['Sale']['invoice_no'];
						$Single_Customer['action'] = 'Quotation';
						$Single_Customer['remarks'] = 'Quotation No:'.$Saleqtn['Sale']['invoice_no'];
						$Single_Customer['sort'] = 2;
					}
					$Single_Customer['quotation']=$All_qtn;
					$NoSale=$this->NoSale->find('first',array(
						'conditions'=>array(
							'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
							'NoSale.executive_id'=>$executive,
							'NoSale.date'=>$date,
							),
						'fields'=>array(
							'NoSale.id',
							'NoSale.description',
							),
						));
					$All_nosale =[];
					if(!empty($NoSale)){
						$All_nosale['no_sale_id']=$NoSale['NoSale']['id'];
						$All_nosale['description']=$NoSale['NoSale']['description'];
						$Single_Customer['action'] = 'No Sale';
						$Single_Customer['remarks'] = $NoSale['NoSale']['description'];
						$Single_Customer['sort'] = 1;
					}
					$Single_Customer['nosale']=$All_nosale;
//
					if($Single_Customer['action']!=''){
						array_push($All_Customer, $Single_Customer);
					}
				}
				array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
			}
//new lines end
// foreach ($Journal_credit as $key => $value) 
// {
// 	$Journal_single['date']=$value['Journal']['date'];
// 	$Journal_single['name']=$value['AccountHeadDebit']['name'];
// 	$Journal_single['remarks']=$value['Journal']['remarks'];
// 	$Journal_single['credit']=$value['Journal']['amount'];
// 	$Journal_single['debit']=0;
// 	$Journal_all[]=$Journal_single;
// }
// foreach ($Sale as $key => $value) {
// 	$Journal_single['date']=$value['Sale']['date_of_delivered'];
// 	$Journal_single['name']=$value['AccountHead']['name'];
// 	$Journal_single['remarks']='Invoice No : '.$value['Sale']['invoice_no'];
// 	$Journal_single['debit']=$value['Sale']['grand_total'];
// 	$Journal_single['credit']=0;
// 	$Journal_all[]=$Journal_single;
// }
			$balance=0;
//$Journal_all = Set::sort($Journal_all, '{n}.date', 'asc');
			$i=0;
			$Slno =0;
			$display_date = date('d-m-Y',strtotime($date));
			foreach ($All_Customer as $key => $value) {
				$Slno ++;
				$pdf->Text($x, $y+5+(($i+1)*8), $Slno);
				$pdf->Text($Date_line_x+10, $y+5+(($i+1)*8), $value['name']);
				$pdf->Text($AccountHead_line_x+10, $y+5+(($i+1)*8), $value['action']);
				$pdf->Text($Remark_line_x+5, $y+5+(($i+1)*8),$value['remarks']);
// $pdf->Text($Credit_line_x+5, $y+5+(($i+1)*8),'' );
// $balance+=$value['debit']-$value['credit'];
// $pdf->Text($Debit_line_x+5, $y+5+(($i+1)*8), 0);
				if($i>33)
				{
					$i=-1;
					header_section($pdf);
				}
				$i++;
			}
			$pdf->Output();
// $pdf->Output($account_head_id.'.pdf', 'D');
			exit;
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function CustomerPerformance()
	{
		$this->request->data['from_date']=date("d-m-Y", strtotime('-1 day'));
		$this->request->data['to_date']=date("d-m-Y");
		$this->set('CustomerType',$this->CustomerType->find('list',array('fields'=>array('id','name'))));
	}
	public function customer_performance_ajax_new()
	{
		$requestData=$this->request->data;
		$conditions=[];
		$from=date('Y-m-d',strtotime($requestData['from_date']));
		$to=date('Y-m-d',strtotime($requestData['to_date']));
		$customer_type_id = $requestData['customer_type_id'];
		if($customer_type_id) $conditions['CustomerType.id']=$customer_type_id;
		$CustomerType=$this->CustomerType->find('list',array('conditions'=>$conditions,'fields'=>['id','name']));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$Data=[];
		$totalData=0;
		$totalFiltered=0;
		foreach ($CustomerType as $customer_type_id => $customer_type_name) {
			$conditions=[];
			$conditions['Sale.date_of_delivered between ? and ?' ]=array($from,$to);
			$conditions['Customer.customer_type_id']=$customer_type_id;
			$Customer=$this->Sale->find('list',[
				'joins'=>array(
					array(
						'table'=>'customers',
						'alias'=>'Customer',
						'type'=>'INNER',
						'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
						),
					),
				'conditions'=>$conditions,
				'fields'=>['account_head_id','account_head_id'],
				]);
			$conditions=[];
			$conditions['SalesReturn.date between ? and ?' ]=array($from,$to);
			$conditions['Customer.customer_type_id']=$customer_type_id;
			$Customer+=$this->SalesReturn->find('list',[
				'joins'=>array(
					array(
						'table'=>'customers',
						'alias'=>'Customer',
						'type'=>'INNER',
						'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
						),
					),
				'conditions'=>$conditions,
				'fields'=>['account_head_id','account_head_id'],
				]);
			$this->SaleItem->virtualFields = array( 
				'sale_amount' => "SUM(Sale.grand_total)",
				'sale_cost' => "SUM(SaleItem.net_value)",
				'product_cost' => "SUM(SaleItem.quantity*Product.cost)",
				);
			$this->SalesReturnItem->virtualFields = array( 
				'sale_return_amount' => "SUM(SalesReturn.grand_total)",
				'sale_return_cost' => "SUM(SalesReturnItem.net_value)",
				'product_cost' => "SUM(SalesReturnItem.quantity*Product.cost)",
				);
			$totalData+=$totalFiltered+=count($Customer);
			foreach ($Customer as $key => $account_head_id) {
				$conditions=[];
				$conditions['Sale.date_of_delivered between ? and ?' ]=array($from,$to);
				$conditions['Sale.account_head_id']=$account_head_id;
				if($customer_type_id) $conditions['CustomerType.id']=$customer_type_id;
				$SaleItem=$this->SaleItem->find('first',array(
					'joins'=>array(
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'INNER',
							'conditions'=>array('AccountHead.id=Sale.account_head_id')
							),
						array(
							'table'=>'customers',
							'alias'=>'Customer',
							'type'=>'INNER',
							'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
							),
						array(
							'table'=>'customer_types',
							'alias'=>'CustomerType',
							'type'=>'INNER',
							'conditions'=>array('CustomerType.id=Customer.customer_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>[
					'AccountHead.name',
					'CustomerType.name',
					'SaleItem.sale_amount',
					'SaleItem.sale_cost',
					'SaleItem.product_cost',
					]
					));
				$conditions=[];
				$conditions['SalesReturn.date between ? and ?' ]=array($from,$to);
				$conditions['SalesReturn.account_head_id']=$account_head_id;
				if($customer_type_id) $conditions['CustomerType.id']=$customer_type_id;
				$SalesReturnItem=$this->SalesReturnItem->find('first',array(
					'joins'=>array(
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'INNER',
							'conditions'=>array('AccountHead.id=SalesReturn.account_head_id')
							),
						array(
							'table'=>'customers',
							'alias'=>'Customer',
							'type'=>'INNER',
							'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
							),
						array(
							'table'=>'customer_types',
							'alias'=>'CustomerType',
							'type'=>'LEFT',
							'conditions'=>array('CustomerType.id=Customer.customer_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>[
					'AccountHead.name',
					'CustomerType.name',
					'SalesReturnItem.sale_return_amount',
					'SalesReturnItem.sale_return_cost',
					'SalesReturnItem.product_cost',
					]
					));
				$single['AccountHead']=$SaleItem['AccountHead']['name'];
				$single['CustomerType']=$SaleItem['CustomerType']['name']?$SaleItem['CustomerType']['name']:'GENERAL';
				$single['sale_amount']=floatval($SaleItem['SaleItem']['sale_amount']);
				$single['sale_cost']=floatval($SaleItem['SaleItem']['sale_cost']);
				$single['sale_product_cost']=floatval($SaleItem['SaleItem']['product_cost']);
				$single['sale_return_amount']=floatval($SalesReturnItem['SalesReturnItem']['sale_return_amount']);
				$single['sale_return_cost']=floatval($SalesReturnItem['SalesReturnItem']['sale_return_cost']);
				$single['return_product_cost']=floatval($SalesReturnItem['SalesReturnItem']['product_cost']);
				$single['profit']=$single['sale_cost']-($single['sale_product_cost']+$single['sale_return_cost'])+$single['return_product_cost'];
				if($single['sale_amount'] || $single['sale_cost'] || $single['sale_product_cost'] || $single['sale_return_amount'] || $single['sale_return_cost'] || $single['return_product_cost'] || $single['return_product_cost'])
					$Data[]=$single;
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData), 
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	}
	public function customer_performance_ajax()
	{
		$data=$this->request->data;
		$customer_type_id = $data['customer_type_id'];
		$from = date('Y-m-d',strtotime($data['from_date']));
		$to = date('Y-m-d',strtotime($data['to_date']));
		$conditions=[];
		if($customer_type_id) $conditions['CustomerType.id']=$customer_type_id;
		$CustomerType=$this->CustomerType->find('list',array('conditions'=>$conditions,'fields'=>array('CustomerType.name')));
		$list_array=[];
		$list_array_single=[];
		$cutomertype=[];
		foreach ($CustomerType as $id => $name) {
			$Customer = $this->AccountHead->find('all',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('AccountHead.id=Customer.account_head_id'),
						),
					),
				'conditions'=>array('Customer.customer_type_id'=>$id),
				'fields'=>array(
					'AccountHead.id',
					'AccountHead.name',
					)
				));
			$cutomertype['name']=$name;
			$cutomertype['Customer']='';
			$cutomertype['id']=$id;
			$cutomertype['amount']=0;
			$cutomertype['return']=0;
			$cutomertype['cost']=0;
			$cutomertype['cost_Return']=0;
			$cutomertype['single_customer']=[];
			foreach ($Customer as $key => $value) {
				$customer_id=$value['AccountHead']['id'];
				$siglecutomertype['Customer']=$value['AccountHead']['name'];
				$Sale = $this->Sale->find('all',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$customer_id,
						'Sale.flag=1',
						'Sale.status=2',
						'Sale.date_of_delivered between ? and ?' => array($from,$to)
						),
					'fields'=>array(
						'Sale.*',
						)
					));
				$siglecutomertype['name']=$name;
				$siglecutomertype['id']=$id;
				$amount=0;
				$return_amount=0;
				$return_cost=0;
				$cost=0;
				$net_value=0;
				foreach ($Sale as $keySOff => $value) {
					$offline_id=$value['Sale']['id'];
					$amount+=$value['Sale']['grand_total'];
					foreach ($value['SaleItem'] as $keyO => $value1) {
						$product_id=$value1['product_id'];
						$net_value+=$value1['net_value'];
						$product_qty=$value1['quantity'];
						$Product=$this->Product->find('first',array('conditions'=>array('Product.id'=>$product_id)));
						if(!empty($Product)) {
							$cost+=$Product['Product']['cost']*$product_qty;
						}
					}
				}
				$SalesReturn = $this->SalesReturn->find('all',array(
					'conditions'=>array(
						'SalesReturn.account_head_id'=>$customer_id,
						'SalesReturn.flag=1',
						'SalesReturn.status=2',
						'SalesReturn.date between ? and ?' => array($from,$to)
						),
					'fields'=>array(
						'SalesReturn.*',
						)
					));
				foreach ($SalesReturn as $keySOff => $value) {
					$offline_id=$value['SalesReturn']['id'];
					$return_amount+=$value['SalesReturn']['grand_total'];
					foreach ($value['SalesReturnItem'] as $keyO => $value1) {
						$product_id=$value1['product_id'];
						$product_qty=$value1['quantity'];
						$Product=$this->Product->findById($product_id);
						if(!empty($Product)) {
							$return_cost+=$Product['Product']['cost']*$product_qty;
						}
					}
				}
				$siglecutomertype['cost']=$cost;
				$cutomertype['cost']+=$cost;
				$siglecutomertype['return']=$return_amount;
				$cutomertype['return']+=$return_amount;
				$siglecutomertype['amount']=$net_value;
				$cutomertype['amount']+=$net_value;
				$siglecutomertype['cost_Return']=$return_cost;
				$cutomertype['cost_Return']+=$return_cost;
				$cutomertype['single_customer'][]=$siglecutomertype;
			}
			$list_array[]=$cutomertype;
		}
		if($list_array)
		{
			$per=0;
			foreach ($list_array as $key => $value) {
				if($value['amount']){
					echo "<tr class='blue-pd' id='parent-".$value['id']."'>";
					echo '<td>'.$value['name'].'</td>';
					echo '<td>'.$value['Customer'].'</td>';
					echo '<td>'.$value['amount'].'</td>';
					echo '<td>'.$value['cost'].'</td>';
					echo '<td>'.$value['return'].'</td>';
					echo '<td>'.$value['cost_Return'].'</td>';
					$per=$value['amount']-($value['cost']+$value['return'])+$value['cost_Return'];
					echo '<td>'.$per.'</td>';
					echo '</tr>';
					$perS=0;
					foreach ($value['single_customer'] as $keyS => $valueS) :
						echo "<tr class='toggle_rows blue-pd child-parent-".$valueS['id']."'>";
					echo '<td></td>';
					if(isset($valueS['Customer'])){
						echo '<td>'.$valueS['Customer'].'</td>';
					}
					else
					{
						echo "<td><td>";
					}
					echo '<td>'.$valueS['amount'].'</td>';
					echo '<td>'.$valueS['cost'].'</td>';
					echo '<td>'.$valueS['return'].'</td>';
					echo '<td>'.$valueS['cost_Return'].'</td>';
					$perS=$valueS['amount']-($valueS['cost']+$valueS['return'])+$valueS['cost_Return'];
					echo '<td>'.$perS.'</td>';
					echo '</tr>';
					endforeach;
				}
			}
		}
		else
		{
			echo '<tr><td colspan="9" align="center"><h3>No Data is Available</h3></td></tr>';
		}
		exit;
	}
	public function SaleOutstanding1($id = null,$to_date = null)
	{
		if(!$to_date)
			$to_date=date('d-m-Y');
		$this->request->data['to_date']=$to_date;
		$this->Customer->virtualFields = array('customer_name' => "CONCAT(AccountHead.name, ' ', Customer.code)");

		$customer_list=$this->Customer->find('list',array(
			"joins"=>array(
				array(
					"table"=>'account_heads',
					"alias"=>'AccountHead',
					"type"=>'INNER',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				),
			'fields'=>array(
				'AccountHead.id','customer_name'
				)
			)
		);
		$this->set('Customers',$customer_list);
		$this->set('customer_id',$id);
		if($id){
			$Outstanding=$this->getOutstandingData($id,$to_date);
// $OutstandingCheques=$this->getOutstandingDataCheques($id,$to_date);
// $this->set('OutstandingCheques',$OutstandingCheques);
			$this->set('Outstanding',$Outstanding);
		}
	}
// public function getOutstandingDataCheques($id,$to_date)
// {
// 	$conditions=array('AccountHead.id'=>$id,'Journal.work_flow'=>'Account Recievable');
// 	$Cheques=$this->Check->find('all',array('conditions'=>array('status != 1','account_head_id'=>$id)));
// 	$OutstandingCheque=array();
// 	$sl_no=1;
// 	foreach ($Cheques as $key => $value) {
// 		$status='Uncleared';
// 		if($value['Check']['status']==1){
// 			$status='Cleared';
// 		}else if($value['Check']['status']==2){
// 			$status='Bounce';
// 		}
// 		$OutstandingCheque[]=array(
// 			'sl_no'=>$sl_no,
// 			'check_no'=>$value['Check']['check_no'],
// 			'check_amount'=>$value['Check']['check_amount'],
// 			'recieved_date'=>date('d-M-Y',strtotime($value['Check']['recieved_date'])),
// 			'check_date'=>date('d-M-Y',strtotime($value['Check']['check_date'])),
// 			'status'=>$status,
// 			);
// 		$sl_no++;
// 	}
// 	return $OutstandingCheque;
// }
	public function getOutstandingData($id,$to_date)
	{
		$conditions=array('AccountHead.id'=>$id,'Journal.work_flow'=>'Account Recievable');
		$OpeningBalance=$this->AccountHead->findById($id);
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array(
				'Sale.status > 1',
				'Sale.account_head_id'=>$id,
				'Sale.date_of_delivered <= '=>date('Y-m-d',strtotime($to_date)),
				),
			'fields'=>array(
				),
			));
		$SalesReturn=$this->SalesReturn->find('all',array(
			'conditions'=>array(
				'SalesReturn.status >1',
				'SalesReturn.account_head_id'=>$id,
				),
			'fields'=>array(
				),
			));
		$Journal_credit=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.credit'=>$id,
				'Journal.flag=1',
				'OR' => array(
					array('AccountHeadDebit.sub_group_id'=>array('1','2')),
					array('AccountHeadDebit.id'=>'135')
					),
				)
			));
		foreach ($Sale as $key => $value) {
			$Sale[$key]['Sale']['paid_amount']=0;
			foreach ($Journal_credit as $keyJC => $valueJC) {
				if ($valueJC['Journal']['remarks'] ==  "Sale Invoice No :".$value['Sale']['invoice_no']) {
					$Sale[$key]['Sale']['paid_amount']+=$valueJC['Journal']['amount'];
					unset($Journal_credit[$keyJC]);
				}
			}
		}
		$total_opening_paid=0;
		foreach ($Journal_credit as $keyJC => $valueJC) {
			$total_opening_paid+=$valueJC['Journal']['amount'];
		}
		foreach ($SalesReturn as $keySR => $valueSR) {
			$total_opening_paid+=$valueSR['SalesReturn']['grand_total'];
		}
		$Outstanding[0]=array(
			'sl_no'=>1,
			'date'=>'',
			'invoice_no'=>'Opening Balance',
			'total'=>round($OpeningBalance['AccountHead']['opening_balance']),
			'paid'=>round($total_opening_paid),
			'balance'=>round($OpeningBalance['AccountHead']['opening_balance']-$total_opening_paid),
			'days'=>''
			);
		$sl_no=2;
		foreach ($Sale as $key => $value) {
			$datedif = time() - strtotime($value['Sale']['date_of_delivered']);
			$days=floor($datedif / (60 * 60 * 24));
			$Outstanding[]=array(
				'sl_no'=>$sl_no,
				'date'=>date('d-M-Y',strtotime($value['Sale']['date_of_delivered'])),
				'invoice_no'=>$value['Sale']['invoice_no'],
				'total'=>round($value['Sale']['grand_total']),
				'paid'=>round($value['Sale']['paid_amount']),
				'balance'=>round($value['Sale']['grand_total']-$value['Sale']['paid_amount']),
				'days'=>$days
				);
			$sl_no++;
		}
		$total_amount=0;
		$total_paid=0;
		$total_balance=0;
		foreach ($Outstanding as $key => $value) {
			$total_amount+=$value['total'];
			$total_paid+=$value['paid'];
			$total_balance+=$value['balance'];
			if($value['balance'] == 0){
				unset($Outstanding[$key]);
			}
		}
		$Outstanding[]=array(
			'sl_no'=>'',
			'date'=>'',
			'invoice_no'=>'Total',
			'total'=>$total_amount,
			'paid'=>$total_paid,
			'balance'=>$total_balance,
			'days'=>''
			);
		return $Outstanding;
	}
	public function OutstandingPrint($id=null,$to_date = null,$flag = null)
	{
		if($id){
			if(!$to_date)
				$to_date=date('d-m-Y');
			$AccountHead=$this->AccountHead->findById($id);
			$Outstanding=$this->getOutstandingData($id,$to_date);
// $OutstandingCheque=$this->getOutstandingDataCheques($id,$to_date);
			$header=array('Sl No','Invoice Date','Invoice Number','Invoice Amount','Paid','Balance','Days');
// $header_cheque=array('Sl No','Cheque Number','Cheque Amount','Received Date','Cheque Date','Status');
// $width_cheque=array(12,33,40,40,25,25);
			$width=array(12,30,40,40,25,25);
			if($flag != 1){
				foreach ($Outstanding as $key => $value) {
					unset($Outstanding[$key]['days']);
				}
				unset($header[6]);
			}
// pr($Outstanding);
// pr($header);
			if($flag == 1)
				$width[6]=15;
			require('fpdf/fpdf.php');
			$pdf = new FPDF('p');
// $pdf->SetFont('Arial','B',14);
			$Profile=$this->Global_Var_Profile;
// pr($Profile); exit;
			$pdf->AddPage();
			$pdf->setFont("Arial",'B','12');           
// $pdf->Image('profile/'.$Profile['Profile']['logo'],85,5,-300);
			$pdf->Text(62, 15, $Profile['Profile']['company_name']);
			$pdf->setFont("Arial",'B','9');
			$pdf->Text(60, 20, $Profile['Profile']['address_line_1']);
			$pdf->Text(53, 24, $Profile['Profile']['mail_address'].",".$Profile['Profile']['web_site_adddress']);
			$pdf->Line(1,26,209,26);
			$pdf->SetFont('Arial','B','11');
			$pdf->Text(10, 33, 'Outstanding Report Of '.$AccountHead['AccountHead']['name'].' To '.date('d-M-Y',strtotime($to_date)));
			$pdf -> SetY(37);
// pr($header);exit; 
			for($i=0;$i<count($header);$i++)
				$pdf->Cell($width[$i],7,$header[$i],1,0,'C');
			$pdf->Ln();
			$pdf->SetFillColor(224,235,255);
			$fill = false;
			$pdf->SetFont('Arial','','10');
			foreach($Outstanding as $row)
			{
				$i=0;
				foreach($row as $col){
					$pdf->Cell($width[$i],6,$col,'LR',0,'L',$fill);
// $pdf->Cell($width[$i],6,$col,1,0,'L',$fill);
					$i++;
				}
				$pdf->Ln();
				$fill = !$fill;
			}
			$pdf->Cell(array_sum($width),0,'','T');
// $pdf->AddPage();
// $pdf->SetFont('Arial','B','11');
// $pdf->Text(10, $i*8+35, 'Post Dated Cheques Of '.$AccountHead['AccountHead']['name']);
// $pdf->setFont("Arial",'B','9');           
// $pdf->Image("img/liyalams_logo.jpeg",85,5,-300);
// $pdf->Text(48, 20, '15/85 A.VALIYORA P O,VENGARA. MALAPPURAM-676304,TEL:0494 2450242');
// $pdf->Text(76, 24, 'www.liyalams.com,liyalams@gmail.com');
// $pdf->Line(1,26,209,26);
			$count=count($Outstanding);
			$pdf->SetFont('Arial','B','11');
// $pdf->Text(10, 48+($count*8), 'Post Dated Cheques Of '.$AccountHead['AccountHead']['name'].' To '.date('d-M-Y',strtotime($to_date)));
			$pdf -> SetY(49+($count*8));
// echo $count;
// exit;
// for($i=0;$i<count($header_cheque);$i++)
// 	$pdf->Cell($width_cheque[$i],7,$header_cheque[$i],1,0,'C');
			$pdf->Ln();
			$pdf->SetFillColor(224,235,255);
			$fill = false;
// $pdf->SetFont('Arial','','10');
// foreach($OutstandingCheque as $row)
// {
// 	$i=0;
// 	foreach($row as $col){
// 		$pdf->Cell($width_cheque[$i],6,$col,'LR',0,'L',$fill);
// 	// $pdf->Cell($width[$i],6,$col,1,0,'L',$fill);
// 		$i++;
// 	}
// 	$pdf->Ln();
// 	$fill = !$fill;
// }
// $pdf->Cell(array_sum($width_cheque),0,'','T');
			$pdf->Output();
		}
		exit;
	}
	public function InvoiceMargin()
	{
		$data=$this->request->data;
		$list_array=array();
		if(!$data)
		{
			$date0= date('m/d/Y');
			$date1=date("d-m-Y", strtotime($date0));
			$time = strtotime($date1);
			$final1 = date('m/d/Y', strtotime("-1 day",$time));
			$final=date("d-m-Y", strtotime($final1) );
			$this->set('date1',$date1);
			$this->set('final',$final);
		}
		else
		{
			$from_date=$data['InvoiceMargin']['from_date'];
			$to_date=$data['InvoiceMargin']['to_date'];
			$from=date("Y-m-d", strtotime($from_date));
			$to=date("Y-m-d", strtotime($to_date));
			$this->set('date1',$to_date);
			$this->set('final',$from_date);
			$list_array=$this->invoice_margin_report($from,$to);
		}
		$this->set('list_array',$list_array);
	}
	public function invoice_margin_report($from,$to)
	{
		$list_array=array();
		$list_array_single=array();
		$cutomertype=array();
		$SalesOffline = $this->Sale->find('all',array(
			'conditions'=>array(
// 'Sale.account_head_id'=>$customer_id,
				'Sale.flag=1',
				'Sale.status=2',
				'Sale.date_of_delivered between ? and ?' => array($from,$to)
				),
			'fields'=>array(
				'Sale.*',
				)
			));
		$cutomertype['single_invoice']=[];
		foreach ($SalesOffline as $keySOff => $valueSoff) {
			$offline_id=$valueSoff['Sale']['id'];
			$sale_invoice_no=$valueSoff['Sale']['invoice_no'];
			$siglecutomertype['invoice_no']=$sale_invoice_no;
			$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($valueSoff['Sale']['date_of_delivered']));
			$amountOffline=0;
			$cost=0;
			foreach ($valueSoff['SaleItem'] as $keySO => $valueSoff11) {
				$product_id_fk=$valueSoff11['product_id'];
				$Product=$this->Product->find('first',array('conditions'=>array('Product.id'=>$product_id_fk)));
				$product_qty=$valueSoff11['quantity'];
				$amountOffline+=$valueSoff11['quantity']*$valueSoff11['unit_price'];
				if(!empty($Product)) {
					$cost+=$Product['Product']['cost']*$product_qty;
				}
			}
			$siglecutomertype['amount']=$amountOffline;
			$siglecutomertype['cost']=$cost;
			$siglecutomertype['margin']=round(($amountOffline-$cost),3);
			$cutomertype['single_invoice'][]=$siglecutomertype;
		}
		$list_array[]=$cutomertype;
		return $list_array;
	}
	public function TaxReport()
	{
		$data['TaxReport']['from_date']=$from_date=date('d-m-Y',strtotime('-1 month'));
		$data['TaxReport']['to_date']=$to_date=date('d-m-Y');
		$this->request->data=$data;
		$SaleItem_DISTINCT_cgst=$this->SaleItem->find('all',array(
			'fields'=>array('DISTINCT SaleItem.tax'),
			));
		$PurchaseItem_DISTINCT_cgst=$this->PurchasedItem->find('all',array(
			'fields'=>array('DISTINCT PurchasedItem.tax'),
			));
		$taxes=[];
		foreach ($SaleItem_DISTINCT_cgst as $key => $value) {
			$taxes[floatval($value['SaleItem']['tax'])]=floatval($value['SaleItem']['tax'])."%";
		}
		foreach ($PurchaseItem_DISTINCT_cgst as $key => $value) {
			$taxes[floatval($value['PurchasedItem']['tax'])]=floatval($value['PurchasedItem']['tax'])."%";
		}
		$taxes['Zero']='0%';
		$this->set(compact('taxes'));
	}
	public function TaxReportAjax()
	{
		$system_state_id=$this->Global_Var_Profile['Profile']['state_id'];
		$data=$this->request->data;
		$data['account_head_id']='';
		$data['tax']='';
		$date=$data['date'];
		$account_head_id=$data['account_head_id'];
		$tax=$data['tax'];
		$explode_date=explode('-',$date);
		$from=date('Y-m-d',strtotime(trim($explode_date[0])));
		$to=date('Y-m-d',strtotime(trim($explode_date[1])));
		$list=[];
		$type=$data['type'];
		$foot['net_value']=0;
		$foot['discount']=0;
		$foot['taxable_amount']=0;
		$foot['tax_amount']=0;
		$foot['cgst']=0;
		$foot['sgst']=0;
		$foot['igst']=0;
		$foot['total']=0;
		$foot['vat']=0;
		$foot['insurance']=0;
		$foot['miscellanious']=0;
		if($type=='Purchase')
		{
			$party_conditions=[];
			if($data['account_head_id'])
				$party_conditions['Party.account_head_id']=$data['account_head_id'];

		
				
				$conditions=[];
				$conditions['Purchase.date_of_delivered between ? and ?']=array($from,$to);
				//$conditions['Purchase.account_head_id']=$party['Party']['account_head_id'];
				$conditions['Purchase.status >=']=2;
				$conditions['Purchase.total_tax_amount !=']=0;
				$this->Purchase->unbindModel(array('hasMany' => array('PurchasedItem')));
				$Purchase=$this->Purchase->find('all',[
					'conditions'=>$conditions,
					'fields'=>array(
						'Purchase.id','Purchase.account_head_id',
						'Purchase.invoice_no',
						'Purchase.date_of_delivered',
						'Purchase.date_of_purchase',
						'Purchase.total_tax_amount',
						'Purchase.grand_total',
						'Purchase.discount_amount', 
//'Purchase.current_date',
						'Purchase.total', 
//'Purchase.insurance_total', 
//'Purchase.miscellanious_expense_total', 
						),
					'order'=>array('Purchase.date_of_delivered ASC'),
					]);
				foreach ($Purchase as $key => $value) {
					$party=$this->Party->find('first',array('conditions'=>array('Party.account_head_id'=>$value['Purchase']['account_head_id'])));
					$single['name']=$party['AccountHead']['name'];
					$single['gstin']=$party['Party']['vat_no']?$party['Party']['vat_no']:'';
				$single['place']=$party['Party']['place']?$party['Party']['place']:'';
					$single['invoice_no']=$value['Purchase']['invoice_no'];
					$single['date']=date('d-m-Y',strtotime($value['Purchase']['date_of_delivered']));
					$single['invoice_date']=date('d-m-Y',strtotime($value['Purchase']['date_of_purchase']));
					$single['purchase_total']=floatval($value['Purchase']['total']);
//$single['insurance']=floatval($value['Purchase']['insurance_total']);
//$single['miscellanious']=floatval($value['Purchase']['miscellanious_expense_total']);
					$conditionsItem=[];
					$conditionsItem['Purchase.id']=$value['Purchase']['id'];
					if($tax)
					{
						$conditionsItem['PurchasedItem.tax']=$tax;
						if($tax=='Zero')
							$conditionsItem['PurchasedItem.tax']=0;	
					}
					$this->PurchasedItem->virtualFields = array( 
						'total_tax_amount' => "SUM(PurchasedItem.tax_amount)",
						'total_net_value' => "SUM(PurchasedItem.unit_price*PurchasedItem.quantity)",
						'total_discount' => "SUM(PurchasedItem.discount)",
						);
					$PurchasedItem=$this->PurchasedItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'PurchasedItem.total_tax_amount',
							'PurchasedItem.total_net_value',
							'PurchasedItem.total_discount',
							),
						]);
					//$single['tax_amount']=floatval($PurchasedItem['PurchasedItem']['total_tax_amount']);
					$single['tax_amount']=$value['Purchase']['total_tax_amount'];
					$single['net_value']=$PurchasedItem['PurchasedItem']['total_net_value'];
					$single['discount']=$value['Purchase']['discount_amount'];
					$single['taxable_amount']=$PurchasedItem['PurchasedItem']['total_net_value']-$value['Purchase']['discount_amount'];
					$single['total']=$value['Purchase']['grand_total'];
					//$single['total']+=$PurchasedItem['PurchasedItem']['total_tax_amount'];
					 $single['net_value']=round($single['net_value'],2);
					  $single['tax_amount']=round($single['tax_amount'],2);
					$single['discount']=round($single['discount'],2);
					$single['taxable_amount']=round($single['taxable_amount'],2);
					$single['total']=round($single['total'],2);
					$foot['net_value']+=$single['net_value'];
					$foot['discount']+=$single['discount'];
					$foot['taxable_amount']+=$single['taxable_amount'];
					$foot['tax_amount']+=$single['tax_amount'];
					$foot['total']+=$single['total'];
					$list['body'][]=$single;
				//}
			}
			$list['foot']=$foot;
		}
		else if($type=='Notaxpurchase')
		{
			$party_conditions=[];
			if($data['account_head_id'])
				$party_conditions['Party.account_head_id']=$data['account_head_id'];

		
				
				$conditions=[];
				$conditions['Purchase.date_of_delivered between ? and ?']=array($from,$to);
				//$conditions['Purchase.account_head_id']=$party['Party']['account_head_id'];
				$conditions['Purchase.status >=']=2;
				$conditions['Purchase.total_tax_amount']=0;
				$this->Purchase->unbindModel(array('hasMany' => array('PurchasedItem')));
				$Purchase=$this->Purchase->find('all',[
					'conditions'=>$conditions,
					'fields'=>array(
						'Purchase.id','Purchase.account_head_id',
						'Purchase.invoice_no',
						'Purchase.date_of_delivered',
						'Purchase.date_of_purchase',
						'Purchase.total_tax_amount',
						'Purchase.grand_total',
						'Purchase.discount_amount', 
//'Purchase.current_date',
						'Purchase.total', 
//'Purchase.insurance_total', 
//'Purchase.miscellanious_expense_total', 
						),
					'order'=>array('Purchase.date_of_delivered ASC'),
					]);
				foreach ($Purchase as $key => $value) {
					$party=$this->Party->find('first',array('conditions'=>array('Party.account_head_id'=>$value['Purchase']['account_head_id'])));
					$single['name']=$party['AccountHead']['name'];
					$single['gstin']=$party['Party']['vat_no']?$party['Party']['vat_no']:'';
				$single['place']=$party['Party']['place']?$party['Party']['place']:'';
					$single['invoice_no']=$value['Purchase']['invoice_no'];
					$single['date']=date('d-m-Y',strtotime($value['Purchase']['date_of_delivered']));
					$single['invoice_date']=date('d-m-Y',strtotime($value['Purchase']['date_of_purchase']));
					$single['purchase_total']=floatval($value['Purchase']['total']);
//$single['insurance']=floatval($value['Purchase']['insurance_total']);
//$single['miscellanious']=floatval($value['Purchase']['miscellanious_expense_total']);
					$conditionsItem=[];
					$conditionsItem['Purchase.id']=$value['Purchase']['id'];
					if($tax)
					{
						$conditionsItem['PurchasedItem.tax']=$tax;
						if($tax=='Zero')
							$conditionsItem['PurchasedItem.tax']=0;	
					}
					$this->PurchasedItem->virtualFields = array( 
						'total_tax_amount' => "SUM(PurchasedItem.tax_amount)",
						'total_net_value' => "SUM(PurchasedItem.unit_price*PurchasedItem.quantity)",
						'total_discount' => "SUM(PurchasedItem.discount)",
						);
					$PurchasedItem=$this->PurchasedItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'PurchasedItem.total_tax_amount',
							'PurchasedItem.total_net_value',
							'PurchasedItem.total_discount',
							),
						]);
					//$single['tax_amount']=floatval($PurchasedItem['PurchasedItem']['total_tax_amount']);
					$single['tax_amount']=$value['Purchase']['total_tax_amount'];
					$single['net_value']=$PurchasedItem['PurchasedItem']['total_net_value'];
					$single['discount']=$value['Purchase']['discount_amount'];
					$single['taxable_amount']=$PurchasedItem['PurchasedItem']['total_net_value']-$value['Purchase']['discount_amount'];
					$single['total']=$value['Purchase']['grand_total'];
					//$single['total']+=$PurchasedItem['PurchasedItem']['total_tax_amount'];
					 $single['net_value']=round($single['net_value'],2);
					  $single['tax_amount']=round($single['tax_amount'],2);
					$single['discount']=round($single['discount'],2);
					$single['taxable_amount']=round($single['taxable_amount'],2);
					$single['total']=round($single['total'],2);
					$foot['net_value']+=$single['net_value'];
					$foot['discount']+=$single['discount'];
					$foot['taxable_amount']+=$single['taxable_amount'];
					$foot['tax_amount']+=$single['tax_amount'];
					$foot['total']+=$single['total'];
					$list['body'][]=$single;
				//}
			}
			$list['foot']=$foot;
		}
		else if($type=='Expense')
		{

          
      $Expense_conditions=[];
      $master_group_id=5;
        $priliminary_expense_written_off=18;
        $user_id=1;
        $tax_on_head=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
     
       $ExpenseVat=$this->ExpenseVatDetail->find('all',array(
      'fields'=>array(
        'ExpenseVatDetail.*',
        ),
      'order'=>array('ExpenseVatDetail.date ASC'),
      'conditions'=>array('ExpenseVatDetail.vat_amount !='=>0,'ExpenseVatDetail.flag'=>1,'ExpenseVatDetail.date between ? and ?'=>array($from,$to)
        )
      ));
          foreach ($ExpenseVat  as $key => $value) 
          {
            $single['date']=date('d-m-Y',strtotime($value['ExpenseVatDetail']['date']));
          $AccountHead=$this->AccountHead->findById($value['ExpenseVatDetail']['expense_id']);
          $single['supplier_name']="";
         if(!empty($value['ExpenseVatDetail']['supplier_name']))
         {
         $single['supplier_name']=$value['ExpenseVatDetail']['supplier_name'];
         }
          $single['invoice_number']="";
          if(!empty($value['ExpenseVatDetail']['trn_number']))
         {
          $single['invoice_number']=$value['ExpenseVatDetail']['trn_number'];
         }
          $single['expense']=$AccountHead['AccountHead']['name'];
          $single['Invoice_number']=$value['ExpenseVatDetail']['external_voucher'];
          $single['amount']=$value['ExpenseVatDetail']['amount'];       
          $single['vat_amount']=$value['ExpenseVatDetail']['vat_amount'];
          $single['total']=$value['ExpenseVatDetail']['total'];
          $list['body'][]=$single;
          $foot['tax_amount']+=$single['vat_amount'];
          $foot['taxable_amount']+=$single['amount'];
          $foot['total']+=$single['total'];
          }
      $list['foot']=$foot;
    
		}
		else if($type=='ZeroVatExpense')
		{
          
      $Expense_conditions=[];
      $master_group_id=5;
        $priliminary_expense_written_off=18;
        $user_id=1;
        $tax_on_head=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
     
       $zeroExpenseVat=$this->ExpenseVatDetail->find('all',array(
      'fields'=>array(
        'ExpenseVatDetail.*',
        ),
      'order'=>array('ExpenseVatDetail.date ASC'),
      'conditions'=>array('ExpenseVatDetail.vat_amount'=>0,'ExpenseVatDetail.flag'=>1,'ExpenseVatDetail.date between ? and ?'=>array($from,$to)
        )
      ));
          foreach ($zeroExpenseVat  as $key => $value) 
          {
            $single['date']=date('d-m-Y',strtotime($value['ExpenseVatDetail']['date']));
          $AccountHead=$this->AccountHead->findById($value['ExpenseVatDetail']['expense_id']);
          $single['supplier_name']="";
         if(!empty($value['ExpenseVatDetail']['supplier_name']))
         {
         $single['supplier_name']=$value['ExpenseVatDetail']['supplier_name'];
         }
          $single['invoice_number']="";
          if(!empty($value['ExpenseVatDetail']['trn_number']))
         {
          $single['invoice_number']=$value['ExpenseVatDetail']['trn_number'];
         }
          $single['expense']=$AccountHead['AccountHead']['name'];
          $single['Invoice_number']=$value['ExpenseVatDetail']['external_voucher'];
          $single['amount']=$value['ExpenseVatDetail']['amount'];       
          $single['vat_amount']=$value['ExpenseVatDetail']['vat_amount'];
          $single['total']=$value['ExpenseVatDetail']['total'];
          $list['body'][]=$single;
          $foot['tax_amount']+=$single['vat_amount'];
          $foot['taxable_amount']+=$single['amount'];
          $foot['total']+=$single['total'];
          }
      $list['foot']=$foot;
    
		}
		else
		{
			$customer_conditions=[];
			if($data['account_head_id'])
				$customer_conditions['Customer.account_head_id']=$data['account_head_id'];
			$customers=$this->Customer->find('all',array(
				'conditions'=>$customer_conditions,
				'fields'=>['Customer.account_head_id','Customer.vat_no','AccountHead.name','Customer.place','Customer.state_id','CustomerType.name','CustomerType.name']
				));
			$Sale=$this->Sale->find('all',array(
			'conditions'=>array('Sale.invoice_no IS NOT NULL',
				'Sale.status=2',
				'Sale.flag=1',
				'Sale.date_of_delivered between ? and ?' => array($from,$to),
			),
			'fields'=>array(
				'Sale.id',
				'Sale.invoice_no',
				'Sale.date_of_delivered',
				'Sale.grand_total',
				'Sale.account_head_id',
				'Sale.discount_amount',
			),
				'order'=>array('Sale.date_of_delivered ASC'),
		));
			foreach ($Sale  as $key => $value) {
				$customer=$this->Customer->find('first',array('conditions'=>array('Customer.account_head_id'=>$value['Sale']['account_head_id'])));
				$single['gstin']="";$single['name']="";$single['place']="";$single['type']="";
				if(!empty($customer))
				{
                 $single['gstin']=$customer['Customer']['vat_no']?$customer['Customer']['vat_no']:'';
                //$single['state']=$customer['State']['name'];
				$single['name']=$customer['AccountHead']['name'];
				$single['place']=$customer['Customer']['place']?$customer['Customer']['place']:'';
				$single['type']=$customer['CustomerType']['name'];
				}
				$single['invoice_no']=$value['Sale']['invoice_no'];
				$single['date']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
				//$single['Sale_total']=floatval($value['Sale']['total']);
					$conditionsItem=[];
					$conditionsItem['Sale.id']=$value['Sale']['id'];
					$this->SaleItem->virtualFields = array( 
				     'total_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
						'total_tax_amount' => "SUM(SaleItem.tax_amount)",
						//'total_net_value' => "SUM(SaleItem.net_value)",
						'SaleItem_total' => "SUM((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount)",
						);
					$SaleItem=$this->SaleItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'SaleItem.total_tax_amount',
							'SaleItem.total_net_value',
							'SaleItem.SaleItem_total',
							),
						]);

				    $single['vat']=$SaleItem['SaleItem']['total_tax_amount'];
					$single['net_value']=$SaleItem['SaleItem']['total_net_value'];
					$single['discount']=$value['Sale']['discount_amount'];
					$single['taxable_amount']=$SaleItem['SaleItem']['total_net_value']-$value['Sale']['discount_amount'];
					$single['total']=$value['Sale']['grand_total'];
					$single['net_value']=round($single['net_value'],2);
					$single['vat']=round($single['vat'],2);
					$single['discount']=round($single['discount'],2);
					$single['taxable_amount']=round($single['taxable_amount'],2);
					$single['total']=round($single['total'],2);
					$foot['net_value']+=$single['net_value'];
					$foot['vat']+=$single['vat'];
					$foot['discount']+=$single['discount'];
					$foot['total']+=$single['total'];
					$foot['taxable_amount']+=$single['taxable_amount'];
					$list['body'][]=$single;
			}
			$list['foot']=$foot;
		}
		echo json_encode($list);exit;
	}
	public function print_invoice_margin_report($from_date,$to_date) 
	{
		$this->response->download("invoice_margin.csv");
		$conditions=array();
		$from=date("Y-m-d", strtotime($from_date));
		$to=date("Y-m-d", strtotime($to_date));
		$list_array=array();
		$list_array=$this->invoice_margin_report($from,$to);
		$stock_array=array();
		foreach($list_array[0] as $key1 => $value1) { 
			foreach($value1 as $keysingle => $valuesingle) { 
				array_push($stock_array,$valuesingle);
			}
		}
		$this->set('Stock_array', $stock_array);
		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function DueList()
	{
		$sub_group_id=3;
		$CustomerType_list=$this->CustomerType->find('list',array('fields'=>array('id','name')));
		$route_list=$this->Route->find('list',array('fields'=>array('id','name')));
		$customer_group=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
		$this->Customer->virtualFields = array('customer_name' => "CONCAT(AccountHead.name, ' ', Customer.code)");

		$Customer_list=$this->Customer->find('list',array(
			"joins"=>array(
				array(
					"table"=>'account_heads',
					"alias"=>'AccountHead',
					"type"=>'INNER',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				),
			'conditions'=>array('sub_group_id'=>3),
			'fields'=>array(
				'AccountHead.id','customer_name'
				)
			)
		);
		$State_list=$this->State->find('list',array('fields'=>array('id','name')));
		$this->set(compact('State_list'));
		$Customer=$this->Customer->find('all',array(
			'conditions'=>array('Customer.route_id'=>1),

			'order'=>array('AccountHead.name ASC'),

			));

		$this->set('CustomerType_list',$CustomerType_list);
		$this->set('Customer_list',$Customer_list);
		$this->set('Customer',$Customer);
//$this->set('route_list',$route_list);
		$route_list[0]='All';
		$this->set(compact('route_list'));
		$this->set('customer_group',$customer_group);
		$new_customer_array=array();
		$this->set('new_customer_array',$new_customer_array);
	}
	public function ExecutiveDueList(){
		$sub_group_id=3;
		$Executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
		$this->set('Executive_list',$Executive_list);
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0));
		$this->set('date1',$date1);
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 month",$time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('final',$final);
		$date2=date("Y-m-d", strtotime($date1));
		$final2=date("Y-m-d", strtotime($final));
	}
	public function executive_due_report_ajax()
	{
		$data=$this->request->data;
		$from_date=date('Y-m-d',strtotime($data['from_date']));
//$to_date=date('Y-m-d',strtotime($data['to_date']));
		$conditions=[];
		if($data['executive_id']){
			$conditions['ExecutiveRouteMapping.executive_id']=$data['executive_id'];
		}
//$conditions['Sale.account_head_id']=892;
		$data['row']='';
		$data['tfoot']='';
		$data['result']='Error';

		$Customer=$this->Customer->find('all',array(
			'joins'=>array(
// array(
// 	'table'=>'executives',
// 	'alias'=>'Executive',
// 	'type'=>'LEFT',
// 	'conditions'=>array('Executive.id=Sale.executive_id')
// ),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Executive.id=ExecutiveRouteMapping.executive_id')
					),
				),
			'conditions'=>$conditions,
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				'Executive.id',
				'Executive.name',
				'Customer.route_id',
				'Executive.account_head_id',
				'ExecutiveRouteMapping.route_id',
				)
			));
//pr($Customer);exit;
		if($Customer)
		{
			$invoice_array=array();
			foreach ($Customer as $key => $value) {
				$account_id=$value['AccountHead']['id'];
				$Sales = $this->Sale->find('all', array(
					'conditions' => array(
						'Sale.account_head_id' =>$account_id,
//'Sale.executive_id'=>$value['Executive']['id'],
						'Sale.flag'=>1,
						'Sale.status'=>array(2,3),
						),
					'order' => array('Sale.date_of_delivered' => 'ASC'),
					'fields' => array(
						'Sale.id',
						'Sale.date_of_delivered',
						'Sale.invoice_no',
						'Sale.account_head_id',
						'Sale.executive_id',
						'Sale.grand_total',
						'AccountHead.name',
						'Executive.account_head_id',

						)
					)
				);
				$voucher_amount=0;

				$Journal_voucher=$this->Journal->find('list',array(
					'conditions'=>array(
						'Journal.credit'=>$account_id,
						'Journal.debit !='=>13,
//'NOT' => array(
//'Journal.remarks'=>'Sale Invoice No :'.$value3['Sale']['invoice_no'],
//),
						'Journal.flag=1',
						),
					'fields'=>array(
						'Journal.id',
						'Journal.amount',
						),
					));
				foreach ($Journal_voucher as $key3 => $value_amount) {
					$voucher_amount+=$value_amount;
				}

				$Journal_voucher1=$this->Journal->find('list',array(
					'conditions'=>array(
						'Journal.debit'=>$account_id,
						'Journal.credit'=>1,
						'Journal.flag=1',
						),
					'fields'=>array(
						'Journal.id',
						'Journal.amount',
						),
					));
				$voucher_balance=0;
				foreach ($Journal_voucher1 as $key3 => $value_amount1) {
					$voucher_amount-=$value_amount1;
				}

				foreach ($Sales as $keyj =>$valuej)
				{
					$SaleItem=$this->SaleItem->find('list',array(
						'conditions'=>array(
							'SaleItem.sale_id'=>$valuej['Sale']['id'],
							),
						'fields'=>array(
//'SaleItem.discount',
							)
						));
				}
				$invoice_array_single=array();
				if (!empty($Sales)) {


					$invoice_array_single['party_name'] = $value['AccountHead']['name'];
					$invoice_array_single['Executive_name'] = $value['Executive']['name'];
					$invoice_array_single['invoice_no'] = array();
					$invoice_array_single['balance'] = array();
					$invoice_array_single['delivered_date'] = array();
					$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
					if($AccountHead)
					{
						$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
						$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
					}
					if($outstanding_amount>$voucher_amount)
					{
						$outstanding_amount-=$voucher_amount;
						$voucher_amount=0;
					}
					else
					{
						$voucher_amount-=$outstanding_amount;
						$outstanding_amount=0;
					}
//pr($voucher_amount);
					if($outstanding_amount>=1)
						{$from12=date("Y-m-d", strtotime($data['from_date']));
					$outstanding_amount_date1=date("Y-m-d", strtotime($outstanding_amount_date));
					if($outstanding_amount_date1>=$from12){
						array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['invoice_no'], 'Opening Balance');
						array_push($invoice_array_single['balance'],$outstanding_amount);
					}
				}
				foreach ($Sales as $keySC2 => $valueSC2) {
					$Journal=$this->Journal->find('all',array(
						'conditions'=>array(
							'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
							'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
							'Journal.debit=1',
							'Journal.flag=1',
							),
						));

					$balance_amount=$valueSC2['Sale']['grand_total'];
					$paid_amount=0;
// foreach ($Journal as $key0 => $amount) {
// 	$paid_amount+=$amount['Journal']['amount'];
// 	$balance_amount-=$amount['Journal']['amount'];
// }
					if($voucher_amount)
					{
						if($balance_amount<=$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Sale']['grand_total']>0)
					{
						if($balance_amount){

// if($valueSC2['Sale']['executive_id']==$data['executive_id'])
// {
							$check_date=$valueSC2['Sale']['date_of_delivered'];

							$from11=date("Y-m-d", strtotime($data['from_date']));
							$to11=date('Y-m-d');
							if($check_date>=$from11){
								array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
								array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
								array_push($invoice_array_single['balance'],$balance_amount) ;  
							}
//}
						}
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      

			}
			else
			{


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
				$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{
					$from12=date("Y-m-d", strtotime($data['from_date']));
					$outstanding_amount_date1=date("Y-m-d", strtotime($outstanding_amount_date));
					if($outstanding_amount_date1>=$from12){
						array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['invoice_no'], 'Opening Balance');
						array_push($invoice_array_single['balance'],$outstanding_amount);
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}

		}
		$data['result']='Success';
		$data['data']=$invoice_array;
	}
//pr($invoice_array);
	echo json_encode($data);
	exit;

}
public function get_oustanding_amount($account_head_id,$opening_balance){
//pr($account_head_id."--".$opening_balance);
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$account_head_id,
					'Journal.credit'=>$account_head_id,
					),
				'AND' => array(
					'Journal.flag=1',
					)
				)
			),
		));
	$Journal_credit=$this->Journal->find('all',array(
		'conditions'=>array(
			'Journal.credit'=>$account_head_id,
			'Journal.flag=1',
			)
		));

	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>[
	'Capital'=>'credit_account',
	'Drawing'=>'debit_account',
	],
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$AccountingsController = new AccountingsController;
	$type=$AccountingsController->get_type_by_account_dead($account_head_id);
	$Journal_all=[];
	foreach ($Journal_credit as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['mode']=$value['AccountHeadDebit']['name'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['credit']=$value['Journal']['amount'];
		$Journal_single['debit']=0.00;
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$Journal_debit=$this->Journal->find('all',array(
		'conditions'=>array(
			'Journal.debit'=>$account_head_id,
			'Journal.flag=1',
			)
		));
	foreach ($Journal_debit as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['mode']=$value['AccountHeadCredit']['name'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['credit']=0.00;
		$Journal_single['debit']=$value['Journal']['amount'];
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$return['row']['tbody']='';
	$return['row']['tfoot']='';
	$category_name='';
	$Balance_Total=0;
	$credit_Total=0;
	$debit_Total=0;
	if(!empty($type)){
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		}
	}
	if($category_name=='credit_account')
	{
		$credit=$opening_balance;
		$debit='0';
	}
	else
	{
		$debit=$opening_balance;
		$credit='0';
	}
	$Journal_all[0]=array(
		'id'=>0,
		'mode'=>'Opening Balance',
		'remarks'=>'',
		'credit'=>$credit,
		'debit'=>$debit,
		);
	foreach ($Journal_all as $key => $value) 
	{
		$credit_Total+=$value['credit'];
		$debit_Total+=$value['debit'];
	}

	$Balance_Total=$debit_Total-$credit_Total;	
	return $Balance_Total;
}
public function due_report_ajax()
{
	$conditions=array();
	if(!empty($this->request->data['customer_type']))
	{
		$conditions['Customer.customer_type_id']=$this->request->data['customer_type'];
	}
	if(!empty($this->request->data['route_id']))
	{
		$conditions['Customer.route_id']=$this->request->data['route_id'];
	}
	if(!empty($this->request->data['group_id']))
	{
		$conditions['Customer.customer_group_id']=$this->request->data['group_id'];
	}
	if(!empty($this->request->data['name']))
	{
		$AccountHead_id=$this->AccountHead->find('first',array('conditions'=>array('AccountHead.name'=>$this->request->data['name'])));
		$Balance_Total=0;
		if(!empty($AccountHead_id)){
			$conditions['Customer.account_head_id']=$AccountHead_id['AccountHead']['id'];   
		}
	}
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
	$data['row']='';
	$data['tfoot']='';
	$grand_total=0;
	$q=0;$w=0;$p=0;$y=0;$cheque_total=0;
	if($Customer)
	{
		foreach ($Customer as $key => $value) {
        

			$invoice_array=array();

			$account_id=$value['AccountHead']['id'];
			$cheque=$this->Cheque->find('all',array(
		'conditions'=>array(
					'Cheque.account_head_id'=>$account_id,
					'Cheque.status'=>array(0,4),
			),
		'order'=>array('Cheque.id ASC'),
		'fields'=>array(
			'Cheque.cheque_amount',
			
			),
		));
			$cheque_amount=0;
			foreach ($cheque as $key => $cheque) {
                $cheque_amount+=$cheque['Cheque']['cheque_amount'];
			}
			$Sales = $this->Sale->find('all', array(
				'conditions' => array(
					'Sale.account_head_id' =>$account_id,
//'Sale.executive_id'=>$value['Executive']['id'],
					'Sale.flag'=>1,
					'Sale.status'=>array(2,3),
					),
				'order' => array('Sale.date_of_delivered' => 'ASC'),
				'fields' => array(
					'Sale.id',
					'Sale.date_of_delivered',
					'Sale.invoice_no',
					'Sale.account_head_id',
					'Sale.executive_id',
					'Sale.grand_total',
					'AccountHead.name',
					'Executive.account_head_id',

					)
				)
			);
			$voucher_amount=0;

			$Journal_voucher=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$account_id,
					'Journal.debit !='=>13,
//'NOT' => array(
//'Journal.remarks'=>'Sale Invoice No :'.$value3['Sale']['invoice_no'],
//),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));

			foreach ($Journal_voucher as $key3 => $value_amount) {
				$voucher_amount+=$value_amount;
			}

			$Journal_voucher1=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.debit'=>$account_id,
					'Journal.credit'=>1,
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));
			$voucher_balance=0;
			foreach ($Journal_voucher1 as $key3 => $value_amount1) {
				$voucher_amount-=$value_amount1;
			}

			foreach ($Sales as $keyj =>$valuej)
			{
				$SaleItem=$this->SaleItem->find('list',array(
					'conditions'=>array(
						'SaleItem.sale_id'=>$valuej['Sale']['id'],
						),
					'fields'=>array(
//'SaleItem.discount',
						)
					));
			}
			$invoice_array_single=array();

			if (!empty($Sales)) {


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
//pr($voucher_amount);
				if($outstanding_amount>=1)
				{
					array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['balance'],$outstanding_amount);

				}
				foreach ($Sales as $keySC2 => $valueSC2) {
					$Journal=$this->Journal->find('all',array(
						'conditions'=>array(
							'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
							'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
							'Journal.debit=1',
							'Journal.flag=1',
							),
						));
					$balance_amount=$valueSC2['Sale']['grand_total'];
					$paid_amount=0;
// foreach ($Journal as $key0 => $amount) {
// 	$paid_amount+=$amount['Journal']['amount'];
// 	$balance_amount-=$amount['Journal']['amount'];
// }
					if($voucher_amount)
					{
						if($balance_amount<=$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Sale']['grand_total']>0)
					{
						if($balance_amount){
//if($valueSC2['Sale']['account_head_id']==$data['customer_id'])
//{
							$check_date=$valueSC2['Sale']['date_of_delivered'];


							array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
							array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
							array_push($invoice_array_single['balance'],$balance_amount) ;  

//}
						}
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      

			}
			else
			{


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{

					array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['balance'],$outstanding_amount);

				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}
//pr($invoice_array);
//if($invoice_array){

			$b11='';$b51='';$b12='';$b41='';

			foreach ($invoice_array as $key => $value4)
			{

				$inner_array_count=count($value4['invoice_no']);
				$a1=array();
				$b1=array();
				$c1=array();
				$f1=array();
				$g=array();
				$m=array();
				foreach ($value4['delivered_date'] as $key=>$value1)
				{
					$delivered_date=$value1;
					$now = time();
					$expected_days_diff=$now-strtotime($delivered_date);
					$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
					array_push($m,$diff_day);
				}
				foreach($value4['invoice_no'] as $key=>$value2)
				{
					$invoice_no=$value2;

				}
				$new_balance=0;

				foreach ($value4['balance'] as $key=>$value3)
				{
					$balance=$value3;
					array_push($g,$balance);
					$new_balance+=$balance;
				}
				for($i=0;$i<count($m);$i++)
				{

					if($m[$i]<=30){ $a=$g[$i];array_push($a1,$a);}
					if($m[$i]<=60 && $m[$i]>30){$b=$g[$i];array_push($b1,$b);}
					if($m[$i]<=90 && $m[$i]>60){$c=$g[$i];array_push($c1,$c);}
					if($m[$i]>90){$f=$g[$i];array_push($f1,$f);}

				}

//$grand_total+=$new_balance;

				$b=array_sum($a1);if($b!=0){$b11=ROUND($b,3);}else{$b11='';}
				$b5=array_sum($b1);if($b5!=0){$b51=ROUND($b5,3);}else{$b51='';}
				$b19=array_sum($c1);if($b19!=0){$b12=ROUND($b19,3);}else{$b12='';}
				$b4=array_sum($f1);if($b4!=0){$b41=ROUND($b4,3);}else{$b41='';}

				$q+=$b11;$w+=$b51;$p+=$b12;$y+=$b41;
				$grand_total=$q+$w+$p+$y;

			}

			$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);

			$Balance_Total = number_format($Balance_Total,3);
//$grand_total+=$Balance_Total;
			if($this->request->data['check_id'] !=0)
			{
				if($Balance_Total !=0)
				{
//pr($value['AccountHead']['name']);
                         $cheque_total+=$cheque_amount;
					$data['row']= $data['row'].'<tr>';
					$data['row']= $data['row'].'<td>'.$value["Customer"]["code"].'</td>';
					$data['row']= $data['row'].'<td><span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span><span>'.$value['AccountHead']['name'].'</span></td>';
					$data['row']= $data['row'].'<td>'.$value["Route"]["name"].'</td>';
					$data['row']= $data['row'].'<td>'.$value["CustomerGroup"]["name"].'</td>';
					$data['row']= $data['row'].'<td>'.$b11.'</td>';
					$data['row']= $data['row'].'<td>'.$b51.'</td>';
					$data['row']= $data['row'].'<td>'.$b12.'</td>';
					$data['row']= $data['row'].'<td>'.$b41.'</td>';
				    $data['row']= $data['row'].'<td>'.$cheque_amount.'</td>';
					$data['row']= $data['row'].'<td>'.$Balance_Total.'</td>';
					$data['row']= $data['row'].'</tr>';
				}
			}
			else{
                $cheque_total+=$cheque_amount;
				$data['row']= $data['row'].'<tr>';
				$data['row']= $data['row'].'<td>'.$value["Customer"]["code"].'</td>';
				$data['row']= $data['row'].'<td><span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span><span>'.$value['AccountHead']['name'].'</span></td>';
				$data['row']= $data['row'].'<td>'.$value["Route"]["name"].'</td>';
				$data['row']= $data['row'].'<td>'.$value["CustomerGroup"]["name"].'</td>';
				$data['row']= $data['row'].'<td>'.$b11.'</td>';
				$data['row']= $data['row'].'<td>'.$b51.'</td>';
				$data['row']= $data['row'].'<td>'.$b12.'</td>';
				$data['row']= $data['row'].'<td>'.$b41.'</td>';
		        $data['row']= $data['row'].'<td>'.$cheque_amount.'</td>';
				$data['row']= $data['row'].'<td>'.$Balance_Total.'</td>';
				$data['row']= $data['row'].'</tr>';

			}


           
//$data['result']='Success';
		}
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td colspan="3"></td>';

		$data['tfoot']= $data['tfoot'].'<td><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$q.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$w.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$p.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$y.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$cheque_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$grand_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';
	}
	else
	{
		$data['result']='Error';
	}
	echo json_encode($data);
	exit;
}
public function DueListPrint()
{
	$data=$this->request->data;
	$conditions=array();
	if(!empty($data['Report']['customer_type'])){
		$conditions['Customer.customer_type_id']=$data['Report']['customer_type'];
	}
	if(!empty($data['Report']['route_id'])){
		$conditions['Route.id']=$data['Report']['route_id'];
	}
	if(!empty($data['Report']['name']))
	{
		$conditions['AccountHead.id']=$data['Report']['name'];
	}
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions,
		'order'=>array('AccountHead.name ASC'),
		'fields'=>array(
			'Customer.account_head_id',
			'CustomerType.name',
			'Customer.code',
			'Route.name',
			'AccountHead.name',
			'AccountHead.opening_balance',
			)));
	$final_array=array();
	$header=array(
		'Code',
		'Name',
		'Outstanding'
		);
	$width=array(
		'50',
		'115',
		'100'
		);
	foreach ($Customer as $key => $value) {
		$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);
		$Balance_Total = number_format($Balance_Total,2);
		if($Balance_Total){
			$final_array[$key][]=$value['Customer']['code'];
			$final_array[$key][]=$value['AccountHead']['name'];
			$final_array[$key][]=$Balance_Total;
		}
	}
	require('fpdf/fpdf.php');
	$pdf = new FPDF('l');
	$pdf->addPage();
	$pdf->SetFont('Arial','',16);
	$pdf->text(130,20,'CUSTOMER DUE  LIST');
	$pdf->SetFont('Arial','',14);
	$pdf -> SetY(35); 
	for($i=0;$i<count($header);$i++)
		$pdf->Cell($width[$i],7,$header[$i],1,0,'C');
	$pdf->Ln();
	$pdf->SetFillColor(224,235,255);
	$fill = false;
	$pdf->SetFont('Arial','','10');
	foreach($final_array as $row)
	{
		$i=0;
		foreach($row as $col){
			$pdf->Cell($width[$i],6,$col,'LR',0,'L',$fill);
			$i++;
		}
		$pdf->Ln();
		$fill = !$fill;
	}
	$pdf->Cell(array_sum($width),0,'','T');
	$pdf->Output();
	exit;
}
public function account_recievable_print($customer_type,$customer)
{
	$conditions=array();
	if($customer_type!=0){
		$CustomerType=$this->CustomerType->findById($customer_type);
		$CustomerType_name=$CustomerType['CustomerType']['name'];
		$conditions['Customer.customer_type_id']=$customer_type;
	}
	else{
		$CustomerType_name='All';
	}
	if($customer!=0){
		$conditions['Customer.account_head_id']=$customer;
	}
	$Customers=$this->Customer->find('all',array(
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			),
		'order' => array('Customer.id' => 'ASC'),
		'conditions'=>$conditions,
		)
	);
	$invoice_array=array();
	foreach ($Customers as $key => $value)
	{
		$account_id=$value['AccountHead']['id'];
		$Sales = $this->Sale->find('all', array(
			'conditions' => array(
				'Sale.account_head_id' =>$account_id,
				'Sale.flag'=>1,
				'Sale.status'=>array(2,3),
				),
			'order' => array('Sale.date_of_delivered' => 'ASC'),
			'fields' => array(
				'Sale.id',
				'Sale.date_of_delivered',
				'Sale.invoice_no',
				'Sale.account_head_id',
				'Sale.executive_id',
				'Sale.grand_total',
				'AccountHead.name',
				'Executive.account_head_id',

				)
			)
		);

		$voucher_amount=0;
		$discount_paid_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DISCOUNT PAID'));
		$Journal_voucher=$this->Journal->find('list',array(
			'conditions'=>array(
				'Journal.credit'=>$account_id,
				//'Journal.debit'=>$value['Executive']['account_head_id'],
				'NOT' => array(
					'Journal.debit'=>array($discount_paid_account_head_id),
					'Journal.remarks LIKE'=>'%Sale Invoice No :%',
				),
				'Journal.flag=1',
				),
			'fields'=>array(
				'Journal.id',
				'Journal.amount',
				),
			));
		foreach ($Journal_voucher as $key3 => $value_amount) {
			$voucher_amount+=$value_amount;
		}
		$Journal_extra_debited=$this->Journal->find('list',array(
		'conditions'=>array(
			'Journal.debit'=>$account_id,
			'Journal.work_flow'=>array('Cheques'),
			'Journal.flag=1',
			),
		'fields'=>array(
			'Journal.id',
			'Journal.amount',
			),
		));
		$other_debited_amount=0;
		foreach ($Journal_extra_debited as $key3 => $value_amount) {
			$other_debited_amount+=$value_amount;
		}
		$total_debited_amount=0;
		$total_credited_amount=0;
		$total_credited_amount+=$voucher_amount;
		//$total_debited_amount+=$other_debited_amount;
		$voucher_balance=0;
		foreach ($Sales as $keyj =>$valuej)
		{
			$SaleItem=$this->SaleItem->find('list',array(
				'conditions'=>array(
					'SaleItem.sale_id'=>$valuej['Sale']['id'],
					),
				'fields'=>array(
					)
				));
		}
		$invoice_array_single=array();
		if (!empty($Sales)) {


			$invoice_array_single['party_name'] = $value['AccountHead']['name'];
			$invoice_array_single['invoice_no'] = array();
			$invoice_array_single['balance'] = array();
			$invoice_array_single['delivered_date'] = array();
			$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
			$Journal_opening_received=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.remarks'=>'Sale Invoice No :0',
				'Journal.credit'=>$value['AccountHead']['id'],
				// 'NOT' => array(
				// 	'Journal.work_flow'=>'From Mobile',
				// ),
				// 'Journal.debit=1',
				'Journal.flag=1',
				),
			));

			if($AccountHead)
			{
				$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
			}
			$opening_paid_amount=0;
			foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
				$opening_paid_amount+=$value_each_sale['Journal']['amount'];
			}
			$outstanding_amount+=$other_debited_amount;
			$total_debited_amount+=$outstanding_amount;
			$total_credited_amount+=$opening_paid_amount;
			$outstanding_amount-=$opening_paid_amount;
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;
			}
			if($outstanding_amount>0)
			{
				array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
				array_push($invoice_array_single['invoice_no'], 'Opening Balance');
				array_push($invoice_array_single['balance'],$outstanding_amount);

			}

			foreach ($Sales as $keySC2 => $valueSC2) {
				$Journal=$this->Journal->find('all',array(
					'conditions'=>array(
						'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
						'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
						// 'NOT' => array(
						// 	'Journal.work_flow'=>'From Mobile',
						// ),
						'AccountHeadDebit.sub_group_id=1',
						'Journal.flag=1',
						),
					));
				$balance_amount=$valueSC2['Sale']['grand_total'];
				$paid_amount=0;
				$app_paid_amount=0;
				foreach ($Journal as $key_each_sale => $value_each_sale) {
					$paid_amount+=$value_each_sale['Journal']['amount'];
				}
				$total_paid_in_amount=$paid_amount+$app_paid_amount;
				$balance_amount-=$paid_amount+$app_paid_amount;
				$total_debited_amount+=$valueSC2['Sale']['grand_total'];
				$total_credited_amount+=$paid_amount+$app_paid_amount;
				
				if($voucher_amount)
				{
					if($balance_amount<=$voucher_amount)
					{
						$voucher_balance=$balance_amount;
						$balance_amount=0;
						$voucher_amount-=$voucher_balance;
					}
					else
					{
						$balance_amount-=$voucher_amount;
						$voucher_amount=0;
					}
				}
				if($valueSC2['Sale']['grand_total']>0)
				{
					if($balance_amount>0){
						if($valueSC2['Sale']['account_head_id']==$customer)
						{
							$check_date=$valueSC2['Sale']['date_of_delivered'];


							array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
							array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
							array_push($invoice_array_single['balance'],$balance_amount) ;  

						}
					}
				}
			}
			// pr($total_debited_amount);
			// pr($total_credited_amount);
			// pr($account_id);
			// exit;
			if(count($invoice_array_single['invoice_no']))
			{
				array_push($invoice_array, $invoice_array_single);  
			}      

		}
		else
		{


			$invoice_array_single['party_name'] = $value['AccountHead']['name'];
			$invoice_array_single['invoice_no'] = array();
			$invoice_array_single['balance'] = array();
			$invoice_array_single['delivered_date'] = array();
			$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
			if($AccountHead)
			{
				$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
			}
			$Journal_opening_received=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.remarks'=>'Sale Invoice No :0',
				'Journal.credit'=>$value['AccountHead']['id'],
				'Journal.flag=1',
				),
			));
			$opening_paid_amount=0;
			foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
				$opening_paid_amount+=$value_each_sale['Journal']['amount'];
			}
			$outstanding_amount+=$other_debited_amount;
			$outstanding_amount-=$opening_paid_amount;
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;
			}
			if($outstanding_amount>=1)
			{

				array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
				array_push($invoice_array_single['invoice_no'], 'Opening Balance');
				array_push($invoice_array_single['balance'],$outstanding_amount);

			}
			if(count($invoice_array_single['invoice_no']))
			{
				array_push($invoice_array, $invoice_array_single);  
			}      
		}

	}
	require('fpdf/fpdf.php');   
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	function header_section($pdf,$CustomerType_name,$Profile){
		$pdf->SetFont('Arial','B',11);
		$pdf->AddPage();
		$pdf->Text(10, 7,$Profile['company_name']);
		$pdf->SetFont('Arial','',9);
		$pdf->Text(10, 12,$Profile['address_line_1']);
		$pdf->Text(10, 17,$Profile['address_line_2']);
		$pdf->Text(10, 22,$Profile['mobile']);
		$pdf->SetFont('Arial','B',11);
		$pdf->Text(85,25,"INVOICE WISE DUE LIST");
		$pdf->rect(5, 30, 200, 260);
		$first_table_x=25;
		$pdf->SetFont('Arial','B',10);
		$pdf->SetFont('Arial','B',11);
		$i=0;
	}
	function footer($pdf,$grand_total,$Line_y)
	{
		$pdf->SetFont('Arial','B',10);
		$pdf->Text(45, $Line_y+5, "Grand Total   : ");
		$pdf->Text(150, $Line_y+5, " ".$grand_total." ");
	}
	header_section($pdf,$CustomerType_name,$Profile);
	$i=0;
	$first_table_x=30;
	$grand_total=0;
	$Line_y=$first_table_x;
	foreach($invoice_array as $key=>$value) {
		$m=1;
		$l=1;
		$n=1;
		$pdf->SetFont('Arial','B',11);
		$product_name_position=65;
		$party_name=$value['party_name'];
		$inner_array_count=count($value['invoice_no']);
		if(($Line_y+($inner_array_count*5)+15)>290)
		{
			$Line_y=$first_table_x;
			header_section($pdf,$CustomerType_name,$Profile);
		}
		$party_name_y_axis=$first_table_x+($i*15)+($inner_array_count-1);
		$pdf->Text($product_name_position, $Line_y+5, $party_name); 
		$pdf->SetFont('Arial','',10);
		$pdf->SetFont('Arial','',9);
		$invoice_no=array_values($value['invoice_no']);
		foreach ($value['delivered_date'] as $key=>$value1)
		{
			$delivered_date=$value1;
			$pdf->Text(10, $Line_y+5+($m*5), date("d-m-Y", strtotime($delivered_date)));
			$now = time();
			$expected_days_diff=$now-strtotime($delivered_date);
			$expected_days=floor($expected_days_diff / (60 * 60 * 24))+1;
			$pdf->Text(30, $Line_y+5+($m*5), '('.$expected_days.' Days)');
			$m++;
		}
		foreach ($value['invoice_no'] as $key=>$value2)
		{
			$invoice_no=$value2;
			$pdf->Text(80,$Line_y+5+($l*5), "Sales Bill No:".$invoice_no);
			$l++;
		}
		$new_balance=0;
		foreach ($value['balance'] as $key=>$value3)
		{
			$balance=$value3;
			$pdf->Text(150,$Line_y+5+($n*5), $balance);
			$n++;
			$new_balance+=$balance;
		}
		$pdf->Text(80,$Line_y+($inner_array_count*5)+10, "Total Amount:");
		$pdf->Line(5,$Line_y+($inner_array_count*5)+10+5,205,$Line_y+($inner_array_count*5)+10+5);
		$pdf->Text(150,$Line_y+($inner_array_count*5)+10, $new_balance);
		$Line_y=$Line_y+($inner_array_count*5)+15;
		$i++;
		$grand_total+=$new_balance;
	}
	footer($pdf,$grand_total,$Line_y);
	$pdf->Output();
	exit;
}
public function PartyDueList()
{
	$Party_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('sub_group_id'=>15)));
	$Party=$this->Party->find('all',array(
		'order'=>array('AccountHead.name ASC'),
		));
	$this->set('Party_list',$Party_list);
	$this->set('Party',$Party);
	$new_Party_array=array();
	foreach ($Party as $key => $value) {
		$result['Party']=$value;
		$result['Balance_Total']=$this->get_oustanding_amount($value['Party']['account_head_id'],$value['AccountHead']['opening_balance']);
		$result['Balance_Total']=-($result['Balance_Total']);
		array_push($new_Party_array, $result);
	}
	$this->set('new_Party_array',$new_Party_array);
}
public function party_due_report_ajax()
{
	$conditions=array();
	if(!empty($this->request->data['name']))
	{
		$AccountHead_id=$this->AccountHead->find('first',array('conditions'=>array('AccountHead.name'=>$this->request->data['name'])));
		$Balance_Total=0;
		if(!empty($AccountHead_id)){
			$conditions['Party.account_head_id']=$AccountHead_id['AccountHead']['id'];   
		}
	}
	$Party=$this->Party->find('all',array('conditions'=>$conditions));
	$data['row']='';
	if($Party)
	{
		foreach ($Party as $key => $value) {
			$Balance_Total=$this->get_oustanding_amount($value['Party']['account_head_id'],$value['AccountHead']['opening_balance']);
			$Balance_Total= -($Balance_Total);
			$data['row']= $data['row'].'<tr>';
			$data['row']= $data['row'].'<td><span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span>
			<span>'.$value['AccountHead']['name'].'</span></td>';
			$data['row']= $data['row'].'<td>'.substr($value['Party']['place'],0,80).'</td>';
			$data['row']= $data['row'].'<td>'.$Balance_Total.'</td>';
			$data['row']= $data['row'].'</tr>';
		}
		$data['result']='Success';
	}
	else
	{
		$data['result']='Error';
	}
	echo json_encode($data);
	exit;
}
public function purchase_invoice_wise_print($party)
{
	$conditions=array();
	if($party!=0){
		$conditions['Party.account_head_id']=$party;
	}
	$Parties=$this->Party->find('all',array(
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			),
		'order' => array('Party.id' => 'ASC'),
		'conditions'=>$conditions,
		)
	);
	$invoice_array=array();
	foreach ($Parties as $key => $value)
	{
		$Purchases = $this->Purchase->find('all', array(
			'conditions' => array(
				'Purchase.account_head_id' =>$value['AccountHead']['id'],
				'Purchase.flag'=>1,
				'Purchase.status'=>array(2,3),
				),
			'fields' => array(
				'Purchase.*',
				'AccountHead.name',
				)
			)
		);
		$Journal_voucher=$this->Journal->find('list',array(
			'conditions'=>array(
				'Journal.debit'=>$value['AccountHead']['id'],
				'NOT' => array(
					'Journal.remarks LIKE' => 'Purchase Invoice No :%',
					),
				'Journal.flag=1',
				),
			'fields'=>array(
				'Journal.id',
				'Journal.amount',
				),
			));
		$voucher_amount=0;
		$voucher_balance=0;
		foreach ($Journal_voucher as $key3 => $value_amount) {
			$voucher_amount+=$value_amount;
		}
		foreach ($Purchases as $keyj =>$valuej)
		{
			$PurchasedItem=$this->PurchasedItem->find('list',array(
				'conditions'=>array(
					'PurchasedItem.purchase_id'=>$valuej['Purchase']['id'],
					),
				'fields'=>array(
					'PurchasedItem.discount',
					)
				));
		}
		$invoice_array_single=array();
		if (!empty($Purchases)) {
			$invoice_array_single['party_name'] = $value['AccountHead']['name'];
			$invoice_array_single['invoice_no'] = array();
			$invoice_array_single['balance'] = array();
			$invoice_array_single['delivered_date'] = array();
			$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
			if($AccountHead)
			{
				$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
			}
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;
			}
			if($outstanding_amount>=1)
			{
				array_push($invoice_array_single['delivered_date'],$outstanding_amount_date);
				array_push($invoice_array_single['invoice_no'], 'Opening Balance');
				array_push($invoice_array_single['balance'],$outstanding_amount);
			}
			foreach ($Purchases as $keySC2 => $valueSC2) {
// $Journal=$this->Journal->find('all',array(
// 	'conditions'=>array(
// 		'Journal.remarks'=>'Purchase Invoice No :'.$valueSC2['Purchase']['invoice_no'],
// 		'Journal.debit'=>$valueSC2['Purchase']['account_head_id'],
// 		'Journal.credit=1',
// 		'Journal.flag=1',
// 		),
// 	));
				$balance_amount=$valueSC2['Purchase']['grand_total'];
//$paid_amount=0;
// foreach ($Journal as $key0 => $amount) {
// 	$paid_amount+=$amount['Journal']['amount'];
// 	$balance_amount-=$amount['Journal']['amount'];
// }
				if($voucher_amount)
				{
					if($balance_amount<$voucher_amount)
					{
						$voucher_balance=$balance_amount;
						$balance_amount=0;
						$voucher_amount-=$voucher_balance;
					}
					else
					{
						$balance_amount-=$voucher_amount;
						$voucher_amount=0;
					}
				}
				if($valueSC2['Purchase']['grand_total']>0)
				{
					if($balance_amount){
						array_push($invoice_array_single['delivered_date'], $valueSC2['Purchase']['date_of_delivered']);
						array_push($invoice_array_single['invoice_no'], $valueSC2['Purchase']['invoice_no']);
						array_push($invoice_array_single['balance'],$balance_amount) ;  
					}
				}
			}
			if(count($invoice_array_single['invoice_no']))
			{
				array_push($invoice_array, $invoice_array_single);  
			}      
		}
		else
		{
			$invoice_array_single['party_name'] = $value['AccountHead']['name'];
			$invoice_array_single['invoice_no'] = array();
			$invoice_array_single['balance'] = array();
			$invoice_array_single['delivered_date'] = array();
			$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
			if($AccountHead)
			{
				$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
			}
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;
			}
			if($outstanding_amount>=1)
			{
				array_push($invoice_array_single['delivered_date'],$outstanding_amount_date);
				array_push($invoice_array_single['invoice_no'], 'Opening Balance');
				array_push($invoice_array_single['balance'],$outstanding_amount);
			}
			if(count($invoice_array_single['invoice_no']))
			{
				array_push($invoice_array, $invoice_array_single);  
			}      
		}
	}
	require('fpdf/fpdf.php');   
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	function header_section($pdf,$Profile){
		$pdf->SetFont('Arial','B',11);
		$pdf->AddPage();
		$pdf->Text(10, 7,$Profile['company_name']);
		$pdf->SetFont('Arial','',9);
		$pdf->Text(10, 12,$Profile['address_line_1']);
		$pdf->Text(10, 17,$Profile['address_line_2']);
		$pdf->Text(10, 22,$Profile['mobile']);
		$pdf->SetFont('Arial','B',11);
		$pdf->Text(85,22,"INVOICE WISE DUE LIST");
		$pdf->rect(5, 30, 200, 260);
		$first_table_x=25;
		$pdf->SetFont('Arial','B',10);
		$pdf->SetFont('Arial','B',11);
		$i=0;
	}
	function footer($pdf,$grand_total,$Line_y)
	{
		$pdf->SetFont('Arial','B',10);
		$pdf->Text(45, $Line_y+5, "Grand Total   : ");
		$pdf->Text(150, $Line_y+5, " ".$grand_total." ");
	}
	header_section($pdf,$Profile);
	$i=0;
	$first_table_x=30;
	$grand_total=0;
	$Line_y=$first_table_x;
	foreach($invoice_array as $key=>$value) {
		$m=1;
		$l=1;
		$n=1;
		$pdf->SetFont('Arial','B',11);
		$product_name_position=65;
		$party_name=$value['party_name'];
		$inner_array_count=count($value['invoice_no']);
		if(($Line_y+($inner_array_count*5)+15)>290)
		{
			$Line_y=$first_table_x;
			header_section($pdf,$Profile);
		}
		$party_name_y_axis=$first_table_x+($i*15)+($inner_array_count-1);
		$pdf->Text($product_name_position, $Line_y+5, $party_name); 
		$pdf->SetFont('Arial','',10);
		$pdf->SetFont('Arial','',9);
		$invoice_no=array_values($value['invoice_no']);
		foreach ($value['delivered_date'] as $key=>$value1)
		{
			$delivered_date=$value1;
			$pdf->Text(10, $Line_y+5+($m*5), date("d-m-Y", strtotime($delivered_date)));
			$now = time();
			$expected_days_diff=$now-strtotime($delivered_date);
			$expected_days=floor($expected_days_diff / (60 * 60 * 24))+1;
			$pdf->Text(30, $Line_y+5+($m*5), '('.$expected_days.' Days)');
			$m++;
		}
		foreach ($value['invoice_no'] as $key=>$value2)
		{
			$invoice_no=$value2;
			$pdf->Text(80,$Line_y+5+($l*5), "Purchases Bill No:".$invoice_no);
			$l++;
		}
		$new_balance=0;
		foreach ($value['balance'] as $key=>$value3)
		{
			$balance=$value3;
			$pdf->Text(150,$Line_y+5+($n*5), $balance);
			$n++;
			$new_balance+=$balance;
		}
		$pdf->Text(80,$Line_y+($inner_array_count*5)+10, "Total Amount:");
		$pdf->Line(5,$Line_y+($inner_array_count*5)+10+5,205,$Line_y+($inner_array_count*5)+10+5);
		$pdf->Text(150,$Line_y+($inner_array_count*5)+10, $new_balance);
		$Line_y=$Line_y+($inner_array_count*5)+15;
		$i++;
		$grand_total+=$new_balance;
	}
	footer($pdf,$grand_total,$Line_y);
	$pdf->Output();
	exit;
}
public function VATReport()
{
	$data['GSTReport']['from_date']=$from_date=date('d-m-Y',strtotime('-1 month'));
	$data['GSTReport']['to_date']=$to_date=date('d-m-Y');
	$SaleItem_DISTINCT_cgst=$this->SaleItem->find('all',array(
		'fields'=>array('DISTINCT SaleItem.tax'),
		)
	);
	$PurchaseItem_DISTINCT_cgst=$this->PurchasedItem->find('all',array(
		'fields'=>array('DISTINCT PurchasedItem.vat'),
		)
	);
	$GST_RATE=[];
	foreach ($SaleItem_DISTINCT_cgst as $key => $value) {
		$GST_RATE[floatval($value['SaleItem']['tax'])]=floatval($value['SaleItem']['tax'])."%";
	}
	foreach ($PurchaseItem_DISTINCT_cgst as $key => $value) {
		$GST_RATE[floatval($value['PurchasedItem']['vat'])]=floatval($value['PurchasedItem']['vat'])."%";
	}
	unset($GST_RATE[0]);
	$GST_RATE['Zero']='0%';
	$this->set(compact('GST_RATE'));
	$this->request->data=$data;
	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$GSTReport=['list'=>[],'total'=>[]];
	$cgst_in=0;
	$cgst_out=0;
	$sgst_in=0;
	$sgst_out=0;
	$igst_in=0;
	$igst_out=0;
	$total_in=0;
	$total_out=0;
	$taxable_value=0;
	foreach ($Product as $key => $value) {
		$single_GSTReport=$this->get_VATReport($key,$from_date,$to_date);
		if($single_GSTReport)
		{
			foreach ($single_GSTReport as $keyG => $valueG) {
// if($valueG['total']['in'] || $valueG['total']['out'])
// {
				$cgst_out+=$valueG['cgst']['out'];
				$cgst_in+=$valueG['cgst']['in'];
				$sgst_out+=$valueG['sgst']['out'];
				$sgst_in+=$valueG['sgst']['in'];
				$igst_out+=$valueG['igst']['out'];
				$igst_in+=$valueG['igst']['in'];
				$taxable_value+=$valueG['taxable_value'];
				$GSTReport['list'][]=$valueG;
// }
			}
		}
		$GSTReport['total']['cgst']['in']=$cgst_in;
		$GSTReport['total']['cgst']['out']=$cgst_out;
		$GSTReport['total']['sgst']['in']=$sgst_in;
		$GSTReport['total']['sgst']['out']=$sgst_out;
		$GSTReport['total']['igst']['in']=$igst_in;
		$GSTReport['total']['igst']['out']=$igst_out;
		$GSTReport['total']['taxable_value']=$taxable_value;
		$GSTReport['total']['total']['in']=$cgst_in+$sgst_in+$igst_in;
		$GSTReport['total']['total']['out']=$cgst_out+$sgst_out+$igst_out;
	}
	$this->set(compact('GSTReport'));
}
public function VATReport_ajax()
{
	$data=$this->request->data;
	$conditions=[];
	$product_type_id='';
	if(isset($data['product_type_id']))
	{
		if($data['product_type_id'])
		{
			$product_type_id=$data['product_type_id'];	
			$conditions['Product.product_type_id']=$product_type_id;
		}
	}
	if(isset($data['product_id']))
	{
		if($data['product_id'])
		{
			$product_id=$data['product_id'];	
			$conditions['Product.id']=$product_id;
		}
	}
	$type='';
	if(isset($data['type']))
	{
		if($data['type'])
		{
			$type=$data['type'];	
		}
	}
	$gst='';
	if(isset($data['gst']))
	{
		if($data['gst'])
		{
			$gst=$data['gst'];	
		}
	}
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$GSTReport=['list'=>[],'total'=>[]];
	$cgst_in=0;
	$cgst_out=0;
	$sgst_in=0;
	$sgst_out=0;
	$igst_in=0;
	$igst_out=0;
	$total_in=0;
	$total_out=0;
	$taxable_value=0;
	$Product=$this->Product->find('list',array(
		'conditions'=>$conditions,
		));
	foreach ($Product as $key => $value) {
		$single_GSTReport=$this->get_VATReport($key,$from_date,$to_date,$gst,$type,$product_type_id);
		if($single_GSTReport)
		{
			foreach ($single_GSTReport as $keysingle_ => $single_GSTReport_value) {
// if($single_GSTReport_value['total']['in'] || $single_GSTReport_value['total']['out'])
// {
				$cgst_out+=$single_GSTReport_value['cgst']['out'];
				$cgst_in+=$single_GSTReport_value['cgst']['in'];
				$sgst_out+=$single_GSTReport_value['sgst']['out'];
				$sgst_in+=$single_GSTReport_value['sgst']['in'];
				$igst_out+=$single_GSTReport_value['igst']['out'];
				$igst_in+=$single_GSTReport_value['igst']['in'];
				$taxable_value+=$single_GSTReport_value['taxable_value'];
				$GSTReport['list'][]=$single_GSTReport_value;
// }
			}
		}
		$GSTReport['total']['cgst']['in']=$cgst_in;
		$GSTReport['total']['cgst']['out']=$cgst_out;
		$GSTReport['total']['sgst']['in']=$sgst_in;
		$GSTReport['total']['sgst']['out']=$sgst_out;
		$GSTReport['total']['igst']['in']=$igst_in;
		$GSTReport['total']['igst']['out']=$igst_out;
		$GSTReport['total']['taxable_value']=$taxable_value;
		$GSTReport['total']['total']['in']=$cgst_in+$sgst_in+$igst_in;
		$GSTReport['total']['total']['out']=$cgst_out+$sgst_out+$igst_out;
	}
	echo json_encode($GSTReport);
	exit;
}	
public function SalesVATReport()
{
	$data=$this->request->data;
	$SalesGSTReport=['list'=>[],'total'=>[]];
	$single_GSTReport=[];
	$all_GSTReport=[];
	if(!$data)
	{
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0));
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 day",$time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('date1',$date1);
		$this->set('final',$final);
	}
	else
	{
		$from_date=$data['SalesVATReport']['from_date'];
		$to_date=$data['SalesVATReport']['to_date'];
		$from=date("Y-m-d", strtotime($from_date));
		$to=date("Y-m-d", strtotime($to_date));
		$this->set('date1',$to_date);
		$this->set('final',$from_date);
		$SalesGSTReport=$this->sales_vat_report($from,$to);
	}
	$this->set(compact('SalesGSTReport'));
}
public function sales_vat_report($from_date,$to_date)
{
	$SalesGSTReport=['list'=>[],'total'=>[]];
	$single_GSTReport=[];
	$all_GSTReport=[];
	$Sale=$this->Sale->find('all',array(
		'order' => 'Sale.invoice_no ASC',
		'conditions'=>array(
			'Sale.status=2',
//'Sale.account_head_id'=>$value['Customer']['account_head_id'],
			'Sale.date_of_delivered between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
			),
		'fields'=>array(
			'Sale.account_head_id',
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			'Sale.grand_total',
			),
		));
	foreach ($Sale as $keyS => $valueS) {
		$Customer_details=$this->Customer->findByAccountHeadId($valueS['Sale']['account_head_id']);
		$customer_name=$Customer_details['AccountHead']['name'];
		$customergstin=$Customer_details['Customer']['gstin'];
//$single_GSTReport['customer_name']=$customer_name;
		$single_GSTReport['customer_gstin']=$customergstin;
		$single_GSTReport['date']=date('d-m-Y',strtotime($valueS['Sale']['date_of_delivered']));
		$single_GSTReport['invoice_no']=$valueS['Sale']['invoice_no'];
		$single_GSTReport['invoice_amount']=$valueS['Sale']['grand_total'];
		$single_GSTReport['taxable_value']=0;
		$single_GSTReport['gross_total']=0;
		$single_GSTReport['vat_5_total']=0;
		$single_GSTReport['vat_amount']=0;
		foreach ($valueS['SaleItem'] as $keyitem => $valueitem) {
			$single_GSTReport['taxable_value']+=$valueitem['net_value'];
			$single_GSTReport['gross_total']+=$valueitem['total'];
			$single_GSTReport['vat_amount']+=$valueitem['tax_amount'];
			if($valueitem['tax']==5)
			{
				$single_GSTReport['vat_5_total']+=$valueitem['tax_amount'];
			}
		}
		$all_GSTReport[]=$single_GSTReport;
	}
	if($all_GSTReport)
	{
		foreach ($all_GSTReport as $keyG => $valueG) {
			$SalesGSTReport['invoice_date']=$valueG['date'];
			$SalesGSTReport['invoice_no']=$valueG['invoice_no'];
//$SalesGSTReport['customer_name']=$valueG['customer_name'];
			$SalesGSTReport['invoice_amount']=$valueG['invoice_amount'];
			$SalesGSTReport['taxable_value']=$valueG['taxable_value'];
			$SalesGSTReport['gross_total']=$valueG['gross_total'];
			$SalesGSTReport['vat_5_total']=$valueG['vat_5_total'];
			$SalesGSTReport['vat_amount']=$valueG['vat_amount'];
			$SalesGSTReport['gstin']=$valueG['customer_gstin'];
			$SalesGSTReport['list'][]=$valueG;
		}
	}
	return $SalesGSTReport;
}
public function export_sales_vat_report($from_date,$to_date) 
{
	$this->response->download("sales_vat_report.csv");
	$conditions=array();
	$from=date("Y-m-d", strtotime($from_date));
	$to=date("Y-m-d", strtotime($to_date));
	$SalesGSTReport=$this->sales_vat_report($from_date,$to_date);
	$stock_array=array();
	foreach($SalesGSTReport['list'] as $key1 => $value1) { 
		array_push($stock_array,$value1);
	}
	$this->set('stock_array', $stock_array);
	$this->layout = 'ajax';
	return false;
	exit;
}
public function sales_offline_vat_print($from_date,$to_date)
{
	try{
// $SalesGSTReport=['list'=>[],'total'=>[]];
// $single_GSTReport=[];
// $all_GSTReport=[];
		$SalesGSTReport=$this->sales_vat_report($from_date,$to_date);
		$final_array=array();
		$header=array(
			'Date',
			'Inv.No',
// 'Customer',
//'Inv.Amount',
			'Taxable Value',
			'Gross Total',
			'VAT Sales 5%',
// 'GST Sales 18%',
// 'GST Sales 28%',
			'VAT Amount',
// 'SGST',
// 'IGST',
// 'GST in No'
			);
		$width=array(
			'25',
			'20',
// '75',
//'30',
			'38',
			'38',
			'40',
// '37',
// '37',
// '18',
			'45',
// '20',
// '32'
			);
		foreach ($SalesGSTReport['list'] as $key => $value) {
			$final_array[$key][]=$value['date'];
			$final_array[$key][]=$value['invoice_no'];
//$final_array[$key][]=$value['customer_name'];
//$final_array[$key][]=$value['invoice_amount'];
			$final_array[$key][]=$value['taxable_value'];
			$final_array[$key][]=$value['gross_total'];
			$final_array[$key][]=$value['vat_5_total'];
// $final_array[$key][]=$value['18_total'];
// $final_array[$key][]=$value['28_total'];
// $final_array[$key][]=$value['cgst'];
			$final_array[$key][]=$value['vat_amount'];
// $final_array[$key][]=$value['igst'];
// $final_array[$key][]=$value['customer_gstin'];
		}
		require('fpdf/fpdf.php');
		$pdf = new FPDF('l');
		$pdf->addPage();
		$pdf->SetFont('Arial','',14);
// $pdf->Text(105, 10, 'NIGELLAS HERBAL HEALTH CARE');
// 	$pdf->setFont("Arial",'B','9');    
// 	$pdf->Text(80, 15, '7/77, Industrial Compound.Thiruvangoor,Calicut-673304,PH:+91 9745 005 600');
// 	$pdf->Text(110, 19, 'www.nigellas.in,info@nigellas.in');
// 	$pdf->Line(1,22,295,22);
		$pdf->SetFont('Arial','B','11');
// if($flag!='B2C')
// {
// 	$pdf->Text(15, 30, 'Sales Offline B2B GST Report of '.$customer_type_name.' for the Period From '.date('d-M-Y',strtotime($from_date)).' To '.date('d-M-Y',strtotime($to_date)));
// }
// else{
// 	$pdf->Text(15, 30, 'Sales Offline B2C GST Report of '.$customer_type_name.' for the Period From '.date('d-M-Y',strtotime($from_date)).' To '.date('d-M-Y',strtotime($to_date)));
// }
		$pdf -> SetY(32); 
// $pdf->text(120,20,'Sales Offline GST Report');
// $pdf->text(110,28,'Customer Type:');
// $pdf->text(155,28,$customer_type_name);
		$pdf->SetFont('Arial','',14);
		$pdf -> SetY(35); 
		for($i=0;$i<count($header);$i++)
			$pdf->Cell($width[$i],7,$header[$i],1,0,'C');
		$pdf->Ln();
		$pdf->SetFillColor(224,235,255);
		$fill = false;
		$pdf->SetFont('Arial','','10');
		foreach($final_array as $row)
		{
			$i=0;
			foreach($row as $col){
				$pdf->Cell($width[$i],6,$col,'LR',0,'L',$fill);
// $pdf->Cell($width[$i],6,$col,1,0,'L',$fill);
				$i++;
			}
			$pdf->Ln();
			$fill = !$fill;
		}
		$pdf->Cell(array_sum($width),0,'','T');
		$pdf->Output();
		exit;
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function ItemwiseVATReport()
{
	$data=$this->request->data;
	$list_array=array();
	$VATReport=['list'=>[],'total'=>[]];
	if(!$this->request->is('post'))
	{
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0));
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 day",$time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('date1',$date1);
		$this->set('final',$final);
		$VATReport['total']['net_amount']=0;
		$VATReport['total']['taxable_value']=0;
		$VATReport['total']['vat_amount']=0;
		$VATReport['total']['quantity']=0;
		$VATReport['total']['total_amount']=0;
		$VATReport['total']['total_amount']=0;
	}
	else
	{
		$from_date=$data['ItemwiseVATReport']['from_date'];
		$to_date=$data['ItemwiseVATReport']['to_date'];
		$from=date("Y-m-d", strtotime($from_date));
		$to=date("Y-m-d", strtotime($to_date));
		$this->set('date1',$to_date);
		$this->set('final',$from_date);
		$list_array=$this->itemwise_vat_report($from,$to);
		$net_amount=0;
		$taxable_value=0;
		$vat_amount=0;
		$quantity=0;
		$total_amount=0;
		$single_VATReport=$this->itemwise_vat_report($from,$to);
		if($single_VATReport)
		{
			foreach ($single_VATReport as $keyG => $valueG) {
				foreach ($valueG['single_item'] as $keyG => $valuesingle) {
					$net_amount+=$valuesingle['net_value'];
					$taxable_value+=$valuesingle['taxable_value'];
					$vat_amount+=$valuesingle['vat_amount'];
					$quantity+=$valuesingle['quantity'];
					$total_amount+=$valuesingle['total'];
					$VATReport['list'][]=$valuesingle;
				}
			}
		}
		$VATReport['total']['net_amount']=$net_amount;
		$VATReport['total']['taxable_value']=$taxable_value;
		$VATReport['total']['vat_amount']=$vat_amount;
		$VATReport['total']['quantity']=$quantity;
		$VATReport['total']['total_amount']=$total_amount;
	}
	$this->set('VATReport',$VATReport);
}
public function itemwise_vat_report($from,$to)
{
	$list_array=array();
	$list_array_single=array();
	$cutomertype=array();
	$SaleItem=$this->SaleItem->find('all',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'LEFT',
				'conditions'=>array('Sale.account_head_id=AccountHead.id')
				),
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
// array(
//   'table'=>'units',
//   'alias'=>'Unit',
//   'type'=>'RIGHT',
//   'conditions'=>array('SaleItem.unit_id=Unit.id')
//   ),
			),
		'conditions'=>array(
// 'Sale.account_head_id'=>$customer_id,
			'Sale.flag=1',
			'Sale.status=2',
			'Sale.date_of_delivered between ? and ?' => array($from,$to)
			),
		'fields'=>array(
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			'AccountHead.name',
			'Customer.place',
			'Customer.gstin',
			'SaleItem.net_value',
			'SaleItem.tax',
			'SaleItem.tax_amount',
			'SaleItem.quantity',
			'SaleItem.total',
			'Product.name',
//'Unit.name',
			'Product.hsn_code',
			),
		  'order'=>array('Sale.id DESC'),
		));
	$cutomertype['single_item']=[];
	foreach ($SaleItem as $keySOff => $valueSoff) {
		$invoice_no=$valueSoff['Sale']['invoice_no'];
		$place_trim=$valueSoff['Customer']['place'];
		$place_explod=explode(',', $place_trim);
		$place=$place_explod[0];
		$siglecutomertype['invoice_no']=$invoice_no;
		$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($valueSoff['Sale']['date_of_delivered']));
		$siglecutomertype['customer_name']=$valueSoff['AccountHead']['name'];
		$siglecutomertype['gststin']=$valueSoff['Customer']['gstin'];
		$siglecutomertype['place']=preg_replace('/[\n\r\t]+/', ' ', $place);
		$siglecutomertype['net_value']=$valueSoff['SaleItem']['net_value'];
		$siglecutomertype['vat']=$valueSoff['SaleItem']['tax'];
		$siglecutomertype['vat_amount']=$valueSoff['SaleItem']['tax_amount'];
		$siglecutomertype['taxable_value']=$valueSoff['SaleItem']['net_value'];
		$siglecutomertype['quantity']=$valueSoff['SaleItem']['quantity'];
		$siglecutomertype['total']=$valueSoff['SaleItem']['total'];
		$siglecutomertype['product_name']=str_replace(',', ' ', $valueSoff['Product']['name']);
//$siglecutomertype['unit_name']=$valueSoff['Unit']['name'];
		$siglecutomertype['hsn_code']=$valueSoff['Product']['hsn_code'];
		$cutomertype['single_item'][]=$siglecutomertype;
	}
	/******customer details hidden special case******/
// 	$Saleone=$this->Sale->find('first',array(
// 		'conditions'=>array('Sale.status'=>2,'Sale.flag'=>1,'Sale.invoice_no'=>"R196-1",'Sale.date_of_delivered between ? and ?'=>array($from,$to),),
// 		'fields'=>array(
// 			'AccountHead.id',
// 			'Sale.discount_amount',
// 			'AccountHead.name',
// 			'Executive.id',
// 			'Sale.id',
// 			'Sale.invoice_no',
// 			'Sale.date_of_delivered',
// 			'Executive.name',
// 		)
// 	));
// 					if($Saleone){
// 					$SaleItem1=$this->SaleItem->find('all',array(
// 					'conditions'=>array(
// 					'Sale.id'=>$Saleone['Sale']['id'],
// 					'Sale.date_of_delivered between ? and ?'=>array($from,$to),
// 					'Sale.status'=>2,'Sale.flag'=>1,
// 					),
// 					'fields'=>array(
// 						'Product.name',
// 					'SaleItem.net_value',
// 					'SaleItem.tax_amount',
// 					'SaleItem.total',
// 					'SaleItem.tax',
// 					'SaleItem.quantity'
// 					),
// 					));
// 					foreach ($SaleItem1 as $key => $value) {
// 			$siglecutomertype['invoice_no']=$Saleone['Sale']['invoice_no'];
// 		$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($Saleone['Sale']['date_of_delivered']));
// 		$siglecutomertype['customer_name']="";
// 		$siglecutomertype['gststin']="";
// 		$siglecutomertype['place']="";
// 		$siglecutomertype['net_value']=$value['SaleItem']['net_value'];
// 		$siglecutomertype['vat']=$value['SaleItem']['tax'];
// 		$siglecutomertype['vat_amount']=$value['SaleItem']['tax_amount'];
// 		$siglecutomertype['taxable_value']=$value['SaleItem']['net_value'];
// 		$siglecutomertype['quantity']=$value['SaleItem']['quantity'];
// 		$siglecutomertype['total']=$value['SaleItem']['total'];
// 		$siglecutomertype['product_name']=str_replace(',', ' ', $value['Product']['name']);
// //$siglecutomertype['unit_name']=$SaleItem1['Unit']['name'];
// 		$siglecutomertype['hsn_code']="";
// 		$cutomertype['single_item'][]=$siglecutomertype;
// 	}
// 	}
	$list_array[]=$cutomertype;
	return $list_array;
}
public function print_itemwise_vat_report($from_date,$to_date) 
{
	$this->response->download("vat_report_itemwise.csv");
	$conditions=array();
	$from=date("Y-m-d", strtotime($from_date));
	$to=date("Y-m-d", strtotime($to_date));
	$list_array=array();
	$list_array=$this->itemwise_vat_report($from,$to);
	$stock_array=array();
	foreach($list_array[0] as $key1 => $value1) { 
		foreach($value1 as $keysingle => $valuesingle) { 
			array_push($stock_array,$valuesingle);
		}
	}
	$this->set('Stock_array', $stock_array);
	$this->layout = 'ajax';
	return false;
	exit;
}
public function TaxWiseVATReport()
{
	$data=$this->request->data;
	$list_array=array();
	$VATReport=['list'=>[],'total'=>[]];
	if(!$this->request->is('post'))
	{
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0));
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 day",$time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('date1',$date1);
		$this->set('final',$final);
		$VATReport['total']['net_amount']=0;
		$VATReport['total']['taxable_value']=0;
		$VATReport['total']['vat_amount']=0;
		$VATReport['total']['quantity']=0;
		$VATReport['total']['total_amount']=0;
		$VATReport['total']['total_amount']=0;
	}
	else
	{
		$from_date=$data['TaxWiseVATReport']['from_date'];
		$to_date=$data['TaxWiseVATReport']['to_date'];
		$from=date("Y-m-d", strtotime($from_date));
		$to=date("Y-m-d", strtotime($to_date));
		$this->set('date1',$to_date);
		$this->set('final',$from_date);
		$VATReport=$this->tax_wise_vat_report($from,$to);
	}
	$this->set('VATReport',$VATReport);
}
public function tax_wise_vat_report($from,$to)
{
	$list_array=array();
	$list_array_single=array();
	$cutomertype=array();
	$all_taxes_array=array();
	$return['total']['taxable_value']=0;
	$return['total']['vat_amount']=0;
	$return['total']['quantity']=0;
	$return['total']['total_amount']=0;
	$Sale=$this->Sale->find('list',array(
		'conditions'=>array(
			'status=2',
			'flag=1',
			),
		'fields'=>array(
			'Sale.id',
			'Sale.invoice_no',
			),
		));
	$invoice_wise_tax_list=[];
	$state_id=$this->Global_Var_Profile['Profile']['state_id'];
	foreach ($Sale as $sale_id => $invoice_no) {
		$SaleItem=$this->SaleItem->find('all',array(
			'joins'=>array(
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'LEFT',
					'conditions'=>array('Sale.account_head_id=AccountHead.id')
					),
				array(
					'table'=>'customers',
					'alias'=>'Customer',
					'type'=>'INNER',
					'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
					),
// array(
// 	'table'=>'units',
// 	'alias'=>'Unit',
// 	'type'=>'RIGHT',
// 	'conditions'=>array('Product.unit_id=Unit.id')
// 	),
				),
			'conditions'=>array(
				'Sale.id'=>$sale_id,
				'Sale.flag=1',
				'Sale.status=2',
				'Sale.date_of_delivered between ? and ?' => array($from,$to)
				),
			'fields'=>array(
				'Sale.invoice_no',
				'Sale.date_of_delivered',
				'AccountHead.name',
				'Customer.place',
				'Customer.state_id',
//'Customer.gstin',
				'SaleItem.tax',
				'SaleItem.tax_amount',
				'SaleItem.net_value',
				'SaleItem.quantity',
				'SaleItem.total',
				'Product.name',
				'Sale.discount_amount'
//'Unit.name',
				),
			'order'=>array('Sale.id DESC'),
			));
		foreach ($SaleItem as $key => $valueSoff) {
			$invoice_no=$valueSoff['Sale']['invoice_no'];
			$place_trim=$valueSoff['Customer']['place'];
			$place_explod=explode(',', $place_trim);
			$place=$place_explod[0];
			$siglecutomertype['invoice_no']=$invoice_no;
			$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($valueSoff['Sale']['date_of_delivered']));
			$siglecutomertype['customer_name']=$valueSoff['AccountHead']['name'];
			$siglecutomertype['customer_state_id']=$valueSoff['Customer']['state_id'];
//$siglecutomertype['gststin']=$valueSoff['Customer']['gstin'];
			$siglecutomertype['place']=preg_replace('/[\n\r\t]+/', ' ', $place);
			$siglecutomertype['vat']=$valueSoff['SaleItem']['tax'];
			$siglecutomertype['vat_amount']=$valueSoff['SaleItem']['tax_amount'];
			$siglecutomertype['taxable_value']=$valueSoff['SaleItem']['net_value'];
			$siglecutomertype['quantity']=$valueSoff['SaleItem']['quantity'];
			$siglecutomertype['total']=$valueSoff['SaleItem']['total'];
//$siglecutomertype['unit_name']=$valueSoff['Unit']['name'];
			$return['total']['taxable_value']+=$siglecutomertype['taxable_value'];
			$return['total']['vat_amount']+=$siglecutomertype['vat_amount'];
			$return['total']['quantity']+=$siglecutomertype['quantity'];
			$return['total']['total_amount']+=$siglecutomertype['total'];
			if(isset($invoice_wise_tax_list[$invoice_no]))
			{
				if(isset($invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]))
				{
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['quantity']+=$siglecutomertype['quantity'];
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['vat_amount']+=$siglecutomertype['vat_amount'];
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['taxable_value']+=$siglecutomertype['taxable_value'];
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['total']+=$siglecutomertype['total'];
				}
				else
				{
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]=$siglecutomertype;
				}
			}
			else
			{
				$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]=$siglecutomertype;
			}
		}

	}
	/*****special case*****/
		$Saleone=$this->Sale->find('first',array(
		'conditions'=>array('Sale.status'=>2,'Sale.flag'=>1,'Sale.invoice_no'=>"R196-1",'Sale.date_of_delivered between ? and ?'=>array($from,$to),),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Sale.id',
			'Sale.invoice_no',
			'Executive.name',
			'Sale.date_of_delivered'
		)
	));
		// $this->SaleItem->virtualFields = array(
		// 	'grand_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
		// 	'grand_tax_amount' => "SUM(SaleItem.tax_amount)",
		// 	'total_quantity' => "SUM(SaleItem.quantity)",
		// 	'tax' => "SaleItem.tax",
  //            'grand_total' => "SUM(((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount))",
		// );
		// 			if($Saleone){
		// 			$SaleItem1=$this->SaleItem->find('first',array(
		// 			'conditions'=>array(
		// 			'Sale.id'=>$Saleone['Sale']['id'],
		// 			'Sale.date_of_delivered between ? and ?'=>array($from,$to),
		// 			'Sale.status'=>2,'Sale.flag'=>1,
		// 			),
		// 			'fields'=>array(
		// 			'SaleItem.grand_net_value',
		// 			'SaleItem.grand_tax_amount',
		// 			'SaleItem.grand_total',
		// 			'SaleItem.tax',
		// 			'SaleItem.total_quantity'
		// 			),
		// 			));
		// 	$siglecutomertype['invoice_no']=$Saleone['Sale']['invoice_no'];
		// 	$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($Saleone['Sale']['date_of_delivered']));
		// 	$siglecutomertype['customer_name']="";
		// 	$siglecutomertype['customer_state_id']="";
		// 	$siglecutomertype['place']="";
		// 	$siglecutomertype['vat']=$SaleItem1['SaleItem']['tax'];
		// 	$siglecutomertype['vat_amount']=$SaleItem1['SaleItem']['grand_tax_amount'];
		// 	$siglecutomertype['taxable_value']=$SaleItem1['SaleItem']['grand_net_value'];
		// 	$siglecutomertype['quantity']=$SaleItem1['SaleItem']['total_quantity'];
		// 	$siglecutomertype['total']=$SaleItem1['SaleItem']['grand_total'];
		// 	$return['total']['taxable_value']+=$siglecutomertype['taxable_value'];
		// 	$return['total']['vat_amount']+=$siglecutomertype['vat_amount'];
		// 	$return['total']['quantity']+=$siglecutomertype['quantity'];
		// 	$return['total']['total_amount']+=$siglecutomertype['total'];
		// 	if(isset($invoice_wise_tax_list[$invoice_no]))
		// 	{
		// 		if(isset($invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]))
		// 		{
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['quantity']+=$siglecutomertype['quantity'];
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['vat_amount']+=$siglecutomertype['vat_amount'];
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['taxable_value']+=$siglecutomertype['taxable_value'];
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['total']+=$siglecutomertype['total'];
		// 		}
		// 		else
		// 		{
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]=$siglecutomertype;
		// 		}
		// 	}
		// 	else
		// 	{
		// 		$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]=$siglecutomertype;
		// 	}
		// 			}
           
	$return['list']=$invoice_wise_tax_list;
	return $return;
}
public function print_tax_wise_vat_report($from_date,$to_date) 
{
	$this->response->download("gst_report_vat_wise.csv");
	$conditions=array();
	$from=date("Y-m-d", strtotime($from_date));
	$to=date("Y-m-d", strtotime($to_date));
	$list_array=array();
	$list_array=$this->tax_wise_vat_report($from,$to);
	$stock_array=array();
	foreach($list_array['list'] as $key1 => $value1) { 
		foreach($value1 as $keysingle => $valuesingle) { 
			array_push($stock_array,$valuesingle);
		}
	}
	$this->set('Stock_array', $stock_array);
	$this->layout = 'ajax';
	return false;
	exit;
}
public function General_Journal_Debit_Credit_function($account_head_id)
{
	$account_single=[];
	$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)",);
	$Journal_Debit=$this->Journal->findByDebitAndFlag($account_head_id,'1',['Journal.total_amount']);
	$Journal_Credit=$this->Journal->findByCreditAndFlag($account_head_id,'1',['Journal.total_amount']);
	$account_single['credit']=$Journal_Credit['Journal']['total_amount'];
	$account_single['debit']=$Journal_Debit['Journal']['total_amount'];
	return $account_single;
}
public function DayRegisterPrint()
{
	$this->request->data['Reports']['from']=date('d-m-Y');
	$this->request->data['Reports']['to']=date('d-m-Y');
	$routes=$this->Route->find('list');
	$this->set(compact('routes'));
}
public function DayRegisterPrintRoute()
{
	$this->request->data['Reports']['from']=date('d-m-Y');
	$this->request->data['Reports']['to']=date('d-m-Y');
	$routes=$this->Route->find('list');
	$this->set(compact('routes'));
}
public function CheckDayRegister($route_id,$register)
{
	$register = date('Y-m-d',strtotime($register));
	$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->findByRouteId($route_id);
	$executive=$ExecutiveRouteMapping['ExecutiveRouteMapping']['executive_id'];
	$DayRegister = $this->DayRegister->findAllByExecutiveIdAndDate($executive,$register,['DayRegister.id','DayRegister.id']);
	$return['result']='Empty';
	if($DayRegister)
	{
		$return['result']='Success';
		$list=[];
		foreach ($DayRegister as $key => $value) {
			$list[$value['DayRegister']['id']]=$value['DayRegister']['id'];
		}
		$return['list']=$list;
	}
	echo json_encode($return); exit;
}
public function CheckDayRegister1($route_id,$register)
{
	$register = date('Y-m-d',strtotime($register));
	$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->findByRouteId($route_id);
	$executive=$ExecutiveRouteMapping['ExecutiveRouteMapping']['executive_id'];
	$DayRegister = $this->DayRegister->findAllByExecutiveIdAndDate($executive,$register,['DayRegister.id','DayRegister.id']);
	$cash_id = $this->Route->field('Route.account_head_id',array('Route.id ' =>$route_id));
	$return['result']='Empty';
	if($DayRegister)
	{
		if($cash_id)
		{
			$return['result']='Success';
			$list=[];
			foreach ($DayRegister as $key => $value) {
				$list[$value['DayRegister']['id']]=$value['DayRegister']['id'];
			}
			$return['list']=$list;
		}
	}
	echo json_encode($return); exit;
}
public function dcrpdf($day_register_id,$register,$closed)
{
	$register = date('Y-m-d',strtotime($register));
	$closed = date('Y-m-d',strtotime($closed));
	$date_build = date_parse_from_format("Y-m-d", $register);
	$month = $date_build["month"];
	$content['display_date'] = date('d-m-Y',strtotime($register));
	$footer_content['initial_stock']=0;
	$footer_content['loading_stock']=0;
	$content['route_name']='';
	$day = $this->DayRegister->findById($day_register_id);
	$executive = $day['DayRegister']['executive_id'];
	if($day)
	{
		$footer_content['initial_stock'] = $day['DayRegister']['initial_stock'];
		$footer_content['loading_stock'] = $day['DayRegister']['loading_stock'];
		$content['route_name'] = $day['Route']['name'];
		$day_register_id=$day['DayRegister']['id'];
	}
	$closing_stock = 0;
	$close = $this->ClosingDay->findByExecutiveIdAndDate($executive,$closed);
	if($close)
		$closing_stock = $close['ClosingDay']['closing_stock'];
	$footer_content['closing_stock'] =$closing_stock;
	$warehouse_id = $this->Executive->field('Executive.warehouse_id',array('Executive.id ' => $executive));
	$content['warehouse_name'] = $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $warehouse_id));
	$cash_id = $this->Executive->field('Executive.account_head_id',array('Executive.id ' =>$executive));

	$CustomerList=$this->AccountHead->find('all',array(
		"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('Customer.account_head_id=AccountHead.id'),
				),
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'inner',
				"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
		'fields'=>array(
			'Customer.code',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			),
		));
	$receipt_visit = 0;
	$All_Customer=[];
	if($CustomerList)
	{
		$j =0;
		$no_sale_count =0;
		$max_sale = 0;
		$max_credit_sale = 0;
		$max_cash_sale = 0;
		$max_receipt_sale = 0;
		$max_balance =0;
		$current_market_outstanding =0;
		foreach ($CustomerList as $key => $value) {
			$total_sale =0;
			$Single_Customer['id']=$value['AccountHead']['id'];
			$Single_Customer['name']=$value['AccountHead']['name'];
			$Single_Customer['shope_code']=$value['Customer']['code'];
			$Single_Customer['sort'] = 1;
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$value['AccountHead']['id'],
					'Sale.executive_id'=>$executive,
// 'Sale.date_of_delivered between ? and ?'=>[$register,$closed],
					'Sale.day_register_id'=>$day_register_id,
					'Sale.status'=>2,
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					'Sale.date_of_delivered',
					),
				));
			$All_sale=[];
			$cash_sale_total =0;
			$collected =0;
			foreach ($Sale as $key => $row) {
				$total_sale+=$row['Sale']['grand_total'];
				$remarks='Sale Invoice No :'. $row['Sale']['invoice_no'];
				$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
				$Journal_cash_sale=$this->Journal->find('first',array(
					'conditions'=>array(
						'credit'=>$value['AccountHead']['id'],
						'debit'=>$cash_id,
						'flag=1',
						'day_register_id'=>$day_register_id,
						'remarks'=>$remarks,
						'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
//'date'=>$row['Sale']['date_of_delivered'],
						),
					'fields'=>array('Journal.total_amount'),
					));
				$cash_sale_total+=$Journal_cash_sale['Journal']['total_amount'];
			}
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
			$Journal_credit=$this->Journal->find('first',array(
				'conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'flag=1',
//'executive_id'=>$executive,
					'day_register_id'=>$day_register_id,
					'debit'=>$cash_id,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
// 'date between ? and ?'=>[$register,$closed],
					),
				'fields'=>array('Journal.total_amount'),
				));
			$receipt_flag = 0;
			$collected+=$Journal_credit['Journal']['total_amount'];
			$credit_sale = $total_sale - $cash_sale_total;
			if($credit_sale<0) $credit_sale = 0.00;
			if($collected<0) $collected = 0.00;
			$collected-=$cash_sale_total;
			$max_sale+=$total_sale;
			$max_credit_sale+=$credit_sale;
			$max_cash_sale+=$cash_sale_total;
			$max_receipt_sale+=$collected;
			$Single_Customer['receipt'] = $collected;
			$Single_Customer['cash_sale'] = $cash_sale_total;
			$Single_Customer['credit_sale'] = $credit_sale;
			$No_flag = 0;
			if($total_sale==0 && $collected==0){
				$NoSale=$this->NoSale->find('all',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
						'NoSale.executive_id'=>$executive,
// 'NoSale.date between ? and ?'=>[$register,$closed],
						'NoSale.day_register_id'=>$day_register_id,
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',
						),
					));
				foreach ($NoSale as $key => $row) {
					$No_flag = 1;
					$no_sale_count++;
					$Single_Customer['sort'] = 0;
					$Single_Customer['credit_sale'] = 'No Sale';
					$Single_Customer['receipt'] = '';
					$Single_Customer['cash_sale'] = $row['NoSale']['description'];
				}
			}
			if($total_sale==0 && $collected!=0 && $No_flag==0){
				$receipt_visit++;
			}
			$Debit_N_Credit_function=$this->General_Journal_Debit_Credit_function($value['AccountHead']['id']);
			if($total_sale!=0 || $collected!=0 ||$No_flag!=0)
			{
				$Single_Customer['balance']=$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
				$max_balance+=$Single_Customer['balance'];
				array_push($All_Customer, $Single_Customer);
			}
			$single_market_outstanding =$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
			$current_market_outstanding =$current_market_outstanding + $single_market_outstanding;
		}
	}
	array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
	$content['All_Customer'] = $All_Customer;
	$postive_visit_sale=$this->Sale->find('count',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
// 'Sale.date_of_delivered between ? and ?'=>[$register,$closed],
			'Sale.day_register_id'=>$day_register_id,
			'Sale.is_erp'=>0
			),
		'group' => array('Sale.account_head_id'),
		));
	$footer_content['visit'] = count($All_Customer);
	$visit = count($All_Customer);
	$postive_visit = $postive_visit_sale + $receipt_visit;
	$footer_content['postive_visit'] = $postive_visit;
	$strike_rate =0;
	if($visit) $strike_rate = ($postive_visit/$visit)*100;
	$footer_content['strike_rate'] = number_format((float)$strike_rate, 2, '.', '');
	$footer_content['max_balance'] =number_format((float)$max_balance, 2, '.', '');
	$footer_content['max_credit_sale'] =number_format((float)$max_credit_sale, 2, '.', '');
	$footer_content['max_cash_sale'] = number_format((float)$max_cash_sale, 2, '.', '');
	$footer_content['max_receipt_sale'] = number_format((float)$max_receipt_sale, 2, '.', '');
	$footer_content['max_sale'] = number_format((float)$max_sale, 2, '.', '');
	$footer_content['current_market_outstanding'] = number_format((float)$current_market_outstanding, 2, '.', '');
	$Month_sale_amount =0;
	$MonthSale_list=$this->Sale->find('all',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
			'Sale.day_register_id'=>$day_register_id,
			'Sale.status'=>2
			),
		'fields'=>array(
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			),
		));
	$Monthly_cash_sale = 0;
	foreach ($MonthSale_list as $key => $value) {
		$remarks='Sale Invoice No :'. $value['Sale']['invoice_no'];
		$cash_sale_row = $this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$cash_id,
				'flag=1',
				'day_register_id'=>$day_register_id,
				'remarks'=>$remarks,
				'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
				'date'=>$value['Sale']['date_of_delivered'],
				),
			'fields'=>array('Journal.amount',),
			));
		if(!empty($cash_sale_row)){
			$Monthly_cash_sale = $Monthly_cash_sale + $cash_sale_row['Journal']['amount'];
		}
	}
	$MonthSale=$this->Sale->find('first',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
			'Sale.day_register_id'=>$day_register_id,
			'Sale.status'=>2
			),
		'fields'=>array(
			'sum(Sale.grand_total) as monthly_sale',
			),
		));
	if($MonthSale){
		$Month_sale_amount =$MonthSale[0]['monthly_sale'];
	}
	$footer_content['Month_sale_amount'] = number_format((float)$Month_sale_amount, 2, '.', '');
	$cash_id = $this->Executive->field('Executive.account_head_id',array('Executive.id ' => $executive));
	$Month_collection_amount =0;
	$MonthCollection=$this->Journal->find('first',array(
		'conditions'=>array(
			'debit'=>$cash_id,
			'flag=1',
			'day_register_id'=>$day_register_id,
			'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
			'month(date)'=>$month,
			),
		'fields'=>array(
			'sum(Journal.amount) as monthly_amount',
			),
		));
	if(!empty($MonthCollection)){
		$Month_collection_amount =$MonthCollection[0]['monthly_amount'];
	}
	$footer_content['Month_collection_amount'] = number_format((float)$Month_collection_amount, 2, '.', '');
	require('fpdf/fpdf.php');
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	define('FPDF_FONTPATH','fpdf/font');
	function header_section($pdf,$Profile,$content) {
		$pdf->SetFont('Arial','B',8.4);
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],-1,1,-220);
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(0,100,0);
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(200,00,00);
		$pdf->SetTextColor(10,10,10);
		$pdf->SetFont('Arial','B',8);
		$head_table_x=28;
		$pdf->rect(150, $head_table_x-10, 55, 8);
		$pdf->Text(152, 21, "Route : ".$content['route_name']);
		$pdf->rect(15, $head_table_x, 120, 8);
		$pdf->SetFont('Arial','',11);
		$pdf->Text(18, 33, "Warehouse : ".$content['warehouse_name']);
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(80, 26, "DAILY SALES REPORT");
		$pdf->rect(150, $head_table_x, 55, 8);
		$pdf->SetFont('Arial','B',9);
		$pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($content['display_date'])));
		$first_table_x=$head_table_x+13;
		$pdf->rect(15, $first_table_x, 190, 215);
		$slno_x=16;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
		$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200);
		$item_x=$slno_x+34;
		$item_line_x=$item_x+100;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($item_x, $first_table_x+10, "Customer");
		$tax_line_x=$item_x+35;
		$tax_x=$item_x+35;
		$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200);
		$pdf->Text($tax_x, $first_table_x+5, "");
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($tax_x+5, $first_table_x+5+5, "Credit Sale");
		$tax_line_x=$tax_x+35;
		$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200);
		$unit_x=$tax_x+35;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($unit_x+5, $first_table_x+5+5, "Cash Sale");
		$pdf->Line($item_line_x-3, $first_table_x,$item_line_x-3, 200);
		$qty_x=$item_line_x+4;
		$qty_line_x=$qty_x+8;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($qty_x, $first_table_x+5+5, "Receipt");
		$unit_price_x=$qty_line_x+2;
		$unit_price_line_x=$unit_price_x+15;
		$pdf->SetFont('Arial','B',7);
		$net_amount_x=$qty_line_x+2;
		$net_amount_line_x=$net_amount_x+16;
		$pdf->Line($net_amount_line_x-4, $first_table_x,$net_amount_line_x-4, 200);
		$total_x=$unit_price_line_x+2;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($total_x, $first_table_x+5+5, "O/S Balance");
		$pdf->Line(15, $first_table_x+12, 205, $first_table_x+12);
		$pdf->Line(15, 200, 205, 200);
		$pdf->Line(15, 205, 205, 205);
		$i=0;
	}
	function footer($pdf,$footer_content)
	{
		$footer_line=203;
		$pdf->Line(177, 205,177, 215);
		$pdf->Text(190, 211, $footer_content['max_balance']);
		$pdf->Line(148, 205,148, 215);
		$pdf->Text(159, 211, $footer_content['max_receipt_sale']);
		$pdf->Line(120, 205,120, 215);
		$pdf->Text(131, 211, $footer_content['max_cash_sale']);
		$pdf->Line(85, 205,85, 215);
		$pdf->Text(93, 211, $footer_content['max_credit_sale']);
		$pdf->Text(20, 211, 'Total :');
		$pdf->Line(15, 215, 205, 215);
		$pdf->Line(185, 215,185, 235);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 220, $footer_content['Month_sale_amount']);
		$pdf->Text(135, 220, 'Monthly Sale');
		$pdf->Text(190, 227, $footer_content['Month_collection_amount']);
		$pdf->Text(135, 227, 'Monthly Collection');
		$pdf->Line(105, 215,105, 235);
		$pdf->Line(75, 215,75, 235);
		$pdf->Text(80, 219, $footer_content['initial_stock']);
		$pdf->Text(20, 219, 'Opening Stock ');
		$pdf->Text(80, 223, $footer_content['loading_stock']);
		$pdf->Text(20, 223, 'Today Loading ');
		$pdf->Text(80, 227, $footer_content['closing_stock']);
		$pdf->Text(20, 227, 'Closing Stock ');
		$pdf->Text(80, 231, $footer_content['current_market_outstanding']);
		$pdf->Text(20, 231, 'Current Market Outstanding ');
		$pdf->Line(15, 235, 205, 235);
		$totalSale = ($footer_content['max_cash_sale'] + $footer_content['max_credit_sale']);
		$today_collection = ($footer_content['max_cash_sale'] + $footer_content['max_receipt_sale']);
		$pdf->Line(185, 235,185, 256);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 241, number_format((float)$totalSale,2));
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(135, 241, 'Today Sale');
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 250, number_format((float)$today_collection,2));
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(135, 250, 'Today Collection');
		$pdf->Line(105, 235,105, 256);
		$pdf->Line(75, 235,75, 256);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(80, 240, $footer_content['visit']);
		$pdf->Text(20, 240, 'Total Visit');
		$pdf->Text(80, 245, $footer_content['postive_visit']);
		$pdf->Text(20, 245, 'Positive Visit');
		$pdf->Text(80, 250, $footer_content['strike_rate'].'%');
		$pdf->Text(20, 250, 'Strike Rate');
		$pdf->SetFont('Arial','B',8);
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(20, 295, "Salesman");
		$pdf->Text(60, 295, "Supervisor");
		$pdf->Text(100, 295, "Accountant");
		$pdf->Text(140, 295, "Manager ");
	}
	$slno =1;
	header_section($pdf,$Profile,$content);
	$i=0;
	$head_table_x=10;
	$first_table_x=$head_table_x+36;
	$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; 
	foreach($content['All_Customer'] as $key=>$value) {
		$sl_no_v_x=18;
		$pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $slno);
		$product_name_v_x=$sl_no_v_x+10;
		$pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['name']);
		$tax_v_x=$product_name_v_x+65;
		$pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['credit_sale']);
		$unit_v_x=$tax_v_x+35;
		$pdf->Text($unit_v_x, $first_table_x+11+(5*$i), $value['cash_sale']);
		$quantity_v_x=$product_name_v_x+125;
		$pdf->Text($quantity_v_x, $first_table_x+11+(5*$i),floatval($value['receipt']) );
		$unit_price_v_x=$quantity_v_x+12;
		$total_v_x=$unit_price_v_x+18;
		if($value['balance']=='-0.00'){
			$value['balance'] ='0.00';
		}
		$pdf->Text($total_v_x, $first_table_x+11+(5*$i),floatval($value['balance']));
		$i++;
		$slno++;
		if($i>28)
		{
			$i=0;
			header_section($pdf,$Profile,$content);
		}
	}
	footer($pdf,$footer_content);
	$pdf->Output();
	exit;
}

public function dcrpdf1($day_register_id,$register,$closed)
{
	$register = date('Y-m-d',strtotime($register));
	$closed = date('Y-m-d',strtotime($register));
	$date_build = date_parse_from_format("Y-m-d", $register);
	$month = $date_build["month"];
	$content['display_date'] = date('d-m-Y',strtotime($register));
	$footer_content['initial_stock']=0;
	$footer_content['loading_stock']=0;
	$content['route_name']='';
	$day = $this->DayRegister->findById($day_register_id);
//pr($day);
//exit;
	$route_id=$day['DayRegister']['route_id'];
// pr($route_id);
// exit;
	$executive = $day['DayRegister']['executive_id'];
	if($day)
	{
		$footer_content['day_km']= $day['DayRegister']['km'];
		$day_time = new DateTime($day['DayRegister']['created_at']);
		$day_time = $day_time->format('H:i');
		$footer_content['day_time']= $day_time;
		$footer_content['initial_stock'] = $day['DayRegister']['initial_stock'];
		$footer_content['loading_stock'] = $day['DayRegister']['loading_stock'];
		$content['route_name'] = $day['Route']['name'];
		$day_register_id=$day['DayRegister']['id'];
	}
	$closing_stock = 0;
	$close = $this->ClosingDay->findByExecutiveIdAndDateAndDayRegisterId($executive,$closed,$day_register_id);
//pr($close);
//exit;
	if($close){
		$closing_stock = $close['ClosingDay']['closing_stock'];
		$footer_content['closing_stock'] =$closing_stock;
		$footer_content['close_km']= $close['ClosingDay']['km'];
		$close_time = new DateTime($close['ClosingDay']['closing_at']);
		$close_time = $close_time->format('H:i');

		$footer_content['close_time']= $close_time;
	}
	$warehouse_id = $this->Executive->field('Executive.warehouse_id',array('Executive.id ' => $executive));
	$content['warehouse_name'] = $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $warehouse_id));
	$cash_id = $this->Route->field('Route.account_head_id',array('Route.id ' =>$day['Route']['id']));

	$CustomerList=$this->AccountHead->find('all',array(
		"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('Customer.account_head_id=AccountHead.id'),
				),
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'inner',
				"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
		'fields'=>array(
			'Customer.code',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			),
		));
	$receipt_visit = 0;
	$All_Customer=[];
	if($CustomerList)
	{
		$j =0;
		$no_sale_count =0;
		$max_sale = 0;
		$max_credit_sale = 0;
		$max_cash_sale = 0;
		$max_receipt_sale = 0;
		$max_balance =0;
		$current_market_outstanding =0;
		foreach ($CustomerList as $key => $value) {
			$total_sale =0;
			$Single_Customer['id']=$value['AccountHead']['id'];
			$Single_Customer['name']=$value['AccountHead']['name'];
			$Single_Customer['shope_code']=$value['Customer']['code'];
			$Single_Customer['sort'] = 1;
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$value['AccountHead']['id'],
					'Sale.executive_id'=>$executive,
// 'Sale.date_of_delivered between ? and ?'=>[$register,$closed],
					'Sale.day_register_id'=>$day_register_id,
					'Sale.status'=>2,
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					'Sale.date_of_delivered',
					'Sale.created_at',
					),
				));

			$All_sale=[];
			$cash_sale_total =0;
			$collected =0;
			foreach ($Sale as $key => $row) {
				$total_sale+=$row['Sale']['grand_total'];
				$remarks='Sale Invoice No :'. $row['Sale']['invoice_no'];
				$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
				$Journal_cash_sale=$this->Journal->find('first',array(
					'conditions'=>array(
						'credit'=>$value['AccountHead']['id'],
						'debit'=>$cash_id,
						'flag=1',
						'day_register_id'=>$day_register_id,
						'remarks'=>$remarks,
						'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
//'date'=>$row['Sale']['date_of_delivered'],
						),
					'fields'=>array('Journal.total_amount'),
					));
				$cash_sale_total+=$Journal_cash_sale['Journal']['total_amount'];
			}
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
			$Journal_credit=$this->Journal->find('first',array(
				'conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'flag=1',
					'executive_id'=>$executive,
					'day_register_id'=>$day_register_id,
					'debit'=>$cash_id,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
// 'date between ? and ?'=>[$register,$closed],
					),
				'fields'=>array('Journal.total_amount'),
				));
//pr($Journal_credit);
			$Journal_date=$this->Journal->find('all',array(
				'conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'flag=1',
					'executive_id'=>$executive,
					'day_register_id'=>$day_register_id,
					'debit'=>$cash_id,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
// 'date between ? and ?'=>[$register,$closed],
					),
				'fields'=>array('Journal.created_at'),
				'order' => array('Journal.id DESC'),
				));
			$Single_Customer['time']="";
			if($Sale){
				$time = new DateTime($Sale[0]['Sale']['created_at']);
				$time = $time->format('H:i');
				$Single_Customer['time']=$time;
			}
			else{
				if($Journal_date){
					$time = new DateTime($Journal_date[0]['Journal']['created_at']);
					$time = $time->format('H:i');
					$Single_Customer['time']=$time;
				}
			}
			$receipt_flag = 0;
			$collected+=$Journal_credit['Journal']['total_amount'];
			$credit_sale = $total_sale - $cash_sale_total;
			if($credit_sale<0) $credit_sale = 0.00;
			if($collected<0) $collected = 0.00;
			$collected-=$cash_sale_total;
			$max_sale+=$total_sale;
			$max_credit_sale+=$credit_sale;
			$max_cash_sale+=$cash_sale_total;
			$max_receipt_sale+=$collected;
			$Single_Customer['receipt'] = number_format($collected);
			$Single_Customer['cash_sale'] = $cash_sale_total;
			$Single_Customer['credit_sale'] = $credit_sale;
			$No_flag = 0;
			if($total_sale==0 && $collected==0){
				$NoSale=$this->NoSale->find('all',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
						'NoSale.executive_id'=>$executive,
// 'NoSale.date between ? and ?'=>[$register,$closed],
						'NoSale.day_register_id'=>$day_register_id,
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',
						'NoSale.created_at',
						),
					));
				foreach ($NoSale as $key => $row) {
					$No_flag = 1;
					$no_sale_count++;
					$Single_Customer['sort'] = 0;

					$Single_Customer['credit_sale'] = 'No Sale';
					$Single_Customer['receipt'] = '';
					$Single_Customer['cash_sale'] = $row['NoSale']['description'];
					$Single_Customer['time']="";
					if($row['NoSale']['created_at']){
						$time = new DateTime($row['NoSale']['created_at']);
						$time = $time->format('H:i');
						$Single_Customer['time']=$time;
					}
				}
			}
			if($total_sale==0 && $collected!=0 && $No_flag==0){
				$receipt_visit++;
			}
			$Debit_N_Credit_function=$this->General_Journal_Debit_Credit_function($value['AccountHead']['id']);
			if($total_sale!=0 || $collected!=0 ||$No_flag!=0)
			{
				$Single_Customer['balance']=$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
				$max_balance+=$Single_Customer['balance'];
				array_push($All_Customer, $Single_Customer);
			}
			$single_market_outstanding =$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
			$current_market_outstanding =$current_market_outstanding + $single_market_outstanding;
		}
	}
//pr($All_Customer);
	array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
	$content['All_Customer'] = $All_Customer;
	$postive_visit_sale=$this->Sale->find('count',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
// 'Sale.date_of_delivered between ? and ?'=>[$register,$closed],
			'Sale.day_register_id'=>$day_register_id,
			'Sale.is_erp'=>0
			),
		'group' => array('Sale.account_head_id'),
		));
	$footer_content['visit'] = count($All_Customer);
	$visit = count($All_Customer);
	$postive_visit = $postive_visit_sale + $receipt_visit;
	$footer_content['postive_visit'] = $postive_visit;
	$strike_rate =0;
	if($visit) $strike_rate = ($postive_visit/$visit)*100;
	$footer_content['strike_rate'] = number_format((float)$strike_rate, 2, '.', '');
	$footer_content['max_balance'] =number_format((float)$max_balance, 2, '.', '');
	$footer_content['max_credit_sale'] =number_format((float)$max_credit_sale, 2, '.', '');
	$footer_content['max_cash_sale'] = number_format((float)$max_cash_sale, 2, '.', '');
	$footer_content['max_receipt_sale'] = number_format((float)$max_receipt_sale, 2, '.', '');
	$footer_content['max_sale'] = number_format((float)$max_sale, 2, '.', '');
	$footer_content['current_market_outstanding'] = number_format((float)$current_market_outstanding, 2, '.', '');
	$Month_sale_amount =0;
	$Month_sale_return_amount =0;
	$MonthSale_list=$this->Sale->find('all',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
			'Sale.day_register_id'=>$day_register_id,
			'Sale.status'=>2
			),
		'fields'=>array(
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			),
		));
	$Monthly_cash_sale = 0;
	foreach ($MonthSale_list as $key => $value) {
		$remarks='Sale Invoice No :'. $value['Sale']['invoice_no'];
		$cash_sale_row = $this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$cash_id,
				'flag=1',
				'day_register_id'=>$day_register_id,
				'remarks'=>$remarks,
				'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
				'date'=>$value['Sale']['date_of_delivered'],
				),
			'fields'=>array('Journal.amount',),
			));
		if(!empty($cash_sale_row)){
			$Monthly_cash_sale = $Monthly_cash_sale + $cash_sale_row['Journal']['amount'];
		}
	}
//pr($month);
	$MonthSale=$this->Sale->find('first',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
//'Sale.day_register_id'=>$day_register_id,
			'Sale.status'=>2
			),
		'fields'=>array(
			'sum(Sale.grand_total) as monthly_sale',
			),
		));


	$Customers=$this->Customer->find('all',array(
		'conditions'=>array('Customer.route_id'=>$route_id),
		'fields'=>[
		'Customer.*',
		]
		));
	$list=[];
	foreach ($Customers as $keyC => $valueC) {
		$this->SalesReturn->unbindModel(array('hasMany' => array('SalesReturnItem')));
		$this->SalesReturn->virtualFields = array('monthly_sale_return' => "SUM(SalesReturn.grand_total)");
		$MonthSaleReturn=$this->SalesReturn->find('first',array(
			'conditions'=>array(
				'SalesReturn.account_head_id'=>$valueC['Customer']['account_head_id'],
				'month(SalesReturn.date)'=>$month,
				'SalesReturn.status'=>2
				),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				'monthly_sale_return'
				)
			));
		if($MonthSaleReturn['SalesReturn']['monthly_sale_return'])
		{
			$list[]=$MonthSaleReturn;	
		}
	}
//pr($list);
//exit;
	if($MonthSale){
		$Month_sale_amount =$MonthSale[0]['monthly_sale'];
	}
	if($MonthSaleReturn){
		foreach ($list as $keyl => $valuel) {

			$Month_sale_return_amount +=-($valuel['SalesReturn']['monthly_sale_return']);
		}
//$Month_sale_return_amount =-($MonthSaleReturn['monthly_sale_return']);
	}
	$footer_content['Month_sale_amount'] = number_format((float)$Month_sale_amount, 2, '.', '');
	$footer_content['Month_sale_return_amount'] = number_format((float)$Month_sale_return_amount, 2, '.', '');
	$cash_id = $this->Route->field('Route.account_head_id',array('Route.id ' =>$day['Route']['id']));

	$Month_collection_amount =0;
	$MonthCollection=$this->Journal->find('first',array(
		'conditions'=>array(
			'debit'=>$cash_id,
			'flag=1',
//'day_register_id'=>$day_register_id,
			'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
			'month(date)'=>$month,
			),
		'fields'=>array(
			'sum(Journal.amount) as monthly_amount',
			),
		));

	if(!empty($MonthCollection)){
		$Month_collection_amount =$MonthCollection[0]['monthly_amount'];
	}
	$footer_content['Month_collection_amount'] = number_format((float)$Month_collection_amount, 2, '.', '');
	require('fpdf/fpdf.php');
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	define('FPDF_FONTPATH','fpdf/font');
	function header_section($pdf,$Profile,$content) {
		$pdf->SetFont('Arial','B',8.4);
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],1,-5,-650);
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(0,100,0);
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(200,00,00);
		$pdf->SetTextColor(10,10,10);
		$pdf->SetFont('Arial','B',8);
		$head_table_x=28;
		$pdf->rect(150, $head_table_x-7, 55, 8);
		$pdf->Text(152, 25, "Route : ".$content['route_name']);
		$pdf->rect(15, $head_table_x+3.5, 120, 8);
		$pdf->SetFont('Arial','',11);
		$pdf->Text(18, 36.5, "Warehouse : ".$content['warehouse_name']);
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(80, 29.5, "DAILY SALES REPORT");
		$pdf->rect(150, $head_table_x+3, 55, 8);
		$pdf->SetFont('Arial','B',9);
		$pdf->Text(152, 36.5, "Date : ".date('d-M-Y',strtotime($content['display_date'])));
		$first_table_x=$head_table_x+13;
		$pdf->rect(15, $first_table_x+3, 190, 222);
		$slno_x=16;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
		$pdf->Line($slno_x+7, $first_table_x+3,$slno_x+7, 200);
		$item_x=$slno_x+34;
		$item_line_x=$item_x+100;
		$pdf->SetFont('Arial','B',7);
		$pdf->Line($slno_x+18, $first_table_x+3,$slno_x+18, 200);
		$pdf->Line($slno_x+33, $first_table_x+3,$slno_x+33, 200);
		$pdf->Text($slno_x+10, $first_table_x+5+5, "Time");
		$pdf->Text($slno_x+20, $first_table_x+5+5, "Code");
		$pdf->Text($item_x+15, $first_table_x+10, "Customer");
		$tax_line_x=$item_x+35;
		$tax_x=$item_x+35;
		$pdf->Line($tax_line_x+33, $first_table_x+3,$tax_line_x+33, 200);
		$pdf->Text($tax_x, $first_table_x+5, "");
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($tax_x+35, $first_table_x+5+5, "Credit Sale");
		$tax_line_x=$tax_x+35;
		$pdf->Line($tax_line_x+15, $first_table_x+3,$tax_line_x+15, 200);
		$unit_x=$tax_x+35;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($unit_x+17, $first_table_x+5+5, "Cash Sale");
		$pdf->Line($item_line_x+10, $first_table_x+3,$item_line_x+10, 200);
		$qty_x=$item_line_x+4;
		$qty_line_x=$qty_x+8;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($qty_x+9, $first_table_x+5+5, "Receipt");
		$unit_price_x=$qty_line_x+2;
		$unit_price_line_x=$unit_price_x+15;
		$pdf->SetFont('Arial','B',7);
		$net_amount_x=$qty_line_x+2;
		$net_amount_line_x=$net_amount_x+16;
		$pdf->Line($net_amount_line_x-2, $first_table_x+3,$net_amount_line_x-2, 200);
		$total_x=$unit_price_line_x+2;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($total_x, $first_table_x+5+5, "O/S Balance");
		$pdf->Line(15, $first_table_x+12, 205, $first_table_x+12);
		$pdf->Line(15, 200, 205, 200);
		$pdf->Line(15, 205, 205, 205);
		$i=0;
	}
	function footer($pdf,$footer_content)
	{
		$footer_line=203;
		$f=0;
		$pdf->Line(178, 205,178, 215);
		$pdf->Text(190, 211, $footer_content['max_balance']);
		$pdf->Line(160, 205,160, 215);
		$pdf->Text(165, 211, $footer_content['max_receipt_sale']);
		$pdf->Line(135, 205,135, 215);
		$pdf->Text(140, 211, $footer_content['max_cash_sale']);
		$pdf->Line(118, 205,118, 215);
		$pdf->Text(121, 211, $footer_content['max_credit_sale']);
		$pdf->Text(100, 211, 'Total :');
		$pdf->Line(15, 215, 205, 215);
		$pdf->Line(185, 215,185, 235);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 220, $footer_content['day_time']);
		$pdf->Text(135, 220, 'Day Register Time');
		if (isset($footer_content['close_time'])){
			$pdf->Text(190, 225, $footer_content['close_time']);
		}
		else{
			$pdf->Text(190, 225, $f);
		}
		$pdf->Text(135, 225, 'Day Close Time');
		$pdf->Text(190, 230, $footer_content['day_km']);
		$pdf->Text(135, 230, 'Day Register KM');
		if (isset($footer_content['close_km'])){
			$pdf->Text(190, 235, $footer_content['close_km']);
		}
		else{
			$pdf->Text(190, 235, $f);
		}
		$pdf->Text(135, 235, 'Day Close KM');
		if (isset($footer_content['close_km'])){
			$pdf->Text(190, 240, $footer_content['close_km']-$footer_content['day_km']);
		}
		else{
			$pdf->Text(190, 240, $f-$footer_content['day_km']);
		}
		$pdf->Text(135, 240, 'Travelled KM');
		$pdf->Line(105, 215,105, 235);
		$pdf->Line(75, 215,75, 235);
		$pdf->Text(80, 219, $footer_content['initial_stock']);
		$pdf->Text(20, 219, 'Opening Stock ');
		$pdf->Text(80, 225, $footer_content['loading_stock']);
		$pdf->Text(20, 225, 'Today Loading ');
		if (isset($footer_content['closing_stock'])){
			$pdf->Text(80, 231, $footer_content['closing_stock']);
		}
		else{
			$pdf->Text(80, 231, $f);
		}
		$pdf->Text(20, 231, 'Closing Stock ');
		$pdf->Text(80, 237, $footer_content['current_market_outstanding']);
		$pdf->Text(20, 237, 'Current Market Outstanding ');
		$pdf->Line(15, 242, 205, 242);
		$totalSale = ($footer_content['max_cash_sale'] + $footer_content['max_credit_sale']);
		$today_collection = ($footer_content['max_cash_sale'] + $footer_content['max_receipt_sale']);
		$pdf->Line(185, 235,185, 265);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 246, $footer_content['Month_sale_amount']);
		$pdf->Text(135, 246, 'Monthly Sale');
		$pdf->Text(190, 250, $footer_content['Month_sale_return_amount']);
		$pdf->Text(135, 250, 'Monthly Sale Return');
		$pdf->Text(190, 254, $footer_content['Month_collection_amount']);
		$pdf->Text(135, 254, 'Monthly Collection');
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 258.5, number_format((float)$totalSale,2));
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(135, 258.5, 'Today Sale');
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 262.5, number_format((float)$today_collection,2));
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(135, 262.5, 'Today Collection');
		$pdf->Line(105, 235,105, 265);
		$pdf->Line(75, 235,75, 265);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(80, 249, $footer_content['visit']);
		$pdf->Text(20, 249, 'Total Visit');
		$pdf->Text(80, 255, $footer_content['postive_visit']);
		$pdf->Text(20, 255, 'Positive Visit');
		$pdf->Text(80, 261, $footer_content['strike_rate'].'%');
		$pdf->Text(20, 261, 'Strike Rate');
		$pdf->SetFont('Arial','B',8);
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(20, 295, "Salesman");
		$pdf->Text(60, 295, "Supervisor");
		$pdf->Text(100, 295, "Accountant");
		$pdf->Text(140, 295, "Manager ");
	}
	$slno =1;
	header_section($pdf,$Profile,$content);
	$i=0;
	$head_table_x=10;
	$first_table_x=$head_table_x+36;
	$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; 
	$time  = array_column($content['All_Customer'], 'time');
	array_multisort($time, SORT_ASC,$content['All_Customer']);

	foreach($content['All_Customer'] as $key=>$value) {
		$sl_no_v_x=18;
		$pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $slno);
		$product_name_v_x=$sl_no_v_x+10;
		$pdf->Text($product_name_v_x-3, $first_table_x+11+(5*$i), $value['time']);
		$pdf->Text($product_name_v_x+8, $first_table_x+11+(5*$i), $value['shope_code']);
		$pdf->Text($product_name_v_x+23, $first_table_x+11+(5*$i), $value['name']);
		$tax_v_x=$product_name_v_x+65;
		$pdf->Text($tax_v_x+28, $first_table_x+11+(5*$i), $value['credit_sale']);
		$unit_v_x=$tax_v_x+35;
		$pdf->Text($unit_v_x+10, $first_table_x+11+(5*$i), $value['cash_sale']);
		$quantity_v_x=$product_name_v_x+125;
		$pdf->Text($quantity_v_x+10, $first_table_x+11+(5*$i),$value['receipt']);
		$unit_price_v_x=$quantity_v_x+12;
		$total_v_x=$unit_price_v_x+18;
		if($value['balance']=='-0.00'){
			$value['balance'] ='0.00';
		}
		$pdf->Text($total_v_x, $first_table_x+11+(5*$i),number_format($value['balance']));
		$i++;
		$slno++;
		if($i>28)
		{
			$i=0;
			header_section($pdf,$Profile,$content);
		}
	}
	footer($pdf,$footer_content);
	$pdf->Output();
	exit;
}
public function DayRegisterPrintOld()
{
	$this->request->data['Reports']['from']=date('d-m-Y');
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));
}
public function dcrpdf_old($executive,$date)
{
	$date = date('Y-m-d',strtotime($date));
	$date_build = date_parse_from_format("Y-m-d", $date);
	$month = $date_build["month"];
	$content['display_date'] = date('d-m-Y',strtotime($date));
	$footer_content['initial_stock']=0;
	$footer_content['loading_stock']=0;
	$content['route_name'] = '';
	$day = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' =>$date)));
	if($day)
	{
		$footer_content['initial_stock'] = $day['DayRegister']['initial_stock'];
		$footer_content['loading_stock'] = $day['DayRegister']['loading_stock'];	
		$content['route_name'] = $day['Route']['name'];
	}
	$closing_stock = 0;
	$close = $this->ClosingDay->find('first', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' =>$date)));
	if(!empty($close)){
		$closing_stock = $close['ClosingDay']['closing_stock'];
	}
	$footer_content['closing_stock'] =$closing_stock;
	$warehouse_id = $this->Executive->field(
		'Executive.warehouse_id',
		array('Executive.id ' => $executive));
	$content['warehouse_name'] = $this->Warehouse->field(
		'Warehouse.name',
		array('Warehouse.id ' => $warehouse_id));
	$cash_id = $this->Executive->field(
		'Executive.account_head_id',
		array('Executive.id ' => $executive));
	$CustomerList=$this->AccountHead->find('all',array(
		"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('Customer.account_head_id=AccountHead.id'),
				),
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'inner',
				"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
		'fields'=>array(
			'Customer.*',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			),
		));
	$receipt_visit = 0;
	$All_Customer=[];
	if($CustomerList)
	{
		$j =0;
		$no_sale_count =0;
		$max_sale = 0;
		$max_credit_sale = 0;
		$max_cash_sale = 0;
		$max_receipt_sale = 0;
		$max_balance =0;
		$current_market_outstanding =0;
		foreach ($CustomerList as $key => $value) {
			$total_sale =0;
			$Single_Customer['id']=$value['AccountHead']['id'];
			$Single_Customer['name']=$value['AccountHead']['name'];
			$Single_Customer['shope_code']=$value['Customer']['code'];
			$Single_Customer['sort'] = 1;
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$value['AccountHead']['id'],
					'Sale.executive_id'=>$executive,
					'Sale.date_of_delivered'=>$date,
					'Sale.status'=>2,
//'Sale.is_erp'=>0
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					),
				));
			$All_sale=[];
			$cash_sale_total =0;
			$collected =0;
			foreach ($Sale as $key => $row) {
				$total_sale = $total_sale + $row['Sale']['grand_total'];
				$remarks='Sale Invoice No :'. $row['Sale']['invoice_no'];
				$Journal_cash_sale=$this->Journal->find('all',array('conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'debit'=>$cash_id,
					'flag=1',
					'remarks'=>$remarks,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
					'date'=>$date
					)));
				foreach ($Journal_cash_sale as $key => $row) {
					if(!empty($row['Journal']['amount'])){
						$cash_sale_total = $cash_sale_total + $row['Journal']['amount'];
					}
				}
			}
			$Journal_credit=$this->Journal->find('all',array(
				'conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'flag=1',
					'debit'=>$cash_id,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
					'date'=>$date
					),
				'fields'=>array('Journal.amount'),
				));
			$receipt_flag = 0;
			foreach ($Journal_credit as $key => $row) {
				if(!empty($row['Journal']['amount'])){
					$collected = $collected + $row['Journal']['amount'];
					if($collected <0){
						$collected = 0.00;
					}
				}
			}
			$collected = $collected - $cash_sale_total;
			$max_sale = $max_sale + $total_sale;
			$credit_sale = $total_sale - $cash_sale_total;
			$max_credit_sale = $max_credit_sale + $credit_sale;
			$max_cash_sale = $max_cash_sale + $cash_sale_total;
			$max_receipt_sale = $max_receipt_sale + $collected;
			$Single_Customer['receipt'] = $collected;
			$Single_Customer['cash_sale'] = $cash_sale_total;
			$Single_Customer['credit_sale'] = $credit_sale;
			$No_flag = 0;
			if($total_sale==0 && $collected==0){
				$NoSale=$this->NoSale->find('all',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
						'NoSale.executive_id'=>$executive,
						'NoSale.date'=>$date,
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',
						),
					));
				foreach ($NoSale as $key => $row) {
					$No_flag = 1;
					$no_sale_count++;
					$Single_Customer['sort'] = 0;
					$Single_Customer['credit_sale'] = 'No Sale';
					$Single_Customer['receipt'] = '';
					$Single_Customer['cash_sale'] = $row['NoSale']['description'];
				}
			}
			if($total_sale==0 && $collected!=0 && $No_flag==0){
				$receipt_visit++;
			}
			$Debit_N_Credit_function=$this->General_Journal_Debit_Credit_function($value['AccountHead']['id']);
			if($total_sale!=0 || $collected!=0 ||$No_flag!=0)
			{
				$Single_Customer['balance']=$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
				$max_balance = $max_balance + $Single_Customer['balance'];
				$j++;
				$Single_Customer['sl_no'] = $j;
				array_push($All_Customer, $Single_Customer);
			}
			$single_market_outstanding =$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
			$current_market_outstanding =$current_market_outstanding + $single_market_outstanding;
		}
	}
	array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
	$k=0;
	foreach ($All_Customer as $key => $Customer) {
		$k++;
		$All_Customer[$key]['sl_no'] = $k;
	}
	$content['All_Customer'] = $All_Customer;
	$postive_visit_sale=$this->Sale->find('count',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'Sale.date_of_delivered'=>$date,
			'Sale.is_erp'=>0
			),
		'group' => array('Sale.account_head_id'),
		));
	$footer_content['visit'] = $k;
	$visit = $k;
	$postive_visit = $postive_visit_sale + $receipt_visit;
	$footer_content['postive_visit'] = $postive_visit;
	if($visit!=0){
		$strike_rate = ($postive_visit/$visit)*100;
	}
	else
	{
		$strike_rate =0;
	}
	$footer_content['strike_rate'] = number_format((float)$strike_rate, 2, '.', '');
	$footer_content['max_balance'] =number_format((float)$max_balance, 2, '.', '');
	$footer_content['max_credit_sale'] =number_format((float)$max_credit_sale, 2, '.', '');
	$footer_content['max_cash_sale'] = number_format((float)$max_cash_sale, 2, '.', '');
	$footer_content['max_receipt_sale'] = number_format((float)$max_receipt_sale, 2, '.', '');
	$footer_content['max_sale'] = number_format((float)$max_sale, 2, '.', '');
	$footer_content['current_market_outstanding'] = number_format((float)$current_market_outstanding, 2, '.', '');
	$Month_sale_amount =0;
	$MonthSale_list=$this->Sale->find('all',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>1,
			'Sale.status'=>2
			),
		'fields'=>array(
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			),
		));
	$Monthly_cash_sale = 0;
	foreach ($MonthSale_list as $key => $value) {
		$remarks='Sale Invoice No :'. $value['Sale']['invoice_no'];
		$cash_sale_row = $this->Journal->find('first',array('conditions'=>array(
			'debit'=>$cash_id,
			'flag=1',
			'remarks'=>$remarks,
			'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
			'date'=>$value['Sale']['date_of_delivered'],
			),
		'fields'=>array(
			'(Journal.amount)',
			),
		));
		if(!empty($cash_sale_row)){
			$Monthly_cash_sale = $Monthly_cash_sale + $cash_sale_row['Journal']['amount'];
		}
	}
	$MonthSale=$this->Sale->find('first',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
			'Sale.status'=>2
			),
		'fields'=>array(
			'sum(Sale.grand_total) as monthly_sale',
			),
		));
	if(!empty($MonthSale)){
		$Month_sale_amount =$MonthSale[0]['monthly_sale'];
	}
	$footer_content['Month_sale_amount'] = number_format((float)$Month_sale_amount, 2, '.', '');
	$cash_id = $this->Executive->field(
		'Executive.account_head_id',
		array('Executive.id ' => $executive));
	$Month_collection_amount =0;
	$MonthCollection=$this->Journal->find('first',array('conditions'=>array(
		'debit'=>$cash_id,
		'flag=1',
		'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
		'month(date)'=>$month,
		),
	'fields'=>array(
		'sum(Journal.amount) as monthly_amount',
		),
	));
	if(!empty($MonthCollection)){
		$Month_collection_amount =$MonthCollection[0]['monthly_amount'];
	}
	$footer_content['Month_collection_amount'] = number_format((float)$Month_collection_amount, 2, '.', '');
	require('tfpdf/tfpdf.php');
	$pdf = new tFPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	define('FPDF_FONTPATH','tfpdf/font');
	function header_section($pdf,$Profile,$content) {
		$pdf->SetFont('Arial','B',8.4);
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],-2,-5,-250);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(0,100,0);
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(200,00,00);
		$pdf->SetTextColor(10,10,10);
		$pdf->SetFont('Arial','B',8);
		$head_table_x=28;
		$pdf->rect(150, $head_table_x-10, 55, 8);
		$pdf->Text(152, 21, "Route : ".$content['route_name']);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
		$pdf->rect(15, $head_table_x, 120, 8);
		$pdf->SetFont('Arial','',11);
		$pdf->Text(18, 33, "Warehouse : ".$content['warehouse_name']);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(80, 26, "DAILY SALES REPORT");
		$pdf->rect(150, $head_table_x, 55, 8);
		$pdf->SetFont('Arial','B',9);
		$pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($content['display_date'])));
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',13);
		$first_table_x=$head_table_x+13;
		$pdf->rect(15, $first_table_x, 190, 215);
		$slno_x=16;
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',8);
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200); // vertical line
$item_x=$slno_x+34;
$item_line_x=$item_x+100;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$pdf->Text($item_x, $first_table_x+10, "Customer");
$tax_line_x=$item_x+35;
$tax_x=$item_x+35;
$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200); // vertical line
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($tax_x, $first_table_x+5, "");
$pdf->SetFont('Arial','B',7);
$pdf->Text($tax_x+5, $first_table_x+5+5, "Credit Sale");
$tax_line_x=$tax_x+35;
$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200); // vertical line
$unit_x=$tax_x+35;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$pdf->Text($unit_x+5, $first_table_x+5+5, "Cash Sale");
$pdf->Line($item_line_x-3, $first_table_x,$item_line_x-3, 200); // vertical line
$qty_x=$item_line_x+4;
$qty_line_x=$qty_x+8;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$pdf->Text($qty_x, $first_table_x+5+5, "Receipt");
$unit_price_x=$qty_line_x+2;
$unit_price_line_x=$unit_price_x+15;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$net_amount_x=$qty_line_x+2;
$net_amount_line_x=$net_amount_x+16;
$pdf->Line($net_amount_line_x-4, $first_table_x,$net_amount_line_x-4, 200); // vertical line
$total_x=$unit_price_line_x+2;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$pdf->Text($total_x, $first_table_x+5+5, "O/S Balance");
$pdf->Line(15, $first_table_x+12, 205, $first_table_x+12); // horizontal line
$pdf->Line(15, 200, 205, 200); // horizontal line
$pdf->Line(15, 205, 205, 205); // horizontal line
$i=0;
}
function footer($pdf,$footer_content)
{
	$footer_line=203;
$pdf->Line(177, 205,177, 215); // vertical line
$pdf->Text(190, 211, $footer_content['max_balance']);
$pdf->Line(148, 205,148, 215);
$pdf->Text(159, 211, $footer_content['max_receipt_sale']);
$pdf->Line(120, 205,120, 215);
$pdf->Text(131, 211, $footer_content['max_cash_sale']);
$pdf->Line(85, 205,85, 215);
$pdf->Text(93, 211, $footer_content['max_credit_sale']);
$pdf->Text(20, 211, 'Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Line(15, 215, 205, 215);
$pdf->Line(185, 215,185, 235); // vertical line
$pdf->SetFont('Arial','',8);
$pdf->Text(190, 220, $footer_content['Month_sale_amount']);
$pdf->Text(135, 220, 'Monthly Sale');
$pdf->Text(190, 227, $footer_content['Month_collection_amount']);
$pdf->Text(135, 227, 'Monthly Collection');
$pdf->Line(105, 215,105, 235);
$pdf->Line(75, 215,75, 235);
$pdf->Text(80, 219, $footer_content['initial_stock']);
$pdf->Text(20, 219, 'Opening Stock ');
$pdf->Text(80, 223, $footer_content['loading_stock']);
$pdf->Text(20, 223, 'Today Loading ');
$pdf->Text(80, 227, $footer_content['closing_stock']);
$pdf->Text(20, 227, 'Closing Stock ');
$pdf->Text(80, 231, $footer_content['current_market_outstanding']);
$pdf->Text(20, 231, 'Current Market Outstanding ');
$pdf->Line(15, 235, 205, 235);
$totalSale = ($footer_content['max_cash_sale'] + $footer_content['max_credit_sale']);
$today_collection = ($footer_content['max_cash_sale'] + $footer_content['max_receipt_sale']);
$pdf->Line(185, 235,185, 256); // vertical line
$pdf->Text(190, 241, number_format((float)$totalSale,2));
$pdf->SetFont('Arial','B',8);
$pdf->Text(135, 241, 'Today Sale');
$pdf->SetFont('Arial','',8);
$pdf->Text(190, 250, number_format((float)$today_collection,2));
$pdf->SetFont('Arial','B',8);
$pdf->Text(135, 250, 'Today Collection');
$pdf->Line(105, 235,105, 256);
$pdf->Line(75, 235,75, 256);
$pdf->SetFont('Arial','',8);
$pdf->Text(80, 240, $footer_content['visit']);
$pdf->Text(20, 240, 'Total Visit');
$pdf->Text(80, 245, $footer_content['postive_visit']);
$pdf->Text(20, 245, 'Positive Visit');
$pdf->Text(80, 250, $footer_content['strike_rate'].'%');
$pdf->Text(20, 250, 'Strike Rate');
$pdf->SetFont('Arial','B',8);
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
$pdf->SetFont('Arial','B',8);
$pdf->Text(20, 295, "Salesman");
$pdf->Text(60, 295, "Supervisor");
$pdf->Text(100, 295, "Accountant");
$pdf->Text(140, 295, "Manager ");
}
$slno =1;
header_section($pdf,$Profile,$content);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+36;
$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; 
foreach($content['All_Customer'] as $key=>$value) {
	$sl_no_v_x=18;
	$pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $slno);
	$product_name_v_x=$sl_no_v_x+10;
	$pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['name']);
	$tax_v_x=$product_name_v_x+65;
	$pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['credit_sale']);
	$unit_v_x=$tax_v_x+35;
	$pdf->Text($unit_v_x, $first_table_x+11+(5*$i), $value['cash_sale']);
	$quantity_v_x=$product_name_v_x+125;
	$pdf->Text($quantity_v_x, $first_table_x+11+(5*$i),$value['receipt'] );
	$unit_price_v_x=$quantity_v_x+12;
	$total_v_x=$unit_price_v_x+18;
	if($value['balance']=='-0.00'){
		$value['balance'] ='0.00';
	}
	$pdf->Text($total_v_x, $first_table_x+11+(5*$i),$value['balance'] );
	$i++;
	$slno++;
	if($i>31)
	{
		$i=0;
		header_section($pdf,$Profile,$content);
	}
}
footer($pdf,$footer_content);
$pdf->Output();
exit;
}

public function SaleCollectionReport()
{
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}
public function SaleCollectionReportAjaxOld()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id']){
		$conditions['Sale.executive_id']=$data['executive_id'];
	}
	if($data['customer_id']){
		$conditions['Customer.account_head_id']=$data['customer_id'];
	}
	$conditions['Sale.status']=2;
	$conditions['Sale.flag']=1;
	$Sale_Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
			),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
			),
		),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Route.id',
			'Route.name',
//'CustomerCircle.name',
		)
	));
//pr($Sale_Customer);exit;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Sale_Customer as $key => $value) {
		$this->SaleItem->virtualFields = array(
			//'grand_total' => "SUM(SaleItem.total)",
			'grand_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
			'grand_tax_amount' => "SUM(SaleItem.tax_amount)",
			//'grand_net_value' => "SUM(SaleItem.net_value)",
 'grand_total' => "SUM(((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount))",
		);
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
			),
			'fields'=>array(
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total',
			),
		));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
			),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
			),
		));
		$single['executive']=$value['Executive']['name'];
		$single['route']=$value['Route']['name'];
		$single['name']=$value['AccountHead']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['taxable_amount']    =$SaleItem['SaleItem']['grand_net_value']-$Sale['Sale']['grand_discount_amount'];

		$single['total']         =$single['net_value']+$single['tax_amount'];
			$single['total']        -=$single['dicount_amount'];
			$single['grandtotal']    =round($single['total'],2);
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
public function get_executive_by_route_list($route_id=null)
{
	$conditions=[];
	if($route_id){$conditions['ExecutiveRouteMapping.route_id']=$route_id;} 
	$Executive=$this->Executive->find('list',array(
		"joins"=>array(
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'INNER',
				"conditions"=>array('ExecutiveRouteMapping.executive_id=Executive.id'),
				),

			),
		'conditions'=>$conditions,
		)
	);
	echo json_encode($Executive); exit;
}
public function get_executive_by_product_type($executive_id=null)
{
	$conditions=[];
	if($executive_id){$conditions['Sale.executive_id']=$executive_id;} 
	$Product=$this->ProductType->find('list',array(
		"joins"=>array(

			array(
				"table"=>'products',
				"alias"=>'Product',
				"type"=>'INNER',
				"conditions"=>array('Product.product_type_id=ProductType.id'),
				),
			array(
				"table"=>'sale_items',
				"alias"=>'SaleItem',
				"type"=>'INNER',
				"conditions"=>array('SaleItem.product_id=Product.id'),
				),
			array(
				"table"=>'sales',
				"alias"=>'Sale',
				"type"=>'INNER',
				"conditions"=>array('Sale.id=SaleItem.sale_id'),
				),
			),
		'conditions'=>$conditions,
		)
	);
	echo json_encode($Product); exit;
}
public function get_product_type_by_brand($product_type_id=null)
{
	$conditions=[];
	if($product_type_id){$conditions['Product.product_type_id']=$product_type_id;} 
	$Brand=$this->Brand->find('list',array(
		"joins"=>array(
			array(
				"table"=>'products',
				"alias"=>'Product',
				"type"=>'INNER',
				"conditions"=>array('Product.brand_id=Brand.id'),
				),
			array(
				"table"=>'sale_items',
				"alias"=>'SaleItem',
				"type"=>'INNER',
				"conditions"=>array('SaleItem.product_id=Product.id'),
				),
			array(
				"table"=>'sales',
				"alias"=>'Sale',
				"type"=>'INNER',
				"conditions"=>array('Sale.id=SaleItem.sale_id'),
				),
			),
		'conditions'=>$conditions,
		)
	);
	echo json_encode($Brand); exit;
}
public function get_product_by_brand_list($brand_id=null)
{
	$conditions=[];
	if($brand_id){$conditions['Product.brand_id']=$brand_id;} 
	$Product=$this->Product->find('list',array(
		"joins"=>array(
			array(
				"table"=>'sale_items',
				"alias"=>'SaleItem',
				"type"=>'INNER',
				"conditions"=>array('SaleItem.product_id=Product.id'),
				),
			array(
				"table"=>'sales',
				"alias"=>'Sale',
				"type"=>'INNER',
				"conditions"=>array('Sale.id=SaleItem.sale_id'),
				),
			),
		'conditions'=>$conditions,
		)
	);
	echo json_encode($Product); exit;
}
public function GetCustomerForSaleCollectionReport()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id'])
		$conditions['executive_id']=$data['executive_id'];

	$Customer=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'conditions'=>$conditions,
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
	echo json_encode($Customer); exit;
}
public function SaleCollectionReportAjax()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];

// if($data['customer_id'])
// 	$conditions['account_head_id']=$data['customer_id'];

	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Customer.circle_id',
//'CustomerCircle.name',
			)
		));
//pr($Customer);
//pr($conditions);
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Customer as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}

		$single['name']=$value['AccountHead']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
Public function CollectionReport(){
	$executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
	$this->set(compact('executive_list'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}
Public function CollectionReportExecutive(){
	$executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
	$this->set(compact('executive_list'));
	$route_list=$this->Route->find('list',array('fields'=>array('id','name')));
	$this->set(compact('route_list'));
	$branch_list=$this->Branch->find('list',array('fields'=>array('id','name')));
	$this->set(compact('branch_list'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function get_collection_amount($account_head_id,$from_date=null,$to_date=null) {
	$conditions=array();
	if(!empty($from_date)){
		$conditions['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
	}
	$conditions['Journal.credit']=$account_head_id;
	$conditions['AccountHeadDebit.sub_group_id']=['1','2'];
	$conditions['Journal.flag']=1;
	$this->Journal->virtualFields = array( 'total_amount' => "SUM(Journal.amount)" );
	$Journal=$this->Journal->find('first',array('conditions'=>$conditions,
		'fields'=>array('Journal.total_amount','Journal.date')
		));
	$sale_total_amount=0;
	$collection_date=$Journal['Journal']['date'];
	$sale_total_amount=0;
	if($Journal['Journal']['total_amount'])
	{
		$sale_total_amount=floatval($Journal['Journal']['total_amount']);
	}
	$return['sale_amount']=$sale_total_amount;
	$return['date']=$collection_date;
	return $return;

}

public function collection_report_ajax()
{
	$conditions=array();
	$data=$this->request->data;
//pr($data);
//exit;
	$Customer=$this->Customer->find('all',
		array(
			'fields'=>array(
				'Customer.account_head_id',
				'AccountHead.name',
				),
			));
	$data['row']='';
	$data['tfoot']='';
	$from_date=date('Y-m-d',strtotime($data['from_date']));
	$to_date=date('Y-m-d',strtotime($data['to_date']));
	$data['result']='Error';
	if($Customer)
	{
		$grand_total=0;


		foreach ($Customer as $key => $value) 
		{
			$get_collection_amount_return_value=$this->get_collection_amount($value['Customer']['account_head_id'],$from_date,$to_date,2);		
			if($this->request->data['check_id'] !=0)
			{
				if($get_collection_amount_return_value['sale_amount']!=0)
				{
					$data['row']= $data['row'].'<tr>';
					$data['row'].='<td>'.date('d-m-Y',strtotime($get_collection_amount_return_value['date'])).'</td>';
					$data['row'].='<td>'.$value["AccountHead"]["name"].'</td>';
					$data['row'].='<td>'.$get_collection_amount_return_value['sale_amount'].'</td>';
					$data['row'].='</tr>';
					$grand_total+=$get_collection_amount_return_value['sale_amount'];
				}
			}
			else
			{
				if($get_collection_amount_return_value['date']){$get_date=date('d-m-Y',strtotime($get_collection_amount_return_value['date']));}else{$get_date=date('d-m-Y');}
				$data['row']= $data['row'].'<tr>';
				$data['row'].='<td>'.$get_date.'</td>';
				$data['row'].='<td>'.$value["AccountHead"]["name"].'</td>';
				$data['row'].='<td>'.$get_collection_amount_return_value['sale_amount'].'</td>';
				$data['row'].='</tr>';
				$grand_total+=$get_collection_amount_return_value['sale_amount'];
			}
		}	
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td colspan="2"><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$grand_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';
	}
	echo json_encode($data); exit;
}

public function ExpenseReport()
{
	$this->request->data['from_date']=date('d-m-Y',strtotime('-1 day'));
	$this->request->data['to_date']=date('d-m-Y');
}
public function ExpenseReport_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='SubGroup.name';
	$columns[]='AccountHead.name';
	$columns[]='AccountHead.id';
	$columns[]='AccountHead.id';
	$conditions=[];
	$priliminary_expense_written_off=18;
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['SubGroup.group_id']=['12','13'];
	$purchase=5;
	$salereturn=6;
	$discount=8;
	$tax=7;
	$conditions['SubGroup.id !=']=[
	$purchase,
	$salereturn,
	$discount,
	$tax

	];
	$conditions['AccountHead.id !=']=[
	$priliminary_expense_written_off
	];
	$totalData=$this->AccountHead->find('count',[
		'joins'=>array(
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			),
		'conditions'=>$conditions]
		);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'SubGroup.name LIKE' =>'%'. $q . '%',
			'AccountHead.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->AccountHead->find('count',[
			'conditions'=>$conditions,
			]);
	}
	$Data=$this->AccountHead->find('all',array(
		'joins'=>array(
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			'SubGroup.name',
			'SubGroup.id',
			'AccountHead.sub_group_id',
			)
		));
	foreach ($Data as $key => $value) {
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Total_expense=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$value['AccountHead']['id'],
				'flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				),
			'fields'=>array( 'Journal.total_amount')
			));

		$Total_expense_piad=$this->Journal->find('first',array(
			'joins'=>array(
				array(
					'table'=>'sub_groups',
					'alias'=>'SubGroup',
					'type'=>'INNER',
					'conditions'=>array('SubGroup.id=AccountHeadCredit.sub_group_id')
					),
				),
			'conditions'=>array(
				'SubGroup.id'=>array('1','2'),
				'debit'=>$value['AccountHead']['id'],
				'flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				),
			'fields'=>array( 'Journal.total_amount')
			));
		$accounthead=explode(" ",$value['AccountHead']['name']);
		$c=count($accounthead);
		$c1=$c-1;
		$a=$accounthead[$c1];
//	pr($a);exit;
		if($a=="OUTSTANDING"){
			$Total_expense['Journal']['total_amount']+=$value['AccountHead']['opening_balance'];
		}
		if($a=="PAID"){
			$Total_expense['Journal']['total_amount']+=$value['AccountHead']['opening_balance'];
			$Total_expense_piad['Journal']['total_amount']+=$value['AccountHead']['opening_balance'];

		}
		if($a=="PREPAID"){
			$Total_expense_piad['Journal']['total_amount']+=$value['AccountHead']['opening_balance'];
		}
		if($this->request->data['check_id'])
		{
			if($Total_expense['Journal']['total_amount'])
			{
				$Data[$key]['AccountHead']['paid']=floatval($Total_expense_piad['Journal']['total_amount']);
				$Data[$key]['AccountHead']['exepense']=floatval($Total_expense['Journal']['total_amount']);
			}
			else
			{
				unset($Data[$key]);
			}
		}
		else
		{
			$Data[$key]['AccountHead']['paid']=floatval($Total_expense_piad['Journal']['total_amount']);
			$Data[$key]['AccountHead']['exepense']=floatval($Total_expense['Journal']['total_amount']);
		}
	}
	$Data = array_filter($Data);
	$Data = array_merge($Data);
	$json_data = array(
		"draw"               => intval( $requestData['draw'] ),
		"recordsTotal"       => intval( $totalData ),
		"recordsFiltered"    => intval( $totalFiltered ),
		"records"            => $Data
		);

	echo json_encode($json_data);
	exit;
}

public function CustomerAgeingReport()
{
	$Customer_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('sub_group_id'=>3)));
	$this->set('Customer_list',$Customer_list);
	$this->request->data['from_date']=date('d-m-Y');


}
Public function customer_ageing_report_ajax()
{
	$conditions=[];
	$data=$this->request->data;

	if($data['customer_id'])
	{
		$conditions['Customer.account_head_id']=$data['customer_id'];

	}
	$Customer=$this->Customer->find('all',array(
		'conditions'=>$conditions,
		'order' => array('Customer.id' => 'ASC'),
		'fields'=>array(
			'Customer.account_head_id',
			'AccountHead.opening_balance',
			'AccountHead.name',
			'CustomerType.name',
			'AccountHead.id',

			),
		));
	$data['row']='';
	$data['tfoot']='';
	$data['result']='Error';
	$balance=0;
	$Balance_Total=0;
	if($Customer)
	{

		$invoice_array=array();
		foreach ($Customer as $key => $value) {
			$account_id=$value['AccountHead']['id'];
			$Sales = $this->Sale->find('all', array(
				'conditions' => array(
					'Sale.account_head_id' =>$account_id,
//'Sale.executive_id'=>$value['Executive']['id'],
					'Sale.flag'=>1,
					'Sale.status'=>array(2,3),
					),
				'order' => array('Sale.date_of_delivered' => 'ASC'),
				'fields' => array(
					'Sale.id',
					'Sale.date_of_delivered',
					'Sale.invoice_no',
					'Sale.account_head_id',
					'Sale.executive_id',
					'Sale.grand_total',
					'AccountHead.name',
					'Executive.account_head_id',

					)
				)
			);
			$voucher_amount=0;
			$discount_paid_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DISCOUNT PAID'));
			$Journal_voucher=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$account_id,
					//'Journal.debit'=>$value['Executive']['account_head_id'],
					'NOT' => array(
						'Journal.debit'=>array($discount_paid_account_head_id),
						'Journal.remarks LIKE'=>'%Sale Invoice No :%',
					),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));

			foreach ($Journal_voucher as $key3 => $value_amount) {
				$voucher_amount+=$value_amount;
			}
			$Journal_extra_debited=$this->Journal->find('list',array(
			'conditions'=>array(
				'Journal.debit'=>$account_id,
				'Journal.work_flow'=>array('Cheques'),
				'Journal.flag=1',
				),
			'fields'=>array(
				'Journal.id',
				'Journal.amount',
				),
			));
			$other_debited_amount=0;
			foreach ($Journal_extra_debited as $key3 => $value_amount) {
				$other_debited_amount+=$value_amount;
			}
// $Journal_voucher1=$this->Journal->find('list',array(
// 	'conditions'=>array(
// 		'Journal.credit'=>$account_id,
// 		//'Journal.debit'=>$value['Executive']['account_head_id'],
// 		'NOT' => array(
// 			'Journal.remarks LIKE' => 'Sale Invoice No :%',
// 		),
// 		'Journal.flag=1',
// 	),
// 	'fields'=>array(
// 		'Journal.id',
// 		'Journal.amount',
// 	),
// ));
			$voucher_balance=0;
// foreach ($Journal_voucher1 as $key3 => $value_amount1) {
// 	$voucher_amount+=$value_amount1;
// }
			foreach ($Sales as $keyj =>$valuej)
			{
				$SaleItem=$this->SaleItem->find('list',array(
					'conditions'=>array(
						'SaleItem.sale_id'=>$valuej['Sale']['id'],
						),
					'fields'=>array(
//'SaleItem.discount',
						)
					));
			}
			$invoice_array_single=array();
			if (!empty($Sales)) {


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				$Journal_opening_received=$this->Journal->find('all',array(
				'conditions'=>array(
					'Journal.remarks'=>'Sale Invoice No :0',
					'Journal.credit'=>$value['AccountHead']['id'],
					'Journal.flag=1',
					),
				));
				$opening_paid_amount=0;
				foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
					$opening_paid_amount+=$value_each_sale['Journal']['amount'];
				}
				$outstanding_amount+=$other_debited_amount;
				$outstanding_amount-=$opening_paid_amount;
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
//pr($voucher_amount);
				if($outstanding_amount>=1)
				{
					array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['balance'],$outstanding_amount);

				}
				foreach ($Sales as $keySC2 => $valueSC2) {
					$Journal=$this->Journal->find('all',array(
					'conditions'=>array(
						'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
						'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
						// 'NOT' => array(
						// 	'Journal.work_flow'=>'From Mobile',
						// ),
						'AccountHeadDebit.sub_group_id=1',
						'Journal.flag=1',
						),
					));
					// $Journal=$this->Journal->find('all',array(
					// 	'conditions'=>array(
					// 		'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
					// 		'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
					// 		'Journal.debit=1',
					// 		'Journal.flag=1',
					// 		),
					// 	));
					$balance_amount=$valueSC2['Sale']['grand_total'];
					$paid_amount=0;
					foreach ($Journal as $key0 => $amount) {
						$paid_amount+=$amount['Journal']['amount'];
						$balance_amount-=$amount['Journal']['amount'];
					}
					if($voucher_amount)
					{
						if($balance_amount<=$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Sale']['grand_total']>0)
					{
						if($balance_amount){
//if($valueSC2['Sale']['account_head_id']==$data['customer_id'])
//{
							$check_date=$valueSC2['Sale']['date_of_delivered'];


							array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
							array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
							array_push($invoice_array_single['balance'],$balance_amount) ;  

//}
						}
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      

			}
			else
			{


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				$Journal_opening_received=$this->Journal->find('all',array(
				'conditions'=>array(
					'Journal.remarks'=>'Sale Invoice No :0',
					'Journal.credit'=>$value['AccountHead']['id'],
					'Journal.flag=1',
					),
				));
				$opening_paid_amount=0;
				foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
					$opening_paid_amount+=$value_each_sale['Journal']['amount'];
				}
				$outstanding_amount+=$other_debited_amount;
				$outstanding_amount-=$opening_paid_amount;
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{

					array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['balance'],$outstanding_amount);

				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}

		}
//pr($invoice_array);
		$grand_total=0;
		$q=0;$w=0;$p=0;$r=0;$t=0;$y=0;
		foreach($invoice_array as $key=>$value) {
			$party_name=$value['party_name'];
			$inner_array_count=count($value['invoice_no']);
			$a1=array();
			$b1=array();
			$c1=array();
			$f1=array();
			$g=array();
			$m=array();
			foreach ($value['delivered_date'] as $key=>$value1)
			{
				$delivered_date=$value1;
				$now = time();
				$expected_days_diff=$now-strtotime($delivered_date);
				$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
				array_push($m,$diff_day);
			}
			foreach($value['invoice_no'] as $key=>$value2)
			{
				$invoice_no=$value2;

			}
			$new_balance=0;

			foreach ($value['balance'] as $key=>$value3)
			{
				$balance=$value3;
				array_push($g,$balance);
				$new_balance+=$balance;
			}
			for($i=0;$i<count($m);$i++)
			{

				if($m[$i]<=30){ $a=$g[$i];array_push($a1,$a);}
				if($m[$i]<=60 && $m[$i]>30){$b=$g[$i];array_push($b1,$b);}
				if($m[$i]<=90 && $m[$i]>60){$c=$g[$i];array_push($c1,$c);}
				if($m[$i]>90){$f=$g[$i];array_push($f1,$f);}
			}

			$grand_total+=$new_balance;
			$b=array_sum($a1);if($b!=0){$b11=ROUND($b,3);}else{$b11='';}
			$b5=array_sum($b1);if($b5!=0){$b51=ROUND($b5,3);}else{$b51='';}
			$b19=array_sum($c1);if($b19!=0){$b12=ROUND($b19,3);}else{$b12='';}			
			$b4=array_sum($f1);if($b4!=0){$b41=ROUND($b4,3);}else{$b41='';}

			$q+=$b11;$w+=$b51;$p+=$b12;$y+=$b41;
//pr($b51);
			$data['row'].='<tr>';
			$data['row'].='<td>'.$party_name.'</td>';
			$data['row'].='<td>'.$b11.'</td>';
			$data['row'].='<td>'.$b51.'</td>';
			$data['row'].='<td>'.$b12.'</td>';
			$data['row'].='<td>'.$b41.'</td>';
			$data['row'].='<td>'.ROUND($new_balance,3).'</td>';
			$data['row'].='</tr>';    
		}
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$q.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$w.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$p.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$y.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$grand_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';

	}
	echo json_encode($data); exit;
}
public function SaleOutstanding()
{
	$sub_group_id=3;
	$Customer_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('sub_group_id'=>3)));
	$Customer=$this->Customer->find('all',array(
		'order'=>array('AccountHead.name ASC'),
		));
	$this->set('Customer_list',$Customer_list);
	$this->set('Customer',$Customer);
}
public function customer_due_report_ajax()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['customer_id'])
	{
		$conditions['Customer.account_head_id']=$data['customer_id'];
	}

	$Customer=$this->Customer->find('all',array(
		'conditions'=>$conditions,
		'fields'=>array(
			'Customer.account_head_id',
			'AccountHead.opening_balance',
			'AccountHead.name',
			'CustomerType.name',
			),
		));
	$data['row']='';
	$data['tfoot']='';
	$data['result']='Error';
	$Balance_Total=0;
	if($Customer)
	{
		foreach ($Customer as $key => $value) {
			$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);
			$Customer[$key]['Customer']['Balance_Total']=round($Balance_Total,3);
		}
		//$data['data']=$Customer;
		$total_outstanding=0;
		foreach($Customer as $key=>$value) {
			$data['row'].='<tr>';
			$data['row'].='<td>'.$value['CustomerType']['name'].'</td>';
			$data['row'].='<td>'.$value['AccountHead']['name'].'</td>';
			$data['row'].='<td>'.$value['Customer']['Balance_Total'].'</td>';
			$data['row'].='</tr>';    
			$total_outstanding+=$value['Customer']['Balance_Total'];
		}
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$total_outstanding.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';
		
	}
	else
	{
		$data['result']='Error';
	}
	echo json_encode($data);
	exit;
}
public function expensegraph($user_branch_id=null)
{
	$direct_expense_group_id=12;
	$indirect_expense_group_id=13;
	$priliminary_expense_written_off=18;
	$purchase=5;
	$salereturn=6;
	$discount=8;
	$tax=7;
	$conditions=[];

	$conditions['SubGroup.group_id']=[
	$direct_expense_group_id,
	$indirect_expense_group_id
	];
	$conditions['SubGroup.id !=']=[
	$purchase,
	$salereturn,
	$discount,
	$tax

	];
	$conditions['AccountHead.id !=']=[
	$priliminary_expense_written_off
	];
	$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
	$AccountHead=$this->AccountHead->find('all',array(
		'conditions'=>$conditions,
		'fields'=>[
		'AccountHead.id',
		'AccountHead.name',
		'AccountHead.opening_balance',
		'SubGroup.name',
		]
		));
	$paid=0;
	$from_date = date('Y-m-d',strtotime('first day of this month'));
	$to_date = date('Y-m-d',strtotime('last day of this month'));
	foreach ($AccountHead as $key => $value) {
		$conditions_journal=[];
		if(!empty($user_branch_id))
		{
			$conditions_journal['Journal.branch_id']=$user_branch_id;
		}
		$conditions_journal['flag']=1;
		$conditions_journal['debit']=$value['AccountHead']['id'];
		$conditions_journal['Journal.date between ? and ?']=array($from_date,$to_date);
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>$conditions_journal,

// 'limit'=>1,
			'fields'=>array( 'Journal.total_amount'),
			));
		$paid+=round($Journal_Debit['Journal']['total_amount']);
	}
	return $paid;
}
public function expensegraph1()
{
	$direct_expense_group_id=12;
	$indirect_expense_group_id=13;
	$priliminary_expense_written_off=18;
	$conditions=[];
	$conditions['SubGroup.group_id']=[
	$direct_expense_group_id,
	$indirect_expense_group_id
	];
	$conditions['AccountHead.id !=']=[
	$priliminary_expense_written_off
	];
	$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
	$AccountHead=$this->AccountHead->find('all',array(
		'conditions'=>$conditions,
		'fields'=>[
		'AccountHead.id',
		'AccountHead.name',
		'AccountHead.opening_balance',
		'SubGroup.name',
		]
		));
	$from_date = date('Y-m-d',strtotime('first day of this month'));
	$to_date = date('Y-m-d',strtotime('last day of this month'));
	$paid=0;  
	foreach ($AccountHead as $key => $value) {
		$expense_id=$value['AccountHead']['id'];
		$expense_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id'],$from_date,$to_date);
		$single['key']=$key+1;
		$expense_name=explode(' ',$value['AccountHead']['name']);
		$removed = array_pop($expense_name);
		if($removed!='PAID')
		{
			$expense_name=explode(' ',$value['AccountHead']['name']);
		}
		$expense_name=implode($expense_name, ' ');
		$single['name']=$expense_name;
		$single['sub_group']=$value['SubGroup']['name'];
		$single['total']=$expense_function['debit'];
		$single['first']=0;
		$single['second']=0;
		$OUTSTANDING_AccountHead=$this->AccountHead->findByName($expense_name.' OUTSTANDING');
		if($OUTSTANDING_AccountHead)
		{
			$single['first']=$OUTSTANDING_AccountHead['AccountHead']['opening_balance'];
			$Journal_outstanding=$this->General_Journal_Debit_N_Credit_function($OUTSTANDING_AccountHead['AccountHead']['id'],$from_date,$to_date);
			$single['first']+=$Journal_outstanding['credit']-$Journal_outstanding['debit'];
		}
		$PREPAID_AccountHead=$this->AccountHead->findByName($expense_name.' PREPAID');
		if(!empty($PREPAID_AccountHead))
		{
			$single['second']=$PREPAID_AccountHead['AccountHead']['opening_balance'];
			$single['total']+=$PREPAID_AccountHead['AccountHead']['opening_balance'];
			$Journal_PREPAID=$this->General_Journal_Debit_N_Credit_function($PREPAID_AccountHead['AccountHead']['id'],$from_date,$to_date);
			$single['second']+=$Journal_PREPAID['debit']-$Journal_PREPAID['credit'];
		}
		$single['balance']=$single['total'];
		if($single['total']<$single['first'])
		{
			$single['balance']+=$single['first'];
			$single['total']+=$single['first'];
		}
		elseif($single['total']==0 && $single['first']!=0)
		{
			$single['second']+=$single['first']*-1;
			$single['first']=0;
		}
		$expense_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id'],$from_date,$to_date);
		$single['paid']=$expense_function['debit']-$single['first']+$single['second'];
//pr($single['paid']);
		$paid+=$single['paid'];
	}
	return $paid;
}

public function Get_Master_Group_Ajax()
{
	$return['result']='Empty';
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$type_id=$data['type_id'];
	$MasterGroup=$this->MasterGroup->find('list',[
		'conditions'=>['type_id'=>$type_id],
		'order'=>['MasterGroup.name ASC'],
		]);
	$return['MasterGroup']=$MasterGroup;
	echo json_encode($return); exit;
}
public function Get_Group_Ajax()
{
	$return['result']='Empty';
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$master_group_id=$data['master_group_id'];
	$Group=$this->Group->find('list',[
		'conditions'=>['master_group_id'=>$master_group_id],
		'order'=>['Group.name ASC'],
		]);
	$return['Group']=$Group;
	echo json_encode($return); exit;
}
public function Get_Group_Ajax_trial()
{
	$return['result']='Empty';
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$master_group_id=$data['master_group_id'];
	$Group=$this->Group->find('list',[
		'conditions'=>['master_group_id'=>$master_group_id],
		'order'=>['Group.name ASC'],
		]);
	$return['Group']=$Group;
	echo json_encode($return); exit;
}
public function Get_SubGroup_Ajax()
{
	$return['result']='Empty';
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$group_id=$data['group_id'];
	$SubGroup=$this->SubGroup->find('list',[
		'conditions'=>['group_id'=>$group_id],
		'order'=>['SubGroup.name ASC'],
		]);
	$return['SubGroup']=$SubGroup;
	echo json_encode($return); exit;
}
public function Get_AccountHead_Ajax()
{
	$data=$this->request->data;
	$from_date=date('Y-m-d',strtotime($data['from_date']));
	$to_date=date('Y-m-d',strtotime($data['to_date']));
	$time=strtotime($from_date);
	$final = date('d-m-Y', strtotime("-1 day", $time));
	$to=date('Y-m-d',strtotime($final));
	$conditions=[];
	if(isset($data['sub_group_id'])) {
		$sub_group_id=$data['sub_group_id'];	
		$conditions['sub_group_id']=$sub_group_id;
	}
	$type_id=$data['type_id'];
	$AccountHead=$this->AccountHead->find('list',['conditions'=>$conditions]);
	$GSTReport=['total'=>[]];
	$total['tax']['in']=0;
	$total['tax']['out']=0;
	if(in_array($type_id, ['1','5']))
	{
		$Product=$this->Product_id_list_function($from_date,$to_date);
	}
	$return['result']='Success';
	foreach ($AccountHead as $key => $value) {
		$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function($key,$from_date);
		$function_value_return=$this->General_Journal_Debit_N_Credit_function($key,$from_date,$to_date);
		$AccountHead=$this->AccountHead->findById($key);
		$return['AccountHead'][$key]['name']=$value;
		$return['AccountHead'][$key]['amount']=0;
		if(in_array($key, ['2','17']))
		{
			$return['AccountHead'][$key]['amount']+=$AccountHead['AccountHead']['opening_balance'];
			$ProfitLoss['Stock']=$this->StockCalculatorProfit($from_date,$to_date);
			$ProfitLoss['Income']=$this->IncomeCalculatorForBalansheet($from_date,$to_date);
			$ProfitLoss['Expense']=$this->ExpenseCalculatorForBalansheet($from_date,$to_date);
			$ProfitLoss['Purchase']=$this->PurchaseCalculator($from_date,$to_date);
			$ProfitLoss['Sale']=$this->SaleCalculator($from_date,$to_date);
			$GSTReport=['total'=>[]];
			$gst_in=0;
			$gst_out=0;
			$total_in=0;
			$total_out=0;
			$GSTReport['total']['out']=0;
			$GSTReport['total']['in']=0;
			$single_GSTReport=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date,$gst='All',$type='All');
			$GSTReport['total']['in']=$single_GSTReport['tax']['in'];
			$GSTReport['total']['out']=$single_GSTReport['tax']['out'];
			$gst=round($GSTReport['total']['out']-$GSTReport['total']['in']);
			if($gst>0)
			{
				$tax_amount['left']=$gst;
				$tax_amount['right']=0;
			}
			else
			{
				$tax_amount['left']=0;
				$tax_amount['right']=$gst*-1;
			}
			$ProfitLoss['Expense']['IndirectExpense']['amount']+=$tax_amount['left'];
			$ProfitLoss['Income']['IndirectIncome']['amount']+=$tax_amount['right'];
			$ProfitLoss['First']['Left']=0;
			$ProfitLoss['First']['Right']=0;
			$ProfitLoss['Second']['Left']=0;
			$ProfitLoss['Second']['Right']=0;
			$ProfitLoss['FirstTotal']['Left']=0;
			$ProfitLoss['FirstTotal']['Right']=0;
			$ProfitLoss['Gross']['Left']=0;
			$ProfitLoss['Gross']['Right']=0;
			$ProfitLoss['Net']['Right']=0;
			$ProfitLoss['Net']['Left']=0;
			$ProfitLoss['SecondTotal']['Left']=0;
			$ProfitLoss['SecondTotal']['Right']=0;
			$FirstLeft=$ProfitLoss['Purchase']['PurchaseValue']-$ProfitLoss['Purchase']['PurchaseReturn']+$ProfitLoss['Expense']['DirectExpense']['amount']+$ProfitLoss['Stock']['open']+$tax_amount['right'];
			$FirstLeft=round($FirstLeft,3);
			$FirstRight=$ProfitLoss['Sale']['SaleValue']-$ProfitLoss['Sale']['SalesReturn']+$ProfitLoss['Income']['DirectIncome']['amount']+$ProfitLoss['Stock']['close']+$tax_amount['left'];
			$FirstRight=round($FirstRight,3);
			if($FirstLeft>$FirstRight)
			{
				$ProfitLoss['First']['Right']=$FirstLeft-$FirstRight;
				$ProfitLoss['Gross']['Left']=$FirstLeft-$FirstRight;
			}
			else
			{
				$ProfitLoss['First']['Left']=$FirstRight-$FirstLeft;
				$ProfitLoss['Gross']['Right']=$FirstRight-$FirstLeft;
			}
			$ProfitLoss['FirstTotal']['Left']+=$FirstLeft+$ProfitLoss['First']['Left'];
			$ProfitLoss['FirstTotal']['Right']+=$FirstRight+$ProfitLoss['First']['Right'];
			$SecondLeft=$ProfitLoss['Expense']['IndirectExpense']['amount']+$ProfitLoss['Gross']['Left'];
			$SecondRight=$ProfitLoss['Income']['IndirectIncome']['amount']+$ProfitLoss['Gross']['Right'];
			if($SecondLeft>$SecondRight)
			{
				$ProfitLoss['Second']['Right']=$SecondLeft-$SecondRight;
				$ProfitLoss['Net']['Right']=$SecondLeft-$SecondRight;
			}
			else
			{
				$ProfitLoss['Second']['Left']=$SecondRight-$SecondLeft;
				$ProfitLoss['Net']['Left']=$SecondRight-$SecondLeft;
			}
			if($key==2)
			{
				$return['AccountHead'][$key]['amount']+=isset($ProfitLoss['Net']['Left'])?$ProfitLoss['Net']['Left']:0;
			}
			//if($key==17)
			if($key==2)
			{
				$return['AccountHead'][$key]['amount']-=isset($ProfitLoss['Net']['Right'])?$ProfitLoss['Net']['Right']:0;
			}
		}
		$return['AccountHead'][$key]['id']=$key;
		if(in_array($type_id, ['1','5']))
		{
			$return['AccountHead'][$key]['amount']+=$AccountHead['AccountHead']['opening_balance'];
			if($type_id=='1')
			{
				$return['AccountHead'][$key]['amount']+=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['amount']-=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['amount']+=$function_value_return['debit'];
				$return['AccountHead'][$key]['amount']-=$function_value_return['credit'];
				if($function_value_return['name']=='GST ACCRUED')
				{
					$All_Gst_return_result=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date);
					$gst=$All_Gst_return_result['tax']['out']-$All_Gst_return_result['tax']['in'];
					if($gst>0)
					{
						$tax_amount['Asset']=0;
						$tax_amount['Liabilities']=$gst;
					}
					else
					{
						$tax_amount['Asset']=$gst*-1;
						$tax_amount['Liabilities']=0;
					}
					$return['AccountHead'][$key]['amount']+=$tax_amount['Asset'];
				}
			}
			if($type_id=='5')
			{
				$return['AccountHead'][$key]['amount']+=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['amount']-=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['amount']+=$function_value_return['credit'];
				$return['AccountHead'][$key]['amount']-=$function_value_return['debit'];
				if($function_value_return['name']=='GST OUTSTANDING')
				{
					$All_Gst_return_result=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date);
					$gst=$All_Gst_return_result['tax']['out']-$All_Gst_return_result['tax']['in'];
					if($gst>0)
					{
						$tax_amount['Asset']=0;
						$tax_amount['Liabilities']=$gst;
					}
					else
					{
						$tax_amount['Asset']=$gst*-1;
						$tax_amount['Liabilities']=0;
					}
					$return['AccountHead'][$key]['amount']+=$tax_amount['Liabilities'];
				}						
			}
		}
		if($type_id=='2')
		{
			$AccountHead_Name=$value;
			$AccountHead_Name_split=explode(' ',$AccountHead_Name);
			$return['AccountHead'][$key]['amount']+=$AccountHead['AccountHead']['opening_balance'];
			if($AccountHead_Name_split[1]=='CAPITAL') {
				$return['AccountHead'][$key]['amount']+=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['amount']-=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['amount']+=$function_value_return['credit']-$function_value_return['debit'];
			} else {
				$drawing =$opening_function_value_return['debit']-$opening_function_value_return['credit'];
				$drawing+=$function_value_return['debit']-$function_value_return['credit'];
				$return['AccountHead'][$key]['amount']+=$drawing;
				$return['AccountHead'][$key]['amount']*=-1;
			}
		}
		if($type_id=='3')
		{
			$return['AccountHead'][$key]['amount']+=$function_value_return['debit'];
		}
		if($type_id=='4')
		{
			$return['AccountHead'][$key]['amount']+=$function_value_return['credit'];
		}
		if($key=='24')
		{
			$ProfitLoss=$this->profit_loss_calculator("0000-00-00",$to);
			if($ProfitLoss['Net']['Left'])
			{
			$return['AccountHead'][$key]['amount']=floatval($ProfitLoss['Net']['Left']);
		    }
		    else{
		    	$return['AccountHead'][$key]['amount']=floatval(-$ProfitLoss['Net']['Right']);
		    }
		}
		 if($key=='2')
		 {
		 	  $return['AccountHead'][$key]['amount']=round($return['AccountHead'][$key]['amount'],3);
		}
		if($key==25)
		{
			$return['AccountHead'][$key]['amount']=round($return['AccountHead'][$key]['amount']);
		}
	}
	echo json_encode($return); exit;
}
public function Get_AccountHead_Ajax_trial()
{
	$data=$this->request->data;
	$from_date=date('Y-m-d',strtotime($data['from_date']));
	$to_date=date('Y-m-d',strtotime($data['to_date']));
	$time=strtotime($from_date);
	$final = date('d-m-Y', strtotime("-1 day", $time));
	$to=date('Y-m-d',strtotime($final));
	$conditions=[];
	if(isset($data['sub_group_id'])) {
		$sub_group_id=$data['sub_group_id'];	
		$conditions['sub_group_id']=$sub_group_id;
	}
	$type_id=$data['type_id'];
	$AccountHead=$this->AccountHead->find('list',['conditions'=>$conditions]);
	$GSTReport=['total'=>[]];
	$total['tax']['in']=0;
	$total['tax']['out']=0;
	if(in_array($type_id, ['1','5']))
	{
		$Product=$this->Product_id_list_function($from_date,$to_date);
	}
	$return['result']='Success';
	foreach ($AccountHead as $key => $value) {
		$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function($key,$from_date);
		$function_value_return=$this->General_Journal_Debit_N_Credit_function($key,$from_date,$to_date);
		$AccountHead=$this->AccountHead->findById($key);
		$return['AccountHead'][$key]['name']=$value;
		$return['AccountHead'][$key]['amount']=0;
		if(in_array($key, ['2','17']))
		{
			$return['AccountHead'][$key]['amount']+=$AccountHead['AccountHead']['opening_balance'];
			$ProfitLoss['Stock']=$this->StockCalculator($from_date,$to_date);
			$ProfitLoss['Income']=$this->IncomeCalculatorForBalansheet($from_date,$to_date);
			$ProfitLoss['Expense']=$this->ExpenseCalculatorForBalansheet($from_date,$to_date);
			$ProfitLoss['Purchase']=$this->PurchaseCalculator($from_date,$to_date);
			$ProfitLoss['Sale']=$this->SaleCalculator($from_date,$to_date);
			$GSTReport=['total'=>[]];
			$gst_in=0;
			$gst_out=0;
			$total_in=0;
			$total_out=0;
			$GSTReport['total']['out']=0;
			$GSTReport['total']['in']=0;
			$single_GSTReport=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date,$gst='All',$type='All');
			$GSTReport['total']['in']=$single_GSTReport['tax']['in'];
			$GSTReport['total']['out']=$single_GSTReport['tax']['out'];
			$gst=round($GSTReport['total']['out']-$GSTReport['total']['in']);
			if($gst>0)
			{
				$tax_amount['left']=$gst;
				$tax_amount['right']=0;
			}
			else
			{
				$tax_amount['left']=0;
				$tax_amount['right']=$gst*-1;
			}
			$ProfitLoss['Expense']['IndirectExpense']['amount']+=$tax_amount['left'];
			$ProfitLoss['Income']['IndirectIncome']['amount']+=$tax_amount['right'];
			$ProfitLoss['First']['Left']=0;
			$ProfitLoss['First']['Right']=0;
			$ProfitLoss['Second']['Left']=0;
			$ProfitLoss['Second']['Right']=0;
			$ProfitLoss['FirstTotal']['Left']=0;
			$ProfitLoss['FirstTotal']['Right']=0;
			$ProfitLoss['Gross']['Left']=0;
			$ProfitLoss['Gross']['Right']=0;
			$ProfitLoss['Net']['Right']=0;
			$ProfitLoss['Net']['Left']=0;
			$ProfitLoss['SecondTotal']['Left']=0;
			$ProfitLoss['SecondTotal']['Right']=0;
			$FirstLeft=$ProfitLoss['Purchase']['PurchaseValue']-$ProfitLoss['Purchase']['PurchaseReturn']+$ProfitLoss['Expense']['DirectExpense']['amount']+$ProfitLoss['Stock']['open']+$tax_amount['right'];
			$FirstLeft=round($FirstLeft,3);
			$FirstRight=$ProfitLoss['Sale']['SaleValue']-$ProfitLoss['Sale']['SalesReturn']+$ProfitLoss['Income']['DirectIncome']['amount']+$ProfitLoss['Stock']['close']+$tax_amount['left'];
			$FirstRight=round($FirstRight,3);
			if($FirstLeft>$FirstRight)
			{
				$ProfitLoss['First']['Right']=$FirstLeft-$FirstRight;
				$ProfitLoss['Gross']['Left']=$FirstLeft-$FirstRight;
			}
			else
			{
				$ProfitLoss['First']['Left']=$FirstRight-$FirstLeft;
				$ProfitLoss['Gross']['Right']=$FirstRight-$FirstLeft;
			}
			$ProfitLoss['FirstTotal']['Left']+=$FirstLeft+$ProfitLoss['First']['Left'];
			$ProfitLoss['FirstTotal']['Right']+=$FirstRight+$ProfitLoss['First']['Right'];
			$SecondLeft=$ProfitLoss['Expense']['IndirectExpense']['amount']+$ProfitLoss['Gross']['Left'];
			$SecondRight=$ProfitLoss['Income']['IndirectIncome']['amount']+$ProfitLoss['Gross']['Right'];
			if($SecondLeft>$SecondRight)
			{
				$ProfitLoss['Second']['Right']=$SecondLeft-$SecondRight;
				$ProfitLoss['Net']['Right']=$SecondLeft-$SecondRight;
			}
			else
			{
				$ProfitLoss['Second']['Left']=$SecondRight-$SecondLeft;
				$ProfitLoss['Net']['Left']=$SecondRight-$SecondLeft;
			}
			if($key==2)
			{
				$return['AccountHead'][$key]['amount']+=isset($ProfitLoss['Net']['Left'])?$ProfitLoss['Net']['Left']:0;
			}
			if($key==17)
			{
				$return['AccountHead'][$key]['amount']+=isset($ProfitLoss['Net']['Right'])?$ProfitLoss['Net']['Right']:0;
			}
		}
		$return['AccountHead'][$key]['id']=$key;
		if(in_array($type_id, ['1','5']))
		{
			$return['AccountHead'][$key]['amount']+=$AccountHead['AccountHead']['opening_balance'];
			if($type_id=='1')
			{
				if($key!=19 && $key!=22)
				{
				$return['AccountHead'][$key]['amount']+=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['amount']-=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['amount']+=$function_value_return['debit'];
				$return['AccountHead'][$key]['amount']-=$function_value_return['credit'];
				if($function_value_return['name']=='GST ACCRUED')
				{
					$All_Gst_return_result=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date);
					$gst=$All_Gst_return_result['tax']['out']-$All_Gst_return_result['tax']['in'];
					if($gst>0)
					{
						$tax_amount['Asset']=0;
						$tax_amount['Liabilities']=$gst;
					}
					else
					{
						$tax_amount['Asset']=$gst*-1;
						$tax_amount['Liabilities']=0;
					}
					$return['AccountHead'][$key]['amount']+=$tax_amount['Asset'];
				}
			  }
			  else{
			  	$return['AccountHead'][$key]['amount']+=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['amount']-=$opening_function_value_return['credit'];
				//$return['AccountHead'][$key]['amount']+=$function_value_return['debit'];
				//$return['AccountHead'][$key]['amount']-=$function_value_return['credit'];
				if($function_value_return['name']=='GST ACCRUED')
				{
					$All_Gst_return_result=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date);
					$gst=$All_Gst_return_result['tax']['out']-$All_Gst_return_result['tax']['in'];
					if($gst>0)
					{
						$tax_amount['Asset']=0;
						$tax_amount['Liabilities']=$gst;
					}
					else
					{
						$tax_amount['Asset']=$gst*-1;
						$tax_amount['Liabilities']=0;
					}
					$return['AccountHead'][$key]['amount']+=$tax_amount['Asset'];
				}
			  }
			}
			if($type_id=='5')
			{
				$return['AccountHead'][$key]['amount']+=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['amount']-=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['amount']+=$function_value_return['credit'];
				$return['AccountHead'][$key]['amount']-=$function_value_return['debit'];
				if($function_value_return['name']=='GST OUTSTANDING')
				{
					$All_Gst_return_result=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date);
					$gst=$All_Gst_return_result['tax']['out']-$All_Gst_return_result['tax']['in'];
					if($gst>0)
					{
						$tax_amount['Asset']=0;
						$tax_amount['Liabilities']=$gst;
					}
					else
					{
						$tax_amount['Asset']=$gst*-1;
						$tax_amount['Liabilities']=0;
					}
					$return['AccountHead'][$key]['amount']+=$tax_amount['Liabilities'];
				}						
			}
		}
		if($type_id=='2')
		{
			$AccountHead_Name=$value;
			$AccountHead_Name_split=explode(' ',$AccountHead_Name);
			$return['AccountHead'][$key]['amount']+=$AccountHead['AccountHead']['opening_balance'];
			if($AccountHead_Name_split[1]=='CAPITAL') {
				$return['AccountHead'][$key]['amount']+=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['amount']-=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['amount']+=$function_value_return['credit']-$function_value_return['debit'];
			} else {
				$drawing =$opening_function_value_return['debit']-$opening_function_value_return['credit'];
				$drawing+=$function_value_return['debit']-$function_value_return['credit'];
				$return['AccountHead'][$key]['amount']+=$drawing;
				$return['AccountHead'][$key]['amount']*=-1;
			}
		}
		if($type_id=='3')
		{
			$return['AccountHead'][$key]['amount']+=$function_value_return['debit'];
		}
		if($type_id=='4')
		{
			$return['AccountHead'][$key]['amount']+=$function_value_return['credit'];
		}
		if($key=='24')
		{
			$ProfitLoss=$this->profit_loss_calculator("0000-00-00",$to);
			$return['AccountHead'][$key]['amount']=floatval($ProfitLoss['Net']['Left']);
		}
		if($key=='2')
		{

			$return['AccountHead'][$key]['amount']=0;
		}
		if($key=='17')
		{
           	  $ProfitLoss=$this->profit_loss_calculator("0000-00-00",$to);
			  $return['AccountHead'][$key]['amount']=floatval($ProfitLoss['Net']['Right']);
		}
		// if($key=='17')
		// {
		// 	  $return['AccountHead'][$key]['amount']=0;
		// }
		if($key==25)
		{
			$return['AccountHead'][$key]['amount']=round($return['AccountHead'][$key]['amount']);
		}
	}
	//pr($return);
	echo json_encode($return); exit;
}
public function Product_id_list_function()
{
	$Product_list=[];
	$SaleItem_DISTINCT_Product=$this->SaleItem->find('all',array('conditions'=>array('Sale.status>=2'),'fields'=>array('DISTINCT SaleItem.product_id')));
	$SalesReturnItem_DISTINCT_Product=$this->SalesReturnItem->find('all',array('conditions'=>array('SalesReturn.status>=2'),'fields'=>array('DISTINCT SalesReturnItem.product_id')));
	$PurchaseReturnItem_DISTINCT_Product=$this->PurchaseReturnItem->find('all',array('conditions'=>array('PurchaseReturn.status>=2'),'fields'=>array('DISTINCT PurchaseReturnItem.product_id')));
	$PurchasedItem_DISTINCT_Product=$this->PurchasedItem->find('all',array('conditions'=>array('Purchase.status>=2'),'fields'=>array('DISTINCT PurchasedItem.product_id')));
	foreach ($PurchasedItem_DISTINCT_Product as $key => $value) {$Product_list[$value['PurchasedItem']['product_id']]=$value['PurchasedItem']['product_id'];}
	foreach ($PurchaseReturnItem_DISTINCT_Product as $key => $value) {$Product_list[$value['PurchaseReturnItem']['product_id']]=$value['PurchaseReturnItem']['product_id'];}
	foreach ($SaleItem_DISTINCT_Product as $key => $value) {$Product_list[$value['SaleItem']['product_id']]=$value['SaleItem']['product_id'];}
	foreach ($SalesReturnItem_DISTINCT_Product as $key => $value) {$Product_list[$value['SalesReturnItem']['product_id']]=$value['SalesReturnItem']['product_id'];}
	return $Product_list;
}
public function get_accounthead_by_sub_group_ajax()
{
	$requestData=$this->request->data;
	$sub_group_name=$requestData['sub_group_name'];
	$SubGroup=$this->SubGroup->findByName(trim($sub_group_name));
	$Data=[];
	$totalData=1;
	$totalFiltered=$totalData;
	if($SubGroup)
	{
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));	
		$GroupName=$SubGroup['Group']['name'];
		$Group=$this->Group->find('first',[
			'joins'=>array(
				array(
					'table'=>'types',
					'alias'=>'Type',
					'type'=>'INNER',
					'conditions'=>array('Type.id=MasterGroup.type_id')
					),
				),
			'conditions'=>['Group.name'=>$GroupName],
			'fields'=>[
			'Group.id',
			'Group.name',
			'Type.id',
			'Type.name',
			],
			]);
		$gst_on_sale_account_head_id=9;
		$gst_on_purchase_account_head_id=8;		
		$type_name=$Group['Type']['name'];
		$sub_group_id=$SubGroup['SubGroup']['id'];
		$sub_group_name=$SubGroup['SubGroup']['name'];
		$AccoutHead_list=$this->AccountHead_list_By_SubGroup_id($sub_group_id);
		$return=[];
		foreach ($AccoutHead_list as $key => $name) {
			if($key!=$gst_on_purchase_account_head_id && $key!=$gst_on_sale_account_head_id)
			{
				$AccountHead=$this->AccountHead->findById($key,['AccountHead.id','AccountHead.opening_balance','AccountHead.name','AccountHead.created_at']);
				$single['name']=$name;
				$single['amount']=0;
				if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
				{
					$single['amount']=$AccountHead['AccountHead']['opening_balance'];
				}
				$single['first']=0;
				$single['second']=0;
				$function_value_return=$this->General_Journal_Debit_N_Credit_function($key,$from_date,$to_date);
				if($type_name=='Income')
				{
					$Accrued_amount=0;
					$Advance_amount=0;
					$name=explode(' ',$name);
					array_pop($name);
					$name=implode(' ', $name);
					$Advance=$this->AccountHead->findByName($name.' ADVANCE');
					if(!empty($Advance))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Advance['AccountHead']['id'],$from_date,$to_date);
						if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($Advance['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Advance['AccountHead']['created_at'])))
						{
							$Advance_amount=$Advance['AccountHead']['opening_balance'];
						}
						$Advance_amount+=$return_function['credit']-$return_function['debit'];
					}
					$Accrued=$this->AccountHead->findByName($name.' ACCRUED');
					if(!empty($Accrued))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Accrued['AccountHead']['id'],$from_date,$to_date);
						if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($Accrued['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Accrued['AccountHead']['created_at'])))
						{
							$Accrued_amount=$Accrued['AccountHead']['opening_balance'];
						}
						$single['amount']+=$Accrued_amount;
						$Accrued_amount+=$return_function['debit']-$return_function['credit'];
					}
					$single['amount']+=$function_value_return['credit'];
					$single['first']=$Accrued_amount;
					$single['second']=$Advance_amount;
					$single['total']=$single['amount'];
					if($single['total']<$single['first'])
					{
						$single['total']+=$single['first'];
					}
					elseif($single['total']==0 && $single['first']!=0)
					{
						$single['second']+=$single['first']*-1;
						$single['first']=0;
					}
				}
				if($type_name=='Expense')
				{
					$outstanding_amount=0;
					$prePaid_amount=0;
					$name=explode(' ',$name);
					array_pop($name);
					$name=implode(' ', $name);
					$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
					if(!empty($PrePaid))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($PrePaid['AccountHead']['id'],$from_date,$to_date);
						if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($PrePaid['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($PrePaid['AccountHead']['created_at'])))
						{
							$prePaid_amount=$PrePaid['AccountHead']['opening_balance'];
						}
						$prePaid_amount+=$return_function['debit']-$return_function['credit'];
					}
					$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
					if(!empty($Outstanding))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Outstanding['AccountHead']['id'],$from_date,$to_date);						
						if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($Outstanding['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Outstanding['AccountHead']['created_at'])))
						{
							$outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
						}
						$single['amount']+=$outstanding_amount;
						$outstanding_amount+=$return_function['credit']-$return_function['debit'];
					}
					$single['amount']+=$function_value_return['debit'];
					if($outstanding_amount>0)
					{
						$single['first']+=$outstanding_amount;	
					}
					else
					{
						$single['second']+=$outstanding_amount*-1;	
					}
					$single['second']+=$prePaid_amount;	
					$single['total']=$single['amount'];
					if($single['total']<$single['first'])
					{
						$single['total']+=$single['first'];
					}
					elseif($single['total']==0 && $single['first']!=0)
					{
						$single['second']+=$single['first']*-1;
						$single['first']=0;
					}
				}
				if($single['first'] || $single['second'] || $single['total'] )
					$return[]=$single;
			}
		}
		$Data=$return;
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data);
	exit;
}
public function BrandWiseReport()
{
	$executives=$this->Executive->find('list',array('conditions' => 'block!=1','fields'=>array('id','name')));
// $executives[0]='No Executive';
	$this->set(compact('executives'));
//	pr($executives);
//	exit;
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

}
public function brand_wise_report_ajax()
{
	$conditions=array();
	$conditions_sale=array();
	$data=$this->request->data;
	if(!empty($data['executive_id'])){
		$conditions['Executive.id']=$data['executive_id'];
	}
	$conditions['Executive.block !=']=1;

	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Customer.circle_id',
//'CustomerCircle.name',
			)
		));
	$return=array();
	$return['row']='';
	$return['tfoot']='';
	$return['executives']="All";
	if(!empty($data['executive_id'])){
		$Executivename=$this->Executive->findById($data['executive_id']);
		$return['executives']=$Executivename['Executive']['name'];
	}
// if($Customer)
// {
//  foreach ($Customer as $key => $customer_name) {
	$Brand=$this->Brand->find('list');
//pr($Brand);
//exit;
	$grand_total=0;
	$quantity_total=0;

	foreach($Brand as $brand_id => $brand_name)
	{

// $cutomertype['single_customer']=[];    //$AccountHead=$this->AccountHead->findById($customer_name['AccountHead']['id']);
		$Executive=$this->Executive->find('all',[
			'conditions'=>$conditions,
			]);
//$this->SaleItem->virtualFields = array('total_amount' => "SUM(SaleItem.net_value)");
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$conditions_sale['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
		$conditions_sale['Sale.flag']=1;
		$conditions_sale['Sale.status']=2;
		$conditions_sale['Brand.id']=$brand_id;

		foreach ($Executive as $keyE => $valueE) {
			$conditions_sale['Sale.executive_id']=$valueE['Executive']['id'];
			$SaleItem=$this->SaleItem->find('all',array(
				'joins'=>array(
					array(
						'table'=>'brands',
						'alias'=>'Brand',
						'type'=>'INNER',
						'conditions'=>array('Brand.id=Product.brand_id')
						),
// array(
// 	'table'=>'sub_brands',
// 	'alias'=>'SubBrand',
// 	'type'=>'INNER',
// 	'conditions'=>array('SubBrand.id=Product.sub_brand_id')
// ),
					),
				'conditions'=>$conditions_sale,
				'fields'=>array(
//'total_amount',
					'Product.name',
					'Brand.name',
//'SubBrand.name',
					'Brand.id',
					'SaleItem.net_value',
					'SaleItem.quantity',
					'SaleItem.id',
					'Sale.id',

					),
				)); 
// pr($SaleItem);exit;
			$totalamount=0;
			$quantity=0;
			foreach($SaleItem as $key => $value2)
			{
				$totalamount+=$value2['SaleItem']['net_value'];
				$quantity+=$value2['SaleItem']['quantity'];
			}
			if($totalamount){
				$return['row'].='<tr class="blue-pd" id="parent-'.$brand_id.'">';
				$return['row'].='<td>'.$valueE['Executive']['name'].'</td>';
				$return['row'].='<td>'.$brand_name.'</td>';
				$return['row'].='<td></td>';
				$return['row'].='<td>'.$quantity.'</td>';
				$return['row'].='<td>'.round($totalamount,3).'</td>';
				$return['row'].='</tr>';
				$grand_total+=$totalamount;
				$quantity_total+=$quantity;
				foreach($SaleItem as $key => $value1)
				{
					$return['row'].='<tr class="toggle_rows blue-pd child-parent-'.$value1['Brand']['id'].'">';
					$return['row'].='<td></td>';
					$return['row'].='<td>'.$value1['Brand']['name'].'</td>';
					$return['row'].='<td>'.$value1['Product']['name'].'</td>';
//$return['row'].='<td>'.$value1['SubBrand']['name'].'</td>';
					$return['row'].='<td>'.$value1['SaleItem']['quantity'].'</td>';
					$return['row'].='<td>'.round($value1['SaleItem']['net_value'],3).'</td>';
					$return['row'].='</tr>';
				}
			}
		}
	}

	$return['tfoot'].='<tr>';
	$return['tfoot'].='<td></td>';
	$return['tfoot'].='<td></td>';
	$return['tfoot'].='<td><h3>Total</h3></td>';
	$return['tfoot'].='<td><h3>'.$quantity_total.'<h3></td>';
	$return['tfoot'].='<td><h3>'.$grand_total.'<h3></td>';
	$return['tfoot'].='</tr>';
	$return['result']='Success';
	echo json_encode($return); exit;

}
public function BrandExport($from_date,$to_date)
{
	$date  =date('Y-m-d h:i:sa');
	$this->response->download("brand_export.csv");
	$brands=$this->Brand->find('all',array('fields'=>array('id','name')));
	$conditions=array();
	$conditions['Executive.block !=']=1;
	$conditions_sale=array();
	$Brand=$this->Brand->find('list');
	$grand_total=0;
	$quantity_total=0;
	$Array=[];
	$Brand_count=count($Brand);

	$total=[];
	foreach($Brand as $brand_id => $brand_name)
	{
		$column_total=0;
		$Executive=$this->Executive->find('all',[
			'conditions'=>$conditions,
			]);
		$Executive_count=count($Executive);
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$conditions_sale['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
		$conditions_sale['Sale.flag']=1;
		$conditions_sale['Sale.status']=2;
		$conditions_sale['Brand.id']=$brand_id;
		foreach ($Executive as $keyE => $valueE) {
			$conditions_sale['Sale.executive_id']=$valueE['Executive']['id'];
			$SaleItem=$this->SaleItem->find('all',array(
				'joins'=>array(
					array(
						'table'=>'brands',
						'alias'=>'Brand',
						'type'=>'INNER',
						'conditions'=>array('Brand.id=Product.brand_id')
						),
// array(
// 	'table'=>'sub_brands',
// 	'alias'=>'SubBrand',
// 	'type'=>'INNER',
// 	'conditions'=>array('SubBrand.id=Product.sub_brand_id')
// ),
					),
				'conditions'=>$conditions_sale,
				'fields'=>array(
					'Product.name',
					'Brand.name',
//'SubBrand.name',
					'Brand.id',
					'SaleItem.net_value',
					'SaleItem.quantity',
					'SaleItem.id',
					'Sale.id',
					'Sale.executive_id'
					),
				)); 
			$totalamount=0;
			foreach($SaleItem as $key => $value2)
			{
				$totalamount+=$value2['SaleItem']['net_value'];
				$column_total+=$value2['SaleItem']['net_value'];
			}	
			$single['executive']=$valueE['Executive']['name'];
			$single['brand']=$brand_name;
			$single['total']=$totalamount;
			$Array[]=$single;
		}
		$array_total['brand']=$brand_name;
		$array_total['total']=$column_total;
		$total[]=$array_total;
	}
	$new_array=[];
	foreach($Array as $key3 =>$value3){
		$new_array[$value3['executive']][]=$value3['total'];
	}
	$this->set('column_total',$total);
	$this->set('brand',$brands);
	$this->set('amount',$new_array);
	$this->set('Brand_count',$Brand_count);
	$this->layout = 'ajax';
	return false;
}
public function SupplierAgeingReport()
{
	$Party_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('sub_group_id'=>15)));
	$this->set('Party_list',$Party_list);
	$this->request->data['from_date']=date('d-m-Y');

}
Public function supplier_aging_report_ajax()
{
	$conditions=[];
	$data=$this->request->data;
	if($data['party_id'])
	{
		$conditions['Party.account_head_id']=$data['party_id'];

	}
	$Parties=$this->Party->find('all',array(
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			),
		'order' => array('Party.id' => 'ASC'),
		'conditions'=>$conditions,
		)
	);
	$data['row']='';
	$data['tfoot']='';
	$data['result']='Error';
	$balance=0;
	$Balance_Total=0;
	$to_date1=date('d-m-Y');
	$from1=$data['from_date'];
	$to1=date('d-m-Y');
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($to_date1));
	if($Parties)
	{
		$invoice_array=array();
		$total_opening=0;
		foreach ($Parties as $key => $value)
		{
			$Purchases = $this->Purchase->find('all', array(
				'conditions' => array(
					'Purchase.account_head_id' =>$value['AccountHead']['id'],
//'Purchase.date_of_delivered between ? and ?'=>array($from,$to),
					'Purchase.flag'=>1,
					'Purchase.status'=>array(2,3),
					),
				'order' => array('Purchase.date_of_purchase' => 'ASC'),
				'fields' => array(
					'Purchase.*',
					'AccountHead.name',
					)
				));
			$Journal_voucher=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.debit'=>$value['AccountHead']['id'],
					'NOT' => array(
						'Journal.remarks LIKE' => 'Purchase Invoice No :%',
						),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));
			$voucher_amount=0;
			$voucher_balance=0;
			foreach ($Journal_voucher as $key3 => $value_amount) {

				$voucher_amount+=$value_amount;
			}
			$voucher_bounce=0;
			$Journal_bounce=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$value['AccountHead']['id'],
					'NOT' => array(
						'Journal.remarks LIKE' => 'Purchase Invoice No :%',
					),
					'Journal.flag=1',
				),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
				),
			));
			foreach ($Journal_bounce as $key4 => $value_bounce) {
				$voucher_bounce+=$value_bounce;
			}
			$voucher_amount=$voucher_amount-$voucher_bounce;
			foreach ($Purchases as $keyj =>$valuej)
			{
				$PurchasedItem=$this->PurchasedItem->find('list',array(
					'conditions'=>array('PurchasedItem.purchase_id'=>$valuej['Purchase']['id']),
					'fields'=>array('PurchasedItem.discount')
					));
			}
			$invoice_array_single=array();
			if (!empty($Purchases)) {
				$invoice_array_single['due'] = array();
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['invoice_amount'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['invoice_date'] = array();
				$invoice_array_single['delivered_date'] = array();
				$invoice_array_single['date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{ 
					$from12=date("Y-m-d", strtotime($data['from_date']));
					$outstanding_amount_date1=date("Y-m-d", strtotime($outstanding_amount_date));

					if($outstanding_amount_date1>=$from12){
						array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['invoice_no'], 'Opening Balance');
						array_push($invoice_array_single['invoice_date'], date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['balance'],ROUND($outstanding_amount,3));
						array_push($invoice_array_single['invoice_amount'],ROUND($outstanding_amount,3));
						$now = time();
						$expected_days_diff=$now-strtotime($outstanding_amount_date);
						$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
						$diff_month=intval($diff_day/30);
						$actual_diff_month=$diff_day/30;
						$actual_days=$actual_diff_month-$diff_month;
						$actual_days*=30;
						if($diff_month)
						{
							array_push($invoice_array_single['due'],$diff_month.' Month'.' '.$actual_days.' Days'); 	
						}
						else
						{
							array_push($invoice_array_single['due'],$diff_day.' Days'); 	

						}
					}

				}
				foreach ($Purchases as $keySC2 => $valueSC2) {
					$balance_amount=$valueSC2['Purchase']['grand_total'];
					if($voucher_amount)
					{
						if($balance_amount<$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Purchase']['grand_total']>0)
					{
						if($balance_amount){
							$check_date=$valueSC2['Purchase']['date_of_delivered'];
							$check_date=$valueSC2['Purchase']['date_of_purchase'];
							$from11=date("Y-m-d", strtotime($data['from_date']));
							$to11=date('Y-m-d');
							if($check_date>=$from11){
								array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Purchase']['date_of_purchase'])));
								array_push($invoice_array_single['invoice_no'], $valueSC2['Purchase']['invoice_no']);
								array_push($invoice_array_single['invoice_date'], date("d-m-Y", strtotime($valueSC2['Purchase']['date_of_delivered'])));
								array_push($invoice_array_single['balance'],ROUND($balance_amount,3)) ; 
								array_push($invoice_array_single['invoice_amount'],$valueSC2['Purchase']['grand_total']) ;  
								$now = time();
								$expected_days_diff=$now-strtotime($valueSC2['Purchase']['date_of_purchase']);
								$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
								$diff_month=intval($diff_day/30);
								$actual_diff_month=$diff_day/30;
								$actual_days=$actual_diff_month-$diff_month;
								$actual_days*=30;
								if($diff_month)
								{
									array_push($invoice_array_single['due'],$diff_month.' Month'.' '.$actual_days.' Days'); 	
								}
								else
								{
									array_push($invoice_array_single['due'],$diff_day.' Days'); 	

								}
							}
// else if($check_date<$from11){

// 	$total_opening+=$balance_amount;
// }
						}

					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}
			else
			{

				$invoice_array_single['due'] = array();
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['invoice_amount'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{
					$from12=date("Y-m-d", strtotime($data['from_date']));
					$outstanding_amount_date1=date("Y-m-d", strtotime($outstanding_amount_date));

					if($outstanding_amount_date>=$from1){
						array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['invoice_no'], 'Opening Balance');
						array_push($invoice_array_single['invoice_date'], date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['balance'],ROUND($outstanding_amount,3));
						array_push($invoice_array_single['invoice_amount'],ROUND($outstanding_amount,3));
						$now = time();
						$expected_days_diff=$now-strtotime($outstanding_amount_date);
						$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
						$diff_month=intval($diff_day/30);
						$actual_diff_month=$diff_day/30;
						$actual_days=$actual_diff_month-$diff_month;
						$actual_days*=30;
						if($diff_month)
						{
							array_push($invoice_array_single['due'],$diff_month.' Month'.' '.$actual_days.' Days'); 	
						}
						else
						{
							array_push($invoice_array_single['due'],$diff_day.' Days'); 	

						}
					}
// else if($outstanding_amount_date1<$from12){

// 	$total_opening+=$outstanding_amount;
// }
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}
		}
		$grand_total=0;
		$data['result']='Success';
		$data['balance']=ROUND($total_opening,3);
		$data['data']=$invoice_array;
	}
	echo json_encode($data); exit;
}
public function ProductWise()
{

	$this->set('from',date("d-m-Y", strtotime('-1 day')));
	$this->set('to',date("d-m-Y"));
	$executives=$this->Executive->find('list');
	$route_list=$this->Route->find('list');

	$customer_group=$this->CustomerGroup->find('list');
	$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'),'order'=>array('name ASC'),)));
	$this->set('Product',$this->Product->find('list',array('fields'=>array('id','name'),'order'=>array('name ASC'),)));
	$Brand=$this->Brand->find('list',array('fields'=>array('id','Brand.name'),'order'=>array('Brand.name ASC'),));
	$Brand['G']='GENERAL';
	ksort($Brand);
	$this->set('Brand',$Brand);
	$this->set(compact('executives'));
	$this->set(compact('route_list'));
	$this->set(compact('customer_group'));

}
// public function SaleDetailFunction($account_head_id)
// 	{
// 		try {
// 			//$from_date=date('Y-m-d',strtotime($from_date));
// 			//$to_date=date('Y-m-d',strtotime($to_date));
// 			$Sale=$this->Sale->find('all',array(
// 				'conditions'=>array(
// 					'Sale.status>1',
// 					'Sale.account_head_id'=>$account_head_id,
// 					'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date)
// 				),
// 				'fields'=>array(
// 				),
// 			));
// 			$SalesReturn=$this->SalesReturn->find('all',array(
// 				'conditions'=>array(
// 					'SalesReturn.status >1',
// 					'SalesReturn.account_head_id'=>$account_head_id,
// 					'SalesReturn.date between ? and ?' => array($from_date,$to_date)
// 				),
// 				'fields'=>array(
// 				),
// 			));
// 			$AccountHead=$this->AccountHead->findById($account_head_id);
// 			$AccountingsController = new AccountingsController;
// 			$Journalopen=$AccountingsController->General_Journal_Opening_balance_Debit_N_Credit_With_Date_function($account_head_id,$from_date);
// 			$open=$Journalopen['debit']+$AccountHead['AccountHead']['opening_balance']-$Journalopen['credit'];
// 			$datedif = time() - strtotime($AccountHead['AccountHead']['created_at']);
// 			$day=floor($datedif / (60 * 60 * 24));
// 			$Journal_all=array(
// 				array(
// 					'id'=>'',
// 					'Date'=>date('Y-m-d',strtotime($from_date)),
// 					'Voucher No'=>'',
// 					'Particulars'=>'Opening Balance',
// 					'Debit'=>$open,
// 					'Credit'=>0,
// 					'Balance'=>$open,
// 					// 'Days'=>'',
// 				)
// 			);
// 			$Journal_cheque=$this->Journal->find('all',array(
// 				'conditions'=>array(
// 					'Journal.debit'=>$account_head_id,
// 					'Journal.remarks="cheque Bounce"',
// 					'Journal.flag=1',
// 				)
// 			));
// 			//pr($Journal_cheque);

// 			$Journal_credit=$this->Journal->find('all',array(
// 				'conditions'=>array(
// 					'Journal.credit'=>$account_head_id,
// 					'AccountHeadDebit.sub_group_id'=>array('1','2'),
// 					'Journal.flag=1',
// 					'Journal.date between ? and ?'=>array($from_date,$to_date),
// 				)
// 			));
// 			foreach ($Journal_credit as $key => $value) 
// 			{
// 				$datedif = time() - strtotime($value['Journal']['date']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']=$value['Journal']['id'];
// 				$Journal_single['Date']=$value['Journal']['date'];
// 				$Journal_single['Voucher No']=$value['Journal']['voucher_no'];
// 				$Journal_single['Particulars']=$value['AccountHeadDebit']['name'];
// 				$Journal_single['Debit']=0;
// 				$Journal_single['Credit']=$value['Journal']['amount'];
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single;
// 			}
// 			$discount_paid=135;
// 			$Journal_discount_paid=$this->Journal->find('all',array(
// 				'conditions'=>array(
// 					'Journal.debit'=>$discount_paid,
// 					'Journal.credit'=>$account_head_id,
// 					'Journal.flag=1',
// 					'Journal.date between ? and ?'=>array($from_date,$to_date),
// 				)
// 			));
// 			foreach ($Journal_discount_paid as $key => $value) 
// 			{
// 				$datedif = time() - strtotime($value['Journal']['date']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']=$value['Journal']['id'];
// 				$Journal_single['Date']=$value['Journal']['date'];
// 				$Journal_single['Voucher No']=$value['Journal']['voucher_no'];
// 				$Journal_single['Particulars']=$value['AccountHeadDebit']['name'];
// 				$Journal_single['Debit']=0;
// 				$Journal_single['Credit']=$value['Journal']['amount'];
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single;
// 			}
// 			foreach ($Sale as $key => $value) {
// 				$datedif = time() - strtotime($value['Sale']['date_of_delivered']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']='';
// 				$Journal_single['Date']=$value['Sale']['date_of_delivered'];
// 				$Journal_single['Voucher No']=$value['Sale']['invoice_no'];
// 				$Journal_single['Particulars']='Sale Account';
// 				$Journal_single['Debit']=$value['Sale']['grand_total'];
// 				$Journal_single['Credit']=0;
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single	;
// 			}
// 			foreach ($SalesReturn as $key => $value) {
// 				$datedif = time() - strtotime($value['SalesReturn']['date']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']='';
// 				$Journal_single['Date']=$value['SalesReturn']['date'];
// 				$Journal_single['Voucher No']=$value['SalesReturn']['invoice_no'];
// 				$Journal_single['Particulars']='Sale Return';
// 				$Journal_single['Debit']=0;
// 				$Journal_single['Credit']=$value['SalesReturn']['grand_total'];
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single	;
// 			}
// 			foreach ($Journal_cheque as $key => $value) {
// 				$datedif = time() - strtotime($value['Journal']['date']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']='';
// 				$Journal_single['Date']=$value['Journal']['date'];
// 				$Journal_single['Voucher No']='';
// 				$Journal_single['Particulars']='Cheque Bounce';
// 				$Journal_single['Debit']=$value['Journal']['amount'];
// 				$Journal_single['Credit']=0;
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single	;
// 			}
// 			$total_debit=0;
// 			$total_credit=0;
// 			$total_balance=0;
// 			$balance=0;
// 			$Journal_all = Set::sort($Journal_all, '{n}.id', 'asc');
// 			$Journal_all = Set::sort($Journal_all, '{n}.Date', 'asc');
// 			foreach ($Journal_all as $key => $value) {
// 				unset($Journal_all[$key]['id']);
// 				$total_debit+=$value['Debit'];
// 				$total_credit+=$value['Credit'];
// 				$balance=$total_debit-$total_credit;
// 				$total_balance=$balance;
// 				$Journal_all[$key]['Balance']=$balance;
// 				$Journal_all[$key]['Date']=date('d-M-Y',strtotime($value['Date']));
// 			}


// 			$foot_array=array(
// 				'Date'=>'',
// 				'Voucher No'=>'',
// 				'Particulars'=>'Total',
// 				'Debit'=>round($total_debit),
// 				'Credit'=>round($total_credit),
// 				'Balance'=>round($total_balance),
// 				// 'Days'=>'',
// 			);
// 			array_push($Journal_all, $foot_array);
// 			$header=array(
// 				'Date',
// 				'Voucher No',
// 				'Particulars',
// 				'Debit',
// 				'Credit',
// 				'Balance',
// 				// 'Days',
// 			);
// 			$width[0]=25;
// 			$width[1]=25;
// 			$width[2]=65;
// 			$width[3]=25;
// 			$width[4]=25;
// 			$width[5]=25;
// 			// $width[6]=12;
// 			require('fpdf/fpdf.php');
// 			$pdf = new FPDF('p');
// 		// $pdf->SetFont('Arial','B',14);
// 			$pdf->AddPage();
// 			$pdf->setFont("Arial",'B','9');           
// 			$pdf->Image("img/liyalams_logo.jpeg",85,5,-300);
// 			$pdf->Text(48, 20, '15/85 A.VALIYORA P O,VENGARA. MALAPPURAM-676304,TEL:0494 2450242');
// 			$pdf->Text(76, 24, 'www.liyalams.com,liyalams@gmail.com');
// 			$pdf->Line(1,26,209,26);
// 			$pdf->SetFont('Arial','B','11');
// 			//$pdf->Text(10, 33, 'Ledger Report Of '.$AccountHead['AccountHead']['name'].' for the Period From '.date('d-M-Y',strtotime($from_date)).' To '.date('d-M-Y',strtotime($to_date)));
// 			$pdf -> SetY(37); 
// 			for($i=0;$i<count($header);$i++)
// 				$pdf->Cell($width[$i],7,$header[$i],1,0,'C');
// 			$pdf->Ln();
// 			$pdf->SetFillColor(224,235,255);
// 			$fill = false;
// 			$pdf->SetFont('Arial','','10');
// 			foreach($Journal_all as $row)
// 			{
// 				$i=0;
// 				foreach($row as $col){
// 					$pdf->Cell($width[$i],6,$col,'LR',0,'L',$fill);
// 				// $pdf->Cell($width[$i],6,$col,1,0,'L',$fill);
// 					$i++;
// 				}
// 				$pdf->Ln();
// 				$fill = !$fill;
// 			}
// 			$pdf->Cell(array_sum($width),0,'','T');
// 			$pdf->Output();
// 			exit;
// 			$return['result']='Success';
// 		} catch (Exception $e) {
// 			$return['result']=$e->getMessage();
// 		}
// 		return $return;
// 	}
public function PurchaseItemWiseFilter(){
	$this->Session->delete('purchase_item_conditions');
	$conditions=array();
	if(!empty($this->request->data['product_type_id']))
	{
		$conditions['ProductType.id']=$this->request->data['product_type_id'];
	}
	if(!empty($this->request->data['product_id']))
	{
		$conditions['Product.id']=$this->request->data['product_id'];
	}
	if(!empty($this->request->data['from_date']) && !empty($this->request->data['to_date']))
	{
		$from_date=date('Y-m-d',strtotime($this->request->data['from']));
		$to_date=date('Y-m-d',strtotime($this->request->data['to']));
		$conditions['Purchase.date_of_delivered between ? and ?']=array($from_date,$to_date);
	}
	pr($conditions);
	$conditions['Purchase.flag' ]=1;	
	$conditions['Purchase.status' ]=2;	
	$this->Session->write('purchase_item_conditions',$conditions);
	echo json_encode(array('null'));
	exit;
}
public function ProductLog($name=null)
{
	$this->Product->unbindModel(array('hasMany' => array('SalesReturnItem','SaleItem','PurchasedItem','PurchaseReturnItem','UnwantedList',)));
	$this->Product->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$products=$this->Product->find('list',array(
		'fields'=>array(
			'Product.id',
			'product_name',
			)
		));
//$products=$this->Product->find('list');
	$warehouses=$this->Warehouse->find('list');
	$this->set(compact('products','warehouses'));
}
public function GetProductLogAjax()
{
	$conditions=[];
	$data=$this->request->data;
	if($data['product_id'])
		$conditions['Product.id']=$data['product_id'];
	if($data['warehouse_id'])
		$conditions['Warehouse.id']=$data['warehouse_id'];
	$StockLog=$this->StockLog->find('all',array(
		'conditions'=>$conditions,
		'order'=>array('StockLog.id ASC'),
		'fields'=>array(
			'StockLog.id',
			'StockLog.quantity',
			'StockLog.quantity_in',
			'StockLog.quantity_out',
			'StockLog.remark',
			'StockLog.updated_at',
			'Product.name',
			'Product.code',
			'Warehouse.name',
			),
		));
	echo json_encode($StockLog); exit;
}
public function SaleCollectionReportCustomer()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SaleCollectionReportCustomerAjax()
{
	$data=$this->request->data;
//pr($data);
//exit;
	$conditions=[];
	if($data['customer_id'])
		$conditions['Sale.account_head_id']=$data['customer_id'];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];



	$Executive=$this->Sale->find('all',array(
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Customer.circle_id',
//'CustomerCircle.name',
			)
		));
//pr($Customer);
//pr($conditions);
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Executive as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}

		$single['name']=$value['AccountHead']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
public function collection_report_executive_ajax()
{
	$conditions=array();
	$data=$this->request->data;
//pr($data);
//exit;
// if($data['branch_id'])
// 	$conditions['Customer.route_id']=$data['branch_id'];
	if($data['route_id'])
		$conditions['Customer.route_id']=$data['route_id'];
	if($data['executive_id'])
		$conditions['Executive.id']=$data['executive_id'];





	$this->Executive->unbindModel(array('hasMany' => array('Sale')));
	$Customer=$this->Executive->find('all'
		,
		array(
			'joins'=>array(
				array(
					'table'=>'executive_route_mappings',
					'alias'=>'ExecutiveRouteMapping',
					'type'=>'INNER',
					'conditions'=>array('ExecutiveRouteMapping.executive_id=Executive.id')
					),

				array(
					'table'=>'customers',
					'alias'=>'Customer',
					'type'=>'RIGHT',
					'conditions'=>array('Customer.route_id=ExecutiveRouteMapping.route_id')
					),
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Customer.account_head_id')
					),

				),
//'conditions'=>array('Executive.id'=>$ex_id),
			'conditions'=>$conditions,
			'fields'=>array(
				'Customer.route_id',
				'Customer.account_head_id',
				'AccountHead.id',
				'AccountHead.name',
//'Executive.*',
				'ExecutiveRouteMapping.*',
				),
			));


	$data['row']='';
	$data['tfoot']='';
	$from_date=date('Y-m-d',strtotime($data['from_date']));
	$to_date=date('Y-m-d',strtotime($data['to_date']));
	$data['result']='Error';
	if($Customer)
	{
		$grand_total=0;


		foreach ($Customer as $key => $value) 
		{
			$get_collection_amount_return_value=$this->get_collection_amount_executive($value['Customer']['account_head_id'],$from_date,$to_date,2);		
			if($this->request->data['check_id'] !=0)
			{
				if($get_collection_amount_return_value['sale_amount']!=0)
				{
					$data['row']= $data['row'].'<tr>';
					$data['row'].='<td>'.date('d-m-Y',strtotime($get_collection_amount_return_value['date'])).'</td>';
					$data['row'].='<td>'.$value["AccountHead"]["name"].'</td>';
					$data['row'].='<td>'.$get_collection_amount_return_value['sale_amount'].'</td>';
					$data['row'].='</tr>';
					$grand_total+=$get_collection_amount_return_value['sale_amount'];
				}
			}
			else
			{
				if($get_collection_amount_return_value['date']){$get_date=date('d-m-Y',strtotime($get_collection_amount_return_value['date']));}else{$get_date=date('d-m-Y');}
				$data['row']= $data['row'].'<tr>';
				$data['row'].='<td>'.$get_date.'</td>';
				$data['row'].='<td>'.$value["AccountHead"]["name"].'</td>';
				$data['row'].='<td>'.$get_collection_amount_return_value['sale_amount'].'</td>';
				$data['row'].='</tr>';
				$grand_total+=$get_collection_amount_return_value['sale_amount'];
			}
		}	
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td colspan="2"><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$grand_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';

	}
	echo json_encode($data); exit;
}

public function get_collection_amount_executive($account_head_id,$from_date=null,$to_date=null) {
	$conditions=array();
	if(!empty($from_date)){
		$conditions['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
	}
	$conditions['Journal.credit']=$account_head_id;
	$conditions['AccountHeadDebit.sub_group_id']=['1','2'];
	$conditions['Journal.flag']=1;
	$this->Journal->virtualFields = array( 'total_amount' => "SUM(Journal.amount)" );
	$Journal=$this->Journal->find('first',array('conditions'=>$conditions,
		'fields'=>array('Journal.total_amount','Journal.date')
		));
	$sale_total_amount=0;
	$collection_date=$Journal['Journal']['date'];
	$sale_total_amount=0;
	if($Journal['Journal']['total_amount'])
	{
		$sale_total_amount=floatval($Journal['Journal']['total_amount']);
	}
	$return['sale_amount']=$sale_total_amount;
	$return['date']=$collection_date;
	return $return;

}
public function SaleCollectionReportAll()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SaleCollectionReportAllAjax()
{
	$data=$this->request->data;
//pr($data);
//exit;
	$conditions=[];
	if($data['product_type_id'])
		$conditions['Product.product_type_id']=$data['product_type_id'];
	if($data['product_id'])
		$conditions['SaleItem.product_id']=$data['product_id'];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];

	if($data['customer_id'])
		$conditions['Sale.account_head_id']=$data['customer_id'];

	if($data['route_id'])
		$conditions['Customer.route_id']=$data['route_id'];
	if($data['brand_id'])
		$conditions['Product.brand_id']=$data['brand_id'];
//if($data['customer_id']){
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			array(
				'table'=>'sale_items',
				'alias'=>'SaleItem',
				'type'=>'INNER',
				'conditions'=>array('Sale.id=SaleItem.sale_id')
				),
			array(
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'INNER',
				'conditions'=>array('SaleItem.product_id=Product.id')
				),
			array(
				'table'=>'product_types',
				'alias'=>'ProductType',
				'type'=>'INNER',
				'conditions'=>array('Product.product_type_id=ProductType.id')
				),
			array(
				'table'=>'brands',
				'alias'=>'Brand',
				'type'=>'INNER',
				'conditions'=>array('Product.brand_id=Brand.id')
				),

			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Product.*',
			'ProductType.*',
			'Sale.*',
			'SaleItem.*',
			'Brand.*',
//'Customer.circle_id',
//}
//'CustomerCircle.name',
			)
		));
//}
//pr($Customer);
//pr($conditions);
//exit;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Customer as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.product_id',
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}

		$single['name']=$value['AccountHead']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
public function NosaleCustomerReport()
{

	$this->request->data['from_date']=date("d-m-Y", strtotime('-1 day'));
	$this->request->data['to_date']=date("d-m-Y");
	$route_list=$this->Route->find('list',array('fields'=>array('id','name')));
	$this->set(compact('route_list'));

}
public function nosale_report_ajax()
{
	$conditions=array();
	$conditions_route_id=array();
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	if($data['route_id'])
		$conditions_route_id['Customer.route_id']=$data['route_id'];
	$conditions['Sale.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
	$Customer = $this->AccountHead->find('all',array(
		"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('AccountHead.id=Customer.account_head_id'),
				),
			),
		'conditions'=>$conditions_route_id,
		'fields'=>array(
			'Customer.code',
			'Customer.route_id',
			'AccountHead.id',
			'AccountHead.name',
			)
		));
	$data['row']='';
	$data['tfoot']='';
	$data['result']='Error';

	foreach ($Customer as $key => $value) {
		$conditions['Sale.account_head_id']=$value['AccountHead']['id'];   

		$Sale=$this->Sale->find('all',array(
			'conditions'=>$conditions,
			'fields'=>array(
				'Sale.date_of_delivered',
				'Sale.invoice_no',
				'Sale.grand_total',
				),
			));
		$amount=0;
		foreach ($Sale as $key => $value_Sale) {
			$amount=$amount+$value_Sale['Sale']['grand_total'];
		}      if($amount<$data['Amount'])
		{
			$data['row']= $data['row'].'<tr>';
			$data['row'].='<td>'.$value["Customer"]["code"].'</td>';
			$data['row'].='<td>'.$value["AccountHead"]["name"].'</td>';
			$data['row'].='<td>'.floatval($amount).'</td>';
			$data['row'].='</tr>';
			$data['result']='Success';
		}
	}
	echo json_encode($data); exit;

}

public function SaleCollectionReportProduct()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
	$Branch=$this->Branch->find('list');
	$this->set(compact('Branch'));
	$CustomerGroup=$this->CustomerGroup->find('list');
	$this->set(compact('CustomerGroup'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SaleCollectionReportProductAjax()
{
	$data=$this->request->data;
//pr($data);
//exit;
	$conditions=[];
	if($data['branch_id'])
		$conditions['Sale.branch_id']=$data['branch_id'];
	if($data['route_id'])
		$conditions['Customer.route_id']=$data['route_id'];
// if($data['customer_id'])
// 	$conditions['Sale.account_head_id']=$data['customer_id'];
// if($data['customer_group_id'])
//     $conditions['Customer.customer_group_id']=$data['customer_group_id'];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];
	if($data['product_type_id'])
		$conditions['Product.product_type_id']=$data['product_type_id'];

	if($data['brand_id'])
		$conditions['Product.brand_id']=$data['brand_id'];
	if($data['product_id'])
		$conditions['SaleItem.product_id']=$data['product_id'];

//if($data['customer_id']){
	$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
// 	$Customer=$this->SaleItem->find('all',array(
// 		'joins'=>array(
// 			array(
// 				'table'=>'sales',
// 				'alias'=>'Sale',
// 				'type'=>'INNER',
// 				'conditions'=>array('SaleItem.sale_id=Sale.id')
// 			),
// 			array(
// 				'table'=>'executives',
// 				'alias'=>'Executive',
// 				'type'=>'INNER',
// 				'conditions'=>array('Sale.executive_id=Executive.id')
// 			),
// 			array(
// 				'table'=>'products',
// 				'alias'=>'Product',
// 				'type'=>'INNER',
// 				'conditions'=>array('SaleItem.product_id=Product.id')
// 			),
// 			// array(
// 			// 	'table'=>'product_types',
// 			// 	'alias'=>'ProductType',
// 			// 	'type'=>'INNER',
// 			// 	'conditions'=>array('Product.product_type_id=ProductType.id')
// 			// ),
// 			// array(
// 			// 	'table'=>'brands',
// 			// 	'alias'=>'Brand',
// 			// 	'type'=>'INNER',
// 			// 	'conditions'=>array('Product.brand_id=Brand.id')
// 			// ),
// 			// array(
// 			// 	'table'=>'customer_groups',
// 			// 	'alias'=>'CustomerGroup',
// 			// 	'type'=>'INNER',
// 			// 	'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
// 			// ),
// 			// array(
// 			// 	'table'=>'branches',
// 			// 	'alias'=>'Branch',
// 			// 	'type'=>'INNER',
// 			// 	'conditions'=>array('Sale.branch_id=Branch.id')
// 			// ),

// 		),
// 		'conditions'=>$conditions,
// 		'fields'=>array(
// 			//'AccountHead.id',
// 			//'AccountHead.name',
// 			'Executive.id',
// 			'Executive.name',
// 			'Product.*',
// 			//'ProductType.*',
// 			'Sale.*',
// 			'SaleItem.*',
// 			//'Brand.*',
// 			//'Route.name',
// 			//'Branch.*',
// 			//'Customer.circle_id',
// //}
// //'CustomerCircle.name',
// 		)
// 	));

	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			array(
				'table'=>'sale_items',
				'alias'=>'SaleItem',
				'type'=>'INNER',
				'conditions'=>array('Sale.id=SaleItem.sale_id')
				),
			array(
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'INNER',
				'conditions'=>array('SaleItem.product_id=Product.id')
				),
			array(
				'table'=>'product_types',
				'alias'=>'ProductType',
				'type'=>'INNER',
				'conditions'=>array('Product.product_type_id=ProductType.id')
				),
			array(
				'table'=>'brands',
				'alias'=>'Brand',
				'type'=>'INNER',
				'conditions'=>array('Product.brand_id=Brand.id')
				),
			array(
				'table'=>'customer_groups',
				'alias'=>'CustomerGroup',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),
// array(
// 	'table'=>'branches',
// 	'alias'=>'Branch',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.branch_id=Branch.id')
// ),

			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Product.*',
			'ProductType.*',
			'Sale.*',
			'SaleItem.*',
			'Brand.*',
			'Route.name',
//'Branch.*',
//'Customer.circle_id',
//}
//'CustomerCircle.name',
			)
		));
//}
//pr($Customer);
//pr($conditions);
//exit;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Customer as $key => $value) {

		if(!empty($value['Executive']['name'])){$exec=$value['Executive']['name'];}else{$exec="No Executive";}

		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'SaleItem.product_id'=>$value['Product']['id'],
				'Sale.executive_id'=>$exec,
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.product_id',
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
//pr($SaleItem);
//exit;
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
//if($SaleItem){
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$exec,
//'SaleItem.product_id'=>$value['Product']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
//pr($value);
//}


		$single['name']=$value['Product']['name'];
// pr($single['name']);
// exit;
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}
		$single['route']=$value['Route']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;

		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;

		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
//pr($single['net_value']);
//exit;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
//}
//exit;
//pr($single);
//exit;
	echo json_encode($list);
	exit;
}

public function SaleCollectionReportCustomerwiseold()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
	$Branch=$this->Branch->find('list');
	$this->set(compact('Branch'));
	$CustomerGroup=$this->CustomerGroup->find('list');
	$this->set(compact('CustomerGroup'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SaleCollectionReportCustomerwiseAjax0ld()
{
	$data=$this->request->data;
//pr($data);
//exit;
	$conditions=[];
//if($data['branch_id'])
// $conditions['Sale.branch_id']=$data['branch_id'];
	if($data['route_id'])
		$conditions['Customer.route_id']=$data['route_id'];
	if($data['customer_id'])
		$conditions['Sale.account_head_id']=$data['customer_id'];
	if($data['customer_group_id'])
		$conditions['Customer.customer_group_id']=$data['customer_group_id'];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];
	if($data['product_type_id'])
		$conditions['Product.product_type_id']=$data['product_type_id'];

	if($data['brand_id'])
		$conditions['Product.brand_id']=$data['brand_id'];
	if($data['product_id'])
		$conditions['SaleItem.product_id']=$data['product_id'];

//if($data['customer_id']){
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			array(
				'table'=>'sale_items',
				'alias'=>'SaleItem',
				'type'=>'INNER',
				'conditions'=>array('Sale.id=SaleItem.sale_id')
				),
			array(
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'INNER',
				'conditions'=>array('SaleItem.product_id=Product.id')
				),
			array(
				'table'=>'product_types',
				'alias'=>'ProductType',
				'type'=>'INNER',
				'conditions'=>array('Product.product_type_id=ProductType.id')
				),
			array(
				'table'=>'brands',
				'alias'=>'Brand',
				'type'=>'INNER',
				'conditions'=>array('Product.brand_id=Brand.id')
				),
			array(
				'table'=>'customer_groups',
				'alias'=>'CustomerGroup',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),
// array(
// 	'table'=>'branches',
// 	'alias'=>'Branch',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.branch_id=Branch.id')
// ),

			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Product.*',
			'ProductType.*',
			'Sale.*',
			'SaleItem.*',
			'Brand.*',
			'Route.name',
//'Branch.*',
//'Customer.circle_id',
//}
//'CustomerCircle.name',
			)
		));
//}
//pr($Customer);
//pr($conditions);
//exit;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Customer as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.product_id',
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}

		$single['name']=$value['AccountHead']['name'];
		$single['route']=$value['Route']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
public function SaleCollectionReportProductwise()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Reports/SaleItemWise'));
	if(!in_array($menu_id, $PermissionList))
	{
		$this->Session->setFlash("Permission denied");
		return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	}
	$this->set('from',date("d-m-Y", strtotime('-1 day')));
	$this->set('to',date("d-m-Y"));
	$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'))));
	//$this->set('Product',$this->Product->find('list',array('fields'=>array('id','name'))));
	$this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
   // $Product_list=$this->Product->find('list',array('fields'=>['id','product_name']));
	$this->set('Product',$this->Product->find('list',array('fields'=>array('id','product_name'))));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

// $ProductType=$this->ProductType->find('list');
// 	$this->set(compact('ProductType'));
// 	$Product=$this->Product->find('list');
// 	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
	$Branch=$this->Branch->find('list');
	$this->set(compact('Branch'));

}
public function SaleCollectionReportProductwiseAjax()
{
	$requestData=$this->request->data;
//zpr($requestData);
//exit;
	$columns = [];
	$columns[]='Product.name';
	$columns[]='Executive.name';
	$columns[]='Route.name';
	$columns[]='Brand.name';
	$columns[]='SaleItem.unit_price';

	$columns[]='SaleItem.tax_amount';
	$columns[]='SaleItem.total';
	$conditions=[];
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	if(!empty($requestData['product_id']))
	{
		if($requestData['product_id'])
			$conditions['Product.id']=$requestData['product_id'];	
	}
	if(!empty($requestData['product_type_id']))
	{
		if($requestData['product_type_id'])
			$conditions['ProductType.id']=$requestData['product_type_id'];	
	}
	if(!empty($requestData['branch_id']))
	{
		if($requestData['branch_id'])
			$conditions['Sale.branch_id']=$requestData['branch_id'];
	}
	if(!empty($requestData['route_id']))
	{
		if($requestData['route_id'])
			$conditions['Customer.route_id']=$requestData['route_id'];
	}
// if($data['customer_id'])
// 	$conditions['Sale.account_head_id']=$data['customer_id'];
// if($data['customer_group_id'])
//     $conditions['Customer.customer_group_id']=$data['customer_group_id'];
	if(!empty($requestData['executive_id']))
	{
		if($requestData['executive_id'])
			$conditions['Sale.executive_id']=$requestData['executive_id'];
	}
	if(!empty($requestData['brand_id']))
	{
		if($requestData['brand_id'])
			$conditions['Product.brand_id']=$requestData['brand_id'];
	}
	$conditions['Sale.flag']=1;
	$conditions['Sale.status']=2;
	$conditions['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
	$this->Product->unbindModel(array('belongsTo' => array('Brand')));
	$totalData=$this->Product->find('count',array(
		"joins"=>array(
			array(
				"table"=>'sale_items',
				"alias"=>'SaleItem',
				"type"=>'INNER',
				"conditions"=>array('SaleItem.product_id=Product.id'),
				),
			array(
				"table"=>'sales',
				"alias"=>'Sale',
				"type"=>'INNER',
				"conditions"=>array('Sale.id=SaleItem.sale_id'),
				),
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'LEFT',
				"conditions"=>array('Sale.executive_id=Executive.id'),
				),
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			array(
				'table'=>'brands',
				'alias'=>'Brand',
				'type'=>'INNER',
				'conditions'=>array('Product.brand_id=Brand.id')
				),
			),
		'conditions'=>$conditions,
		));

	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Product.name LIKE' =>'%'. $q . '%',
			'Route.name LIKE' =>'%'. $q . '%',
			'Executive.name LIKE' =>'%'. $q . '%',
			'Sale.date_of_delivered LIKE' =>'%'. $q . '%',
			'SaleItem.unit_price LIKE' =>'%'. $q . '%',
			'SaleItem.tax_amount LIKE' =>'%'. $q . '%',
			'SaleItem.total LIKE' =>'%'. $q . '%',
			);
		$this->Product->unbindModel(array('belongsTo' => array('Brand')));
		$totalFiltered=$this->Product->find('count',array(
			"joins"=>array(
				array(
					"table"=>'sale_items',
					"alias"=>'SaleItem',
					"type"=>'INNER',
					"conditions"=>array('SaleItem.product_id=Product.id'),
					),
				array(
					"table"=>'sales',
					"alias"=>'Sale',
					"type"=>'INNER',
					"conditions"=>array('Sale.id=SaleItem.sale_id'),
					),
				array(
					"table"=>'executives',
					"alias"=>'Executive',
					"type"=>'LEFT',
					"conditions"=>array('Sale.executive_id=Executive.id'),
					),
				array(
					'table'=>'customers',
					'alias'=>'Customer',
					'type'=>'INNER',
					'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
					),
				array(
					'table'=>'routes',
					'alias'=>'Route',
					'type'=>'INNER',
					'conditions'=>array('Customer.route_id=Route.id')
					),
				array(
					'table'=>'brands',
					'alias'=>'Brand',
					'type'=>'INNER',
					'conditions'=>array('Product.brand_id=Brand.id')
					),
				),
			'conditions'=>$conditions,
			));
	}
	$this->Product->unbindModel(array('belongsTo' => array('Brand')));
	$Data=$this->Product->find('all',array(
		"joins"=>array(
			array(
				"table"=>'sale_items',
				"alias"=>'SaleItem',
				"type"=>'INNER',
				"conditions"=>array('SaleItem.product_id=Product.id'),
				),
			array(
				"table"=>'sales',
				"alias"=>'Sale',
				"type"=>'INNER',
				"conditions"=>array('Sale.id=SaleItem.sale_id'),
				),
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'LEFT',
				"conditions"=>array('Sale.executive_id=Executive.id'),
				),
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			array(
				'table'=>'brands',
				'alias'=>'Brand',
				'type'=>'INNER',
				'conditions'=>array('Product.brand_id=Brand.id')
				),
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=>'Sale.date_of_delivered desc',
		//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'Product.name',
			'Sale.id',
			'Sale.account_head_id',
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			'SaleItem.unit_price',
			'SaleItem.quantity',
			'SaleItem.total',
			'SaleItem.tax_amount',
			'Executive.*',
			'Route.*',
			'Brand.*',
			)
		));
//pr($Data);
	foreach ($Data as $key => $value) {
        $Data[$key]['Customer']['name']=$this->AccountHead->field('AccountHead.name',array('AccountHead.id'=>$value['Sale']['account_head_id']));
		$Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
	}

	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        => $Data
		);
	echo json_encode($json_data);
	exit;

}

public function SaleCollectionReportCustomerWise()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
	$Branch=$this->Branch->find('list');
	$this->set(compact('Branch'));
	$CustomerGroup=$this->CustomerGroup->find('list');
	$this->set(compact('CustomerGroup'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}
public function SaleCollectionReportCustomerWiseAjax()
{
	$requestData=$this->request->data;
	$columns = [];
//$columns[]='Product.name';
	$columns[]='Executive.name';
	$columns[]='AccountHead.name';
	$columns[]='Route.name';
	$columns[]='SaleItem.unit_price';

	$columns[]='SaleItem.tax_amount';
	$columns[]='SaleItem.total';
	$conditions=[];
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
//if($data['branch_id'])
// $conditions['Sale.branch_id']=$data['branch_id'];
	if($requestData['route_id'])
		$conditions['Customer.route_id']=$requestData['route_id'];
	if($requestData['customer_id'])
		$conditions['Sale.account_head_id']=$requestData['customer_id'];
	if($requestData['customer_group_id'])
		$conditions['Customer.customer_group_id']=$requestData['customer_group_id'];
	if($requestData['executive_id'])
		$conditions['Sale.executive_id']=$requestData['executive_id'];
// if($requestData['product_type_id'])
// 	$conditions['Product.product_type_id']=$requestData['product_type_id'];

// if($requestData['brand_id'])
// 	$conditions['Product.brand_id']=$requestData['brand_id'];
// if($requestData['product_id'])
// 	$conditions['SaleItem.product_id']=$requestData['product_id'];
	$conditions['Sale.flag']=1;
	$conditions['Sale.status']=2;
	$conditions['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
	$totalData=$this->Sale->find('count',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
// array(
// 	'table'=>'sale_items',
// 	'alias'=>'SaleItem',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.id=SaleItem.sale_id')
// ),
// array(
// 	'table'=>'products',
// 	'alias'=>'Product',
// 	'type'=>'INNER',
// 	'conditions'=>array('SaleItem.product_id=Product.id')
// ),
// array(
// 	'table'=>'product_types',
// 	'alias'=>'ProductType',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.product_type_id=ProductType.id')
// ),
// array(
// 	'table'=>'brands',
// 	'alias'=>'Brand',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.brand_id=Brand.id')
// ),
			array(
				'table'=>'customer_groups',
				'alias'=>'CustomerGroup',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),
// array(
// 	'table'=>'branches',
// 	'alias'=>'Branch',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.branch_id=Branch.id')
// ),

			),
		'conditions'=>$conditions,
		'group' => array('Sale.account_head_id'),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Product.*',
//'ProductType.*',
			'Sale.*',
//'SaleItem.*',
//'Brand.*',
			'Route.name',
//'Branch.*',
//'Customer.circle_id',
			)
		));
//pr($totalData);
//exit;
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Executive.name LIKE' =>'%'. $q . '%',
			'AccountHead.name LIKE' =>'%'. $q . '%',
			'Route.name LIKE' =>'%'. $q . '%',
//'SaleItem.unit_price LIKE' =>'%'. $q . '%',
//'SaleItem.tax_amount LIKE' =>'%'. $q . '%',
//'SaleItem.total LIKE' =>'%'. $q . '%',
			);
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$totalFiltered=$this->Sale->find('count',array(
			'joins'=>array(
				array(
					'table'=>'customers',
					'alias'=>'Customer',
					'type'=>'INNER',
					'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
					),
				array(
					'table'=>'routes',
					'alias'=>'Route',
					'type'=>'INNER',
					'conditions'=>array('Customer.route_id=Route.id')
					),
// array(
// 	'table'=>'sale_items',
// 	'alias'=>'SaleItem',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.id=SaleItem.sale_id')
// ),
// array(
// 	'table'=>'products',
// 	'alias'=>'Product',
// 	'type'=>'INNER',
// 	'conditions'=>array('SaleItem.product_id=Product.id')
// ),
// array(
// 	'table'=>'product_types',
// 	'alias'=>'ProductType',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.product_type_id=ProductType.id')
// ),
// array(
// 	'table'=>'brands',
// 	'alias'=>'Brand',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.brand_id=Brand.id')
// ),
				array(
					'table'=>'customer_groups',
					'alias'=>'CustomerGroup',
					'type'=>'INNER',
					'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
					),
				),
			'conditions'=>$conditions,
			'group' => array('Sale.account_head_id'),
			));
	}
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
	$Data=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
// array(
// 	'table'=>'sale_items',
// 	'alias'=>'SaleItem',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.id=SaleItem.sale_id')
// ),
// array(
// 	'table'=>'products',
// 	'alias'=>'Product',
// 	'type'=>'INNER',
// 	'conditions'=>array('SaleItem.product_id=Product.id')
// ),
// array(
// 	'table'=>'product_types',
// 	'alias'=>'ProductType',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.product_type_id=ProductType.id')
// ),
// array(
// 	'table'=>'brands',
// 	'alias'=>'Brand',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.brand_id=Brand.id')
// ),
			array(
				'table'=>'customer_groups',
				'alias'=>'CustomerGroup',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),

			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'group' => array('Sale.account_head_id'),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Product.*',
//'ProductType.*',
			'Sale.*',
//'SaleItem.*',
//'Brand.*',
			'Route.name',
			)
		));
//pr($Data);
//exit;
	$from=date('Y-m-d',strtotime($requestData['from_date']));
	$to=date('Y-m-d',strtotime($requestData['to_date']));
	foreach ($Data as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
//'SaleItem.product_id',
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
//pr($SaleItem);
//exit;
		$Data[$key]['AccountHead']['name']=$value['AccountHead']['name'];
		if($value['Executive']['name']){$Data[$key]['Executive']['name']=$value['Executive']['name'];}else{$Data[$key]['Executive']['name']="No Executive";}
		$Data[$key]['Route']['name']=$value['Route']['name'];
		$Data[$key]['SaleItem']['unit_price']=$SaleItem['SaleItem']['grand_net_value'];
		$Data[$key]['SaleItem']['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount'];
		$Data[$key]['SaleItem']['total']=$SaleItem['SaleItem']['grand_total'];
		$Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
//pr($Data);
//exit;
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        => $Data
		);
	echo json_encode($json_data);
	exit;

}
public function DailyPerformanceAnalysis()
{
}
public function DailyPerformanceAnalysisExport($day)
{
	$date  =date('Y-m-d h:i:sa');
	$this->response->download("daily_performance_analysis_export.csv");
//$date  =date('Y-m-d h:i:sa');
//$this->response->download("brand_export.csv");
	$Executive=$this->ExecutiveRouteMapping->find('all',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'inner',
				"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
				),
			),
		'fields'=>array('Route.name','Executive.name','Executive.id')));

	$conditions=array();
	$conditions['Executive.block !=']=1;
	$conditions_sale=array();
	$grand_total=0;
	$quantity_total=0;
	$total=[];
	$opening_total=[];
	$Array=[];
	$last_total=[];
	$Executive_count=count($Executive);

	$Route=$this->Route->find('list',array(
		"joins"=>array(
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'inner',
				"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
				),
			),
//'order' => array('Route.id ASC'),
		'fields'=>array('Route.id','Route.name')));
	$date_array=[];
	$date1 = date('Y-m-d',strtotime('first day of this month'));
	$date2=date('Y-m-d');
	$diff = strtotime($date2) - strtotime($date1); 
	$dateDiff=abs(round($diff / 86400))+1;
	$last_date1= date('Y-m-d', strtotime('first day of last month'));
	$last_date2=date('Y-m-d', strtotime('last day of last month'));
	for($i=1;$i<=$dateDiff;$i++)
	{
		$to_date = date('Y-m-'.$i);
		array_push($date_array,$to_date);
	}    $last_saleamount_total=0;$last_collection_total=0;
	$conditions=array();

	$executive1=$this->ExecutiveRouteMapping->find('all',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'inner',
				"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
				),
			),
		'fields'=>array('ExecutiveRouteMapping.route_id')));
	foreach ($executive1 as $key => $value1) {
		$conditions['Customer.route_id']=$value1['ExecutiveRouteMapping']['route_id'];;
		$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
		$balance=0;$opening_balance=0;$present_balance=0;
		if($Customer)
		{
			foreach ($Customer as $key => $value) {
				$opening_Balance_Total=$this->daily_get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance'],$last_date2);

				$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);

				$Balance_Total = number_format($Balance_Total,2);
				$balance+=$Balance_Total;
				$opening_Balance_Total = number_format($opening_Balance_Total,2);
				$opening_balance+=$opening_Balance_Total;


			}
		}
		$array_opening_total['opening_balance']=$opening_balance;
		$array_opening_total['present_opening_balance']=$balance;
		$opening_total[]=$array_opening_total;
//pr($balance);
//pr($opening_balance);
	}
	foreach($Route as $key => $value)
	{

		$column_total_sale=0;$column_total_collection=0;
		$last_saleamount = $this->sale_amount($key,$last_date1,$last_date2);
		$data_last_saleamount = 0;
		if(!empty($last_saleamount))
		{
			$data_last_saleamount = $last_saleamount;
		}
		$last_saleamount_total+=$data_last_saleamount;
		$last_collection= $this->collection_amount($key,$last_date1,$last_date2);
		$last_collection_total+=$last_collection;

		foreach ($date_array as $key1 => $value1) {
			$saleamount = $this->sale_amount($key,$value1,$value1);
//pr($saleamount);
			$collection_total_amount= $this->collection_amount($key,$value1,$value1);
			$data['sale_amount'] =$saleamount;
			$data['collection_total_amount'] = 0;
			if(!empty($collection_total_amount))
			{
				$data['collection_total_amount'] = $collection_total_amount;
			}
			$single['sale_amount']=$data['sale_amount'];
			$single['collection_amount']=$data['collection_total_amount'];
			$single['date']=$value1;
			$column_total_sale+=$data['sale_amount'];
			$column_total_collection+=$data['collection_total_amount'];
			$Array[]=$single;
		} 
		$data['target'] = $this->sale_target($key,$date1);
		$data['collection_target'] = $this->collection_target($key,$date1);
		$array_total['total']['sale_amount']=$column_total_sale;
		$array_total['total']['collection_amount']=$column_total_collection;
		$array_total['total']['target']=$data['target'];

		if($data['collection_target'])
		{
			$array_total['total']['collection_target']= $data['collection_target'];

		}
		else
		{
			$array_total['total']['collection_target']=0;

		}
		if($column_total_collection==0 || $data['collection_target']==0)
		{
			$array_total['total']['collection_percentage']=0;
		}
		else
		{
			$array_total['total']['collection_percentage']=round((($column_total_collection*100)/$data['collection_target']),3);
		}
		if($column_total_sale==0 || $data['target']==0)
		{
			$array_total['total']['sale_percentage']=0;
		}
		else
		{
			$array_total['total']['sale_percentage']= round((($column_total_sale*100)/$data['target']),3);

		}

		$total[]=$array_total;
// exit;
	}
	$new_array=[];
	foreach($Array as $key3 =>$value3){
//$new_array[$value3['date']][]=$value3['sale_amount'];
		$new_array[$value3['date']]['collection_amount'][]=floatval($value3['collection_amount']);
		$new_array[$value3['date']]['sale_amount'][]=floatval($value3['sale_amount']);

	}
//pr($total);
	$last_collection_total=$last_collection_total;
	$last_saleamount_total=$last_saleamount_total;
	$Executive_count=count($Executive);
	$this->set('dva_day',$day);
	$this->set('last_collection_total',$last_collection_total);
	$this->set('last_saleamount_total',$last_saleamount_total);
	$this->set('column_total',$total);
	$this->set('opening_total',$opening_total);
	$this->set('last_total',$last_total);
	$this->set('amount',$new_array);
	$this->set('Route',$Executive);
	$this->set('Executive_count',$Executive_count);
	$this->layout = 'ajax';
	return false;
} 

public function daily_get_oustanding_amount($account_head_id,$opening_balance,$last_date){
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$account_head_id,
					'Journal.credit'=>$account_head_id,
					),
				'AND' => array(
					'Journal.flag=1',
					'(Journal.date) <='=>$last_date,
					)
				)
			),
		));
	$Journal_credit=$this->Journal->find('all',array(
		'conditions'=>array(
			'Journal.credit'=>$account_head_id,
			'Journal.flag=1',
			'(Journal.date) <='=>$last_date,
			)
		));
	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>[
	'Capital'=>'credit_account',
	'Drawing'=>'debit_account',
	],
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$AccountingsController = new AccountingsController;
	$type=$AccountingsController->get_type_by_account_dead($account_head_id);
	$Journal_all=[];
	foreach ($Journal_credit as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['mode']=$value['AccountHeadDebit']['name'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['credit']=$value['Journal']['amount'];
		$Journal_single['debit']=0.00;
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$Journal_debit=$this->Journal->find('all',array(
		'conditions'=>array(
			'Journal.debit'=>$account_head_id,
			'Journal.flag=1',
			'(Journal.date) <='=>$last_date,
			)
		));
	foreach ($Journal_debit as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['mode']=$value['AccountHeadCredit']['name'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['credit']=0.00;
		$Journal_single['debit']=$value['Journal']['amount'];
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	krsort($Journal_all);
	$return['row']['tbody']='';
	$return['row']['tfoot']='';
	$category_name='';
	$Balance_Total=0;
	$credit_Total=0;
	$debit_Total=0;
	if(!empty($type)){
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		}
	}
	if($category_name=='credit_account')
	{
		$credit=$opening_balance;
		$debit='0';
	}
	else
	{
		$debit=$opening_balance;
		$credit='0';
	}
	$Journal_all[0]=array(
		'id'=>0,
		'mode'=>'Opening Balance',
		'remarks'=>'',
		'credit'=>$credit,
		'debit'=>$debit,
		);
	foreach ($Journal_all as $key => $value) 
	{
		$credit_Total+=$value['credit'];
		$debit_Total+=$value['debit'];
	}
	$Balance_Total=$debit_Total-$credit_Total;	
	return $Balance_Total;
}
function sale_amount($id,$first_date,$last_date){
	$conditions=array();
	$total_sale=0;
	$conditions['Customer.route_id']=$id;
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
	if($Customer)
	{
		foreach ($Customer as $key => $value) {

			$SaleAmountList=$this->Sale->find('first',array(
				'conditions'=>array('(Sale.date_of_delivered) between ? and ? '=>array($first_date,$last_date),'Sale.account_head_id'=>$value['Customer']['account_head_id']),
				'fields'=>'sum(grand_total) as amount',
				'group'=> 'Sale.account_head_id',

				));
			$total = 0;
			if(!empty($SaleAmountList))
			{
				$total = $SaleAmountList[0]['amount'];
			}
			$total_sale+=$total;

		}
	}
	return $total_sale;
}
function collection_amount($id,$first_date,$last_date){

	$conditions=array();
	$collection_total_amount=0;
	$conditions['Customer.route_id']=$id;
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
	if($Customer)
	{
		foreach ($Customer as $key => $value) 
		{
			$get_collection_amount_return_value=$this->get_collection_amount($value['Customer']['account_head_id'],$first_date,$last_date,2);
			$collection_total_amount+=$get_collection_amount_return_value['sale_amount'];
		}
	}
	return $collection_total_amount;
}
function sale_target($id,$first_date){
	$executive1=$this->ExecutiveRouteMapping->find('first',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'inner',
				"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.route_id'=>$id),
		'fields'=>array('Executive.id')));
	$SaleTargetList=$this->SaleTarget->find('first',array(
		'conditions'=>array('(SaleTarget.wef_date) <='=>$first_date,'executive_id'=>$executive1['Executive']['id']),
		'fields'=>'max(SaleTarget.wef_date) as date',

		));
	$target = $this->SaleTarget->field(

		'SaleTarget.target',

		array('SaleTarget.wef_date' => $SaleTargetList[0]['date'],'executive_id'=>$executive1['Executive']['id']));
	return $target;
}
function collection_target($id,$first_date){
	$executive1=$this->ExecutiveRouteMapping->find('first',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'inner',
				"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.route_id'=>$id),
		'fields'=>array('Executive.id')));
	$CollectionTargetList=$this->SaleTarget->find('first',array(
		'conditions'=>array('(SaleTarget.wef_date) <='=>$first_date,'executive_id'=>$executive1['Executive']['id']),
		'fields'=>'max(SaleTarget.wef_date) as date',

		));
	$collection_target = $this->SaleTarget->field(

		'SaleTarget.collection_target',

		array('SaleTarget.wef_date' => $CollectionTargetList[0]['date'],'executive_id'=>$executive1['Executive']['id']));
	return $collection_target;
}
public function ExecutiveBonusReport()
{
	$executives=$this->Executive->find('list',array('conditions' => 'block!=1','fields'=>array('id','name')));
	$this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

}
public function executive_bonus_report_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='Executive.name';
	$columns[]='ExecutiveBonusDetails.invoice_no';
	$columns[]='ExecutiveBonusDetails.credit_note_no';
	$columns[]='AccountHead.name';
	$columns[]='ExecutiveBonusDetails.sale_date';
	$columns[]='ExecutiveBonusDetails.sales_return_date';
	$columns[]='ExecutiveBonusDetails.receipt_date';
	$columns[]='ExecutiveBonusDetails.eligibility';
	$columns[]='Sale.grand_total';
	$columns[]='SalesReturn.grand_total';
	//$columns[]='ExecutiveBonusDetails.amount';
	$columns[]='ExecutiveBonusDetails.bonus_amount';
	$columns[]='ExecutiveBonusDetails.bonus_return_amount';
	$columns[]='ExecutiveBonusDetails.id';
	$conditions=[];
	$conditions['ExecutiveBonusDetails.flag']=1;
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['ExecutiveBonusDetails.receipt_date between ? and ?']=[$from_date,$to_date];
	$executive_id=$requestData['executive_id'];
	if(!empty($executive_id)) { $conditions['ExecutiveBonusDetails.executive_id']=$executive_id; }
	$totalData=$this->ExecutiveBonusDetails->find('count',[
			'joins'=>array(
				array(
					'table'=>'sales',
					'alias'=>'Sale',
					'type'=>'INNER',
					'conditions'=>array('Sale.invoice_no=ExecutiveBonusDetails.invoice_no')
					),
				// array(
				// 	'table'=>'sales_returns',
				// 	'alias'=>'SalesReturn',
				// 	'type'=>'RIGHT',
				// 	'conditions'=>array('SalesReturn.invoice_no=ExecutiveBonusDetails.credit_note_no')
				// 	),
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Sale.account_head_id')
					),
				),
			'conditions'=>$conditions,
			]);

// pr($totalData);
// exit;
	$totalFiltered=$totalData;
	if(!empty($requestData['search']['value'])){ 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Executive.name LIKE' =>'%'. $q . '%',
			'AccountHead.name LIKE' =>'%'. $q . '%',
			'ExecutiveBonusDetails.sale_date LIKE'    =>'%'. date('Y-m-d',strtotime($q)) . '%',
			'ExecutiveBonusDetails.receipt_date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
			'ExecutiveBonusDetails.invoice_no LIKE'   =>'%'. $q . '%',
			'ExecutiveBonusDetails.amount LIKE'       =>'%'. $q . '%',
			'Sale.grand_total LIKE'                   =>'%'. $q . '%',
			//'SalesReturn.grand_total LIKE'            =>'%'. $q . '%',
			'ExecutiveBonusDetails.eligibility LIKE'  =>'%'. $q . '%',
			'ExecutiveBonusDetails.bonus_amount LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->ExecutiveBonusDetails->find('count',[
			'joins'=>array(
				array(
					'table'=>'sales',
					'alias'=>'Sale',
					'type'=>'INNER',
					'conditions'=>array('Sale.invoice_no=ExecutiveBonusDetails.invoice_no')
					),
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Sale.account_head_id')
					),
				),
			'conditions'=>$conditions,
			]);

	}
	$Data=$this->ExecutiveBonusDetails->find('all',array(
		'conditions'=>$conditions,
		'joins'=>array(
			array(
				'table'=>'sales',
				'alias'=>'Sale',
				'type'=>'LEFT',
				'conditions'=>array('Sale.invoice_no=ExecutiveBonusDetails.invoice_no')
				),
			 array(
			 		'table'=>'sales_returns',
			 		'alias'=>'SalesReturn',
			 		'type'=>'LEFT',
			 		'conditions'=>array('SalesReturn.invoice_no=ExecutiveBonusDetails.credit_note_no')
			 		),
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Sale.account_head_id')
				),
			),
//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'Executive.name',
			'AccountHead.name',
			'Sale.grand_total',
			'SalesReturn.grand_total',
			'ExecutiveBonusDetails.*',
			),
		));
	$conditions['ExecutiveBonusDetails.flag']=1;
	if($Data){
	foreach ($Data as $key => $value) {
		$Data[$key]['ExecutiveBonusDetails']['sale_date']=date('d-m-Y',strtotime($value['ExecutiveBonusDetails']['sale_date']));
		if($value['ExecutiveBonusDetails']['sales_return_date']!=null)
			$Data[$key]['ExecutiveBonusDetails']['sales_return_date']=date('d-m-Y',strtotime($value['ExecutiveBonusDetails']['sales_return_date']));
		else
			$Data[$key]['ExecutiveBonusDetails']['sales_return_date']='';
		$Data[$key]['ExecutiveBonusDetails']['receipt_date']=date('d-m-Y',strtotime($value['ExecutiveBonusDetails']['receipt_date']));
		// $sale_type=$this->Sale->field('Sale.sale_type1',array('Sale.invoice_no'=>$value['ExecutiveBonusDetails']['invoice_no']));
		// if($sale_type=='CreditSale')
		// 	$Data[$key]['ExecutiveBonusDetails']['amount']=0.00;
		// else
		// 	$Data[$key]['ExecutiveBonusDetails']['amount']=round($value['ExecutiveBonusDetails']['amount'],2);
		$Data[$key]['ExecutiveBonusDetails']['bonus_amount']=round($value['ExecutiveBonusDetails']['bonus_amount'],2);
		$Data[$key]['ExecutiveBonusDetails']['bonus_return_amount']=round($value['ExecutiveBonusDetails']['bonus_return_amount'],2);
		if(($value['ExecutiveBonusDetails']['sales_return_date']=='')){
			$Data[$key]['Sale']['grand_total']=number_format(($value['Sale']['grand_total']),2,'.','');
		}
		else{
			$Data[$key]['Sale']['grand_total']=0;
		}
		if(($value['ExecutiveBonusDetails']['sales_return_date']!='')){
		$Data[$key]['SalesReturn']['grand_total']=number_format(($value['SalesReturn']['grand_total']),2,'.','');
	}
	else{

		$Data[$key]['SalesReturn']['grand_total']=0;
	}

		$Data[$key]['ExecutiveBonusDetails']['action']='<span hidden class="ExecutiveBonusDetails_id">'.$value['ExecutiveBonusDetails']['id'].'</span><span><a><i class="fa fa-trash fa ExecutiveBonusDetails_delete"></i></a></span>';
	//}
	}
}
//pr($Data);
 
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function TailorBonusReport()
{
	$tailor_role_id=$this->Role->field('Role.id',array('Role.name'=>'TAILOR'));
	$tailors=$this->Staff->find('list',array('conditions'=>array('Staff.role_id'=>$tailor_role_id),'fields'=>array('Staff.id','Staff.name')));
	$this->set(compact('tailors'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

}
public function tailor_bonus_report_ajax()
{
	$requestData=$this->request->data;

	$columns = [];
	$columns[]='Staff.name';
	$columns[]='Production.production_no';
	$columns[]='ExecutiveBonusDetails.receipt_date';
	$columns[]='Product.name';
	$columns[]='Production.production_cost';
	$columns[]='Production.quantity';
	$columns[]='ExecutiveBonusDetails.bonus_amount';
	$conditions=[];
	$conditions['ExecutiveBonusDetails.flag']=1;
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['ExecutiveBonusDetails.receipt_date between ? and ?']=[$from_date,$to_date];
	$tailor_id=$requestData['tailor_id'];
	if(!empty($tailor_id)) { $conditions['ExecutiveBonusDetails.staff_id']=$tailor_id; }
	 
	$totalData=$this->ExecutiveBonusDetails->find('count',[
			'joins'=>[
			 [
				'table'=>'staffs',
				'alias'=>'Staff',
				'type'=>'LEFT',
				'conditions'=>array('Staff.id=ExecutiveBonusDetails.staff_id')
				],
				[
				'table'=>'productions',
				'alias'=>'Production',
				'type'=>'inner',
				'conditions'=>array('Production.production_no=ExecutiveBonusDetails.production_no')
				],
				[
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'inner',
				'conditions'=>array('Production.product_id=Product.id')
				],
			],
			'conditions'=>$conditions,
			]);

	$totalFiltered=$totalData;
	if(!empty($requestData['search']['value'])){ 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Staff.name LIKE' =>'%'. $q . '%',
			'ExecutiveBonusDetails.receipt_date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
			'Product.name LIKE'   =>'%'. $q . '%',
			'Production.production_no LIKE'   =>'%'. $q . '%',
			'Production.production_cost LIKE'   =>'%'. $q . '%',
			'Production.quantity LIKE'   =>'%'. $q . '%',
			'ExecutiveBonusDetails.bonus_amount LIKE' =>'%'. $q . '%',
			);
		$totalData=$this->ExecutiveBonusDetails->find('count',[
			'joins'=>[
			 [
				'table'=>'staffs',
				'alias'=>'Staff',
				'type'=>'LEFT',
				'conditions'=>array('Staff.id=ExecutiveBonusDetails.staff_id')
				],
				[
				'table'=>'productions',
				'alias'=>'Production',
				'type'=>'inner',
				'conditions'=>array('Production.production_no=ExecutiveBonusDetails.production_no')
				],
				[
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'inner',
				'conditions'=>array('Production.product_id=Product.id')
				],
			],
			'conditions'=>$conditions,
			]);

	}

	$Data=$this->ExecutiveBonusDetails->find('all',array(
		'conditions'=>$conditions,
		'joins'=>array(
			array(
				'table'=>'staffs',
				'alias'=>'Staff',
				'type'=>'LEFT',
				'conditions'=>array('Staff.id=ExecutiveBonusDetails.staff_id')
				),
			array(
				'table'=>'productions',
				'alias'=>'Production',
				'type'=>'inner',
				'conditions'=>array('Production.production_no=ExecutiveBonusDetails.production_no')
				),
			array(
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'inner',
				'conditions'=>array('Production.product_id=Product.id')
				),
			),
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'Staff.name',
			'Production.production_no',
			'Production.quantity',
			'Product.name',
			'Production.production_cost',
			'ExecutiveBonusDetails.receipt_date',
			'ExecutiveBonusDetails.bonus_amount',
			'ExecutiveBonusDetails.amount',
			'ExecutiveBonusDetails.production_no',
			),
		));
	//pr($Data);exit;
	$conditions['ExecutiveBonusDetails.flag']=1;
	foreach ($Data as $key => $value) {
		$Data[$key]['ExecutiveBonusDetails']['receipt_date']=date('d-m-Y',strtotime($value['ExecutiveBonusDetails']['receipt_date']));
		$Data[$key]['ExecutiveBonusDetails']['bonus_amount']=number_format($value['ExecutiveBonusDetails']['bonus_amount'],3,'.','');
		$Data[$key]['ExecutiveBonusDetails']['amount']=number_format($value['ExecutiveBonusDetails']['amount'],3,'.','');
	}
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function customer_due_list_report_ajax()
{
	$requestData=$this->request->data;
	$conditions=[];
	if($requestData['customer_id'])
	{
		$conditions['Customer.account_head_id']=$requestData['customer_id'];
	}
	$columns = [];
	$columns[]='CustomerType.name';
	$columns[]='AccountHead.name';
	$columns[]='AccountHead.opening_balance';
	$totalData=$this->Customer->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if(!empty($requestData['search']['value'])){ 
		// $q=$requestData['search']['value'];
		// $conditions['OR']=array(
		// 	'CustomerType.name LIKE' =>'%'. $q . '%',
		// 	'AccountHead.name LIKE' =>'%'. $q . '%',
		// 	'AccountHead.opening_balance LIKE'   =>'%'. $q . '%',
		// 	);
		$totalFiltered=$this->Customer->find('count',[
			'conditions'=>$conditions,
			'fields'=>array(
				'Customer.account_head_id',
				'AccountHead.opening_balance',
				'AccountHead.name',
				'CustomerType.name',
				),
			]);
	}
	$Data=$this->Customer->find('all',array(
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
				'Customer.account_head_id',
				'AccountHead.opening_balance',
				'AccountHead.name',
				'CustomerType.name',
				),
			));
 //pr($Data);exit;
	$conditions['ExecutiveBonusDetails.flag']=1;
	$Balance_Total=0;
	if(!empty($Data))
	{
		foreach ($Data as $key => $value) {
			$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);
			$Data[$key]['Customer']['Balance_Total']=round($Balance_Total,3);
		}
	}
	
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function AllSaleReport()
{
	$executives=$this->Executive->find('list',array('conditions' => 'block!=1'));
//$executives[0]='No Executive';
	$routes=$this->Route->find('list');
	$this->set(compact('executives','routes'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}
public function GetCustomerForSaleCollectionReportAll()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id'])
		$conditions['executive_id']=$data['executive_id'];
		$conditions['route_id']=$data['route_id'];
	$Customer=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
			),
		),
		'conditions'=>$conditions,
		'fields'=>array('AccountHead.id','AccountHead.name')
	));
	echo json_encode($Customer); exit;
}
public function SaleCollectionReportAjaxAll()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id']){
		$conditions['Sale.executive_id']=$data['executive_id'];
	}
	if($data['route_id']){
		$conditions['Customer.route_id']=$data['route_id'];
	}
	if($data['customer_id']){
		$conditions['Customer.account_head_id']=$data['customer_id'];
	}
	$conditions['Sale.status']=2;
	$conditions['Sale.flag']  =1;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to  =date('Y-m-d',strtotime($data['to_date']));
	$conditions['Sale.date_of_delivered  between ? and ?']=array($from,$to);
	$Sale_Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
			),
		),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Customer.route_id',
		)
	));
	$list=[];
	foreach ($Sale_Customer as $key => $value) {
		$this->SaleItem->virtualFields = array(
			//'grand_total' => "SUM(SaleItem.total)",
			'grand_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
			'grand_tax_amount' => "SUM(SaleItem.tax_amount)",
			//'grand_net_value' => "SUM(SaleItem.net_value)",
 'grand_total' => "SUM(((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount))",
		);
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				'Sale.status'=>2,'Sale.flag'=>1,
			),
			'fields'=>array(
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total',
			),
		));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array(
			'grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)"
		);
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				'Sale.status'=>2,'Sale.flag'=>1,
			),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
			),
		));
		$single['executive']=$value['Executive']['name'];
		$single['name']=$value['AccountHead']['name'];
		$single['route']="";
		if($value['Customer']['route_id']){
			$route_id=$value['Customer']['route_id'];
			$Route=$this->Route->findById($route_id);
			$single['route']=$Route['Route']['name'];
		}
		if($Sale){
			$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
			$single['other_value']   =$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
			$single['net_value']     =$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
			$single['tax_amount']    =$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
			$single['taxable_amount']    =$SaleItem['SaleItem']['grand_net_value']-$Sale['Sale']['grand_discount_amount'];
			$single['total']         =$single['net_value']+$single['tax_amount'];
			$single['total']        -=$single['dicount_amount'];
			$single['total']        +=$single['other_value'];
			$single['grandtotal']    =round($single['total'],2);
			if($single['net_value']) $list[$value['AccountHead']['id']]=$single;
		}
	}
	/******customer details hidden******/
// 		if(empty($data['route_id'])){

// 	$Saleone=$this->Sale->find('first',array(
// 		'conditions'=>array('Sale.status'=>2,'Sale.flag'=>1,'Sale.invoice_no'=>"R196-1",'Sale.date_of_delivered between ? and ?'=>array($from,$to),),
// 		'fields'=>array(
// 			'AccountHead.id',
// 			'Sale.discount_amount',
// 			'AccountHead.name',
// 			'Executive.id',
// 			'Sale.id',
// 			'Executive.name',
// 		)
// 	));
// 		$this->SaleItem->virtualFields = array(
// 			'grand_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
// 			'grand_tax_amount' => "SUM(SaleItem.tax_amount)",
//  'grand_total' => "SUM(((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount))",
// 		);
// 	// 	if($Saleone){
// 	// 	$SaleItem1=$this->SaleItem->find('first',array(
// 	// 		'conditions'=>array(
// 	// 			'Sale.id'=>$Saleone['Sale']['id'],
// 	// 			'Sale.date_of_delivered between ? and ?'=>array($from,$to),
// 	// 			'Sale.status'=>2,'Sale.flag'=>1,
// 	// 		),
// 	// 		'fields'=>array(
// 	// 			'SaleItem.grand_net_value',
// 	// 			'SaleItem.grand_tax_amount',
// 	// 			'SaleItem.grand_total',
// 	// 		),
// 	// 	));
// 	// 	$single['executive']=$Saleone['Executive']['name'];
// 	// 	$single['name']="";
// 	// 	$single['route']="";
// 	// 	if($Saleone){
// 	// 		$single['dicount_amount']=$Saleone['Sale']['discount_amount']?floatval($Saleone['Sale']['discount_amount']):0;
// 	// 		$single['other_value']   =0;
// 	// 		$single['net_value']     =$SaleItem1['SaleItem']['grand_net_value']?floatval($SaleItem1['SaleItem']['grand_net_value']):0;
// 	// 		$single['tax_amount']    =$SaleItem1['SaleItem']['grand_tax_amount']?floatval($SaleItem1['SaleItem']['grand_tax_amount']):0;
// 	// 		$single['taxable_amount']    =$SaleItem['SaleItem']['grand_net_value']-$Sale['Sale']['grand_discount_amount'];
// 	// 		$single['total']         =$single['net_value']+$single['tax_amount'];
// 	// 		$single['total']        -=$single['dicount_amount'];
// 	// 		$single['total']        +=$single['other_value'];
// 	// 		$single['grandtotal']    =round($single['total'],2);
// 	// 		if($single['net_value']) $list[]=$single;
// 	// 	}
// 	// }
// }
	echo json_encode($list); exit;
}
public function RouteReport()
{
	$this->request->data['from_date']=date('1-m-Y');
	$this->request->data['to_date']=date('d-m-Y');	
}
public function RouteReportAjax()
{
$data=$this->request->data;
	$conditions=[];
	$conditions['Sale.status']=2;
	$conditions['Sale.flag']  =1;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to  =date('Y-m-d',strtotime($data['to_date']));
	$conditions['Sale.date_of_delivered  between ? and ?']=array($from,$to);
	$Route=$this->Route->find('all',array(
		// 'joins'=>array(
		// 	array(
		// 		'table'=>'customers',
		// 		'alias'=>'Customer',
		// 		'type'=>'INNER',
		// 		'conditions'=>array('Route.id=Customer.route_id')
		// 	),
		// ),
		'fields'=>array(
			'Route.id','Route.name',
		))
	);
	$list=[];
	foreach ($Route as $key => $value1) {
	$conditions['Customer.route_id']=$value1['Route']['id'];
    $Sale_Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
			),
		),
		'group'=>array('Sale.account_head_id'),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
		)
	));
	$discount_amount=0;$other_value=0;$net_value=0;$tax_amount=0;$taxable_amount=0;$total=0;$grand_total=0;
	foreach ($Sale_Customer as $key => $value) {
		$this->SaleItem->virtualFields = array(
			'grand_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
			'grand_tax_amount' => "SUM(SaleItem.tax_amount)",
            'grand_total' => "SUM(((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount))",
		);
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				'Sale.status'=>2,'Sale.flag'=>1,
			),
			'fields'=>array(
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total',
			),
		));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array(
			'grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)"
		);
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				'Sale.status'=>2,'Sale.flag'=>1,
			),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
			),
		));
		if($Sale){
			$discount_amount+=($Sale['Sale']['grand_discount_amount']);
			$other_value+=($Sale['Sale']['grand_other_value']);
			$net_value+=($SaleItem['SaleItem']['grand_net_value']);
			$tax_amount+=($SaleItem['SaleItem']['grand_tax_amount']);
			$taxable_amount+=($SaleItem['SaleItem']['grand_net_value']-$Sale['Sale']['grand_discount_amount']);
			$total+=($SaleItem['SaleItem']['grand_net_value']+$SaleItem['SaleItem']['grand_tax_amount']-$Sale['Sale']['grand_discount_amount']+$Sale['Sale']['grand_other_value']);
		}
	}
	     $single['dicount_amount']=$discount_amount;
			$single['other_value']   =$other_value;
			$single['route']   =$value1['Route']['name'];
			$single['net_value']     =round($net_value);
			$single['tax_amount']    =round($tax_amount);
			$single['taxable_amount']    =round($taxable_amount);
			$single['total']         =$total;
			$single['grandtotal']    =round($single['total']);
			if($single['net_value']) $list[$value1['Route']['id']]=$single;
	}
	echo json_encode($list); exit;
}
// public function chart()
// {
// 	$this->AccountHead->virtualFields = array('total' => "SUM(opening_balance)");
// 	$AccountHead=$this->AccountHead->find('first',array(
// 			'joins'=>array(
// 				array(
// 					'table'=>'groups',
// 					'alias'=>'Group',
// 					'type'=>'INNER',
// 					'conditions'=>array('Group.id=SubGroup.group_id')
// 					),
// 				array(
// 					'table'=>'master_groups',
// 					'alias'=>'MasterGroup',
// 					'type'=>'INNER',
// 					'conditions'=>array('MasterGroup.id=Group.master_group_id')
// 					),
// 				array(
// 					'table'=>'types',
// 					'alias'=>'Type',
// 					'type'=>'INNER',
// 					'conditions'=>array('Type.id=MasterGroup.type_id')
// 					),
// 				),
// 			'fields'=>array(
// 				'AccountHead.total',
// 				),
// 			'conditions'=>array('Type.id'=>5)
// 			));
// 	pr($AccountHead);exit;
		
// }
public function expense_vat_migration()
{
	try {
	 $Journal=$this->Journal->find('all',array(
			  'conditions'=>array(
			    'work_flow'=>'Expense',
			    'credit'=>'23',
			     'flag'=>1,
			    ),
			  'fields'=>array(
			    'Journal.date',
			    'Journal.amount',
			    'Journal.id',
			    'Journal.debit',
			    'Journal.external_voucher'
			    )
			  ));
	 foreach ($Journal as $key => $value) 
	 {
	 	$last_id=$value['Journal']['id']-1;
	 	$jornal_last=$this->Journal->findByIdAndExternalVoucher($last_id,$value['Journal']['external_voucher']);
	 	$tax=($value['Journal']['amount']*100)/$jornal_last['Journal']['amount'];
	                $tax_data=[
							'expense_id'=>$value['Journal']['debit'],
							'date'=>$value['Journal']['date'],
							'amount'=>$jornal_last['Journal']['amount'],
							'vat'=>$tax,
							'vat_amount'=>$value['Journal']['amount'],
							'external_voucher'=>$value['Journal']['external_voucher'],
							];
						$this->ExpenseVat->create();
					if(!$this->ExpenseVat->save($tax_data)) 
					{ 
						$errors = $this->Expensesvat->validationErrors; 
						foreach ($errors as $key => $value)
						{ 
							throw new Exception($value[0], 1); 
					   } 
					}
	 }
	}
	catch (Exception $e)
    {
       
    }
exit;
}
public function expense_vat_flag()
{
		try {

	 $Journal=$this->Journal->find('all',array(
			  'conditions'=>array(
			    'Journal.work_flow'=>'Expense',
			    'Journal.credit'=>'23',
			     'Journal.flag'=>1,
			    ),
			  'fields'=>array(
			    'Journal.date',
			    'Journal.amount',
			    'Journal.id',
			    'Journal.debit',
			    'Journal.external_voucher'
			    ),
			  'order'=>array('Journal.id ASC'),
			  ));
	 foreach ($Journal as $key => $value1) 
	 {
	 	$this->Journal->id=$value1['Journal']['id'];
		if(!$this->Journal->saveField('flag',0))
		throw new Exception("Cant set flag", 1);
	               
	 }
	}
	catch (Exception $e)
    {
       
    }
exit;
	
}
public function Expense_CSV_Uploader()
{
  if($this->request->data)
  {
    //pr($this->request->data);
   // exit;
    $name=$this->request->data['ProductDetails']['Upload']['name'];
    $name_explod=explode('.', $name);
    if($name_explod[1]!="csv")
    {
      $this->Session->setFlash(__('It is Not in CSV Format'));
      $this->redirect(array('action' => 'Product_CSV_Uploader'));
    }
    else
    {
     $datasource_Journal = $this->Journal->getDataSource();
     $datasource_expense = $this->ExpenseVatDetail->getDataSource();
      $filename = WWW_ROOT. 'files'.DS.$name;         
      if (move_uploaded_file($this->data['ProductDetails']['Upload']['tmp_name'],$filename)) {
        define('CSV_PATH',WWW_ROOT. 'files'.DS);
        $csv_file = CSV_PATH . $name; 
       // pr($csv_file);
         // exit;
        if (($handle = fopen($csv_file, "r")) !== FALSE) {
          fgetcsv($handle);   
          $i=1;
          $code=12;
      $count=3;
          while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {   

            $num = count($data);

            for ($c=0; $c < $num; $c++) {
              $col[$c] = $data[$c];
            }
            $user_id=1;
            $sub_group_id=15;
            $date = date('Y-m-d');
            if($col[0])
            {
                
                try {
                $datasource_Journal->begin();
                $datasource_expense->begin();
                
                $id=$col[0];

                   $tax=$col[7];
                $tax_on_sale_account_head_id=23;
				$credit=$tax_on_sale_account_head_id;
				$journal_id=$this->Journal->findById($id);
			    $debit=$journal_id['Journal']['debit'];
			   
			    $voucher_no='';
			   $work_flow='Expense';
			   $date=$journal_id['Journal']['date'];
			   $remarks=$journal_id['Journal']['remarks'];
			   $user_id=1;
			   $external_voucher=$journal_id['Journal']['external_voucher'];
			    $branch_id=$journal_id['Journal']['branch_id'];
			    $route_id=$journal_id['Journal']['route_id'];
   if($tax==0)
 {
//  	 $AccountingsController = new AccountingsController;
// $function_return=$AccountingsController->JournalCreate($credit,$debit,$tax,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'',$branch_id,'',$route_id);
//            if($function_return['result']!='Success')
// 	throw new Exception($function_return['message'], 1);

            $tax_data=[
			'expense_id'=>$debit,
			'date'=>date('Y-m-d',strtotime($date)),
			'amount'=>$col[6],
			//'vat'=>$tax,
			'vat_amount'=>$tax,
			'total'=>$col[8],
			'external_voucher'=>$external_voucher,
			'trn_number'=>$col[2],
			'supplier_name'=>$col[3],
			'journal_id'=>0,
			];
				$this->ExpenseVatDetail->create();
				if(!$this->ExpenseVatDetail->save($tax_data)) { $errors = $this->ExpenseVatDetail->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }

              }
			    
                  $datasource_Journal->commit();
                  $datasource_expense->commit();
                } catch (Exception $e) {
                 $datasource_Journal->rollback();
                  $datasource_expense->rollback();
                  pr($e->getMessage());
                }
              // }

            }
               
          }
            // exit;
          fclose($handle);
        }

        $this->Session->setFlash('File uploaded successfuly');
        $this->redirect(array('action' => 'Expense_CSV_Uploader'));
      } else {
        $this->Session->setFlash(__('There was a problem uploading file. Please try again.'));
        $this->redirect(array('action' => 'Expense_CSV_Uploader'));
      }
    }
  }
}
public function Vat_CSV_Uploader()
{
  if($this->request->data)
  {
    //pr($this->request->data);
   // exit;
    $name=$this->request->data['ProductDetails']['Upload']['name'];
    $name_explod=explode('.', $name);
    if($name_explod[1]!="csv")
    {
      $this->Session->setFlash(__('It is Not in CSV Format'));
      $this->redirect(array('action' => 'Product_CSV_Uploader'));
    }
    else
    {
     $datasource_Journal = $this->Journal->getDataSource();
     $datasource_expense = $this->ExpenseVatDetail->getDataSource();
      $filename = WWW_ROOT. 'files'.DS.$name;         
      if (move_uploaded_file($this->data['ProductDetails']['Upload']['tmp_name'],$filename)) {
        define('CSV_PATH',WWW_ROOT. 'files'.DS);
        $csv_file = CSV_PATH . $name; 
       // pr($csv_file);
         // exit;
        if (($handle = fopen($csv_file, "r")) !== FALSE) {
          fgetcsv($handle);   
          $i=1;
          $code=12;
      $count=3;
          while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {   

            $num = count($data);

            for ($c=0; $c < $num; $c++) {
              $col[$c] = $data[$c];
            }
            $user_id=1;
            $sub_group_id=15;
            $date = date('Y-m-d');
            if($col[0])
            {
                
                try {
                $datasource_Journal->begin();
                $datasource_expense->begin();
                
                $id=$col[0];

                   $tax=$col[7];

			     	$this->ExpenseVatDetail->id=$count;
				if(!$this->ExpenseVatDetail->saveField('trn_number',$col[2]))
			throw new Exception("Error Processing Request", 1);
		if($col[3])
		{
		if(!$this->ExpenseVatDetail->saveField('supplier_name',$col[3]))
			throw new Exception("Error Processing Request", 1);
	   }
	   else
	   {
	   	if(!$this->ExpenseVatDetail->saveField('supplier_name',""))
			throw new Exception("Error Processing Request", 1);
	   }
			     $count++;
                  $datasource_Journal->commit();
                  $datasource_expense->commit();
                } catch (Exception $e) {
                 $datasource_Journal->rollback();
                  $datasource_expense->rollback();
                  pr($e->getMessage());
                }
              // }

            }
               
          }
            // exit;
          fclose($handle);
        }

        $this->Session->setFlash('File uploaded successfuly');
        $this->redirect(array('action' => 'Vat_CSV_Uploader'));
      } else {
        $this->Session->setFlash(__('There was a problem uploading file. Please try again.'));
        $this->redirect(array('action' => 'Vat_CSV_Uploader'));
      }
    }
  }
}
public function AllDayRegisterSummary()
	{
$executives=$this->Executive->find('list');
	$this->set(compact('executives'));
$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
	}
//new day register report
	public function AllDayRegisterReport()
	{
		$data=$this->request->data;
		//pr($data);
		//exit;
	// $conditions=[];
	// if($data['from_date']){
	// 	$conditions['DayRegister.date']=date('Y-m-d',strtotime($data['from_date']));
	// }
	//$this->DayRegister->unbindModel(array('belongsTo' => array('Executive','Route','CustomerGroup')));
	$this->Executive->unbindModel(array('belongsTo' => array('Warehouse','Staff'),'hasMany'=> array('Sale')));
	$Executive=$this->Executive->find('all',array(
		// 'joins'=>array(
		// 	array(
		// 		'table'=>'day_registers',
		// 		'alias'=>'DayRegister',
		// 		'type'=>'INNER',
		// 		'conditions'=>array('DayRegister.executive_id=Executive.id')
		// 	),
		// ),
		'conditions'=>array('Executive.block=0'),
		'fields'=>array(
			'Executive.id',
			'Executive.name',
			//'DayRegister.*',
		)
	));
	//pr($Executive);
	//exit;
	$from=date('Y-m-d',strtotime($data['from_date']));
	//$to=date('Y-m-d',strtotime($data['to_date']));
	//$list=[];
		$single=[];
	foreach ($Executive as $key => $value) {
		// $this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
       $this->DayRegister->unbindModel(array('belongsTo' => array('Executive')));
		$DayRegister=$this->DayRegister->find('all',array(
			'joins'=>array(
					array(
						'table'=>'closing_days',
						'alias'=>'ClosingDay',
						'type'=>'Left',
						'conditions'=>array('ClosingDay.day_register_id=DayRegister.id'),
					),
					// array(
					// 	'table'=>'sales',
					// 	'alias'=>'Sale',
					// 	'type'=>'inner',
					// 	'conditions'=>array('Sale.day_register_id=DayRegister.id'),
					// ),
				),
			'conditions'=>array(
				'DayRegister.executive_id'=>$value['Executive']['id'],
				'DayRegister.date'=>array($from),
			),
			'fields'=>array(
				'DayRegister.*',
				'ClosingDay.*',
				'Route.name',
				'CustomerGroup.name',
				// 'Sale.id',
				// 'Sale.invoice_no',
				// 'Sale.grand_total',
				// 'Sale.date_of_delivered',
				// 'Sale.day_register_id',

			),
		));
		//pr($DayRegister);
		 if($value['Executive']['name']){
		 	$single['executive']=$value['Executive']['name'];
		 }
		// else{
		// 	$single['executive']="No Executive";
		// }
		if(!empty($DayRegister))
		{
			foreach ($DayRegister as $keyA => $valueA) {
				$this->Sale->virtualFields = array( 
				'sale_amount' => "SUM(Sale.grand_total)",
			);
				$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
				$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					//'Sale.account_head_id'=>$value['AccountHead']['id'],
					//'Sale.executive_id'=>$value['Executive']['name'],
					'Sale.day_register_id'=>$valueA['DayRegister']['id'],
					'Sale.status'=>2,
				),
				'fields'=>array(
					//'Sale.id',
					//'Sale.invoice_no',
					//'Sale.grand_total',
					//'Sale.date_of_delivered',
					//'Sale.day_register_id',
					//'Sale.sale_type_id',
					'sale_amount',
				),
			));
				$this->Sale->virtualFields = array( 
				'cash_sale_amount' => "SUM(Sale.grand_total)",
			);
				$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
				$CashSale=$this->Sale->find('all',array(
				'conditions'=>array(
					//'Sale.account_head_id'=>$value['AccountHead']['id'],
					'Sale.sale_type1'=>'CashSale',
					'Sale.day_register_id'=>$valueA['DayRegister']['id'],
					'Sale.status'=>2,
				),
				'fields'=>array(
					//'Sale.id',
					//'Sale.invoice_no',
					//'Sale.grand_total',
					//'Sale.date_of_delivered',
					//'Sale.day_register_id',
					//'Sale.sale_type_id',
					'cash_sale_amount',
				),
			));
				$this->Sale->virtualFields = array( 
				'credit_sale_amount' => "SUM(Sale.grand_total)",
			);
				$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
				$CreditSale=$this->Sale->find('all',array(
				'conditions'=>array(
					//'Sale.account_head_id'=>$value['AccountHead']['id'],
					'Sale.sale_type1'=>'CreditSale',
					'Sale.day_register_id'=>$valueA['DayRegister']['id'],
					'Sale.status'=>2,
				),
				'fields'=>array(
					//'Sale.id',
					//'Sale.invoice_no',
					//'Sale.grand_total',
					//'Sale.date_of_delivered',
					//'Sale.day_register_id',
					//'Sale.sale_type_id',
					'credit_sale_amount',
				),
			));
//pr($Sale);
				// foreach ($Sale as $keyS => $valueS) {
				// 	$single['total_sale']=$valueS['Sale']['sale_amount'];
				// }
				//if($valueA['DayRegister']['day_register_at']!=null){
				$single['day_register_time']=date("d-m-Y h:i a", strtotime($valueA['DayRegister']['created_at']));
				$single['starting_km']=$valueA['DayRegister']['km'];
			// }
			// else{
			// 	$single['day_register_time']=date("d-m-Y", strtotime($valueA['DayRegister']['date']));

			// }
				$single['route']=$valueA['Route']['name'];
				$single['customer_group']=$valueA['CustomerGroup']['name'];
				//$single['starting_km']=$valueA['SalesReturn']['id'];
				//$single['date']=date('d-m-Y',strtotime($valueA['SalesReturn']['date']));
				//$single['ending_km']=$valueA['SalesReturn']['discount'];
				if($valueA['DayRegister']['status']==0){
					$single['total_sale']=' ';
					$single['cash_sale']=' ';
					$single['credit_sale']=' ';
			    $single['day_close_time']=' ';
			    $single['ending_km']=' ';
				$single['status']='Day Registered';
				}
				else{
					foreach ($Sale as $keyS => $valueS) {
					
				if($valueS['Sale']['sale_amount']>0){
					$single['total_sale']=$valueS['Sale']['sale_amount'];
				}
				else{
					$single['total_sale']='0';
				}
				}
				foreach ($CashSale as $keyCa => $valueCa) {
					
					if($valueCa['Sale']['cash_sale_amount']>0){
					$single['cash_sale']=$valueCa['Sale']['cash_sale_amount'];
				}
				else{
					$single['cash_sale']='0';
				}
				}
				foreach ($CreditSale as $keyCr => $valueCr) {
					
					if($valueCr['Sale']['credit_sale_amount']>0){
					$single['credit_sale']=$valueCr['Sale']['credit_sale_amount'];
				}
				else{
					$single['credit_sale']='0';
				}
				}
					$single['day_close_time']=date("d-m-Y h:i a", strtotime($valueA['ClosingDay']['closing_at']));
					$single['ending_km']=$valueA['ClosingDay']['km'];
					$single['status']='Day Closed';
				}
				//$single['grandtotal']=$valueA['SalesReturn']['grand_total'];
				//if($single['grandtotal'])
				$list[$value['Executive']['name']]=$single;
			}	
			//exit;		
		}
		else{
			$single['route']=' ';
			$single['total_sale']=' ';
			$single['cash_sale']=' ';
					$single['credit_sale']=' ';
				$single['customer_group']=' ';
			$single['day_register_time']=' ';
			$single['day_close_time']=' ';
			$single['starting_km']=' ';
			$single['ending_km']=' ';
				//$single['starting_km']=$valueA['SalesReturn']['id'];
				//$single['date']=date('d-m-Y',strtotime($valueA['SalesReturn']['date']));
				//$single['ending_km']=$valueA['SalesReturn']['discount'];
				// if($valueA['DayRegister']['status']==0){
				// $single['status']='Day Registered';
				// }
				// else{
			$single['status']='Leave';
				//}
				//$single['grandtotal']=$valueA['SalesReturn']['grand_total'];
				//if($single['grandtotal'])
				$list[$value['Executive']['name']]=$single;
		}


	}
	//pr($list);
	//exit;
//exit;
	echo json_encode($list);
	exit;
	}
}