<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<section class="content-header">
  <h1>Day Register</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <?= $this->Form->create('DayRegister', array('url' => array('controller' => 'Sale', 'action' => 'DayRegister'),'id'=>'DayRegister_Form'));?>
      <div class="form-group">
        <div class="col-md-2 col-sm-2">
          <?= $this->Form->input('executive_id',array('type'=>'select','class'=>'form-control search-field  select_two_class','id'=>'executive_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required','label'=>'Executive')); ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
          <?= $this->Form->input('date',array('type'=>'text','id'=>'date','class'=>'form-control date_range_picker','value'=>"01-01-2018 / 01-15-2018")); ?>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="col-md-5 col-sm-5">
           <?= $this->Form->input('route_id',array('type'=>'select','class'=>'form-control search-field  select_two_class','id'=>'route_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required','label'=>'Route')); ?>
         </div>
         <div id="enddiv" class="col-md-7 col-sm-7">
           <?= $this->Form->input('customer_group_id',array('type'=>'select','class'=>'form-control search-field select_two_class','id'=>'customer_group_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>$CustomerGroup_list,'required','label'=>'Group')); ?>
         </div>
       </div>
       <div class="col-md-3 col-sm-3">
         <div class="col-md-8 col-sm-8">
           <?= $this->Form->input('present',array('type'=>'select','class'=>'form-control search-field select_two_class','id'=>'present','style'=>'width: 100%;','empty'=>[''=>'All'],'options'=>['0'=>'Leave','1'=>'Present'],'required','label'=>'Present')); ?>
         </div>
         <div class="col-md-4 col-sm-4">
           <?= $this->Form->input('km',array('type'=>'text','class'=>'form-control number','id'=>'km','required')); ?>
         </div>
       </div>
       <div class="col-md-2 col-sm-5 col-sm-12"><br>
        <button hidden id='edit_button' value='' class="save pull-right edit_icon">Edit</button>
        <button type='button' id='get_button' value='' class="btn btn-primary">Get</button>
        <button type='Submit' id='add_button' value='' class="btn btn-success create_icon">Create</button>
      </div>
    </div>
  </div>
  <div class="box-body">
    <table id="table_data" class="table table-hover boder">
      <thead>
        <tr class="blue-bg">
          <th>#</th>
          <th>Executive</th>
          <th>Date</th>
          <th>Route</th>
          <th>Group</th>
          <th>Present</th>
          <th>From Km</th>
          <th>To Km</th>
          <th>Diffrence</th>
          <th>Register</th>
           <th>Close</th>
          <th>Status</th>
          <th>Edit</th>
          <th>DayReg</th>
          <th>DayClose</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
</section>
<script type="text/javascript">
  $(document).on('click','.edit_dayRegister',function(){
    var Dayregister_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Sale/get_dayregister_details_ajax/"+Dayregister_id,function( data ) {
     $('#executive_id').val(data.DayRegister.executive_id).trigger('change');
     $('#route_id').val(data.DayRegister.route_id).trigger('change');
     $('#customer_group_id').val(data.DayRegister.customer_group_id).trigger('change');
     $('#present').val(data.DayRegister.present).trigger('change');
     $('#km').val(data.DayRegister.km);
     $('#edit_button').show();
     $("#enddiv").append("<input type='hidden' id='DayRegister_id' name='data[DayRegister][id]' value='"+data.DayRegister.id+"'>");
     $('#DayRegister_Form').attr('action','<?= $this->webroot; ?>Sale/EditDayRegister');
   }, "json");
  });
  $(".datepicker_month").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
  });
  $(document).on('click','.delete_action',function(){
    if(!confirm("Are you sure?")) { return false; }
  });
</script>
<script type="text/javascript">
  $('#table_data').DataTable( {
    "processing": false,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Sale/day_register_table",
      "type": "POST",
      data:function( d ) {
        d.date= $('#date').val();
        d.customer_group_id= $('#customer_group_id').val();
        d.route_id= $('#route_id').val();
        d.present= $('#present').val();
        d.executive_id= $('#executive_id').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "DayRegister.id" },
    { "data" : "Executive.name" },
    { "data" : "DayRegister.date" },
    { "data" : "Route.name" },
    { "data" : "CustomerGroup.name" },
    { "data" : "DayRegister.present" },
    { "data" : "DayRegister.km" },
    { "data" : "DayRegister.to_km" },
    { "data" : "DayRegister.diffrence" },
    { "data" : "DayRegister.time" },
    { "data" : "DayRegister.closetime" },
    { "data" : "DayRegister.status" },
    { "data" : "DayRegister.action" },
    { "data" : "DayRegister.day_register_delete" },
    { "data" : "DayRegister.day_close_delete" },
    ],
    "columnDefs": [
    ],
  });
  $('#get_button').click(function(){
    table = $('#table_data').dataTable();
    table.fnDraw();
  });
</script>
  <script>
    $('.date_range_picker').daterangepicker({
      opens: 'left',
      locale: { format: 'DD.MM.YYYY'}
    });
    $('.date_range_picker').on('blur', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
    });
  </script>