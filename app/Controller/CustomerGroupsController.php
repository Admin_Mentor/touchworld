<?php
App::uses('AppController', 'Controller');
class CustomerGroupsController extends AppController {
	public $helpers= array('Html','Form');
	public $uses=array('CustomerGroup');
	public function customer_group_search()
	{
		$group_name=$this->request->data['customer_group_name'];
		$group_name=trim($group_name);
		$CustomerGroup=$this->CustomerGroup->find('first',array('conditions'=>array('CustomerGroup.name'=>$group_name)));
		if(!empty($CustomerGroup)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function index(){
		$CustomerGroup = $this->CustomerGroup->find('list', array('fields' => array('id','name')));
		$this->set('CustomerGroup',$CustomerGroup);
		$CustomerGroups = $this->CustomerGroup->find('all', array('order'=>array('CustomerGroup.id DESC'),'fields' => array('id','name')));
		$this->set('CustomerGroups',$CustomerGroups);
	}
	public function add_customer_group_ajax() {

        $return = ['result' => 'Empty'];

        try {
            $data = $this->request->data;
            $name = $data['name'];

            $customer_group_data = array(
                'name' => strtoupper($name),
            );

            $this->CustomerGroup->create();
            if (!$this->CustomerGroup->save($customer_group_data))
                throw new Exception("Error In Customer Group Create", 1);
            $return['result'] = 'Success';
            $Customer_Group_id = $this->CustomerGroup->getLastInsertId();
            $CustomerGroup = $this->CustomerGroup->findById($Customer_Group_id);
            
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }

        exit;
    }
	
	
	public function warehouse_type_delete_ajax($id)
	{
		try {
			$Product=$this->Product->findByProductTypeId($id);
			if($Product)
				throw new Exception("This is Used In ".$Product['Product']['name'], 1);
			if(!$this->ProductType->delete($id))
				throw new Exception("Error Processing Request While delete", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function division_get_ajax($id)
	{
		try {
			$Division=$this->Division->findById($id);
			if(!$Division)
				throw new Exception("Empty Division", 1);
			$return['data']=$Division['Division'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	
	public function division_edit_ajax()
	{
		try {
			$data=$this->request->data['DivisionEdit'];
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				);
			$this->Division->id=$data['id'];
			if(!$this->Division->save($Table_data))
			{
				$errors = $this->Division->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Division=$this->Division->read();
			$return['key']=$Division['Division']['id'];
			$return['value']=$Division['Division']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function edit_customer_group_ajax(){
		try {
			$id=$this->request->data['id'];
			if(empty($id))
				throw new Exception("Empty Customer Group Id");
			$name=strtoupper(trim($this->request->data['name']));
			if(empty($name))
				throw new Exception("Empty Group Name");
			
			$tableData = [
				'name'=>$name,
			];
			$this->CustomerGroup->id=$id;
			if(!$this->CustomerGroup->save($tableData))
				throw new Exception("Error In Group Request");
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();			
		}
		echo json_encode($return);
		exit;
	}
	
	
}