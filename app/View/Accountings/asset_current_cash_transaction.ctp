<section class="content-header">
	<h1>Cash Transaction 
		<a href="<?= $this->webroot ?>Accountings/AssetCurrentCash"><input type="button" class="btn btn-success save pull-right" value="Create"></input></a>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box box-header">
			<div class="col-md-12">
				<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Cash_Form']); ?>
				<div class="form-group">
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
						<?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select_two_class','id'=>'account_head','style'=>'width: 100%;','options'=>$AccountHeads,'empty'=>'ALL','required','label'=>'Acc/Name',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
				</div>
				<?= $this->Form->end(); ?>
			</div>
		</div>
		<div class="row-wrapper">
			<div class="box-body">
				<table class="boder table table-condensed table datatable" id="transaction_detailss_table">
					<thead>
						<tr class="blue-bg">
							<th width="9%">Date</th>
							<th>Account's Name</th>
							<th>Remark</th>
							<th>Debit</th>
							<th>Credit</th>
							<th>Closing Balance</th>
							<!-- <th></th> -->
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $credit=0; $debit=0; $closing_balanc=0; foreach ($All_Accounts as $key => $value): ?>
						<tr class="blue-pddng view_transaction">
							<td><span style="display:none" class='journal_id'><?= $value['id']; ?></span><span><?= date('d-m-Y',strtotime($value['date'])); ?></span></td>
							<td class='name'><?= $value['name']; ?></td>
							<td><?= $value['remarks']; ?></td>
							<td><?= $value['credit'];  $credit+=$value['credit']; ?></td>
							<td><?= $value['debit']; $debit+=$value['debit']; ?></td>
							<td><?= $closing_balanc+=$value['credit']-$value['debit']; ?></td>
							<!-- <td><i class="fa fa-pencil-square-o blue-col edit"></i></td> -->
							<td><a href="#"><i class="fa fa-trash blue-col delete"></i></a></td>
						</tr>
					<?php endforeach ?>
				</tbody>
				<tfoot>
					<tr class="blue-pddng">
						<td></td>
						<td></td>
						<td class="total_amount">Total</td>
						<td class="total_amount"><?= $credit; ?></td>
						<td class="total_amount"><?= $debit; ?></td>
						<td class="total_amount"><?= $closing_balanc; ?></td>
						<!-- <th></th> -->
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
<?php require('general_journal_transaction_modal.php') ?>
</script>
<script type="text/javascript">
	<?php require('asset_current_cash_transaction.js'); ?>
</script>