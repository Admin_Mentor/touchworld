<?php
App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');
date_default_timezone_set('Asia/Riyadh');
class AppController extends Controller {
	public $uses=[
	'Type',
	'MasterGroup',
	'Group',
	'SubGroup',
	'AccountHead',
	'Journal',
	'BankDetail',
	'Profile',
	'Module',
	'Menu',
	'BranchRouteMapping',
	'UserRole'
	];
	public $Global_Var_Profile;
	public $components = array('Flash','Session');
	function beforeFilter()
	{
		$Profile=$this->Profile->find('first');
		$this->Global_Var_Profile=$Profile;
		$this->set('Profile',$Profile);
		if(!empty($Profile['Profile']['timezone_city']))
		{
			$timezone_city=$Profile['Profile']['timezone_city'];
		}
		else
		{
			$timezone_city='Riyadh';
		}
		$timezone='Asia/'.$timezone_city;
		date_default_timezone_set($timezone);
		// $currency=$this->Country->field('Country.currency',array('Country.id'=>$Profile['Profile']['country_id']));
		// $this->set(compact('currency'));
		$controller=$this->request->params['controller'];
		if($controller!='Api' || $controller!='Settings'){
			$userid = $this->Session->read('User.id');
			$userid = 1;
				$action=$this->request->params['action'];
			if(!isset($userid))
			{
				//$this->Session->setFlash(__('Please Login!.'));
				$action=$this->request->params['action'];
				if($controller!='Settings' && $action!='login')
				{
					return $this->redirect(array('controller'=>'User','action' => 'login'));	
				}
			}
			else
			{
				if($action=='display'){
					return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));	
				}
				$menupermission = $this->Session->read('UserRole.menus');
				$PermissionList = explode(',', $menupermission);
		//geting complete menulist
				$menulists = $this->Menu->find('all',array('conditions'=>array('active'=>1)));
				$mod_list = array();
		//removig menu's as per privilage
		//three level menu hierarchy is present
				$removeids = array();
				$overlap = array();
				$defAction = array();
				foreach ($menulists as $key => $value) {
					if($value['Menu']['action']!='#'){
						if(!in_array($value['Menu']['id'], $PermissionList)){
							if($value['Menu']['menu_id']!=0)
							{
								$removeids[]= $value['Menu']['menu_id'];
							}
							unset($menulists[$key]);
						}
						else{
							if($value['Menu']['menu_id']!=0)
							{
								$overlap[] = $value['Menu']['menu_id'];
							}
						}
					}
				}
				$removeids = array_diff($removeids, $overlap);
				$removeids =array_unique($removeids);
				$removeOne = array();
				$overlapOne = array();
				foreach ($menulists as $key => $value) {
					if($value['Menu']['action']=='#'){
						if(in_array($value['Menu']['id'], $removeids)){
							if($value['Menu']['menu_id']!=0)
							{
								$removeOne[]= $value['Menu']['menu_id'];
							}
							unset($menulists[$key]);
						}
						else{
							if($value['Menu']['menu_id']!=0)
							{
								$overlapOne[] = $value['Menu']['menu_id'];
							}
						}
					}
				}
				$removeOne = array_diff($removeOne, $overlapOne);
				$removeOne =array_unique($removeOne);
				foreach ($menulists as $key => $value) {
					if($value['Menu']['action']=='#'){
						if(in_array($value['Menu']['id'], $removeOne)){
							unset($menulists[$key]);
						}
						else{
						}
					}
				}
		//creatin menu  hierarchy
				foreach ($menulists as $key => $value) {
					$mod_name['name']= $value['Module']['name'];
					$mod_name['id']= $value['Module']['id'];
					array_push($mod_list, $mod_name);
				}
				$mod_list = array_map("unserialize", array_unique(array_map("serialize", $mod_list)));
				$ullist = array();
				$i=0;
				foreach($mod_list as $key => $value){
				//reindex
					$ullist[$value['id']]['main_ul']['name'][]=$value['name'];
				}
				$menu_ids = array();
				foreach ($menulists as $key => $value) {
					if($value['Menu']['menu_id']==0){
						if($value['Menu']['action']!='#')
						{
							if(empty($defAction[$value['Module']['id']]))
							{
								$defAction[$value['Module']['id']] = $value['Menu']['action'];
							}
						}
				//re indexing
						$ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['id']]['name']=$value['Menu']['name'];
						$ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['id']]['href']=$value['Menu']['action'];
						$menu_ids[] = $value['Menu']['id'];
						unset($menulists[$key]);
					}
				}
				$submenu_ids = array();
				foreach ($menulists as $key => $value) {
					if(in_array($value['Menu']['menu_id'] , $menu_ids) )
					{
						array_push($submenu_ids, $value['Menu']['id']);
						if($value['Menu']['action']!='#')
						{
							if(empty($defAction[$value['Module']['id']]))
							{
								$defAction[$value['Module']['id']] = $value['Menu']['action'];
							}
					// $defAction[$value['Module']['id']] = $value['Menu']['action'];
						}
				//reindex
						$ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['menu_id']]['sub_li'][$value['Menu']['id']]['name']=$value['Menu']['name'];
						$ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['menu_id']]['sub_li'][$value['Menu']['id']]['href']=$value['Menu']['action'];
						unset($menulists[$key]);
					}
				}
				foreach ($menulists as $key => $value) {
					if(in_array($value['Menu']['menu_id'] , $submenu_ids) )
					{
						if($value['Menu']['action']!='#')
						{
							if(empty($defAction[$value['Module']['id']]))
							{
								$defAction[$value['Module']['id']] = $value['Menu']['action'];
							}
					// $defAction[$value['Module']['id']] = $value['Menu']['action'];
						}
				//reindex
						$keys = array_keys($ullist[$value['Module']['id']]['main_ul']['main_li']);
						foreach ($keys as $key => $k) {
							$subkeys = array_keys($ullist[$value['Module']['id']]['main_ul']['main_li'][$k]['sub_li']);
							foreach ($subkeys as $key => $sub) {
								$subkey[$sub] = $k;
							}
						}
					}
				}
				$thirdids = array();
				foreach ($menulists as $key => $value) {
					if(in_array($value['Menu']['menu_id'] , $submenu_ids) )
					{
						if($value['Menu']['action']!='#')
						{
							if(empty($defAction[$value['Module']['id']]))
							{
								$defAction[$value['Module']['id']] = $value['Menu']['action'];
							}
					// $defAction[$value['Module']['id']] = $value['Menu']['action'];
						}
						$thirdids[]=$value['Menu']['id'];
						$pkey = $subkey[$value['Menu']['menu_id']];
					//reindex
						$ullist[$value['Module']['id']]['main_ul']['main_li'][$pkey]['sub_li'][$value['Menu']['menu_id']]['third_sub_li'][$value['Menu']['id']]['name']=$value['Menu']['name'];
						$ullist[$value['Module']['id']]['main_ul']['main_li'][$pkey]['sub_li'][$value['Menu']['menu_id']]['third_sub_li'][$value['Menu']['id']]['href']=$value['Menu']['action'];
					}
				}
				$this->set('ul_list_main',$ullist);
				$mode_catagory=[
				'1'=>'Cash',
				'2'=>'Bank',
				];
				$conditions_branch_route=[];

				$user_branch_id=$this->Session->read('User.branch_id');
				if($user_branch_id)
  {
  	$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
     //$conditions_branch_route['Route.id']=$route_id;
  }
  $conditions_branch_route['sub_group_id']=1;
				$this->set('mode_catagory',$mode_catagory);
				$AccountHead_cash=$this->AccountHead->find('list',array('conditions'=>array('sub_group_id'=>1)));
				$this->set('Mode',$AccountHead_cash);
				// getting module list
				$list = $this->Module->find('all',array(
					'conditions' => array('Module.module_id' => 0,'Module.status' => 1),
					'order'=>array('sort'),
					));
				// dashboard modules structure implemented
				$top_dashboard=array();
				$i=0;
				foreach ($list as $key => $value) {
					$top_dashboardlist['id']=$value['Module']['id'];
					$top_dashboardlist['name']=$value['Module']['name'];
					$top_dashboardlist['controll']=$value['Module']['name'];
					$top_dashboardlist['icon']=$value['Module']['icon'];
					$top_dashboardlist['icon_color']=$value['Module']['icon_color'];
					$top_dashboardlist['href']= '#';
					if(!empty($defAction[$value['Module']['id']]))
					{
						if($top_dashboardlist['href']=='#')
						{
							$top_dashboardlist['href']= $defAction[$value['Module']['id']];
						}
					}
					if($value['Module']['name']=='Master'){
						$top_dashboardlist['href']= 'Homes/Master';
					}
					if($value['Module']['name']=='Expense'){
						$top_dashboardlist['href']= 'Accountings/ExpenseTransaction';
					}
					$i++;
					array_push($top_dashboard, $top_dashboardlist);
				}
				$this->set('top_dashboard',$top_dashboard);
				// getting menu list
				$childlist = $this->Module->find('all',array(
					'conditions' => array('Module.module_id' => 4)
					));
				// dashboard modules structure implemented
				$master_dashboard=array();
				$i=0;
				foreach ($childlist as $key => $value) {
					$top_dashboardlist['id']=$value['Module']['id'];
					$top_dashboardlist['name']=$value['Module']['name'];
					$top_dashboardlist['controll']=$value['Module']['name'];
					$top_dashboardlist['icon']=$value['Module']['icon'];
					$top_dashboardlist['icon_color']=$value['Module']['icon_color'];
					$top_dashboardlist['href']= '#';
					if(!empty($defAction[$value['Module']['id']]))
					{
						$top_dashboardlist['href']= $defAction[$value['Module']['id']];
					}
					$i++;
					array_push($master_dashboard, $top_dashboardlist);
				}
				$this->set('master_dashboard',$master_dashboard);
			}
		}
	}
}