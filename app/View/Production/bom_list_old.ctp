<section class="content-header">
  <h1> BOM List</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding">
            <a  href="<?php echo $this->webroot ?>Production/Bom"><input style="margin-left:20px"type="button" class="btn btn-success save pull-right" value="New BOM"></input></a>
             
            <br>
            <br>
            <table class="table table-hover datatable boder">
              <thead>
                <tr class="blue-bg">
                  <th>BOM ID</th>
                  <th>Product</th>
                  <th>Production Cost</th>
                  <th>Date</th>
                  <th>Actions</th>
                  
                </tr>
              </thead>
              <tbody>
              <?php foreach ($BOMList as $key => $value): ?>
                <tr>
                  <td>
                    <span class='id'><?= $value['id']; ?></span>
                  </td>
                  <td><?= $value['product_id']; ?></td>
                  <td><?= $value['production_cost']; ?></td>
                  <td><?= date('d-m-Y',strtotime($value['date'])); ?></td>
                  <td><span></span><a target="_blank" href="<?= $this->webroot; ?>Production/Bom/<?= $value['id']  ?>"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                 
                </tr>
              <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  <script type="text/javascript">
    $(document).on('click','table tbody tr',function(){
      var id=$(this).closest('tr').find('span.id').html();
      var url = "<?= $this->webroot; ?>Production/Bom/"+id;
      // $(location).attr("href", url);
    });
  </script>