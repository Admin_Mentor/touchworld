<section class="content-header">
  <h2>Role</h2>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        
        <div class="row-wrapper">
          <div class="row">
            <?= $this->Form->create('NewRole', array('url' => array('controller' => 'User', 'action' => 'NewRole')));?>
            <div class="col-md-12">
              <div class="row">
                <div class="form-horizontal" style="margin-top: 15px;">
                  <div class="box-body">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                      <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          <?= $this->Form->input('role_name',array('class'=>'form-control','type'=>'text','required','id'=>'role_name',)); ?>
                           <?= $this->Form->input('menus',array('class'=>'form-control','type'=>'hidden','required','id'=>'menus',)); ?>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                         
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="col-md-12" > 
                    <ul class="" id='dashboard_menu_list'>
          <?php foreach ($MenuList as $key => $value): ?>
            <li class="treeview main_li" >
              <a href="#"><i class="fa fa-share"></i> <span class='name'><?= $value['main_ul']['name'][0]; ?></span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu sub_ul">
                <?php foreach ($value['main_ul']['main_li'] as $keyli => $valueli): ?>
                  <?php if(empty($valueli['sub_li'])) : ?>
                    <li  class='sub_li'>
                      <a href="<?= $this->webroot; ?><?= $valueli['href'] ?>"><i class="fa fa-circle-o"></i> <?= $valueli['name'] ?> </a>
                    </li>
                  <?php else : ?>
                    <li class="treeview second_sub_li">
                      <a href="#"><i class="fa fa-pie-chart"></i> <span><?= $valueli['name']; ?></span> <i class="fa fa-angle-left pull-right"></i> </a>
                      <ul class="treeview-menu second_sub_ul">
                        <?php foreach ($valueli['sub_li'] as $keysub_li => $valuesub_li): ?>
                          <?php if(empty($valuesub_li['third_sub_li'])) : ?>
                            <li class='sub_li'>
                              <a href="<?= $this->webroot; ?><?= $valuesub_li['href']; ?>"><i class="fa fa-circle-o"></i><?= $valuesub_li['name']; ?></a>
                            </li>
                          <?php else : ?>
                            <li class="treeview third_sub_li">
                              <a href="#"><i class="fa fa-pie-chart"></i> <span><?= $valuesub_li['name']; ?></span> <i class="fa fa-angle-left pull-right"></i> </a>
                              <ul class="treeview-menu third_sub_ul">
                                <?php foreach ($valuesub_li['third_sub_li'] as $keythird_sub_li => $valuethird_sub_li): ?>
                                  <li class='fourth_sub_li'>
                                    <a href="<?= $this->webroot; ?><?= $valuethird_sub_li['href']; ?>"><i class="fa fa-circle-o"></i><?= $valuethird_sub_li['name']; ?></a>
                                  </li>
                                <?php endforeach ?>
                              </ul>
                            </li>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </ul>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
              </ul>
            </li>
          <?php endforeach; ?>
        </ul>
        </div>
        <br>
                    
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="row-wrapper">
              <div class="col-md-12">
                <div class="box-body table-responsive no-padding">
                 
                </div>
              </div>
            </div>
          <div class="col-md-12 col-lg-12 col-xs-12 col-sm-3">
                        <br>
                        <button type='submit' value='save' class="btn btn-success pull-right" >Save</button>
                        <br>
                        <br>
                      
                    </div>
          </div>
          <?= $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  
</script>
