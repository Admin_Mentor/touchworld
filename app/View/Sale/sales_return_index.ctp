<?php 
function get_status_name($status)
{
	if($status==0) { $name='Cancelled'; }
	if($status==1) { $name='Order Placed'; }
	if($status==2) { $name='Order Delivered'; }
	return $name;
}
?>
<section class="content-header">
	<h1> Sales Return List
		<div class="pull-right">
			<a href="<?php echo $this->webroot ?>Sale/SalesReturn"><button class='btn btn-success pull-right'>New SalesReturn</button></a>
		</div>
	</h2>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box-body">
				<table class="boder table table-condensed table datatable" id="salesReturn_table" data-order='[[ 1, "asc" ]]' data-page-length='25'>
					<thead>
						<tr class="blue-bg">
							<th>Date</th>
							<th>Customer</th>
							<th>Credit Note No</th>
							<th>Total Amt</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($SalesReturn as $key=>$value ) : ?>
							<tr class="blue-pddng">
								<td>
									<span><?= date('d-m-Y', strtotime($value['SalesReturn']['date'])); ?></span>
									<span class='SalesReturn_id' style='display:none;'><?= $value['SalesReturn']['id']; ?></span>
								</td>
								<td><?= $value['AccountHead']['name']; ?></td>
								<td><?= $value['SalesReturn']['invoice_no']; ?></td>
								<td><?= number_format($value['SalesReturn']['grand_total'],3,'.',''); ?></td>
								<td><?= get_status_name($value['SalesReturn']['status']) ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$(document).on('click','table tbody tr',function(){
		var SalesReturn_id=$(this).closest('tr').find('.SalesReturn_id').html();
		var url = "<?= $this->webroot; ?>Sale/SalesReturn/"+SalesReturn_id;
		$(location).attr("href", url);
	});
</script>