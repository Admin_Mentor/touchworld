<section class="content-header">
  <h1>Manage Sales Man</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body">
      <div class="row">
      <?= $this->Form->create('Executive', array('url' => array('controller' => 'Executive', 'action' => 'AddSalesman'),'id'=>'Executive_Form'));?>
        <div id="staffDiv" class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
          <?php echo $this->Form->input('staff_id',array('type'=>'select','class'=>'form-control  select2','id'=>'staff_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>$Staff,'required','label'=>'Staff')); ?>
            <?php //echo $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
         <div id="staffName" style="display: none;" class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
         <label>Staff</label><br>
          <span id="name"></span>
           
          </div>
        </div>
        <div class="col-md-3 col-sm-3" hidden="">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('cash',array('type'=>'text','id'=>'cashname','readonly','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('mobile',array('type'=>'text','id'=>'mobile','label'=>'Contact No','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('username',array('type'=>'text','id'=>'username','readonly','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('password',array('type'=>'text','id'=>'password','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('warehouse_id',array('type'=>'select','id'=>'warehouse','options'=>$Warehouse,'class'=>'form-control select2','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12 col-lg-6">
                  <div class="col-md-10">
          <div class="form-group ">
            <?php echo $this->Form->input('location_id',array('multiple' => true,'type'=>'select','id'=>'location_id','options'=>$Location,'class'=>'form-control select2','required'=>'required','label'=>'Route')); ?>
          </div>
          </div>
          <br>
          <div class="col-md-2" style="display: none;">
                    <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#addlocation"></i></a>
                    <a href="#" id='Location_Edit_Btn'><i class="fa fa-pencil fa ad-mar"></i></a>
                  </div>
                </div>
        </div>
        <div class="col-md-6 col-sm-6 col-sm-12 col-md-offset-2">
          <button style="margin-top: 5%;" class="save pull-right" id='save_button' >Save</button>
          <button style="margin-top: 5%; display:none" id='edit_button' value='' class="save pull-right">Edit</button>
        </div>
        <?= $this->Form->end(); ?>
      </div>
      <div class="row">
        <hr>
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding">
            <table class="table datatable table-hover boder">
              <thead>
                <tr class="blue-bg">
                  <th>Name</th>
                  <th>Contact No</th>
                  <th>Route</th>
                  <th>User Name</th>
                  <th>Password</th>
                  <th>Warehouse</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  <th>Block</th>
                </tr>
              </thead>
              <?php foreach ($Executive as $key => $value): ?>
                <?php
                if($value['Executive']['block']!=0)
                {
                  $block_unblock = 0;
                  $title ='Unblock';
                  $icon = 'fa-user';
                }
                else{
                  $block_unblock = 1;
                  $title ='Block';
                  $icon = 'fa-user-times';
                }
                ?>
                <tr>
                  <td class='name'><?= $value['Executive']['name'];  ?></td>
                  <td><?= $value['Executive']['mobile']; ?></td>
                   <td><a data-id="<?= $value['Executive']['id'];  ?>" title="Show Routes" class="route-show" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a></td>
                  <!-- <td><?= $value['Location']['name']; ?></td> -->
                  <td><?= $value['Executive']['username']; ?></td>
                  <td><?= $value['Executive']['password']; ?></td>
                  <td><?= $value['Warehouse']['name']; ?></td>
                  <td><i data-id="<?= $value['Executive']['id'];  ?>" class="fa fa-edit edit_executive" aria-hidden="true" style="color: #3c8dbc;"></i></td>
                  <td><a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Executive/DeleteExecutive/<?= $value['Executive']['id']; ?>"><i class="fa fa-trash blue-col blue-col"></i></a></td>
                  <td><a title="<?= $title?>" onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Executive/BlockUnblockExecutive/<?= $value['Executive']['id']."/". $block_unblock ?>"><i class="fa <?= $icon ?> blue-col blue-col"></i></a></td>
                <?php endforeach ?>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <br>
    </div>
  </div>
</div>
</section>
<?php require 'Add_Modal/Location_Add_modal.php'; ?>
<?php require 'Edit_Modal/Location_Edit_Modal.php'; ?>
<?php require 'Edit_Modal/Route_List_Modal.php'; ?>

<script type="text/javascript">
//$(".select_two_class").select2();
$('#staff_id').on('change',function(){
  var Staff_id = $(this).val();
    // var Staff_name=$(this).closest('tr').find('td.name').text();
    $.post( "<?= $this->webroot ?>Executive/get_staff_details_ajax/"+Staff_id,function( data ) {
    
      // $('#contact_no').val(data.Staff.contact_no);
      $('#username').val(data.Staff.code);
      var cashname =  data.Staff.name+data.Staff.code+'_CASH';
      $('#cashname').val(cashname);
     
    }, "json");
});
  $(document).on('click','.edit_executive',function(){
    // var Executive_name=$(this).closest('tr').find('td.name').text();
    var Executive_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Executive/get_executive_details_ajax/"+Executive_id,function( data ) {
       
       $("#location_id").val('').trigger('change');
        //  console.log(data);
      // $('#name').val(data.Executive.name);
       // $("#location_id").select2({
       //      placeholder: "Add Route"
       //  });
       $('#staff_id').val(data.Executive.staff_id).trigger('change');
       $('#staffDiv').css('display','none');
       // $('#staff_id').val(Executive_id).trigger('change');
       var staffName = $('#staff_id option:selected').text();
       $('#staffName').css('display','block');
       $('#name').text(data.staffName)
       $('#cashname').val(data.Cash.name);
       
       // $('#staff_id').attr('readonly');
      $('#mobile').val(data.Executive.mobile);
      $('#password').val(data.Executive.password);
      $('#username').val(data.Executive.username);
      $('#warehouse').val(data.Executive.warehouse_id).trigger('change');
            //console.log(data.ExecutiveRoute);
      $.each(data.ExecutiveRoute, function( key, value ) {
        $("#location_id").find("option[value="+value+"]").prop("selected", "selected").trigger('change');
      });
      // var Values = new Array();
      // $.each(data.ExecutiveRoute, function( key, value ) {
      //   $("#location_id").find("option[value="+value+"]").prop("selected", "selected").trigger('change');
      // // $('#location_id').select2('val',value).change();
      
      // Values.push(value);
      // });
      // $("#location_id").val(Values).trigger('change');
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#mobile").append("<input type='text' id='Executive_id' name='data[Executive][id]' value='"+data.Executive.id+"'>");
      $('#Executive_Form').attr('action','<?= $this->webroot; ?>Executive/EditExecutive');
    }, "json");
  });
  $(document).on('click','.route-show',function(){
    var Executive_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Executive/get_executive_routes/"+Executive_id,function( data ) {
        $('#Route_List_Modal').modal('show');
        $('#RouteTable tbody').html(data.row);
        $('#executive_id').val(data.executive_id);
     
    }, "json");
  });
  $(document).on('click','.del-route',function(){
    if(confirm('Are You Sure want to remove this route'))
    {
      var id=$(this).data('id');
    var parentRow =$(this).parents('tr');
    $.get( "<?= $this->webroot ?>Executive/DeleteExecutiveRoute/"+id,function( data ) {
       parentRow.hide();
       location.reload();
     
    }, "json");
    }

    
  });
  <?php require 'Script/location.js' ?>
</script>
