 $('#category').change(function(){
  var category=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/AccountHead_Option_ListBySubGroupId_ajax/"+category, function( data ) {
    $('#account_head').html('');
    $.each(data.options,function(key,value){
      $('#account_head').append($("<option></option>").attr("value",key).text(value));
    });
    $('#account_head').trigger('change');
  }, "json");
});
 $('#account_head').change(function(){
  var account_head= $(this).val();
  $.post( "<?= $this->webroot ?>Accountings/get_outstanding_balance/"+account_head, function( data ) {
    $('#outstanding').val(data);
    $('#paid').val(data);
  }, "json");
});
 $('#account_head').change();
 $('#paid').keyup(function(){
  var paid=$('#paid').val();
  var outstanding=$('#outstanding').val();
  var balance=parseFloat(outstanding)-parseFloat(paid);
  $('#balance').val(balance);
});