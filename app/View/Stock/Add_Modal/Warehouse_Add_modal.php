<div id="addwarehouse" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Add Warehouse</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Warehouse Name</label>
            <div class="col-sm-9">
              <input class="form-control warehouse_disable toUpperCase " placeholder="" type="text" id="modal_warehouse_name">
              <span id="warehouse_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
            <div class="col-sm-9">
              <input class="form-control" placeholder="" type="text" id="modal_warehouse_desc">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button  type='button' class="save btn btn-success" id="add_warehouse">Save</button>
      </div>
    </div>
  </div>
</div>