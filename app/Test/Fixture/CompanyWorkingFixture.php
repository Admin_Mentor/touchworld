<?php
/**
 * CompanyWorking Fixture
 */
class CompanyWorkingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'month' => array('type' => 'date', 'null' => false, 'default' => null),
		'working_days' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'leave_days' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'month' => '2018-07-21',
			'working_days' => 1,
			'leave_days' => 1
		),
	);

}
