<section class="content-header">
  <h1>Manage Branch</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body">
      <div class="row">
      <?= $this->Form->create('Branch', array('url' => array('controller' => 'Branch', 'action' => 'AddBranch'),'id'=>'Branch_Form'));?>
        <div id="staffDiv" class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required'));
          ?>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('mobile',array('type'=>'text','id'=>'mobile','label'=>'Contact No','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-3" hidden="">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('username',array('type'=>'text','id'=>'username','class'=>'form-control')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3" hidden="">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('password',array('type'=>'text','id'=>'password','class'=>'form-control')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('warehouse_id',array('type'=>'select','id'=>'warehouse','options'=>$Warehouse,'class'=>'form-control select2','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12 col-lg-6">
        <!-- <div class="box-body"> -->
                  <div class="col-md-10">
          <div class="form-group ">
            <?php echo $this->Form->input('location_id',array('multiple' => true,'type'=>'select','id'=>'location_id','options'=>$Location,'class'=>'form-control select2','required'=>'required','label'=>'Route')); ?>
          </div>
          </div>
          <br>
         
        </div>
        <div class="col-md-6 col-sm-6 col-sm-12 col-md-offset-2">
          <button style="margin-top: 5%;" class="save pull-right" id='save_button' >Save</button>
          <button style="margin-top: 5%; display:none" id='edit_button' value='' class="save pull-right">Edit</button>
        </div>
        <?= $this->Form->end(); ?>
      </div>
      <div class="row">
        <hr>
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding">
            <table class="table datatable table-hover boder">
              <thead>
                <tr class="blue-bg">
                  <th>Name</th>
                  <th>Contact No</th>
                  <th>Route</th>
                  <th>Warehouse</th>
                  <th>Edit</th>
<!--                   <th>Delete</th>
 -->                </tr>
              </thead>
             <?php foreach ($BranchAll as $key => $value): ?>
              
                <tr>
                  <td class='name'><?= $value['Branch']['name'];  ?></td>
                  <td ><?= $value['Branch']['mobile'];  ?></td>
                   <td><a data-id="<?= $value['Branch']['id'];  ?>" title="Show Routes" class="route-show" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a></td>
                  <td><?= $value['Warehouse']['name']; ?></td>
                  <td><i data-id="<?= $value['Branch']['id'];  ?>" class="fa fa-edit edit_branch" aria-hidden="true" style="color: #3c8dbc;"></i></td>
                <?php endforeach ?>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <br>
    </div>
  </div>
</div>
</section>
<?php require 'Add_Modal/Location_Add_modal.php'; ?>
<?php require 'Edit_Modal/Location_Edit_Modal.php'; ?>
<?php require 'Edit_Modal/Route_List_Modal.php'; ?>

<script type="text/javascript">
$(document).on('click','.edit_branch',function(){
    var Branch_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Branch/get_branch_details_ajax/"+Branch_id,function( data ) {
       $("#location_id").val('').trigger('change');
       $('#name').val(data.Branch.name);
      $('#mobile').val(data.Branch.mobile);
      //$('#password').val(data.Branch.password);
      //$('#username').val(data.Branch.username);
      $('#warehouse').val(data.Branch.warehouse_id).trigger('change');
      
      // console.log(data.BranchRoute);
      $.each(data.BranchRoute, function( key, value ) {
        $("#location_id").find("option[value="+value+"]").prop("selected", "selected").trigger('change');
              });
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#mobile").append("<input type='text' id='Branch_id' name='data[Branch][id]' value='"+data.Branch.id+"'>");
      $('#Branch_Form').attr('action','<?= $this->webroot; ?>Branch/EditBranch');

  }, "json");
});
 $(document).on('click','.route-show',function(){
    var Branch_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Branch/get_branch_routes/"+Branch_id,function( data ) {
        $('#Route_List_Modal').modal('show');
        $('#RouteTable tbody').html(data.row);
        $('#branch_id').val(data.branch_id);
     
    }, "json");
  });
  $(document).on('click','.del-route',function(){
    if(confirm('Are You Sure want to remove this route'))
    {
      var id=$(this).data('id');
    var parentRow =$(this).parents('tr');
    $.get( "<?= $this->webroot ?>Branch/DeleteBranchRoute/"+id,function( data ) {
       parentRow.hide();
     
    }, "json");
    }
    
  });
    <?php require 'Script/location.js' ?>

</script>