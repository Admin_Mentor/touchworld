<style type="text/css">
  <?php include "style.css" ?>
</style>
<section class="content-header">
  <h1> Product</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4">
              <div class="col-md-12"> <?php echo $this->Form->input('product_type',array(
                'type'=>'select','empty' =>'Select','options'=>$ProductType,'class'=>'form-control select2 product_search_class','label'=>'Product Type','id'=>'product_type',
                )) ?>
              </div>
            </div>
            <div class="col-md-4">
              <div class="col-md-10"> <?php echo $this->Form->input('product_name',array('type'=>'select','empty' =>'Select','options'=>$product_name,'class'=>'form-control select2','label'=>'Product','id'=>'product_name')) ?></div>
              <br>
              <div class="col-sm-1">
              <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" id="Product_modal_add"></i></a>
            </div>
              
            </div>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover boder" id='table_data'>
            <thead>
              <tr class="blue-bg">
                <th>Product Type</th>
                <th>Product</th>
                <th>Product Code</th>
                <th>Barcode</th>
                <th>Unit</th>
                <th>No.of pieces</th>
                <th>Wholesale Price</th>
                <th>Retail Price</th>
                <th>Wholesale Price/Case</th>
                <th>Retail Price/Case</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <?php require('product_type_modal.php') ?>
        <?php require('Product_Add_modal.php') ?>
        <?php require('Product_Type_Add_Modal.php') ?>
        <?php require('Brand_Add_Modal.php') ?>
        <?php require('product_modal.php') ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  table_data=$('#table_data').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Product/Product_Table_ajax",
      "type": "POST",
      data:function( d ) {
        d.product_type= $('#product_type').val();
        d.product= $('#product_name').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "ProductType.name" },
    { "data" : "Product.name" },
    { "data" : "Product.code" },
    { "data" : "Product.barcode" },
    { "data" : "Unit.name" },
    { "data" : "Product.no_of_piece_per_unit" },
    { "data" : "Product.wholesale_price" },
    { "data" : "Product.mrp" },
    { "data" : "Product.wholesale_price_in_cases" },
    { "data" : "Product.mrp_in_cases" },
    { "data" : "Product.action" },
    ],
    "columnDefs": [
    ],
  });
  <?php require('script.js'); ?>
  <?php require('product.js'); ?>
  <?php require('product_type.js'); ?>  
  <?php require('brand.js'); ?>  
</script>