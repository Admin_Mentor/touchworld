 <section class="content-header">
 	<h1> Debters Create </h1>
 </section>
 <section class="content">
 	<div class="row">
 		<div class="col-md-12">
 			<div class="box box-primary box_tp_brdr">
 				<div class="row-wrapper">
 					<div class="row">
 						<?php echo $this->Form->create('AccountHead', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'AccountHead_Form']); ?>
 						<div class="box-body">
 							<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
 								<div class="form-group">
 									<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
 										<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
 									</div>
 								</div>
 							</div>
 							<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
 								<div class="form-group">
 									<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
 										<?= $this->Form->input('customer_type',array('class'=>'form-control select2','type'=>'select','required','options'=>$CustomerType_list,'id'=>'customer_type',)); ?>
 									</div>
 									<br>
 									<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_type_modal"></i></a></div>
 								</div>
 							</div>
 							<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
 								<div class="form-group">
 									<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
 										<?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','required','id'=>'name','label'=>'Customer',)); ?>
 									</div>
 									<br>
 									<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#Account_modal"></i></a></div>
 								</div>
 							</div>
 							<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
 								<div class="form-group">
 									<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
 										<?= $this->Form->input('opening_balance',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','readonly','id'=>'opening_balance',)); ?>
 									</div>
 								</div>
 							</div>
 							<div class="col-md-1"> 
 								<br>
 								<div class="create-wrapper">
 									<button type='Submit' id='add_button' class="user_add_btn">ADD</button>
 									<button type='Submit' id='edit_button' class="user_add_btn" style='display: none'>EDIT</button>
 								</div>
 							</div>
 						</div>
 						<?php require('customer_modal.php') ?> 
 						<?= $this->Form->end(); ?>
 					</div>
 					<br>
 					<div class="row-wrapper">
 						<div class="row">
 							<div class="col-md-12">
 								<div class="box-body table-responsive no-padding">
 									<table class="table table-condensed table table datatable boder boder" id="AccountHead_table">
 										<thead>
 											<tr class="blue-bg">
 												<th class="padding_left">Date</th>
 												<th>Customer Type</th>
 												<th>Customer</th>
 												<th>Opening Balance</th>
 												<th>Description</th>
 												<th></th>
 												<th></th>
 											</tr>
 										</thead>
 										<tbody>
 											<?php foreach ($Customer as $key => $value): ?>
 												<tr class="blue-pddng">
 													<td><?= date('d-m-Y',strtotime($value['AccountHead']['created_at'])) ?></td>
 													<td><?= $value['CustomerType']['name']; ?></td>
 													<td><span style="display:none" class='AccountHead_id'><?= $value['AccountHead']['id']; ?></span><span><?= $value['AccountHead']['name']; ?></span></td>
 													<td><?= $value['AccountHead']['opening_balance']; ?></td>
 													<td><?= $value['AccountHead']['description']; ?></td>
 													<td><i class="fa fa-pencil-square-o blue-col edit_head"></i></td>
 													<td><a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Accountings/AccountHead_delete/<?= $value['AccountHead']['id']; ?>"><i class="fa fa-trash blue-col blue-col"></i></a></td>
 												</tr>
 											<?php endforeach ?>
 										</tbody>
 									</table>
 								</div>
 							</div>
 						</div>
 					</div>
 					<div id="customer_type_modal" class="modal fade" role="dialog">
 						<div class="modal-dialog">
 							<div class="modal-content">
 								<div class="modal-header">
 									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
 									<h4 class="modal-title" id="myModalLabel">Customer Type</h4>
 								</div>
 								<div class="modal-body">
 									<div class="form-group">
 										<label for="inputEmail3" class="col-sm-4 control-label">Customer Type</label>
 										<div class="col-sm-8">
 											<input class="form-control" id="customer_type_name" type="text">
 										</div>
 										<br>
 									</div>
 								</div>
 								<div class="modal-footer">
 									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 									<button type="button" class="btn btn-primary" id='add_customer_type'>Save</button>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>
 </section>
 <script type="text/javascript">
 	$('.name').on('keyup change',function(){
 		$(this).val($(this).val().trim());
 		$('#name').val($(this).val());
 		$('#name_modal').val($(this).val());
 	});
 	$('.opening_balance').on('keyup change',function(){
 		$('#opening_balance').val($(this).val());
 		$('#opening_balance_modal').val($(this).val());
 	});
 	$(document).on('change','#customer_type,#modal_customer_type',function(){
 		$('#modal_customer_type').val($(this).val()).trigger('change.select2');
 		$('#customer_type').val($(this).val()).trigger('change.select2');
 	});
 </script>
 <!-- for edit  -->
 <script type="text/javascript">
 	$(document).on('click','.edit_head',function(){
 		var id=$(this).closest('tr').find('td span.AccountHead_id').text();
 		$.post( "<?= $this->webroot ?>Accountings/Customer_get_ajax/"+id, function( responds ) {
 			if(responds.result!='Success')
 			{
 				alert(responds.message);
 				return false;
 			}
 			$('#name').val(responds.data.AccountHead.name).trigger('change');
 			$('#opening_balance').val(responds.data.AccountHead.opening_balance).trigger('change');
 			$('#description').val(responds.data.AccountHead.description);
 			$("#name").append("<input type='text' id='AccountHead_id' name='data[AccountHead][id]' value='"+responds.data.AccountHead.id+"'>");
 			$('#place').val(responds.data.Customer.place);
 			$('#customer_type').val(responds.data.CustomerType.id).trigger('change');
 			$('#state_id').val(responds.data.Customer.state_id).trigger('change');
 			$('#code').val(responds.data.Customer.code);
 			$('#email').val(responds.data.Customer.email);
 			$('#mobile').val(responds.data.Customer.mobile);
 			$('#gstin').val(responds.data.Customer.gstin);
 			$("#name_modal").append("<input type='text' id='Customer_id' name='data[Customer][id]' value='"+responds.data.Customer.id+"'>");
 			$('#add_button').css('display','none');
 			$('#edit_button').css('display','');
 			$('#AccountHead_Form').attr('action','<?= $this->webroot; ?>Accountings/Customer_account_edit');
 		}, "json");
 	});
 </script>
 <!-- For delete -->
 <script type="text/javascript">
 	$(document).on('click','.delete',function(){
 		if(!confirm("Are you sure?"))
 		{
 			return false;
 		}
 		var id=$(this).closest('tr').find('td span.AccountHead_id').text();
 		var rowindex = $(this).closest('tr').index();
 		$.post( "<?= $this->webroot ?>Accountings/customer_delete/"+id,function( data ) {
 			if(data.result!='Success')
 			{
 				alert(data.message);
 				return false;
 			}
 			$('#AccountHead_table tbody tr:eq(' + rowindex + ')').remove();
 		}, "json");
 	});
 </script>
 <script type="text/javascript">
 	$('#add_customer_type').click(function(){
 		var name=$('#customer_type_name').val();
 		if(name=='')
 		{
 			$('#customer_type_name').focus();
 			return false;
 		}
 		var website_url='<?php echo $this->webroot; ?>CustomerType/add_customer_type_ajax';
 		var data={
 			name:name,
 		};
 		$.ajax({
 			method: "POST",
 			url: website_url,
 			data: data,
 			dataType:'json',
 		}).done(function( data ) {
 			if(data.result!='Success')
 			{
 				alert(data.result);
 				return false;
 			}
 			$('#customer_type_modal').modal('toggle');
 			$('#customer_type_name').val('');
 			$('#customer_type').append($("<option></option>").attr("value",data.key).text(data.value));
 			$('#modal_customer_type').append($("<option></option>").attr("value",data.key).text(data.value));
 			$('#customer_type').val(data.key).trigger('change');
 			$('#modal_customer_type').val(data.key).trigger('change');
 		});
 	});
 </script>
<script type="text/javascript">
	<?php require('state.js'); ?>
</script>