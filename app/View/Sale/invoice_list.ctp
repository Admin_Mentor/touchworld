<?php 
function get_status_name($status)
{
	if($status==0) { $name='Cancelled'; }
	if($status==1) { $name='Order Placed'; }
	if($status==2) { $name='Order Delivered'; }
	return $name;
}
?>
<section class="content-header">
<h1> Invoice List
		<div class="pull-right">
			<a href="<?php echo $this->webroot ?>Sale/Sale"><button class='btn btn-success pull-right'>New Sale</button></a>
		</div>
	</h2>
</section>
<section class="content">
	<div class="row">
		<div class="row">
			<?php
			$firstdate = date('d-m-Y',strtotime('first day of this month'));
			$todate = date('d-m-Y');
			?>
			<div class="col-md-12">
				<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control invoice-search pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$firstdate,'label'=>'From Date')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control invoice-search pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$todate,'label'=>'To Date')); ?>
						</div>
					</div>
				</div>
				<?php
				$user_branch_id=$this->Session->read('User.branch_id');?>
  	
				<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12" 
				<?php if($user_branch_id){ echo "hidden";}?>>
  
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<?= $this->Form->input('branch_id',array('type'=>'select','id'=>'branch_id','empty'=>'All','options'=>$branchs,'class'=>'form-control select_two_class invoice-search','style'=>'width: 100%','label'=>'Branch')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="inn_tabl_wrap">
						<div class="box-body">
							<table id="invoice_table" class="boder table table-condensed table datatable" id="sale_table"  data-page-length='25'>
								<thead>
									<tr class="blue-bg">
									   <th hidden=""></th>
										<th>Date</th>
										<th>Executive</th>
										<th>Customer</th>
										<th>Route</th>
										<th>Invoice No</th>
										<th>Total Amt</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$i =0;
									foreach($Sale as $key=>$value ) : $i++; ?>
									<tr class="blue-pddng">
									   <td hidden=""></td>
										<td><?= date('d-m-Y',strtotime($value['Sale']['date_of_order'])); ?></td>
										<td><?= $value['Executive']['name']; ?></td>
										<td><?= $value['AccountHead']['name'].' '.$value['Customer']['code']; ?></td>
										<td><?= $value['Route']['name']; ?></td>
										<td><?= $value['Sale']['invoice_no']; ?></td>
										<td><?= number_format($value['Sale']['grand_total'],3,'.',''); ?></td>
										<td><?= get_status_name($value['Sale']['status']) ?></td>
										<td>
											<span><a target="_blank" href="<?= $this->webroot; ?>Print/fpdf/<?= $value['Sale']['id']  ?>"><i class="fa fa-2x fa-print" aria-hidden="true"></i></a></span>&nbsp;&nbsp;&nbsp;
											<span><a target='_blank' href="<?= $this->webroot; ?>Sale/Sale/<?= $value['Sale']['id']  ?>"><i class="fa fa-2x fa-eye" aria-hidden="true"></i></a></span>
											<span> &nbsp;&nbsp;&nbsp;<a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Sale/DeleteInvoice/<?= $value['Sale']['id']; ?>"><i class="fa fa-2x fa-trash blue-col blue-col"></i></a></span>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div></div>
</section>
<script type="text/javascript">
	$.fn.table_search=function(){
		var from_date=$('#from_date').val();
		var branch_id=$('#branch_id').val();
		var to_date=$('#to_date').val();
		var data={
			to_date:to_date,
			branch_id:branch_id,
			from_date : from_date,
		};
		var url_address= '<?php echo $this->webroot; ?>'+'Sale/invoice_search_ajax';
		$.ajax({
			type: "post",
			url:url_address,
			data: data,
			dataType:'json',
			success: function(response) {
				$('#invoice_table').DataTable().destroy();
				$('#invoice_table tbody').empty();
				$.each(response,function(key,value){
					var invoice_table='<tr class="blue-pddng">';
					var date_of_order=value.Sale.date_of_order;
					var date_split = date_of_order.split('-');
					invoice_table+="<td hidden>"+value.Sale.id+"</td>";
					invoice_table+="<td>"+date_split[2]+'-'+date_split[1]+'-'+date_split[0]+"</td>";
					invoice_table+="<td>"+value.Executive.name+"</td>";
					invoice_table+="<td>"+value.AccountHead.name+' '+value.Customer.code+"</td>";
					invoice_table+="<td>"+value.Route.name+"</td>";
					invoice_table+="<td>"+value.Sale.invoice_no+"</td>";
					var total=parseFloat(value.Sale.grand_total);
					invoice_table+="<td>"+total+"</td>";
					invoice_table+="<td>"+get_status_name(value.Sale.status)+"</td>";
					invoice_table+="<td>";
					invoice_table+="<span><a target='_blank' href='<?= $this->webroot; ?>Print/fpdf/"+value.Sale.id+"'><i class='fa fa-2x fa-print' aria-hidden='true'></i></a></span>&nbsp;&nbsp;&nbsp;";
					invoice_table+="<span><a target='_blank' href='<?= $this->webroot; ?>Sale/Sale/"+value.Sale.id+"'><i class='fa fa-2x fa-eye' aria-hidden='true'></i></a></span>";
					invoice_table+="<span> &nbsp;&nbsp;&nbsp;<a onclick='return confirm('Are you sure?')' href='<?= $this->webroot; ?>Sale/DeleteInvoice/"+value.Sale.id+"'><i class='fa fa-2x fa-trash blue-col blue-col'></i></a></span>";
					invoice_table+="</td>";
					invoice_table+="</tr>";
					$('#invoice_table tbody').append(invoice_table);
				});
				$('#invoice_table').DataTable(
			    {
				"order": [[ 0, "desc" ]], //or asc 
				});
				$.fn.show_alert('Success');
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}

	$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
	function get_status_name(status)
	{
		var name='';
		if(status==0) { name='Cancelled'; }
		if(status==1) { name='Order Placed'; }
		if(status==2) { name='Order Delivered'; }
		return name;
	}
	$(document).on('change','.invoice-search',function(){
		$.fn.table_search();
	});
</script>