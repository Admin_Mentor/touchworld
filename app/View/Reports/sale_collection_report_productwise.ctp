
<style type="text/css">
.cls_label_all {
  padding-top: 5%;
}
.row_top_row{
  margin-top: 5%;
}
.deaf_btn_btn {
  margin-left: 4%;
}
.row_new_add{
  margin-top: 2%;
}
#radio_butto_add {
  margin-top: 7px;
  margin-left: -51%;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Sales Report(Product Wise)</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3">
                   <?php echo $this->Form->input('branch_id',array('type'=>'select', 'empty' =>'MAIN','options'=>$Branch,'class'=>'form-control select2 rec_select_box search_field','label'=>'Branch')) ?>
                 </div>
                 <div class="col-md-3">
                   <?php echo $this->Form->input('route_id',array('id'=>'route_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
                 </div>
                 <div class="col-md-3">
                   <?php echo $this->Form->input('executive_id',array('id'=>'executive_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'All')); ?>
                 </div>
                 <div class="col-md-3">
                  <?php echo $this->Form->input('product_type_id',array('type'=>'select','empty' =>'All','options'=>$Product_type,'class'=>'form-control select2','label'=>'Product Type')) ?>
                </div>
                <div class="col-md-3">
                  <?php echo $this->Form->input('brand_id',array('type'=>'select', 'empty' =>' All','options'=>$Brand,'class'=>'form-control select2 rec_select_box search_field','label'=>'Brand')) ?>
                </div>
                <div class="col-md-3">
                  <?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>' All','options'=>$Product,'class'=>'form-control select2 rec_select_box search_field','label'=>'Product')) ?>
                </div>

                <div class="col-md-2">
                 <?php echo $this->Form->input('from_date',array(
                  'type'=>'text',
                  'id'=>'from_date',
                  'value'=>$from,
                  'class'=>'form-control cls_label_all date_field date_picker
                  datepicker',
                  'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                  )); ?>
                </div>
                <div class="col-md-2">
                  <?php echo $this->Form->input('to_date',array(
                    'type'=>'text',
                    'id'=>'to_date',
                    'value'=>$to,
                    'class'=>'form-control cls_label_all date_field date_picker
                    datepicker',
                    'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                    )); ?>
                  </div>
                  <div class="col-md-2"><br>
                    <button class='btn' type='button' id='fetch_button'>Fetch</button>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="col-md-4 col-xs-12">
                  <h3 class="muted "></h3>
                </div>
                <table class="table table-condensed table boder" id='table_product_wise_list' >
                  <thead>
                    <tr class="blue-bg">
                     <th width="5%">Date</th>
                      <th width="15%">Executive</th>
                       <th width="10%">Customer Name</th>
                      <th width="8%">Route</th>
                      <th width="25%">Product</th>
                      <th class="" width="10%">Brand</th>
                       <th class="" width="5%">Quantity</th>
                      <th class="" width="5%">Net Amount</th>
                      <th class="" width="5%">Tax Amount</th>
                      <th class="" width="5%">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="6"><h4 style="font-size:20px; font-weight bold; color:#dd4b39;">Total</h4></td>
                      <td class="" style="font-size:20px; font-weight bold; color:#dd4b39;"></td>
                      <td class="" style="font-size:20px; font-weight bold; color:#dd4b39;"></td>
                      <!--  <td class="text-right" style="font-size:20px; font-weight bold; color:#dd4b39;"></td> -->
                      <td class="" style="font-size:20px; font-weight bold; color:#dd4b39;"></td>
                      <td class="" style="font-size:20px; font-weight bold; color:#dd4b39;"></td>

                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  //$('#table_product_wise_list').DataTable( );
  $.fn.product_type_change=function(product_type_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/product_type_select_ajax/'+product_type_id;
    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#product_id').empty();
       $('#product_id').append($("<option></option>").attr("value", '').text('All'));
       $.each(response, function(key, value) {
        $('#product_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
  $('#product_type_id').change(function(){
   // alert();
   var product_type_id=$(this).val();
   $.fn.product_type_change(product_type_id);
 });
   $(document).on('click','#fetch_button',function(){
    $('#table_product_wise_list').DataTable().destroy();
  $('#table_product_wise_list').DataTable( {
    "processing": false,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/SaleCollectionReportProductwiseAjax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.product_type_id= $('#product_type_id').val();
        d.to_date= $('#to_date').val();
        d.product_id= $('#product_id').val();
        d.branch_id=$('#branch_id').val();
        d.route_id=$('#route_id').val();
        d.executive_id=$('#executive_id').val();
        d.brand_id=$('#brand_id').val();
      },
      "dataSrc": "records",
    },
    dom: 'Bfrtip',

    lengthMenu: [
    [25, 50,100,-1],
    ['25 rows', '50 rows','100 rows','Show all' ]
    ],
    buttons: [
    { extend: 'colvis', },

    {
      extend: 'print',
      footer: true,
      customize: function ( win ) {
        $(win.document.body)
        .css( 'font-size', '10pt' )
        .prepend(
          '<h3 align="center">Sales Report Product Wise</h3>',
          '<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
          );
        $(win.document.body).find( 'table' )
        .addClass( 'compact' )
        .css( 'font-size', 'inherit' )
      }
    },
    { 
      extend: 'excel',
      footer:true,
     title:'Sales Report Product Wise'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
     
     //title:'Sales Report Product Wise',
     exportOptions: { columns: ':visible' } 
   },

    { extend: 'pdf'  ,
    footer:true,
     title:'Sales Report Product Wise'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      //title:'Sales Report Product Wise',
      
      exportOptions: { columns: ':visible' } }, 
    { extend: 'csv'  ,
    footer:true, 
    title:'Sales Report Product Wise'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
     //title:'Sales Report Product Wise',
     customize: function (csv) {
                 return "\tSales Report Product Wise\n"+"\t(Period : From "+$("#from_date").val()+"    To : "+$("#to_date").val()+")\n"+  csv ;
               },
     exportOptions: { columns: ':visible' } },
    'pageLength',
    ],
    "columns": [
    { "data" : "Sale.date_of_delivered" },
    { "data" : "Customer.name" },
    { "data" : "Executive.name" },
    { "data" : "Route.name" },
    { "data" : "Product.name" },
    { "data" : "Brand.name" },
    { "data" : "SaleItem.quantity" },
    { "data" : "SaleItem.unit_price" },

    { "data" : "SaleItem.tax_amount" },
    { "data" : "SaleItem.total" },
    ],

    "rowCallback": function( row, data ) {
    $.fn.show_alert('Success');

  },
    "footerCallback":function(row,data,start,end,display){
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      pageTotal=api.column(6,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(6).footer()).html(''+pageTotal+'');
      pageTotal=api.column(7,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(7).footer()).html(''+pageTotal+'');
      pageTotal=api.column(8,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(8).footer()).html(''+pageTotal+'');
       pageTotal=api.column(9,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(9).footer()).html(''+pageTotal+'');
      //pageTotal=api.column(6,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      //$(api.column(6).footer()).html(''+Math.round(pageTotal)+'');
    },
    "columnDefs": [
    // { className: "dt-body-right", "targets": [ 3 ] },
    // { className: "dt-body-right", "targets": [ 4 ] },
    // { className: "dt-body-right", "targets": [ 5 ] },
    // { className: "dt-body-right", "targets": [ 6 ] },
    //{ "visible": false, "targets": [ 8 ] },
    ],
  });
  table.fnDraw();
   });
  // $(document).on('click','#fetch_button',function(){
  //   table = $('#table_product_wise_list').dataTable();
  //   table.fnDraw();
  // });
  $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>
