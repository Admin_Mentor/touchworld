<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<section class="content-header">
  <h1>All Day Register Summary</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-2">
            <div class="col-md-12">
              <?php echo $this->Form->input('from_date',array(
                'type'=>'text',
                'id'=>'from_date',
                'class'=>'form-control search_field date_picker datepicker',
                'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                'label'=>'Date',
              )); ?>
            </div>
          </div>
          <div class="col-md-1"><br>
            <button class='btn btn-primary' type='button' id='fetch_button'>Fetch</button>
          </div>
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-4 col-xs-12">
          <h3 class="muted "></h3>
        </div>
        <table class="table table-condensed table boder no-footer" id='table_collection_list'>
          <thead>
            <tr class="blue-bg">
              <th style="width:15%">Executive</th>
              <th style="width:12%">Route</th>
              <th style="width:12%">Group</th>
              <th style="width:10%">Day Register Time</th>
              <th style="width:10%">Day Close Time</th>
              <th style="width:10%">Starting Km</th>
              <th style="width:10%">Ending Km</th>
              <th style="width:10%">CashSale</th>
              <th style="width:10%">CreditSale</th>
              <th style="width:10%">Sale Total</th>
              <th style="width:8%">Status</th>


            </tr>
          </thead>
          <tbody>
          </tbody>
          <!-- <tfoot>
          </tfoot> -->
        </table>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">

  $('#fetch_button').click(function(){
   // var customer_id=$('#customer_id').val();
   // var executive_id=$('#executive_id').val()
    var from_date=$('#from_date').val();
   // var to_date=$('#to_date').val();;
    var data={
    //  customer_id:customer_id,
    //  executive_id:executive_id,
      from_date:from_date,
     // to_date:to_date,
    };
//console.log(data);
//if(executive_id!=""){
  var url_address= "<?= $this->webroot; ?>Reports/AllDayRegisterReport";
  $.post( url_address,data, function( response ) {
    //console.log(response);
    $('#table_collection_list').DataTable().destroy();
   $('#table_collection_list tbody').empty();
   // $('#table_collection_list tfoot').empty();
    var total=0;
    $.each(response,function(key,value){

      //console.log(value);
      var tr='<tr>';
      tr+='<td>'+value.executive+'</td>';
      tr+='<td>'+value.route+'</td>';
      tr+='<td>'+value.customer_group+'</td>';
      tr+='<td>'+value.day_register_time+'</td>';
      tr+='<td>'+value.day_close_time+'</td>';
      tr+='<td>'+value.starting_km+'</td>';
      tr+='<td>'+value.ending_km+'</td>';
      tr+='<td align="right">'+value.cash_sale+'</td>';
      tr+='<td align="right">'+value.credit_sale+'</td>';
      tr+='<td align="right">'+value.total_sale+'</td>';
      if(value.status=='Day Registered'){
      tr+='<td style="color:blue" align="center">'+value.status+'</td>';
    }
    else if(value.status=='Day Closed'){
     tr+='<td style="color:#00FF00" align="center">'+value.status+'</td>';
}
else{
     tr+='<td style="color:red" align="center">'+value.status+'</td>';
}
     
tr+='</tr>';
//total+=parseFloat(value.grandtotal);
$('#table_collection_list tbody').append(tr);
});
$('#table_collection_list').DataTable({
                dom: 'Bfrtip',
                buttons: [
                'print','csv'
                ]
              });
  }, "json");
});
  $(document).ready(function(){
    var from_date=$('#from_date').val();
   // var to_date=$('#to_date').val();;
    var data={
    //  customer_id:customer_id,
    //  executive_id:executive_id,
      from_date:from_date,
     // to_date:to_date,
    };
  var url_address= "<?= $this->webroot; ?>Reports/AllDayRegisterReport";
  $.post( url_address,data, function( response ) {
    //console.log(response);
    $('#table_collection_list').DataTable().destroy();
   $('#table_collection_list tbody').empty();
   // $('#table_collection_list tfoot').empty();
    var total=0;
    $.each(response,function(key,value){
      var tr='<tr>';
      tr+='<td>'+value.executive+'</td>';
      tr+='<td>'+value.route+'</td>';
      tr+='<td>'+value.customer_group+'</td>';
      tr+='<td>'+value.day_register_time+'</td>';
      tr+='<td>'+value.day_close_time+'</td>';
      tr+='<td>'+value.starting_km+'</td>';
      tr+='<td>'+value.ending_km+'</td>';
      tr+='<td align="right">'+value.cash_sale+'</td>';
      tr+='<td align="right">'+value.credit_sale+'</td>';
      tr+='<td align="right">'+value.total_sale+'</td>';
      if(value.status=='Day Registered'){
      tr+='<td style="color:blue" align="center">'+value.status+'</td>';
    }
    else if(value.status=='Day Closed'){
     tr+='<td style="color:#00FF00" align="center">'+value.status+'</td>';
}
else{
     tr+='<td style="color:red" align="center">'+value.status+'</td>';
}
     
tr+='</tr>';
//total+=parseFloat(value.grandtotal);
$('#table_collection_list tbody').append(tr);
});
$('#table_collection_list').DataTable({
                dom: 'Bfrtip',
                buttons: [
                'print','csv'
                ]
              });
  }, "json");
  });
</script>
