<style type="text/css">
  <?php include "style.css" ?>
  .ad-stk{
    color: #FFF !important;
    background-color: #13689e !important;
    padding: 5px 20px !important;
    border: 0px solid #FFF !important;
    border-radius: 3px !important;
    transition: all ease .5s;
    margin-top: 20%;
  }
  .ad-stk:hover {
    background-color: #023a5e !important;
  }
  .list-arrows {
    padding-top: 100px;
  }
  .list-arrows button {
    margin-bottom: 20px;
  }
  .mr-tp-30 {
    margin-top: 30px;
  }
</style>
<section class="content-header">
  <h1> Stock Transfer </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('StockTransfer', array('url' => array('controller' => 'Stock', 'action' => 'StockTransfer')));?>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="row">
            <div class="form-group col-md-10">
              <?php echo $this->Form->input('warehouse_from',array('type'=>'select','empty'=>'Select Warehouse','value'=>'','options'=>$Warehouse,'class'=>'form-control select2 search_class','label'=>'From','id'=>'warehouse_from')) ?>                   
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10">
              <?php echo $this->Form->input('warehouse_to',array('type'=>'select','empty'=>'Select Warehouse','value'=>'','options'=>$Warehouse,'class'=>'form-control select2 search_class','label'=>'To','id'=>'warehouse_to')) ?>                
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="form-group col-md-12">
            <?= $this->Form->input('remarks', array('type' => 'textarea','class'=>"form-control",'rows'=>4)); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-md-offset-3">
          <div class="row">
            <div class="form-group col-md-10">
              <?= $this->Form->input('transfer_no',array('class'=>'form-control','label'=>'Transfer ID','type'=>'text','required','id'=>'transfer_no',)); ?>                 
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10">
              <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding boder">
            <table class="table table-hover" id="stock_transfer_tbl" >                    
              <thead>
                <tr  class="blue-bg">
                  <th>Slno</th>
                  <th>Barcode</th>
                  <th>Product</th>
                  <th>Unit</th>
                  <th>Current Quantity</th>
                  <th>Quantity To Move</th>
                  <th>Balance Quantity</th>
                  <th></th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $this->Form->input('barcode',   ['type'=>'text'  ,'id'=>'barcode','class'=>'form-control','label'=>false,'style'=>'width:100%',"tabindex"=>"7"]); ?></td>
                  <td>
                    <?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>'Select Product','class'=>'form-control select2 search_class','label'=>false,'id'=>'product_id','style'=>'width:100%')) ?> 
                  </td>
                  <td>
                    <?php echo $this->Form->input('product_unit',array('type'=>'select', 'empty' =>'Select Unit','class'=>'form-control select2 product_unit','label'=>false,'id'=>'product_unit','options'=>$Unit)) ?> 
                  </td>
                  <td><input class="form-control transfer-field current_quantity" id="current_quantity" type="text" readonly="readonly" >
                    <span id="hidden_unit_level" style="display:none;" class="hidden_unit_level"></span>
                    <span id="hidden_quantity" style="display:none;" class="hidden_quantity"></span>
                    <span id="hidden_no_of_pieces_per_unit" style="display:none;" class="hidden_no_of_pieces_per_unit"></span>
                  </td>
                  <td><input class="form-control transfer-field move_quantity" id="move_quantity" type="text" ><span id="hidden_move_quantity"  style="display:none;" class="hidden_move_quantity"></span></td>
                  <td><input class="form-control transfer-field balance_quantity" id="balance_quantity" type="hidden" readonly="readonly" class="balance_quantity">
                    <span id="hidden_balance_quantity" class="hidden_balance_quantity"></span></td>
                    <td><i id="add_transfer_product" class="fa fa-plus-circle fa-2x fnt-awsm-btn"></i></td>                         
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        <br/>
        <div class="row">
          <div class="modal-footer">
            <button type="submit" id='save_button' disabled class="btn btn-success">Save</button>
            <!-- <button type="button" class="btn btn-default print">Print</button> -->
          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    $.fn.button_disable=function(){
      var length=$('#stock_transfer_tbl tbody tr').length;
      if(length>0)
      {
        $('button[type="Submit"]').attr('disabled',false);
      }
      else
      {
        $('button[type="Submit"]').attr('disabled',true);
      }


    };
    $.fn.show_alert = function(flash)
    {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
    }
    shortcut.add("alt+s", function() {
      $.fn.button_disable();
      $('#save_button').click();
    });
    $('#StockTransferStockTransferForm').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
        e.preventDefault();
        return false;
      }
    });
    $('.transfer-field').keyup(function(e){

      if (e.keyCode == 13) 
      {
        $('#add_transfer_product').trigger('click');
        $('#product').focus();
        return false;
      }});
    $.fn.product_flitering=function(){
      $('#brand_in_display').val('');
      var warehouse_id=$('#warehouse_from').val();
      var product_type_id='';
      var brand_name_id='';
      var modal='';
      var data={
        warehouse_id:warehouse_id,
        product_type_id:product_type_id,
        brand_name_id:brand_name_id,
        modal:modal,
      };
      var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_list_get_ajax_search';
      $.ajax({
        type: "post",
        url:url_address,
        data: data,
        dataType:'json',
        success: function(response) {              
          $('#product_id').html(response.option);
          $('#product_id').val('');
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    }
    $(document).on('change','#warehouse_from',function(){
      $.fn.product_flitering();
    })
    $(document).on('change','#warehouse_from,#warehouse_to',function(){
      if($('#warehouse_from').val() == $('#warehouse_to').val())
      {
        $.fn.show_alert('Select Diffrent Warehouse');
        $('#warehouse_to').val('').trigger('change.select2');
        return false;
      }
      $('#stock_transfer_tbl tbody tr').closest('tr.product_tr').html('');
    });
    $(document).on('change','#product_id',function(){
      if(!$('#warehouse_from').val()){ $('#warehouse_from').select2('open'); $.fn.show_alert("Select 'From' warehouse"); $('#current_quantity').val(''); return false; }
      if(!$('#warehouse_to').val()){ $('#warehouse_to').select2('open'); $.fn.show_alert("Select 'To' warehouse"); $('#current_quantity').val(''); return false; }
      if(!$('#product_id').val()){ $('#product_id').select2('open'); $.fn.show_alert('Select Product'); $('#current_quantity').val(''); return false; }
      $('#current_quantity').val('');
      $('#hidden_balance_quantity').html('');
      var product_id=$(this).val();
      var warehouse_id=$('#warehouse_from').val();
      var unit_id=$('#product_unit').val();
      var data={
        warehouse_id:warehouse_id,
        product_id:product_id,
        unit_id:unit_id
      };
      var url_address= '<?php echo $this->webroot; ?>'+'Stock/get_product_quantity';
      $.post( url_address,data, function( response ) {
        if(response.Stock.quantity > 0){
          $('#hidden_no_of_pieces_per_unit').html(response.Product.no_of_piece_per_unit);
          $('#hidden_quantity').html(response.Stock.quantity);
          $('#product_unit').val(response.Unit.id).trigger('change');
          $('#move_quantity').focus();
        }
        else{
          $.fn.show_alert("Insufficient Stock");
          $('#product_id').select2('open');
        }

      }, "json");
    });
$(document).on('keyup','.move_quantity',function(){
  var old_quantity=$(this).closest('tr').find('td input.current_quantity').val(); 
  var transfer_unit=$(this).closest('tr').find('td select.product_unit').val();
  var unit_level= $(this).closest('tr').find('td:eq(4) span.hidden_unit_level').text(); 
  var piece_quantity=$(this).closest('tr').find('td:eq(4) span.hidden_quantity').text();  
  var no_of_piece_per_unit=$(this).closest('tr').find('td:eq(4) span.hidden_no_of_pieces_per_unit').text();
  var current_quantity = $(this).closest('tr').find('td input.current_quantity').val()? parseInt( $(this).closest('tr').find('td input.current_quantity').val()):0;
  var move_quantity=$(this).val();
  if(parseInt(current_quantity)  >=  parseInt(move_quantity)){
    if(unit_level == 2){
      var actual_move_quantity=move_quantity*no_of_piece_per_unit;    
      var str=old_quantity.split("/");     
      $(this).closest('tr').find('td:eq(6) span.hidden_balance_quantity').text(parseInt(current_quantity - move_quantity)+'/'+str[1]);
      $(this).closest('tr').find('td:eq(6) input.balance_quantity').val(parseInt(current_quantity - move_quantity)+'/'+str[1]);

    }else{
      var actual_move_quantity=move_quantity;
      var actual_balance_quantity=parseInt(piece_quantity - actual_move_quantity);
      $(this).closest('tr').find('td:eq(6) span.hidden_balance_quantity').text(parseInt(current_quantity - move_quantity));
      $(this).closest('tr').find('td:eq(6) input.balance_quantity').val(actual_balance_quantity);

    }

    $(this).closest('tr').find('td:eq(5) span.hidden_move_quantity').html(actual_move_quantity);
  }
  else{
    $(this).val(parseInt(current_quantity) );
    $(this).closest('tr').find('td:eq(6) input.balance_quantity').val(0);
    $(this).closest('tr').find('td:eq(6) span.hidden_balance_quantity').html(0);
  }
  $('#save_button').attr('disabled',true);
});
$(document).on('change','#product_unit',function(){
  var unit_id=$(this).val();
  var tr=$(this).closest('tr');
  var url='<?= $this->webroot."Purchase/GetUnitLevel/"; ?>'+unit_id;
  var stock_quantity=$('#hidden_quantity').text();
  var hidden_no_of_pieces_per_unit=$('#hidden_no_of_pieces_per_unit').text();
  $.post(url, function(result){
    if(result.Unit.level == 1){
      var total_quantity=Math.floor(stock_quantity);
    }else{
      if(result.Unit.level ==2){
        var quantity=stock_quantity/hidden_no_of_pieces_per_unit;
        if(quantity < 1){
          var fraction=stock_quantity;
          var whole=0;
        }else{
          var whole = Math.floor(quantity); 
          var fraction = stock_quantity%hidden_no_of_pieces_per_unit; 
        }
        var total_quantity=whole+'/'+Math.floor(fraction);
      }
    }
    $('#current_quantity').val(total_quantity);
    $('#hidden_unit_level').html(result.Unit.level);
    $('#move_quantity').val('');
     $('#hidden_balance_quantity').html('');
     $('#move_quantity').focus();
  },'json');
});

$(document).on('click','#add_transfer_product',function(){
  var tablecount=$("#stock_transfer_tbl tbody tr").length;
  var product_text=$('#product_id option:selected').text();
  var product_barcode=$('#barcode').val();
  var move_quantity = $('#move_quantity').val()? $('#move_quantity').val(): 0;
  var piece_move_quantity=$('#hidden_move_quantity').text();
  var unit_text=$('#product_unit option:selected').text();
  var unit_level= $(this).closest('tr').find('td:eq(4) span.hidden_unit_level').text(); 
  var piece_quantity=$(this).closest('tr').find('td:eq(4) span.hidden_quantity').text(); 
  var no_of_piece_per_unit=$(this).closest('tr').find('td:eq(4) span.hidden_no_of_pieces_per_unit').text();
  var hidden_move_quantity= $(this).closest('tr').find('td:eq(5) span.hidden_move_quantity').text();
  var hidden_balance_quantity = $(this).closest('tr').find('td:eq(6) span.hidden_balance_quantity').text();
  var unit_id=$('#product_unit').val();
  var product_id = $('#product_id').val();
  var ProductExist = 0;
  if(!product_id){
    $('#product_id').select2('open');
    $.fn.show_alert("Select Product");
    return false;
  }
  if(move_quantity == 0){
    $('#move_quantity').focus();
    $.fn.show_alert("Enter Quantity");
    return false;
  }
  $("#stock_transfer_tbl tbody tr").each(function () {
    var productId = $(this).closest('tr').find('td input.productsrow').val();
    var table_product_unit = $(this).closest('tr').find('td:eq(2) input:eq(0)').val();;
    if (product_id== productId) {
//if(table_product_unit==unit_id){
  ProductExist = 1;
  return false;
//}
}
});
  if(ProductExist)
  {
    $.fn.show_alert("This Product already in Cart");
    $('#product_id').select2('open');
    return false;
  }
  var current_quantity=$('#current_quantity').val();
  var balance_quantity=$('#hidden_balance_quantity').text();
  var slno=parseInt(tablecount)+parseInt(1);
  var remove_button='<i class="fa fa-minus-circle fa-2x fnt-awsm-btn remove_button"></i>';
  var product_barcode_field='<input type="text" class="form-control" readonly value="'+product_barcode+'">'+'';
  var product_field='<input type="hidden" name="data[StockTransfer][product][]" value="'+product_id+'" class="productsrow"><input type="text" class="form-control" readonly value="'+product_text+'">'+'';
  var move_quantity_field='<input type="hidden" name="data[StockTransfer][quantity][]" class="form-control"  value="'+hidden_move_quantity+'"><input type="text" class="form-control move_quantity"   value="'+move_quantity+'"><span style="display: none;" class="hidden_move_quantity">'+hidden_move_quantity+'</span>'+'';
  var current_quantity_field='<input type="text" class="form-control current_quantity" readonly value="'+current_quantity+'">\
  <span style="display:none;" class="hidden_unit_level">'+unit_level+'</span>\
  <span style="display:none;" class="hidden_quantity">'+piece_quantity+'</span>\
  <span style="display:none;" class="hidden_no_of_pieces_per_unit">'+no_of_piece_per_unit+'</span>'+'';
  var balance_quantity_field='<input type="text" class="form-control balance_quantity" readonly value="'+balance_quantity+'"><span style="display:none;" class="hidden_balance_quantity">'+hidden_balance_quantity+'</span>'+'';
  var unit='<input type="hidden" name="data[StockTransfer][unit][]" value="'+unit_id+'"><input type="text" class="form-control" readonly value="'+unit_text+'">'+'';
  $('#stock_transfer_tbl tbody').prepend('<tr class="product_tr product_'+product_id+'">\
    <td>'+slno+'</td>\
    <td>'+product_barcode_field+'</td>\
    <td>'+product_field+'</td>\
    <td>'+unit+'</td>\
    <td>'+current_quantity_field+'</td>\
    <td>'+move_quantity_field+'</td>\
    <td>'+balance_quantity_field+'</td>\
    <td>'+remove_button+'</td>\
  </tr>');
  $('.transfer-field').val('');
// $('#product_id').val('').change();
//$('#product_id').select2('open');
$('#barcode').val('');
$('#product_id').val('').trigger('change.select2');
$('#product_unit').val('').trigger('change.select2');
if(product_barcode) {
    $('#barcode').focus();
  } else {
    $('#product_id').select2('open');
  }
// $('#save_button').removeAttr('disabled');
$('#save_button').attr('disabled',false);
$('#hidden_balance_quantity').html('');
});
$(document).on('click','.remove_button',function(){
  $(this).closest('tr').remove();
  $('#save_button').attr('disabled',true);
});

$(document).on('click','body',function(){

  $.fn.button_disable();
})
$(document).on('keyup','#barcode',function(){
  var barcode=$('#barcode').val();
  $.post( "<?= $this->webroot ?>Product/get_product_by_barcode/"+barcode ,function( data ) {
    if(data.result=='Success')
    {
//$("#product").select2().find('option').remove();
// $('#product_id').append('<option>Select</option>');
// $('#product_id').append('<option value='+data.Product.id+' >'+data.Product.name+'</option>');
$('#product_id').val(data.Product.id).change();
}
else
{
  $('#product_id').val('').change();
  return false;
}
}, "json");
});
</script>
