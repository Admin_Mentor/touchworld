<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Reports');
/**
* Homes Controller
*/
class HomesController extends AppController {
/**
* Scaffold
*
* @var mixed
*/
public $uses=[
'Type',
'MasterGroup',
'Group',
'SubGroup',
'AccountHead',
'Sale',
'Purchase',
//'Quotation',
'Branch',
'Product',
'Stock',
'SaleItem',
'Journal',
'Customer',
];
public function Dashboard()
{

// 	$countOFF=$this->Sale->find('count',array('conditions'=>array('status=1','flag=1')));
// 	$countP=$this->Purchase->find('count',array('conditions'=>array('Purchase.status=1','Purchase.flag=1')));
// 	$from = date('Y-m-d',strtotime('first day of this month'));
// 	$to = date('Y-m-d',strtotime('last day of this month'));
// 	$this->SaleItem->virtualFields = array(
// 		'total_net_value' => "SUM(SaleItem.net_value)",
// 		'total_sale' => "SUM(SaleItem.total)",
// 		); 
// 	$conditions=[];
// 	$conditions_warehouse=[];
// 	$user_branch_id=$this->Session->read('User.branch_id');
// 	if($user_branch_id)
// 	{
// 		$branch_warehouse=$this->Branch->findById($user_branch_id);
// 		$conditions['Sale.branch_id']=$user_branch_id;
// 		$conditions_warehouse['Stock.warehouse_id']=$branch_warehouse['Branch']['warehouse_id'];;
// 	}
// 	$conditions['Sale.status']=2;
// 	$conditions['Sale.date_of_delivered between ? and ?']=array($from,$to);
// 	$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
// 		$sales_offline_total=$this->Sale->find('all',array('fields'=>array('id','grand_total','date_of_delivered','discount_amount','invoice_no'),
// 			'conditions'=> $conditions));
// // pr($sales_offline_total);exit;
// 		$sale_total=0;
// 		foreach ($sales_offline_total as $key => $value) {
// 			$date2=$value['Sale']['date_of_delivered'];
// 			$SaleItem=$this->SaleItem->find('all',[
// 				'conditions'=>array(
// 					'Sale.id'=>$value['Sale']['id'],
// 				),
// 				'fields'=>array(
// 					'SaleItem.net_value',
// 				),
// 			]);
//           $sale=0;
// 			foreach ($SaleItem as $key => $value1) {
// 				 $sale+=$value1['SaleItem']['net_value'];
// 			}
// 			$sale=$sale+$value['Sale']['discount_amount'];
// 			$sale_total+=$sale;
// 		}
// 	$SaleItem=$this->SaleItem->find('first',array(
// 		'conditions'=>$conditions,

// 		'fields'=>array('total_net_value','total_sale'),
// 		));
// 	$this->Sale->virtualFields = array(
// 		'total_grand_amount' => "SUM(Sale.grand_total)",
// 		);
// 	$Sale=$this->Sale->find('first',array(
// 		'conditions'=>$conditions,
// 		'fields'=>array('total_grand_amount'),
// 		));
// 	$net_amount=floatval($sale_total?$sale_total:0);
// 	//$net_amount=floatval($Sale['Sale']['total_grand_amount']?$Sale['Sale']['total_grand_amount']:0);
// 	$Customer=$this->Customer->find('list',array(
// 		'fields'=>[
// 		'Customer.account_head_id',
// 		'Customer.account_head_id'
// 		]
// 		));
// 	$collection=0;
// 	foreach ($Customer as $key => $value) {
// 		$collection+=$this->get_collection_amount($key,$from,$to);
// 	}
// 	$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchasedItem','PurchaseReturnItem','StockLog','Stock','UnwantedList')));
// 	$StockData=$this->Product->find('all',array(
// 		"joins"=>array(
// 			array(
// 				"table"=>'stocks',
// 				"alias"=>'Stock',
// 				"type"=>'inner',
// 				"conditions"=>array('Product.id=Stock.product_id'),
// 				),
// 			),
// 		'conditions'=>$conditions_warehouse,
// 		'fields'=>array(
// 			'Stock.quantity',
// 			'Product.cost',
// 			'Product.mrp',
// 			)
// 		));
	
// 	$line_total=0;
// 	foreach ($StockData as $key => $value) {
// 		$line_total+=$value['Stock']['quantity']*$value['Product']['cost'];
// 	}
// 	$totalPending_count=$countOFF;
 	$ReportsController = new ReportsController;
 	//$Sales_month=$ReportsController->SalesBar($user_branch_id);
 	//$Purchase_month=$ReportsController->PurchaseBarGraph();
 	$BrandGraph=$ReportsController->BrandGraph();
	//$pieChart=$ReportsController->pieChart($user_branch_id);
// 	//$expensegraph=$ReportsController->expensegraph($user_branch_id);
 	//$this->set('Sales_month',$Sales_month);
 	//$this->set('Purchase_month',$Purchase_month);
 	$this->set('BrandGraph',$BrandGraph);
 	//$this->set('pieChart',$pieChart);
// 	$this->set('Pending_Saleorders',$totalPending_count);
// 	$this->set('Pending_Purchaseorders',$countP);
// 	$this->set('net_amount',ROUND($net_amount));
// 	$this->set('collection',ROUND($collection));
// 	$this->set('stockcost',ROUND($line_total));
	//$this->set('expense',ROUND($expensegraph));
}
public function Dashboard_Table_ajax() {
	$countOFF=$this->Sale->find('count',array('conditions'=>array('status=1','flag=1')));
	$countP=$this->Purchase->find('count',array('conditions'=>array('Purchase.status=1','Purchase.flag=1')));
	$from = date('Y-m-d',strtotime('first day of this month'));
	$to = date('Y-m-d',strtotime('last day of this month'));
	$this->SaleItem->virtualFields = array(
		'total_net_value' => "SUM(SaleItem.net_value)",
		'total_sale' => "SUM(SaleItem.total)",
		); 
	$conditions=[];
	$conditions_warehouse=[];
	$user_branch_id=$this->Session->read('User.branch_id');
	if($user_branch_id)
	{
		$branch_warehouse=$this->Branch->findById($user_branch_id);
		$conditions['Sale.branch_id']=$user_branch_id;
		$conditions_warehouse['Stock.warehouse_id']=$branch_warehouse['Branch']['warehouse_id'];;
	}
	$conditions['Sale.status']=2;
	$conditions['Sale.date_of_delivered between ? and ?']=array($from,$to);
	$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$sales_offline_total=$this->Sale->find('all',array('fields'=>array('id','grand_total','date_of_delivered','discount_amount','invoice_no'),
			'conditions'=> $conditions));
// pr($sales_offline_total);exit;
		$sale_total=0;
		foreach ($sales_offline_total as $key => $value) {
			$date2=$value['Sale']['date_of_delivered'];
			$SaleItem=$this->SaleItem->find('all',[
				'conditions'=>array(
					'Sale.id'=>$value['Sale']['id'],
				),
				'fields'=>array(
					'SaleItem.net_value',
				),
			]);
          $sale=0;
			foreach ($SaleItem as $key => $value1) {
				 $sale+=$value1['SaleItem']['net_value'];
			}
			$sale=$sale+$value['Sale']['discount_amount'];
			$sale_total+=$sale;
		}
	$SaleItem=$this->SaleItem->find('first',array(
		'conditions'=>$conditions,

		'fields'=>array('total_net_value','total_sale'),
		));
	$this->Sale->virtualFields = array(
		'total_grand_amount' => "SUM(Sale.grand_total)",
		);
	$Sale=$this->Sale->find('first',array(
		'conditions'=>$conditions,
		'fields'=>array('total_grand_amount'),
		));
	$net_amount=floatval($sale_total?$sale_total:0);
	//$net_amount=floatval($Sale['Sale']['total_grand_amount']?$Sale['Sale']['total_grand_amount']:0);
	$Customer=$this->Customer->find('list',array(
		'fields'=>[
		'Customer.account_head_id',
		'Customer.account_head_id'
		]
		));
	$collection=0;
	foreach ($Customer as $key => $value) {
		$collection+=$this->get_collection_amount($key,$from,$to);
	}
	$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchasedItem','PurchaseReturnItem','StockLog','Stock','UnwantedList')));
	$StockData=$this->Product->find('all',array(
		"joins"=>array(
			array(
				"table"=>'stocks',
				"alias"=>'Stock',
				"type"=>'inner',
				"conditions"=>array('Product.id=Stock.product_id'),
				),
			),
		'conditions'=>$conditions_warehouse,
		'fields'=>array(
			'Stock.quantity',
			'Product.cost',
			'Product.mrp',
			)
		));
	
	$line_total=0;
	foreach ($StockData as $key => $value) {
		$line_total+=$value['Stock']['quantity']*$value['Product']['cost'];
	}
	$totalPending_count=$countOFF;
	$ReportsController = new ReportsController;
	$Sales_month=$ReportsController->SalesBar($user_branch_id);
	$Purchase_month=$ReportsController->PurchaseBarGraph();
	//$BrandGraph=$ReportsController->BrandGraph();
	$pieChart=$ReportsController->pieChart($user_branch_id);
    $expensegraph=$ReportsController->expensegraph($user_branch_id);
    $Data['expense']= $expensegraph;
    $Data['stockcost']= ROUND($line_total);
    $Data['net_amount']= $net_amount;
    $Data['collection']= $collection;
    $Data['pieChart']= $pieChart;
    //$Data['Sales_month']= $Sales_month;
   $Data['jan']=$Sales_month[0]['jan'];
  $Data['feb']=$Sales_month[0]['feb'];
 $Data['mar']=$Sales_month[0]['mar'];
   $Data['apr']=$Sales_month[0]['apr'];
  $Data['may']=$Sales_month[0]['may'];
  $Data['jun']=$Sales_month[0]['jun'];
  $Data['jul']=$Sales_month[0]['jul'];
 $Data['aug']=$Sales_month[0]['aug'];
  $Data['sep']=$Sales_month[0]['sep'];
  $Data['oct']=$Sales_month[0]['oct'];
  $Data['nov']=$Sales_month[0]['nov'];
 $Data['dec']=$Sales_month[0]['dec'];

 $Data['janP']=$Purchase_month[0]['jan'];
  $Data['febP']=$Purchase_month[0]['feb'];
 $Data['marP']=$Purchase_month[0]['mar'];
   $Data['aprP']=$Purchase_month[0]['apr'];
  $Data['mayP']=$Purchase_month[0]['may'];
  $Data['junP']=$Purchase_month[0]['jun'];
  $Data['julP']=$Purchase_month[0]['jul'];
 $Data['augP']=$Purchase_month[0]['aug'];
  $Data['sepP']=$Purchase_month[0]['sep'];
  $Data['octP']=$Purchase_month[0]['oct'];
  $Data['novP']=$Purchase_month[0]['nov'];
 $Data['decP']=$Purchase_month[0]['dec'];
     	//$this->set('Sales_month',$Sales_month);
// 	$this->set('Purchase_month',$Purchase_month);
// 	$this->set('BrandGraph',$BrandGraph);
// 	$this->set('pieChart',$pieChart);
// 	$this->set('Pending_Saleorders',$totalPending_count);
// 	$this->set('Pending_Purchaseorders',$countP);
// 	$this->set('net_amount',ROUND($net_amount));
// 	$this->set('collection',ROUND($collection));
// 	$this->set('stockcost',ROUND($line_total));
	//$this->set('expense',ROUND($expensegraph));
    //$this->set('expense',ROUND($expensegraph));
    echo json_encode($Data);
	exit;
}
public function get_collection_amount($account_head_id,$from_date=null,$to_date=null) {
	$conditions=array();
	if(!empty($from_date)){
		$conditions['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
	}
	$user_branch_id=$this->Session->read('User.branch_id');
	if($user_branch_id)
	{
		$conditions['Journal.branch_id']=$user_branch_id;
	}
	$conditions['Journal.credit']=$account_head_id;
	$conditions['AccountHeadDebit.sub_group_id']=['1','2'];
	$conditions['Journal.flag']=1;
	$this->Journal->virtualFields = array( 'total_amount' => "SUM(Journal.amount)" );
	$Journal=$this->Journal->find('first',array('conditions'=>$conditions,
		'fields'=>array('Journal.total_amount')
		));
	$sale_total_amount=0;
	if($Journal['Journal']['total_amount'])
	{
		$sale_total_amount=floatval($Journal['Journal']['total_amount']);
	}
	return $sale_total_amount;
}
public function Master()
{	
}
}
