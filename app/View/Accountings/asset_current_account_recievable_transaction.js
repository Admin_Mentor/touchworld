$(document).on('change','#customer_type',function(){
  var customer_type_id=$(this).val();
  $.post( "<?= $this->webroot ?>Customer/get_customer_by_customer_type_ajax/"+customer_type_id ,function( data ) {
    $('#account_head').empty();
      $('#account_head').append($("<option></option>").attr("value",'').text('All'));
    $.each(data.options,function(key,value){
      $('#account_head').append($("<option></option>").attr("value",key).text(value));
    });
    $('#account_head').change();
  }, "json");
});
$(document).on('click','#sale_print',function(){
  var account_holder_name=$('#account_holder_name').text();
  var from_date=$('#from_date').val();
  var to_date=$('#to_date').val();
  var data={
    account_head_name:account_holder_name,
  }
  $.post( "<?= $this->webroot ?>Reports/sales_print_ajax",data ,function( data ) {
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
    url='<?= $this->webroot."Reports/SaleDetailFunction/"; ?>'+data.id+'/'+from_date+'/'+to_date;
    window.open(url);
  }, "json");
});

$(document).on('click','#ledger_pdf',function(){
  var account_head=$('#account_head').val();
if(account_head!=""){
    url='<?= $this->webroot."Accountings/ledger_print_ajax/"; ?>'+account_head;
    window.open(url);
  }
  else{
    alert("select Accountname");
  }
  
});
