
<style type="text/css">
  
.scroll{
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
}

.scroll tbody{
  display:block;
  width: 100%;
  overflow: auto;
  height: 300px;
}

.scroll thead tr {
   display: block;
}

.scroll thead {
  background: black;
  color:#fff;
}
.scroll th {
   width: 100%;
 

}
.scroll td {
  padding: 5px;
  text-align: left;
  width: 80%;
}

</style>
<section class="content-header">
  <h1> Customer Settings </h1>
</section>
<section class="content">
  <div class="row">
    <?php echo $this->Form->create('settings', ['type' => 'file','class'=>'form-horizontal','id'=>'Setting_Form']); ?>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header">
          <div class="row-wrapper">
            <div class="row">
              <div class="col-md-12">
                <div class="test" style="margin-top: 2%;">
              
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <?= $this->Form->input('timezone_city',array('type'=>'select','id'=>'timezone_city','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>$Timezone_cities_list,'label'=>'Timezone City','required'=>'required')); ?>
                  </div> 
                
                  
                </div>
                <div class="col-md-6">
                  <table class="boder table table-condensed table scroll" data-order='[[ 5, "asc" ]]' data-page-length='25'>
                    <thead>
                      <tr class="blue-bg">
                        <th>Option</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- <tr>
                        <td><h4>Barcode</h4></td>
                        <td><?php  $attributes = array( 'legend' => false); echo $this->Form->radio('barcode', $options, $attributes); ?></td>
                      </tr>
                      <tr>
                        <td><h4>Big Data</h4></td>
                        <td><?php  $attributes = array( 'legend' => false); echo $this->Form->radio('big_data', $options, $attributes); ?></td>
                     
                    
                      </tr> -->
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <button class="btn btn-success btn_cstm_style pull-right">Save</button>
          </div>
        </div>
      </div>
    </div>
    <?= $this->Form->end(); ?>
  </div>
</section>
<script>
</script>
