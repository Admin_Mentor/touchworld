<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<style type="text/css">
.number_format_alignclass="text-right"  {
  text-align:right;
}
</style>

<section class="content-header">
  <h1>VAT REPORT</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body"> 
      <?= $this->Form->create('TaxReport'); ?>
      <div class="col-md-12">
        <div class="row">
          <ul class="nav nav-pills" role="tablist">
            <li class="nav-item active">
              <a class="nav-link active" id="Purchase-tab" data-toggle="tab" href="#Purchase" role="tab" aria-controls="Purchase" aria-selected="true">PURCHASE</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="notax_purchase_tab" data-toggle="tab" href="#Notaxpurchase" role="tab" aria-controls="Sale" aria-selected="false">ZERO TAX PURCHASE</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="Sale-tab" data-toggle="tab" href="#Sale" role="tab" aria-controls="Sale" aria-selected="false">SALE</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" id="Expense-tab" data-toggle="tab" href="#Expense" role="tab" aria-controls="Expense" aria-selected="false">EXPENSE VAT</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="ZeroVatExpense-tab" data-toggle="tab" href="#ZeroVatExpense" role="tab" aria-controls="Expense" aria-selected="false">ZERO TAX EXPENSE</a>
            </li>
          </ul>
        </div>
        <div class="tab-content"><br>
          <div class="tab-pane active" id="Purchase" role="tabpanel" aria-labelledby="Purchase-tab">
            <div class="col-md-3 col-md-offset-4">
             <?php echo $this->Form->input('date',array('type'=>'text','id'=>'date-Purchase','class'=>'form-control date_range_picker','value'=>"01-01-2018 / 01-15-2018")); ?>
           </div>
           <div class="col-md-1"><br>
            <button type="button" value="Purchase" class="btn btn-primary get_button">Fetch</button>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="box-body table-responsive no-padding"><br>
                <table id='table_list-Purchase' style="width: 100% !important" class="table table-hover boder text-center table-bordered"  >
                  <thead>
                    <tr class="blue-bg">
                      <th>#</th>
                      <th>PURCHASE DATE</th>
                      <th>INV.No</th>
                      <th>INV.DATE</th>
                      <th width="30%"> SUPPLIER NAME </th>
                      <th>PLACE </th>
                    <th>VAT NO:</th>
                      <!-- <th>GSTIN</th> -->
<!--                       <th>STATE</th>
 -->                      <th>NET AMOUNT</th>
                      <th>DISCOUNT</th>
                      <th>TAXABLE AMOUNT</th>
                       <th>VAT</th>
                      <!-- <th>SGST</th>
                      <th>CGST</th>
                      <th>IGST</th>
                      <th>INSURANCE</th>
                      <th>MISCELLANIOUS</th>-->
                      <th>TOTAL</th> 
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
          <div class="tab-pane" id="Notaxpurchase" role="tabpanel" aria-labelledby="notax_purchase_tab">
          <div class="col-md-3 col-md-offset-4">
           <?php echo $this->Form->input('date',array('type'=>'text','id'=>'zero_purchase','class'=>'form-control date_range_picker','value'=>"01-01-2018 / 01-15-2018")); ?>
         </div>
         <div class="col-md-1"><br>
          <button type="button" value='Notaxpurchase' class="btn btn-primary get_button">Fetch</button>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="box-body table-responsive no-padding"><br>
                <table id='table_list-Notaxpurchase' style="width: 100% !important" class="table table-hover boder text-center table-bordered"  >
               <thead>
                    <tr class="blue-bg">
                      <th>#</th>
                      <th>PURCHASE DATE</th>
                      <th>INV.No</th>
                      <th>INV.DATE</th>
                      <th width="30%"> SUPPLIER NAME </th>
                      <th>PLACE </th>
                    <th>VAT NO:</th>
                      <!-- <th>GSTIN</th> -->
<!--                       <th>STATE</th>
 -->                      <th>NET AMOUNT</th>
                      <th>DISCOUNT</th>
                      <th>TAXABLE AMOUNT</th>
                       <th>VAT</th>
                      <!-- <th>SGST</th>
                      <th>CGST</th>
                      <th>IGST</th>
                      <th>INSURANCE</th>
                      <th>MISCELLANIOUS</th>-->
                      <th>TOTAL</th> 
                    </tr>
                  </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
        <div class="tab-pane" id="Sale" role="tabpanel" aria-labelledby="Sale-tab">
          <div class="col-md-3 col-md-offset-4">
           <?php echo $this->Form->input('date',array('type'=>'text','id'=>'date-Sale','class'=>'form-control date_range_picker','value'=>"01-01-2018 / 01-15-2018")); ?>
         </div>
         <div class="col-md-1"><br>
          <button type="button" value='Sale' class="btn btn-primary get_button">Fetch</button>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="box-body table-responsive no-padding"><br>
                <table id='table_list-Sale' style="width: 100% !important" class="table table-hover boder text-center table-bordered"  >
                  <thead>
                  <tr class="blue-bg">
                    <th>#IO</th>
                    <th>INV.No</th>
                    <th width="15%">SALE DATE</th>
                    <th width="15%">CUSTOMER</th>
                    <th>PLACE </th>
                    <th>VAT NO:</th>
<!--                     <th>STATE</th>
 -->                   <th>NET AMOUNT</th>
                      <th>DISCOUNT</th>
                    <th>TAXABLE AMOUNT</th>
                    <th>VAT</th>
                    <!-- <th>CGST</th>
                    <th>SGST</th>
                    <th>IGST</th> -->
                    <th>TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="Expense" role="tabpanel" aria-labelledby="Expense-tab">
          <div class="col-md-3 col-md-offset-4">
           <?php echo $this->Form->input('date',array('type'=>'text','id'=>'date-Expense','class'=>'form-control date_range_picker','value'=>"01-01-2018 / 01-15-2018")); ?>
         </div>
         <div class="col-md-1"><br>
          <button type="button" value='Expense' class="btn btn-primary get_button">Fetch</button>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="box-body table-responsive no-padding"><br>
                <table id='table_list-Expense' style="width: 100% !important" class="table table-hover boder text-center table-bordered"  >
                  <thead>
                  <tr class="blue-bg">
                   <th>#</th>
                    <th>Date</th>
                    <th width="15%">TRN Number</th>
                    <th width="15%">Supplier Name</th>
                    <th>Account Name</th>
                     <th>Invoice Number</th>
                    <th>Amount</th>
                    <th>VAT</th>
                    <th>Total Amount</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
       <div class="tab-pane" id="ZeroVatExpense" role="tabpanel" aria-labelledby="ZeroVatExpense-tab">
          <div class="col-md-3 col-md-offset-4">
           <?php echo $this->Form->input('date',array('type'=>'text','id'=>'date-ZeroVatExpense','class'=>'form-control date_range_picker','value'=>"01-01-2018 / 01-15-2018")); ?>
         </div>
         <div class="col-md-1"><br>
          <button type="button" value='ZeroVatExpense' class="btn btn-primary get_button">Fetch</button>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="box-body table-responsive no-padding"><br>
                <table id='table_list-ZeroVatExpense' style="width: 100% !important" class="table table-hover boder text-center table-bordered"  >
                  <thead>
                  <tr class="blue-bg">
                   <th>#</th>
                    <th>Date</th>
                    <th width="15%">TRN Number</th>
                    <th width="15%">Supplier Name</th>
                    <th>Account Name</th>
                     <th>Invoice Number</th>
                    <th>Amount</th>
                    <th>VAT</th>
                    <th>Total Amount</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?= $this->Form->end(); ?>
</div>
</section>
<script type="text/javascript">
  $('.get_button').click(function(){
    $.fn.Search($(this).val());
  });
  $.fn.Search=function(type){
    var date=$('#date-Purchase').val();
    if(type=='ZeroVatExpense')
    {
    var date=$('#date-ZeroVatExpense').val();

    }
    else if(type=='Expense')
      {
      var date=$('#date-Expense').val();

      }
    var tax_id=$('#tax_id').val();
    var data={
      date:date,
      type:type,
      tax:tax_id,
    };
    $.post( "<?= $this->webroot ?>Reports/TaxReportAjax",data ,function( data ) {
      $('#table_list-'+type).DataTable().destroy();
      $('#table_list-'+type+' tbody').empty();
      $('#table_list-'+type+' tfoot').empty();
      if(type=='Purchase')
      {
        $.each(data.body,function(key,value){
          var tr_body='<tr>';
          tr_body+='<td>'+key+'</td>';
          tr_body+='<td>'+value.date+'</td>';
          tr_body+='<td>'+value.invoice_no+'</td>';
          tr_body+='<td>'+value.invoice_date+'</td>';
          tr_body+='<td>'+value.name+'</td>';
          tr_body+='<td>'+value.place+'</td>';
          tr_body+='<td>'+value.gstin+'</td>';
          //tr_body+='<td>'+value.gstin+'</td>';
         // tr_body+='<td>'+value.state+'</td>';
          tr_body+='<td class="dt-body-right">'+value.net_value+'</td>';
          tr_body+='<td class="dt-body-right">'+value.discount+'</td>';
          tr_body+='<td class="dt-body-right">'+value.taxable_amount+'</td>';
         tr_body+='<td class="dt-body-right">'+value.tax_amount+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.cgst+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.sgst+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.igst+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.insurance+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.miscellanious+'</td>';
          tr_body+='<td class="dt-body-right">'+value.total+'</td>';
          tr_body+='</tr>';
          $('#table_list-'+type+' tbody').append(tr_body);
        });
      }
      else if(type=='Notaxpurchase')
      {
       $.each(data.body,function(key,value){
          var tr_body='<tr>';
          tr_body+='<td>'+key+'</td>';
          tr_body+='<td>'+value.date+'</td>';
          tr_body+='<td>'+value.invoice_no+'</td>';
          tr_body+='<td>'+value.invoice_date+'</td>';
          tr_body+='<td>'+value.name+'</td>';
          tr_body+='<td>'+value.place+'</td>';
          tr_body+='<td>'+value.gstin+'</td>';
          //tr_body+='<td>'+value.gstin+'</td>';
         // tr_body+='<td>'+value.state+'</td>';
          tr_body+='<td class="dt-body-right">'+value.net_value+'</td>';
          tr_body+='<td class="dt-body-right">'+value.discount+'</td>';
          tr_body+='<td class="dt-body-right">'+value.taxable_amount+'</td>';
         tr_body+='<td class="dt-body-right">'+value.tax_amount+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.cgst+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.sgst+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.igst+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.insurance+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.miscellanious+'</td>';
          tr_body+='<td class="dt-body-right">'+value.total+'</td>';
          tr_body+='</tr>';
          $('#table_list-'+type+' tbody').append(tr_body);
        });
      }
      else if(type=='Expense')
      {
       $.each(data.body,function(key,value){
          var tr_body='<tr>';
          tr_body+='<td></td>';
          tr_body+='<td>'+value.date+'</td>';
          tr_body+='<td>'+value.invoice_number+'</td>';
          tr_body+='<td>'+value.supplier_name+'</td>';
          tr_body+='<td>'+value.expense+'</td>';
           tr_body+='<td>'+value.Invoice_number+'</td>';
          tr_body+='<td>'+value.amount+'</td>';
          tr_body+='<td>'+value.vat_amount+'</td>';
          tr_body+='<td>'+value.total+'</td>';
          tr_body+='</tr>';
          $('#table_list-'+type+' tbody').append(tr_body);
        });
      }
       else if(type=='ZeroVatExpense')
      {
       $.each(data.body,function(key,value){
          var tr_body='<tr>';
          tr_body+='<td></td>';
          tr_body+='<td>'+value.date+'</td>';
          tr_body+='<td>'+value.invoice_number+'</td>';
          tr_body+='<td>'+value.supplier_name+'</td>';
          tr_body+='<td>'+value.expense+'</td>';
           tr_body+='<td>'+value.Invoice_number+'</td>';
          tr_body+='<td>'+value.amount+'</td>';
          tr_body+='<td>'+value.vat_amount+'</td>';
          tr_body+='<td>'+value.total+'</td>';
          tr_body+='</tr>';
          $('#table_list-'+type+' tbody').append(tr_body);
        });
      }
      else
      {
        $.each(data.body,function(key,value){
          var tr_body='<tr>';
          tr_body+='<td>'+value.invoice_order+'</td>';
          tr_body+='<td>'+value.invoice_no+'</td>';
          tr_body+='<td>'+value.date+'</td>';
          tr_body+='<td>'+value.name+'</td>';
          tr_body+='<td>'+value.place+'</td>';
          tr_body+='<td>'+value.gstin+'</td>';
          // tr_body+='<td>'+value.state+'</td>';
           tr_body+='<td class="dt-body-right">'+value.net_value+'</td>';
          tr_body+='<td class="dt-body-right">'+value.discount+'</td>';
          tr_body+='<td class="dt-body-right">'+value.taxable_amount+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.cgst+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.sgst+'</td>';
          // tr_body+='<td class="dt-body-right">'+value.igst+'</td>';
          tr_body+='<td class="dt-body-right">'+value.vat+'</td>';
          tr_body+='<td class="dt-body-right">'+value.total+'</td>';
          tr_body+='</tr>';
          $('#table_list-'+type+' tbody').append(tr_body);
        });
      }
      var tr_foot='<tr>';
      if(type=='Purchase') 
      {
        tr_foot+='<td colspan="6"></td>'; 
        tr_foot+='<td colspan><h4><b>Total</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.net_value)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.discount)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.taxable_amount)+'</h4></b></td>';
          tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.tax_amount)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.cgst)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.sgst)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.igst)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.insurance)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.miscellanious)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.total)+'</h4></b></td>';
        tr_foot+='</tr>';
      }
      else if(type=='Notaxpurchase')
      {
       tr_foot+='<td colspan="6"></td>'; 
        tr_foot+='<td colspan><h4><b>Total</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.net_value)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.discount)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.taxable_amount)+'</h4></b></td>';
          tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.tax_amount)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.cgst)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.sgst)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.igst)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.insurance)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.miscellanious)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.total)+'</h4></b></td>';
        tr_foot+='</tr>';
      }
      else if(type=='Expense')
      {
       tr_foot+='<td colspan="6"></td>'; 
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.taxable_amount)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.tax_amount)+'</h4></b></td>';
       tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.total)+'</h4></b></td>';
        tr_foot+='</tr>';
      }
      else if(type=='ZeroVatExpense')
      {
       tr_foot+='<td colspan="6"></td>'; 
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.taxable_amount)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.tax_amount)+'</h4></b></td>';
       tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.total)+'</h4></b></td>';
        tr_foot+='</tr>';
      }
      else 
      {
        tr_foot+='<td colspan="5"></td>';
        tr_foot+='<td colspan><h4><b>Total</h4></b></td>';
         tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.net_value)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.discount)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.taxable_amount)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.cgst)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.sgst)+'</h4></b></td>';
        // tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.igst)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.vat)+'</h4></b></td>';
        tr_foot+='<td class="dt-body-right"><h4><b>'+parseFloat(data.foot.total)+'</h4></b></td>';
        tr_foot+='</tr>';
      }
      $('#table_list-'+type+' tfoot').append(tr_foot);
      $('#table_list-'+type).DataTable({
        dom: 'Bfrtip',
        buttons: [
        {
         extend: 'colvis',
       },
       {
         extend: 'csv',
          title:'VAT Report',

               footer: true,
               customize: function (csv) {
                 return "\tVAT Report\n"+  csv ;
               },
        // footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'excel',
         title:'VAT Report',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
        {
         extend: 'print',
         title:'VAT Report',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
        {
         extend: 'pdf',
         title:'VAT Report',
         pageSize: 'A4',
          orientation: 'landscape',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'pageLength',
       },
       ],
       "columnDefs": [{"targets": [ 0 ], "visible": false,"searchable": false}]
     });
        $.fn.show_alert('Success');
    }, "json");
}
$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
$('.main-sidebar').ready(function(){
  $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
});
</script>
<script>
  $('.date_range_picker').daterangepicker({
    opens: 'left',
    locale: { format: 'DD.MM.YYYY'}
  });
  $('.date_range_picker').on('blur', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });
  $('#date-Purchase').change(function(){
    $('#date-Sale').val($(this).val());
  });
   $('#zero_purchase').change(function(){
    $('#date-Purchase').val($(this).val());
  });
  $('#date-Sale').change(function(){
    $('#date-Purchase').val($(this).val());
  });
   $('#date-Expense').change(function(){
    $('#date-Expense').val($(this).val());
  });
   $('#date-ZeroVatExpense').change(function(){
    $('#date-ZeroVatExpense').val($(this).val());
  });
</script>
<script type="text/javascript">
  $('#table_list-Purchase').DataTable( {
    dom: 'Bfrtip',
    buttons: [
    //'colvis',
    //'csv', 
    //'excel',

    //'pageLength'
     {
         extend: 'colvis',
       },
       {
         extend: 'csv',
          title:'VAT Report',

               footer: true,
               customize: function (csv) {
                 return "\tVAT Report\n"+  csv ;
               },
        // footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'excel',
         title:'VAT Report',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'pageLength',
       },
    ],
    "columnDefs": [{"targets": [ 0 ], "visible": false,"searchable": false}]
  });
   $('#table_list-ZeroVatExpense').DataTable( {
    dom: 'Bfrtip',
    buttons: [
    //'colvis',
    //'csv', 
    //'excel',

    //'pageLength'
     {
         extend: 'colvis',
       },
       {
         extend: 'csv',
          title:'VAT Report',

               footer: true,
               customize: function (csv) {
                 return "\tVAT Report\n"+  csv ;
               },
        // footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'excel',
         title:'VAT Report',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'pageLength',
       },
    ],
    "columnDefs": [{"targets": [ 0 ], "visible": false,"searchable": false}]
  });
  $('#table_list-notax_purchase').DataTable( {
    dom: 'Bfrtip',
    buttons: [
    //'colvis',
    //'csv', 
    //'excel',

    //'pageLength'
     {
         extend: 'colvis',
       },
       {
         extend: 'csv',
          title:'VAT Report',

               footer: true,
               customize: function (csv) {
                 return "\tVAT Report\n"+  csv ;
               },
        // footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'excel',
         title:'VAT Report',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'pageLength',
       },
    ],
    "columnDefs": [{"targets": [ 0 ], "visible": false,"searchable": false}]
  });
  $('#table_list-Sale').DataTable( {dom: 'Bfrtip',buttons: [
    // 'colvis',
    // 'csv', 
    // 'excel',
    // 'pageLength'
    {
         extend: 'colvis',
       },
       {
         extend: 'csv',
          title:'VAT Report',

               footer: true,
               customize: function (csv) {
                 return "\tVAT Report\n"+  csv ;
               },
        // footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'excel',
         title:'VAT Report',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'pageLength',
       },
    ]
    ,"columnDefs": [{"targets": [ 0 ], "visible": false,"searchable": false}]
  });
$('#table_list-Expense').DataTable( {dom: 'Bfrtip',buttons: [
    // 'colvis',
    // 'csv', 
    // 'excel',
    // 'pageLength'
    {
         extend: 'colvis',
       },
       {
         extend: 'csv',
          title:'VAT Report',

               footer: true,
               customize: function (csv) {
                 return "\tVAT Report\n"+  csv ;
               },
        // footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'excel',
         title:'VAT Report',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'pageLength',
       },
    ]
    ,"columnDefs": [{"targets": [ 0 ], "visible": false,"searchable": false}]
  });

</script>