<?php
App::uses('AppController', 'Controller');
class WarehouseController extends AppController {
	public $helpers= array('Html','Form');
	public $uses=array('Product','Warehouse');
	public function warehouse_add_ajax()
	{
		$data=array(
			'name'=>strtoupper($this->request->data['modal_warehouse_name']),
			'description'=>$this->request->data['modal_warehouse_desc'],
			);
		$this->Warehouse->create();
		if($this->Warehouse->save($data))
		{
			$this->Session->setFlash('data is saved');
			$last_iserted_pt=$this->Warehouse->findById($this->Warehouse->getLastInsertId());
			echo "<option value='".$last_iserted_pt['Warehouse']['id']."'>".$last_iserted_pt['Warehouse']['name']." </option>";
		}
		exit;
	}
	public function edit_ajax(){
		try {
			$prdct_id=$this->request->data['prdct_id'];
			if(!$prdct_id)
				throw new Exception("Empty ProductType Id", 1);
			$prdct_typ=strtoupper(trim($this->request->data['prdct_typ']));
			$this->ProductType->id=$prdct_id;
			if(!$this->ProductType->saveField('name',$prdct_typ ))
				throw new Exception("Error Processing ProductType Name Updation", 1);
			if(!$this->ProductType->saveField('updated_at',date('Y-m-d H:i:s')))
				throw new Exception("Error Processing ProductType updated_at Updation", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function warehouse_type_delete_ajax($id)
	{
		try {
			$Product=$this->Product->findByProductTypeId($id);
			if($Product)
				throw new Exception("This is Used In ".$Product['Product']['name'], 1);
			if(!$this->ProductType->delete($id))
				throw new Exception("Error Processing Request While delete", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function warehouse_get_ajax($id)
	{
		try {
			$Warehouse=$this->Warehouse->findById($id);
			if(!$Warehouse)
				throw new Exception("Empty Warehouse", 1);
			$return['data']=$Warehouse['Warehouse'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function warehouse_edit_ajax()
	{
		try {
			$data=$this->request->data['WarehouseEdit'];
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				'updated_at'=>trim(date('Y-m-d h:i:s')),
				// 'code'=>trim(strtoupper($data['code'])),
				);
			$this->Warehouse->id=$data['id'];
			if(!$this->Warehouse->save($Table_data))
			{
				$errors = $this->Warehouse->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Warehouse=$this->Warehouse->read();
			$return['key']=$Warehouse['Warehouse']['id'];
			$return['value']=$Warehouse['Warehouse']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function delete()
	{
		$this->before_load();
		$id=$this->request->params['pass'][0];
		$Product=$this->Product->findByWarehouseIdFk($id);
		if(!empty($Product))
		{
			$this->Flash->set('product Type Is Used.');
			$this->redirect(array('controller'=>'Stock','action'=>'index'));
		}
		else
		{
			if($this->Warehouse->delete($id))
			{
				$this->Flash->set(__('product type deleted.',h($id)));
				$this->redirect(array('controller'=>'Stock','action'=>'index'));
			}
			else
			{
				$this->Flash->set(__('Error Occured'));
			}
		}
	}
	public function check_producttype_ajax(){
		$product_type=$this->request->data['prdct_type'];
		$result=$this->ProductType->find('all',array('conditions'=>array('ProductType.name'=>$product_type)));
		if(empty($result)){
			echo "No";
		}
		else{
			echo "Yes";
		}
		exit;
	}
	public function delete_ProductType_ajax($id)
	{
		$userid = $this->Session->read('User.id');
		if($this->ProductType->delete($id))
		{
			$return['result']="Success";
		}
		else
		{
			$return['result']="Error";
		}
		echo json_encode($return);
		exit;
	}
	
}