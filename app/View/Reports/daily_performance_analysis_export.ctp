<?php echo ""; ?><?php echo ","; ?>
<?php echo "FabTrading "; ?><?php echo ","; ?>
<?php echo 'Daily Performance Analysis'; ?><?php echo ","; ?>
 <?php echo '';?><?php echo "\n"; ?>
<?php echo '';?><?php echo "\n"; ?>
<?php echo "Date"; ?>
<?php echo ","; ?>
<?php foreach ($Route as $key1 => $value1) { ?>
<?php echo $value1['Route']['name']."-";?><?php echo "," ?><?php echo $value1['Executive']['name'];?><?php echo "," ?>
<?php } ?>
<?php echo "Total"; ?><?php echo "," ?>
<?php echo "\n"; ?><?php echo "," ?>
<?php foreach ($Route as $key1 => $value1) { ?>
<?php echo "Sale"; ?><?php echo ","; ?><?php echo "Collection"; ?><?php echo "," ?>
<?php }
 echo "Sale"; echo ",";echo "Collection"; echo "\n";
?>
<?php 
$grand_row_total_sale=0;$grand_row_total_collection=0; $sale_target=0;$collection_target=0;
foreach ($amount as $key => $value) {?>
<?php echo date("d-m-Y", strtotime($key));?><?php echo "," ?>
<?php $row_total_sale=0;$row_total_collection=0;for($j=0;$j<$Executive_count;$j++){?>
<?php echo $value['sale_amount'][$j];echo ","; echo $value['collection_amount'][$j];?><?php echo "," ?>
<?php $row_total_sale+=$value['sale_amount'][$j];?>
<?php $row_total_collection+=$value['collection_amount'][$j];?>
<?php } ?>
<?php $grand_row_total_sale+=$row_total_sale;?>
<?php $grand_row_total_collection+=$row_total_collection;?>
<?php echo $row_total_sale;echo ","; echo $row_total_collection;?><?php echo "," ?>
<?php echo "\n" ?>
<?php }?>
<?php echo '';?><?php echo "\n"; ?>
<?php echo "Total"; ?>
<?php echo ","; ?>
<?php foreach ($column_total as $key2 => $value2) { ?>
<?php echo $value2['total']['sale_amount'];echo ","; echo $value2['total']['collection_amount']; ?><?php echo "," ?>
<?php } ?>
<?php echo $grand_row_total_sale; ?><?php echo "," ?><?php echo $grand_row_total_collection; ?><?php echo "," ?>
<?php echo "\n" ?>
<?php echo "Target"; ?>
<?php echo ","; ?>
<?php foreach ($column_total as $key2 => $value2) { ?>
<?php $sale_target+=$value2['total']['target'];?>
<?php $collection_target+=$value2['total']['collection_target'];?>
<?php echo $value2['total']['target'];echo ","; echo $value2['total']['collection_target']; ?><?php echo "," ?>
<?php } ?>
<?php echo $sale_target; ?><?php echo "," ?><?php echo $collection_target; ?><?php echo "," ?>
<?php echo "\n" ?>
<?php echo "%"; ?>
<?php echo ","; ?>
<?php foreach ($column_total as $key2 => $value2) { ?>
<?php echo $value2['total']['sale_percentage'];echo ","; echo $value2['total']['collection_percentage']; ?><?php echo "," ?>
<?php } ?>
<?php if($collection_target!=0){
	$total_collection_per=($grand_row_total_collection*100)/$collection_target;
}
else
{
	$total_collection_per=0;
}?>
<?php $total_sale_per=($grand_row_total_sale*100)/$sale_target;?>
<?php echo round($total_sale_per,2); ?><?php echo "," ?><?php echo round($total_collection_per,2); ?><?php echo "," ?>
<?php echo "\n" ?>
<?php echo "DVA"; ?>
<?php echo ","; ?>
<?php $dva=$dva_day;?>
<?php foreach ($column_total as $key2 => $value2) { ?>
<?php echo $value2['total']['target']/$dva;echo ","; echo $value2['total']['collection_target']/$dva; ?><?php echo "," ?>
<?php } ?>
<?php echo $sale_target/$dva;echo ","; echo $collection_target/$dva; ?><?php echo "," ?>
<?php echo "\n" ?>
<?php echo "Remain"; ?>
<?php echo ","; ?>
<?php foreach ($column_total as $key2 => $value2) { ?>
<?php echo $value2['total']['target']-$value2['total']['sale_amount'];echo ","; echo $value2['total']['collection_target']-$value2['total']['collection_amount']; ?><?php echo "," ?>
<?php } ?>
<?php echo $sale_target-$grand_row_total_sale;echo ","; echo $collection_target-$grand_row_total_collection; ?><?php echo "," ?>
<?php echo "\n" ?>
<?php foreach ($column_total as $key2 => $value2) { ?>
<?php echo ",";echo ","; ?>
<?php } ?>
<?php echo "Last Month Sales & Collection"; ?><?php echo "," ?>
<?php echo $last_saleamount_total;echo ","; echo $last_collection_total; ?><?php echo "," ?>
<?php echo "\n" ?>
<?php foreach ($column_total as $key2 => $value2) { ?>
<?php echo ",";echo ","; ?>
<?php } ?>
<?php echo "Increment v/s last month volume"; ?><?php echo "," ?>
<?php echo ($grand_row_total_sale-$last_saleamount_total);echo ","; echo ($grand_row_total_collection-$last_collection_total); ?><?php echo "," ?>
<?php echo "\n" ?>
<?php foreach ($column_total as $key2 => $value2) { ?>
<?php echo ",";echo ","; ?>
<?php } ?>
<?php echo "Percentage %"; ?><?php echo "," ?>
<?php echo round((($grand_row_total_sale-$last_saleamount_total)*100)/$last_saleamount_total,2);echo ","; echo round((($grand_row_total_collection-$last_collection_total)*100)/$last_collection_total,2); ?><?php echo "," ?>
<?php echo "\n" ?><?php echo "\n" ?>
<?php echo "Opening O/B"; ?>
<?php echo ","; ?>
<?php $opening=0;?>
<?php foreach ($opening_total as $key3 => $value3) { ?>
<?php $opening+=$value3['opening_balance'];?>
<?php echo ","; echo $value3['opening_balance']; ?><?php echo "," ?>
<?php } ?>
<?php echo ",";echo $opening; ?><?php echo "," ?>
<?php echo "\n" ?>
<?php echo "Present O/B"; ?>
<?php echo ","; ?>
<?php $present_opening_balance=0;?>
<?php foreach ($opening_total as $key3 => $value3) { ?>
<?php $present_opening_balance+=$value3['present_opening_balance'];?>
<?php echo ","; echo $value3['present_opening_balance']; ?><?php echo "," ?>
<?php } ?>
<?php echo ",";echo $present_opening_balance; ?><?php echo "," ?>
