 <div id="Upload_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload File</h4>
      </div>
      <?php echo $this->Form->create('Upolad_Form',array('type'=>'file','id'=>'Upolad_Form')); ?>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('file',array('class'=>'form-control toUpperCase','type'=>'file','label'=>'Documents')); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'form-control','id'=>'staff_id')); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id='add_upload'>Upload</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>