<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<section class="content-header">
  <h1>Daily Performance Analysis Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
         <div class="col-md-3">
            <?php echo $this->Form->input('day',array(
                      'type'=>'text',
                      'id'=>'day_id',
                      'style'=>'width:100%',
                      'class'=>'form-control',
                      'required')); ?>
         </div>
          <div class="col-md-4 pd-lt-0"> <br>
                <label>Export</label> <span class="print-span"><a id="exportExcel" target="_blank"><i class="fa fa-download fa fa-2x" aria-hidden="true" style="padding-left: 10px;"></i></a></span>
              </div>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
      
    </div>
  </div>
</div>

</section>
<script type="text/javascript">
$(document).ready(function(){});
$('#exportExcel').on('click',function(e)   { 
    e.preventDefault();
     var day_id=$('#day_id').val();
    if(!day_id)
    {
   $('#day_id').focus();
return false;
    }
    // var website_url3='<?= $this->webroot; ?>Stock/FilterExcel/'+brand_id+'/'+product_type_id +'/'+'/'+warehouse_id+'/' +product_id;
    var website_url3='<?= $this->webroot; ?>Reports/DailyPerformanceAnalysisExport/'+day_id;
    $(location).attr("href", website_url3);
  });
// $.fn.table_search();
</script>