<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<section class="content-header">
  <h1>All Route Sales Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
        <div class="col-md-4">
          <div class="col-md-12">
            <?= $this->Form->input('from_date',array('type'=>'text', 'id'=>'from_date', 'class'=>'form-control search_field date_picker datepicker', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', )); ?>
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-12">
            <?= $this->Form->input('to_date',array('type'=>'text', 'id'=>'to_date', 'class'=>'form-control  search_field date_picker datepicker', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', )); ?>
          </div>
        </div>
        <div class="col-md-2"><br>
          <button class='btn' type='button' id='fetch_button'>Fetch</button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
      <table class="table table-condensed datatable table boder no-footer" id='table_list'>
        <thead>
          <tr class="blue-bg">
            <th>Route</th>
              <th>Net Amount</th>
              <th>Discount</th>
             <th>Taxable Value</th>
            <th>Tax Amount</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
        </tfoot>
      </table>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $('#fetch_button').click(function(){
   var data={
    from_date   :$('#from_date').val(),
    to_date     :$('#to_date').val(),
  };
  var url_address= "<?= $this->webroot; ?>Reports/RouteReportAjax";
  $.post( url_address,data, function( response ) {
      $('#table_list tbody').empty();
      $('#table_list tfoot').empty();
      $.each(response,function(key,value){
        var tr='<tr>';
        tr+='<td>'+value.route+'</td>';
         tr+='<td>'+value.net_value+'</td>';
        tr+='<td>'+value.dicount_amount+'</td>';
        tr+='<td>'+value.taxable_amount+'</td>';
        tr+='<td>'+value.tax_amount+'</td>';
        tr+='<td>'+value.grandtotal+'</td>';
        tr+='</tr>';
        $('#table_list tbody').append(tr);
      });
    }, "json");
});
</script>
