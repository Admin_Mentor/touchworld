Brand=[
<?php foreach($Brand as $key=>$value) :  ?>
"<?php echo $value; ?>",
<?php endforeach; ?>
];
//$("#brand_name").autocomplete({
//  source: Brand,
 // select:function(){
 //   setTimeout(brand_name_click,1);
 // },
//});
function brand_name_click()
{
  $( "#brand_name" ).trigger('keyup');
}
//$( "#brand_name" ).autocomplete( "option", "appendTo", ".eventInsForm" );
$('.brand_select').change(function(){
  $('#Brand').val($(this).val());
  $('#Brand_id_modal').val($(this).val());
});
$('#brand_id').change(function(){
  var id=$(this).val();
  $('#Brand_id_modal').val(id).trigger('change.select2');;
});
$('#Brand_id_modal').change(function(){
  var id=$(this).val();
  $('#brand_id').val(id);
});
$('#product_type_id,#product_type_id_modal').change(function(){
  var product_type_id=$(this).find('option:selected').val();
  var url_address= '<?php echo $this->webroot; ?>Brand/get_brand_name_ajax/'+product_type_id;
  $.ajax({
    method: "POST",
    url:url_address,
    dataType:'json',
  }).done(function( result ) {
    $('#brand_id').html(result.option);
    $('#brand_id').val('');
    $('#Brand_id_modal').html(result.option);
    $('#Brand_id_modal').val('');
  });
});
$('#add_Brand').click(function(){
  var product_type_id=$('#product_type_id option:selected').val();
  if(!product_type_id)
  {
    $('#product_type_id').select2('open');
    return false;
  }
  $('#BrandProductTypeId').val(product_type_id);
  var data=$('#Brand_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>Brand/brand_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      if(response.result=='Already Added')
      {
        $('#brand_id').val(response.key).trigger('change');   
        $('#Brand_Add_Form')[0].reset();
        $('#Brand_Add_Modal').modal('toggle');
      }
      else
      {
        return false;
      }
    }
    else
    {
      $('#brand_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#Brand_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#brand_id').val($('#brand_id option:last-child').val()).trigger('change');

      $('.brand_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('.brand_id').val($('#brand_id option:last-child').val()).trigger('change');

      $('#Brand_Add_Form')[0].reset();
      $('#Brand_Add_Modal').modal('toggle');
    }
  }, "json");
});
$('#Brand_Delete_Btn').click(function(){
  var brand_id=$('#brand_id').val();
  if(!brand_id)
  {
    $('#brand_id').select2('open');
    return false; 
  }
  if(!confirm("Are you sure?"))
  {
    return false;
  }
  var url_address= "<?= $this->webroot; ?>Brand/brand_delete_ajax/"+brand_id;
  $.get( url_address, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $("#brand_id option[value='" + brand_id+ "']").remove();
      $("#Brand_id_modal option[value='" + brand_id+ "']").remove();
    }
  }, "json");
});
$('#Brand_Edit_Btn').click(function(){
  var brand_id=$('#brand_id').val();
  if(!brand_id)
  {
    $('#brand_id').select2('open');
    return false; 
  }
  var url_address= "<?= $this->webroot; ?>Brand/brand_get_ajax/"+brand_id;
  $.get( url_address, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $('#BrandEditName').val(response.data.name);
      $('#BrandEditId').val(response.data.id);
      // $('#BrandEditCode').val(response.data.code);
      $('#Brand_Edit_Modal').modal('show');
    }
  }, "json");
});
$('#edit_Brand').click(function(){
  var data=$('#Brand_Edit_Form').serialize();
  var brand_id=$('#brand_id').val();
  var url_address= "<?= $this->webroot; ?>Brand/brand_edit_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $("#brand_id option[value='" + brand_id+ "']").remove();
      $("#Brand_id_modal option[value='" + brand_id+ "']").remove();
      $('#brand_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#Brand_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#brand_id').val($('#brand_id option:last-child').val()).trigger('change');
      $('#Brand_Edit_Form')[0].reset();
      $('#Brand_Edit_Modal').modal('toggle');
    }
  }, "json");
});